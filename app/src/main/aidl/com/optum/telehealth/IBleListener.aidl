/*
 * File: IBleListener.aidl
 *
 * Abstract: AIDL file for BleService.
 *
 * Copyright (c) 2015 OMRON HEALTHCARE Co., Ltd. All rights reserved.
 */

package com.optum.telehealth;

import android.bluetooth.BluetoothDevice;

interface IBleListener {
	void BleAdvCatch();
	void BleConnected();
	void BleDisConnected();
	void BleWmDataRecv(in byte[] data);
	void BleBatteryDataRecv(in byte[] data);
	void BleCtsDataRecv(in byte[] data);
	void BleBpmDataRecv(in byte[] data);
	void BleBpfDataRecv(in byte[] data);
	void BleAdvCatchDevice(in BluetoothDevice dev);
	void BleWsfDataRecv(in byte[] data);
	void BleConnectionStateRecv(in int state_code);
}
