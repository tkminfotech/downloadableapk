/*
 * File: ILogViewService.aidl
 *
 * Abstract: AIDL file for LogViewService.
 *
 * Copyright (c) 2015 OMRON HEALTHCARE Co., Ltd. All rights reserved.
 */

package com.optum.telehealth;

interface ILogViewService
{
	java.util.List<String> getDispData();
	boolean clearLog();
	void setLogLevel(int logLevel);
    int getLogLevel();
	int saveLog(String FileName);
	void setLogBreak();
}
