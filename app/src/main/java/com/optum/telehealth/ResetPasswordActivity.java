package com.optum.telehealth;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import com.optum.telehealth.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.optum.telehealth.bean.State;
import com.optum.telehealth.dal.State_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.ConnectionDetector;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class ResetPasswordActivity extends Activity implements View.OnClickListener {

    private EditText edtFirstName;
    private EditText edtLastName;
    private Button btn_dob;
    private Spinner spn_state;
    private EditText edt_zip;
    private Button btn_back;
    private Button btn_submit;
    private TextView txt_supportDescHeader;
    private TextView txt_supportDescNumber;
    private boolean fnameStatus = false, lnameStatus = false, stateStatus = false, dobStatus = false, zipStatus = false;
    private List<State> statesListFromDB;
    private ProgressDialog progressDialog;
    State_db state_db = new State_db(this);
    private String TAG = "ResetPasswordActivity";
    private String serviceUrl;
    private String temp_fname = "", temp_lname = "", temp_state = "", temp_dob = "", temp_zip = "";
    private int year, month, day;
    private GlobalClass appClass;
    private TextView txt_errorMessage;
    private boolean isCloseClick = false;
    private String res_message;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        appClass = (GlobalClass) getApplicationContext();
        edtFirstName = (EditText) findViewById(R.id.edt_resetFirstName);
        edtLastName = (EditText) findViewById(R.id.edt_resetLastName);
        edt_zip = (EditText) findViewById(R.id.edt_resetZip);
        btn_dob = (Button) findViewById(R.id.btn_resetDOB);
        btn_back = (Button) findViewById(R.id.btn_resetBack);
        btn_submit = (Button) findViewById(R.id.btn_resetSubmit);
        btn_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        spn_state = (Spinner) findViewById(R.id.spn_resetState);

        txt_supportDescHeader = (TextView) findViewById(R.id.txt_resetPasswordSupportHeader);
        txt_supportDescNumber = (TextView) findViewById(R.id.txt_resetPasswordSupportNumber);
        txt_supportDescNumber.setOnClickListener(this);
        txt_errorMessage = (TextView) findViewById(R.id.txt_errorMessage);
        SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);
        String support = settings.getString("forgot_password_msg2", "");
        String supportNumber = settings.getString("forgot_password_msg3", "");
        String supportHeader = getResources().getString(R.string.resetPasswordSupportDesc);
        txt_supportDescHeader.setText(Html.fromHtml(settings.getString("msg_contact_support", "")));
        txt_supportDescNumber.setText(Html.fromHtml("<u>" + supportNumber + "</u>"));
        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");

        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtFirstName.getText().length() == 1) {
                    if (edtFirstName.getText().toString().equals(" ")) {
                        edtFirstName.setText("");
                    } else {
                        fnameStatus = true;
                        // button_Enable();
                    }
                } else if (edtFirstName.getText().length() > 1) {
                    fnameStatus = true;
                    //button_Enable();
                } else {
                    fnameStatus = false;
                    //button_Disable();
                }
            }
        });
        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtLastName.getText().length() == 1) {
                    if (edtLastName.getText().toString().equals(" ")) {
                        edtLastName.setText("");
                    } else {
                        lnameStatus = true;
                        //button_Enable();
                    }
                } else if (edtLastName.getText().length() > 1) {
                    lnameStatus = true;
                    // button_Enable();
                } else {
                    lnameStatus = false;
                    //button_Disable();
                }
            }
        });
        edt_zip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edt_zip.getText().length() == 1) {
                    if (edt_zip.getText().toString().equals(" ")) {
                        edt_zip.setText("");
                    } else {
                        zipStatus = true;
                        //button_Enable();
                    }
                } else if (edt_zip.getText().length() > 1) {
                    zipStatus = true;
                    // button_Enable();
                } else {
                    zipStatus = false;
                    //button_Disable();
                }
            }
        });
        spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    stateStatus = false;
                    //button_Disable();
                } else {
                    temp_state = statesListFromDB.get(position).getStateName();
                    stateStatus = true;
                    //button_Enable();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showTimePickerDialog();
                btn_dob.requestFocus();
                return false;
            }
        });
        Boolean isInternetPresent = (new ConnectionDetector(
                getApplicationContext())).isConnectingToInternet();
        if (isInternetPresent) {

            downloadStateDetails();
        } else {

            populateSpinner();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private void downloadStateDetails() {

        DownloadStateList task = new DownloadStateList();
        task.execute(new String[]{serviceUrl
                + "/droid_website/mob_get_state.ashx"});
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_resetSubmit:
                sublitClick();
                break;
            case R.id.btn_resetBack:
                backClick();
                break;
            case R.id.txt_resetPasswordSupportNumber:
                callSupportNumber();
                break;

        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("ResetPassword Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private class DownloadStateList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(ResetPasswordActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String response = "";
            List<State> stateList = new ArrayList<State>();
            try {

                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_get_state.ashx", new String[]{},
                        new String[]{});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return ""; // process
                }
                String str = Util
                        .readStream(response1.getEntity().getContent());

                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                str.toString();
                //Log.i("response:", "handler_response:" + str);

                if (str.length() < 10) {
                    Log.i(TAG, "Advice message data null");
                    return "";
                }
                state_db.clearTable();
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("state_list");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    NodeList nodes = doc.getElementsByTagName("state");

                    for (int j = 0; j < nodes.getLength(); j++) {
                        State state = new State();
                        state.setStateId(Integer.parseInt(Util.getTagValue(
                                nodes, j, "id")));
                        state.setStateName(Util.getTagValue(
                                nodes, j, "name"));

                        stateList.add(state);
                    }
                }
                state_db.insertState(stateList);
            } catch (Exception e) {

            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            //loadApkDetails();
            try {
                progressDialog.dismiss();
                populateSpinner();

            } catch (Exception ex) {
                Log.i("onPostExecute", ex.getMessage());
            }
        }
    }

    private void populateSpinner() {
        statesListFromDB = new ArrayList<State>();
        List<State> states = state_db.getAllStates();
        State state = new State();
        state.setStateName(getResources().getString(R.string.state));
        statesListFromDB.add(state);
        for (int i = 0; i < states.size(); i++) {

            State state1 = states.get(i);
            statesListFromDB.add(state1);

        }
        SpinnerAdapter adapter = new SpinnerAdapter(ResetPasswordActivity.this, android.R.layout.simple_spinner_item, statesListFromDB);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_state.setAdapter(adapter);
    }

//    private void button_Enable() {
//        if (fnameStatus && lnameStatus && stateStatus && dobStatus) {
//            btn_submit.setEnabled(true);
//            if (getResources().getBoolean(R.bool.isTab)) {
//                btn_submit.setBackground(getResources().getDrawable(R.drawable.orange));
//            } else {
//                btn_submit.setBackground(getResources().getDrawable(R.drawable.button_orange));
//            }
//        }
//    }
//
//    private void button_Disable() {
//        btn_submit.setEnabled(false);
//        if (getResources().getBoolean(R.bool.isTab)) {
//            btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable_tab));
//        } else {
//            btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable));
//        }
//    }

    public void showTimePickerDialog() {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            //DatePickerDialog datePicker = new DatePickerDialog(this, pickerListener, year, month, day);
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialogSpinner datePicker = new DatePickerDialogSpinner(this, pickerListener, year, month, day);
            datePicker.updateDate(year, month, day);
            return datePicker;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth + 1;
            day = selectedDay;

            String s_month = String.format("%02d", month);
            String s_day = String.format("%02d", day);

            btn_dob.setText(s_month + "/" + s_day + "/" + year);
            dobStatus = true;
            btn_dob.setTextColor(getResources().getColor(R.color.black));
            temp_dob = s_day + "/" + s_month + "/" + year;
            //button_Enable();
            Log.i(TAG, "year=" + year + " month=" + month + " day=" + day);
        }
    };

    public void setNotificationMessage(String msg) {
        txt_errorMessage.setVisibility(View.VISIBLE);
        txt_errorMessage.setText(msg);
        btn_submit.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMessage.animate().alpha(0.0f);
                        txt_errorMessage.setVisibility(View.GONE);
                        btn_submit.setEnabled(true);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }

    private class requestForResetPassword extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_request_pin");
            try {
                progressDialog = new ProgressDialog(ResetPasswordActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_request_pin #doInBackground");
            String response = "";
            String language = "0";
            try {
                SharedPreferences settings_n = getSharedPreferences(
                        CommonUtilities.USER_SP, 0);
                String lngID = settings_n.getString("language_id", "0");
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_reset_password.ashx", new String[]{
                                "f_name", "l_name", "dob", "state", "zip","language_id"},
                        new String[]{temp_fname, temp_lname, temp_dob, temp_state, temp_zip,lngID});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                //str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i(TAG, "mob_request_pin: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("reset_password");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("Success")) {
                        response = "success";
                        language = Util.getTagValue(nodes, i, "language_id");
                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("language_id", language);
                        editor.commit();
                        return response;
                    } else {
                        response = "failed";
                        language = Util.getTagValue(nodes, i, "language_id");
                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("language_id", language);
                        editor.commit();
                        res_message = Util.getTagValue(nodes, i, "message");
                        //Log.i(TAG, "==========FAILED==========" + res_message);
                        return response;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("success")) {

                    showPopup("success");
                } else if (s.equals("failed")) {
                    showPopup("invalid");
                } else {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String connection_error = ERROR_MSG.getString("connection_error", "-1");
                    setNotificationMessage(connection_error);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }


    private void showPopup(final String header) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reset_password_popup);
        dialog.setTitle("Custom Alert Dialog");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgview_resetPopupClose);
        TextView txtPopupHeader = (TextView) dialog.findViewById(R.id.txt_resetPopupHeader);
        TextView txtError = (TextView) dialog.findViewById(R.id.txt_resetPasswordPopupError);
        final TextView txtErrorNumber = (TextView) dialog.findViewById(R.id.txt_resetPasswordPopupErrorNumber);
        Button btnClose = (Button) dialog.findViewById(R.id.btn_resetPopupClose);
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing()) {
                    if (isCloseClick == false) {
                        dialog.dismiss();

                        Intent resultIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
                        startActivity(resultIntent);
                        finish();
                    }
                }
            }
        };

        if (header.equals("error")) {
            txtPopupHeader.setText(R.string.error);
            txtErrorNumber.setVisibility(View.GONE);
            btnClose.setVisibility(View.GONE);
            txtError.setText(R.string.errorDesc);
        } else if (header.equals("success")) {
            txtPopupHeader.setText(R.string.resetSuccessful);
            txtErrorNumber.setVisibility(View.GONE);
            btnClose.setVisibility(View.GONE);
            txtError.setText(R.string.resetSuccessfulDesc);
            handler.postDelayed(runnable, 5000);
        } else {
            txtPopupHeader.setText(R.string.resetInvalidDetails);
            SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);
            String number = settings.getString("forgot_password_msg3", "");
            txtErrorNumber.setText(Html.fromHtml("<u>" + number + "</u>"));
            txtError.setText(Html.fromHtml(res_message));
           // txtError.setText(R.string.resetInvalidDetailsDesc);
            txtErrorNumber.setVisibility(View.VISIBLE);
            btnClose.setVisibility(View.VISIBLE);
        }


        dialog.show();
        imgClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                if (header.equals("invalid")) {
                    clearAlldata();
                } else if (header.equals("success")) {
                    isCloseClick = true;

                    Intent resultIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
                    startActivity(resultIntent);

                    finish();
                }
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                if (header.equals("invalid")) {
                    clearAlldata();
                }
            }
        });

        txtErrorNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String phone_no = txtErrorNumber.getText().toString().trim();
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + phone_no));
                    startActivity(callIntent);
                } catch (ActivityNotFoundException e) {
                    Log.i("ActivityNotFoundExcept", e.getMessage());
                }
            }
        });


    }

    private void sublitClick() {

        if (fnameStatus && lnameStatus && stateStatus && dobStatus && zipStatus) {
            temp_fname = edtFirstName.getText().toString().trim();
            temp_lname = edtLastName.getText().toString().trim();
            temp_zip = edt_zip.getText().toString().trim();
            //temp_state = reg_state.getText().toString().trim();
            if (appClass.isConnectingToInternet(getBaseContext())) {
                new requestForResetPassword().execute();
            } else {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String NoConnectionError = ERROR_MSG.getString("NoConnectionError", "-1");
                setNotificationMessage(NoConnectionError);
            }
        } else {

            showPopup("error");
        }
    }

    private void backClick() {

        Intent i = new Intent(ResetPasswordActivity.this, AuthenticationActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        finish();
    }

    private void clearAlldata() {

        edtFirstName.setText("");
        edtLastName.setText("");
        edt_zip.setText("");
        btn_dob.setText(getResources().getString(R.string.dob));
        btn_dob.setTextColor(getResources().getColor(R.color.Mob_Gray3));
        spn_state.setSelection(0);
        fnameStatus = false;
        lnameStatus = false;
        stateStatus = false;
        dobStatus = false;
        zipStatus = false;
    }

    private void callSupportNumber() {

//        PackageManager pm = this.getPackageManager();
//        if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
//            System.out.println("horray");
//        } else {
//            System.out.println("nope");
//        }
        try {
            String phone_no = txt_supportDescNumber.getText().toString().trim();
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phone_no));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            Log.i("ActivityNotFoundExcept", e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        //NO ACTION
    }

}
