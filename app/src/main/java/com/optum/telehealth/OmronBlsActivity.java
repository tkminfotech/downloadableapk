
package com.optum.telehealth;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.bean.ClasssPressure;
import com.optum.telehealth.dal.Pressure_db;
import com.optum.telehealth.service.OmronBleService;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.PINReceiver;
import com.optum.telehealth.util.Util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

@TargetApi(18)
public class OmronBlsActivity extends Titlewindow{
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private String resultValue = "";

    private Date sensorCurrentTime;
    private Date sensorReadingTime;
    private Date tabletRecievedTime;
    private Date tabletPageOpenTime;

    private String pageName = "Blood Pressure Sensor Page";
    private boolean isMSG_BPM_DATA_RECV = false;
    private boolean isPaired = false;
    private boolean isFinish = false;
    private String macAddress = "";
    private final static String TAG = "OmronBlsActivity";
    private final static String DEBUG_TAG = ">Omron";
    Pressure_db dbcreatepressure = new Pressure_db(this);
    protected final static String LOG_TAG = "BLE_LOG";
    private String connectionStatus = "Connect";
    int flag = 0;
    BluetoothAdapter mBluetoothAdapter;
    private String bodyMovement = "";
    private String irregularPulse = "";
    private String batteryLevel = "";
    private String positionFlag = "1";
    private String cuffFitFlag = "1";

    protected UUID[] mScanServiceUuids = null;
    private final PINReceiver omronAutomaticPinEntryBroadcastReceiver = new PINReceiver();
    protected static final int MSG_CONNECTING = 0;
    protected static final int MSG_CONNECTED = 1;
    protected static final int MSG_DISCONNECTED = 2;
    protected static final int MSG_WM_DATA_RECV = 3;
    protected static final int MSG_ADV_CATCH_DEV = 4;
    protected static final int MSG_SCAN_CANCEL = 5;
    protected static final int MSG_BATTERY_DATA_RECV = 6;
    protected static final int MSG_CTS_DATA_RECV = 7;
    protected static final int MSG_BPM_DATA_RECV = 8;
    protected static final int MSG_BPF_DATA_RECV = 9;
    protected static final int MSG_LISTVIEW_CLR = 10;
    protected static final int MSG_WSF_DATA_RECV = 11;
    protected static final int MSG_BLE_CONNECT_STATE_RECV = 101;

    protected static final int BLE_STATE_IDLE = 0;
    protected static final int BLE_STATE_SCANNING = 1;
    protected static final int BLE_STATE_CONNECTING = 2;
    protected static final int BLE_STATE_CONNECT = 3;
    protected static final int BLE_STATE_DATA_RECV = 4;
    protected int mBleState = BLE_STATE_IDLE;
    protected OmronBleService mBleService;

    private GlobalClass appClass;
    TextView text, txt_errorMsg;
    Button btnmanual;
    TextView txtwelcom, txtReading;
    ImageView rocketImage;
    AnimationDrawable rocketAnimation;
    ObjectAnimator AnimPlz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        tabletPageOpenTime = parseTimeToMilliseconds(mdformat.format(calendar.getTime()));


        super.onCreate(savedInstanceState); // execute at the end
        Log.d(TAG, "[IN]onCreate");
        setContentView(R.layout.activity_aand_dreader);
        isMSG_BPM_DATA_RECV = false;
        isPaired = false;
        isFinish = false;
        appClass = (GlobalClass) getApplicationContext();
        appClass.isEllipsisEnable = true;
        appClass.isSupportEnable = false;
        btnmanual = (Button) findViewById(R.id.aAndDManuval);
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        txtwelcom = (TextView) findViewById(R.id.txtwelcome);
        txtReading = (TextView) findViewById(R.id.takereading);

        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/FrutigerLTStd-Roman.otf");

        txtwelcom.setTypeface(type, Typeface.NORMAL);
        txtReading.setTypeface(type, Typeface.NORMAL);

        rocketImage = (ImageView) findViewById(R.id.loading);
        try{
            rocketImage.setBackgroundResource(R.drawable.pressure_animation);
            rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
            rocketAnimation.start();
            rocketImage.setVisibility(View.INVISIBLE);
            txtReading.setVisibility(View.INVISIBLE);
        } catch (OutOfMemoryError ex){
            Log.i("exception",ex.toString());
            rocketImage.setBackgroundResource(R.drawable.bloodpressure0011);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        macAddress = extras.getString("macaddress");
        setwelcome();
        Animations();

        mScanServiceUuids = new UUID[]{OmronBleService.Blood_Pressure_SERVICE_UUID};

        btnmanual.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                appClass.appTracking(pageName,"Click on Manual entry button");
                isFinish = true;
                Intent intentpl = new Intent(getApplicationContext(), PressureEntry.class);

                finish();
                startActivity(intentpl);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= 18) {
            mBluetoothAdapter = ((BluetoothManager) getBaseContext().getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();

        } else {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);

                // enable location if on Android 7 or greater and the location service is turned off
                // for Android 7, the location service must be turned on for the device to be able to LE scan and receive scan results
            } else if (Build.VERSION.SDK_INT >= 24 && !Util.isLocationOn(this)) {
                Util.displayLocationSettingsRequest(this);

            } else {
                //entry point to discovery
                CheckBlueToothState();
            }
        } else {
            //entry point to discovery
            CheckBlueToothState();
        }

        if (mBleService != null) {
            mBleService.setCurrentContext(getApplicationContext(), (IBleListener) mBinder);
        }
        bindService(new Intent(this, OmronBleService.class), mConnection, Context.BIND_AUTO_CREATE);

        IntentFilter filter1 = new IntentFilter();
        filter1.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        registerReceiver(omronAutomaticPinEntryBroadcastReceiver, filter1);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "[IN]onResume");
        resultValue = "";
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "[IN]onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[IN]onStop");
        isFinish = true;

        try {
            unbindService(mConnection);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            unregisterReceiver(omronAutomaticPinEntryBroadcastReceiver);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void onReceiveMessage(Message msg) {

        byte[] data;
        byte[] buf = new byte[2];
        ByteBuffer byteBuffer;

        switch (msg.what) {
            case MSG_CONNECTING:
                break;

            case MSG_CONNECTED:
                Log.d(TAG, "[LOG]MSG_CONNECTED");
                isPaired = true;
                connectionStatus = getResources().getString(R.string.disconnect);
                Log.i(LOG_TAG, "[LOG_OUT]Connect to " + mBleService.BleGetLocalName() + "(" + mBleService.BleGetAddress() + ")");
                break;

            case MSG_DISCONNECTED:
                Log.d(TAG, "[LOG]MSG_DISCONNECTED");
                connectionStatus = getResources().getString(R.string.connect);
                break;

            case MSG_ADV_CATCH_DEV:
                Log.d(TAG, "[LOG]MSG_ADV_CATCH_DEV");
                ListViewDisp((BluetoothDevice) msg.obj);
                break;

            case MSG_BATTERY_DATA_RECV:
                connectionStatus = getResources().getString(R.string.disconnect);
                byte[] batterydata = (byte[]) msg.obj;
                int bl = batterydata[0];
                batteryLevel = "" + bl;
                Log.i(LOG_TAG, "[LOG_OUT]Battery Level Data:" + batteryLevel);
                break;

            case MSG_CTS_DATA_RECV:
                Log.d(TAG, "[LOG]MSG_CTS_DATA_RECV");
                connectionStatus = getResources().getString(R.string.disconnect);
                byte[] ctsdata = (byte[]) msg.obj;
                byte[] buf1 = new byte[2];
                System.arraycopy(ctsdata, 0, buf1, 0, 2);
                ByteBuffer ctsyearbyteBuffer = ByteBuffer.wrap(buf1);
                ctsyearbyteBuffer.order(ByteOrder.LITTLE_ENDIAN);

                int ctsYear = ctsyearbyteBuffer.getShort();
                int ctsMonth = ctsdata[2];
                int ctsDay = ctsdata[3];
                int ctsHour = ctsdata[4];
                int ctsMinute = ctsdata[5];
                int ctsSecond = ctsdata[6];
                byte AdjustReason = ctsdata[9];

                String ctsTime = String.format("%1$04d", ctsYear) + "-" + String.format("%1$02d", ctsMonth) + "-" + String.format("%1$02d", ctsDay) + " " + String.format("%1$02d", ctsHour) + ":" + String.format("%1$02d", ctsMinute) + ":" + String.format("%1$02d", ctsSecond);
                //mCtsView.setText(ctsTime);
                sensorCurrentTime = parseTimeToMilliseconds(ctsTime);
                AddReadingAndWaitForTime(null, null);
                Log.i(LOG_TAG, "[LOG_OUT]CTS Data:" + ctsTime + " (AdjustReason:" + AdjustReason + ")");
                break;

            case MSG_LISTVIEW_CLR:
                Log.d(TAG, "[LOG]MSG_LISTVIEW_CLR");
                break;

            case MSG_BLE_CONNECT_STATE_RECV:
                if (!BLEIsEnabled()) {
                    break;
                }
                int state_code = Integer.parseInt(msg.obj.toString());
                switch (state_code) {
                    case OmronBleService.ACL_STATE_CONNECTED:
                        Log.i(TAG, "ACL_STATE_CONNECTED");
                        break;
                    case OmronBleService.ACL_STATE_DISCONNECTED:
                        Log.i(TAG, "ACL_STATE_DISCONNECTED");

                        break;
                    case OmronBleService.GATT_STATE_CONNECTED:
                        Log.i(TAG, "GATT_STATE_CONNECTED");
                        break;
                    case OmronBleService.GATT_STATE_DISCONNECTED:
                        Log.i(TAG, "GATT_STATE_DISCONNECTED------------------------------------------------------------------");
                        if (isPaired) {
                            Log.i(TAG, "GATT_STATE_DISCONNECTED_Paired----------------------------------------------------------");
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startPairing();
                                }
                            }, 4000);
                        } else {
                            Log.i(TAG, "GATT_STATE_DISCONNECTED_UnPaired");
                            isPaired = false;
                            mBleService.BleDisconnect();
                            startPairing();
                        }
                        break;
                    default:
                        break;
                }
                break;


            case MSG_BPM_DATA_RECV:
                Log.i(TAG, "MSG_BPM_DATA_RECV------------------------------------------------------------------------");
                isMSG_BPM_DATA_RECV = true;
                connectionStatus = getResources().getString(R.string.disconnect);
                int idx = 0;
                data = (byte[]) msg.obj;

                byte flags = data[idx++];

                // 0: mmHg	1: kPa
                boolean kPa = (flags & 0x01) > 0;
                // 0: No Timestamp info 1: With Timestamp info
                boolean timestampFlag = (flags & 0x02) > 0;
                // 0: No PlseRate info 1: With PulseRate info
                boolean pulseRateFlag = (flags & 0x04) > 0;
                // 0: No UserID info 1: With UserID info
                boolean userIdFlag = (flags & 0x08) > 0;
                // 0: No MeasurementStatus info 1: With MeasurementStatus info
                boolean measurementStatusFlag = (flags & 0x10) > 0;

                // Set BloodPressureMeasurement unit
                String unit;
                if (kPa) {
                    unit = "kPa";
                } else {
                    unit = "mmHg";
                }

                // Parse Blood Pressure Measurement
                short systolicVal = 0;
                short diastolicVal = 0;
                short meanApVal = 0;

                System.arraycopy(data, idx, buf, 0, 2);
                idx += 2;
                byteBuffer = ByteBuffer.wrap(buf);
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                systolicVal = byteBuffer.getShort();

                System.arraycopy(data, idx, buf, 0, 2);
                idx += 2;
                byteBuffer = ByteBuffer.wrap(buf);
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                diastolicVal = byteBuffer.getShort();

                System.arraycopy(data, idx, buf, 0, 2);
                idx += 2;
                byteBuffer = ByteBuffer.wrap(buf);
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                meanApVal = byteBuffer.getShort();

                Log.i(LOG_TAG, "[LOG_OUT]systolicValue:" + systolicVal + " " + unit);
                Log.i(LOG_TAG, "[LOG_OUT]diastolicValue:" + diastolicVal + " " + unit);
                Log.i(LOG_TAG, "[LOG_OUT]meanApValue:" + meanApVal + " " + unit);

                // Parse Timestamp
                String timestampStr = "----";
                String dateStr = "--";
                String timeStr = "--";
                if (timestampFlag) {
                    System.arraycopy(data, idx, buf, 0, 2);
                    idx += 2;
                    byteBuffer = ByteBuffer.wrap(buf);
                    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

                    int year = byteBuffer.getShort();
                    int month = data[idx++];
                    int day = data[idx++];
                    int hour = data[idx++];
                    int min = data[idx++];
                    int sec = data[idx++];

                    dateStr = String.format("%1$04d", year) + "-" + String.format("%1$02d", month) + "-" + String.format("%1$02d", day);
                    timeStr = String.format("%1$02d", hour) + ":" + String.format("%1$02d", min) + ":" + String.format("%1$02d", sec);
                    timestampStr = dateStr + " " + timeStr;
                    sensorReadingTime = parseTimeToMilliseconds(timestampStr);
                    Log.i(LOG_TAG, "[LOG_OUT]Timestamp Data:" + timestampStr);
                }

                // Parse PulseRate
                short pulseRateVal = 0;
                String pulseRateStr = "----";
                if (pulseRateFlag) {
                    System.arraycopy(data, idx, buf, 0, 2);
                    idx += 2;
                    byteBuffer = ByteBuffer.wrap(buf);
                    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                    pulseRateVal = byteBuffer.getShort();
                    pulseRateStr = Short.toString(pulseRateVal);
                    Log.i(LOG_TAG, "[LOG_OUT]PulseRate Data:" + pulseRateStr);
                }

                // Parse UserID
                int userIDVal = 0;
                String userIDStr = "----";
                if (userIdFlag) {
                    userIDVal = data[idx++];
                    userIDStr = String.valueOf(userIDVal);
                    Log.i(LOG_TAG, "[LOG_OUT]UserID Data:" + userIDStr);
                }

                // Parse Measurement Status
                int measurementStatusVal = 0;
                String measurementStatusStr = "----";
                if (measurementStatusFlag) {
                    System.arraycopy(data, idx, buf, 0, 2);
                    idx += 2;
                    byteBuffer = ByteBuffer.wrap(buf);
                    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                    measurementStatusVal = byteBuffer.getShort();
                    measurementStatusStr = String.format("%1$04x", (short) measurementStatusVal);
                    Log.i(LOG_TAG, "[LOG_OUT]MeasurementStatus Data:" + measurementStatusStr);

                    bodyMovement = ((measurementStatusVal & 0x0001) == 0 ? "0" : "1");
                    irregularPulse = ((measurementStatusVal & 0x0004) == 0 ? "0" : "1");
                }

                // Output to History
                Log.d(TAG, "Add history");
                String entry = timestampStr + "," + systolicVal + "," + diastolicVal + "," + meanApVal + "," + pulseRateStr + "," + String.format("%1$02x", flags) + "," + measurementStatusStr;

                // Output log for data aggregation
                // Log format: ## For aggregation ## timestamp(date), timestamp(time), systolic, diastolic, meanAP, current date time
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String agg = "## For aggregation ## ";
                agg += dateStr + "," + timeStr;
                agg += "," + systolicVal + "," + diastolicVal + "," + meanApVal;
                agg += "," + sdf.format(c.getTime());
                Log.i(TAG, agg);
                String recievedData = systolicVal + "," + diastolicVal + "," + pulseRateStr + "," + timestampStr;
                resultValue = recievedData;
                if(timestampStr.contains("0000")) {
                    sensorReadingTime = parseTimeToMilliseconds(sdf.format(c.getTime()));
                }

                tabletRecievedTime = parseTimeToMilliseconds(sdf.format(c.getTime()));
                AddReadingAndWaitForTime(recievedData, sensorReadingTime);
                break;

            case MSG_BPF_DATA_RECV:
                Log.i(TAG, "MSG_BPF_DATA_RECV");
                connectionStatus = getResources().getString(R.string.disconnect);
                data = (byte[]) msg.obj;
                System.arraycopy(data, 0, buf, 0, 2);
                ByteBuffer bpfByteBuffer = ByteBuffer.wrap(buf);
                bpfByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                short bpfVal = bpfByteBuffer.getShort();
                String bpfStr = String.format("%1$04x", (short) bpfVal);
                Log.i(LOG_TAG, "[LOG_OUT]Blood Pressure Feature Data:" + bpfStr);
                break;

            default:
                break;
        }
    }

    protected void onBleServiceConnected(IBinder service) {
        Log.d(TAG, "[IN]onBleServiceConnected");
        Log.d(TAG, "[IN]onBleReceiveMessage");
        mBleService = ((OmronBleService.MyServiceLocalBinder) service).getService();
        mBleService.setCurrentContext(getApplicationContext(), (IBleListener) mBinder);
    }

    protected void onClickAssistPairingCheckBox(boolean checked) {
        mBleService.setAssistPairing(checked);

        SharedPreferences pref = getSharedPreferences("Setting", MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean("BlsAssistPairingCheckBox", checked);
        edit.commit();
    }

    protected void onClickWriteCtsCheckBox(boolean checked) {
        // implement in child class
        Log.i(TAG, "mCheckBoxClickListener:" + checked);
        mBleService.setWriteCts(checked);
    }

    public boolean BLEIsEnabled() {
        // Confirm that Bluetooth is working on this device.
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
            if (bluetoothAdapter != null) {
                if (bluetoothAdapter.isEnabled()) {
                    return true;
                }
            }
        }
        return false;
    }

    private void ListViewDisp(BluetoothDevice dev) {
        if (dev.getAddress().equals(macAddress)) {
            onClickWriteCtsCheckBox(true);
            onClickAssistPairingCheckBox(true);
            mBleService.BleScanOff();
            Message msg = new Message();
            msg.what = MSG_LISTVIEW_CLR;
            mHandler.sendMessageDelayed(msg, 300);        // Wait for preventing to display received ADVs on the list before finishing scan Off process
            mBleService.BleConnectDev(dev);
        }
    }

    private final IBleListener.Stub mBinder = new IBleListener.Stub() {
        public void BleAdvCatch() throws RemoteException {
            Log.d(TAG, "[IN]BleAdvCatch");
            mBleState = BLE_STATE_CONNECTING;
            Message msg = new Message();
            msg.what = MSG_CONNECTING;
            mHandler.sendMessage(msg);
        }

        public void BleAdvCatchDevice(BluetoothDevice dev) throws RemoteException {
            Log.d(TAG, "[IN]BleAdvCatchDevice");
            Message msg = new Message();
            msg.what = MSG_ADV_CATCH_DEV;
            msg.obj = dev;
            mHandler.sendMessage(msg);
        }

        public void BleConnected() throws RemoteException {
            Log.d(TAG, "[IN]BleConnected");
            mBleState = BLE_STATE_CONNECT;
            Message msg = new Message();
            msg.what = MSG_CONNECTED;
            mHandler.sendMessage(msg);
        }

        public void BleDisConnected() throws RemoteException {
            Log.d(TAG, "[IN]BleDisConnected");
            mBleState = BLE_STATE_IDLE;
            Message msg = new Message();
            msg.what = MSG_DISCONNECTED;
            mHandler.sendMessage(msg);
        }

        public void BleWmDataRecv(byte[] data) throws RemoteException {
            Log.d(TAG, "[IN]BleDataRecv");
            Message msg = new Message();
            msg.what = MSG_WM_DATA_RECV;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        public void BleBatteryDataRecv(byte[] data) throws RemoteException {
            Log.d(TAG, "[IN]BleBatteryDataRecv");
            Message msg = new Message();
            msg.what = MSG_BATTERY_DATA_RECV;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        public void BleCtsDataRecv(byte[] data) throws RemoteException {
            Log.d(TAG, "[IN]BleCtsDataRecv");
            Message msg = new Message();
            msg.what = MSG_CTS_DATA_RECV;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        public void BleBpmDataRecv(byte[] data) throws RemoteException {
            Log.d(TAG, "[IN]BleBpmDataRecv");
            Message msg = new Message();
            msg.what = MSG_BPM_DATA_RECV;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        public void BleBpfDataRecv(byte[] data) throws RemoteException {
            Log.d(TAG, "[IN]BleBpfDataRecv");
            Message msg = new Message();
            msg.what = MSG_BPF_DATA_RECV;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        public void BleWsfDataRecv(byte[] data) throws RemoteException {
            Log.d(TAG, "[IN]BleWsfDataRecv");
            Message msg = new Message();
            msg.what = MSG_WSF_DATA_RECV;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        public void BleConnectionStateRecv(int state_code) throws RemoteException {
            Log.i(TAG, "[IN]BleConnectionStateRecv");
            Message msg = new Message();
            msg.what = MSG_BLE_CONNECT_STATE_RECV;
            msg.obj = state_code;
            mHandler.sendMessage(msg);
        }
    };

    // Event handler
    protected Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            onReceiveMessage(msg);
        }
    };

    protected ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d(TAG, "[IN]onServiceConnected");
            onBleServiceConnected(service);
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "[IN]onServiceDisconnected");
            mBleService = null;
            unbindService(mConnection);
        }
    };

    private void startPairing() {
        Log.i(TAG, "startPairing()..............................................................");
        if (connectionStatus.equals(getString(R.string.connect))) {
            Log.i(TAG, "startPairing()connect..............................................................");
            if (!BLEIsEnabled()) {
                Toast.makeText(OmronBlsActivity.this,
                        R.string.bluetooth_doesnt_work,
                        Toast.LENGTH_LONG).show();
                return;
            }

            // ensure that the pairing popup appears on the foreground
            // auto pairing is limited to devices running API levels < 24
            if (Build.VERSION.SDK_INT > 23) {
                mBluetoothAdapter.startDiscovery();
                mBluetoothAdapter.cancelDiscovery();
            }

            // Scan BLE devices
            mBleService.BleScan(mScanServiceUuids);
            connectionStatus = getResources().getString(R.string.connecting);
            Log.i(TAG, "[IN]CONNECT --> CONNECTING...");

        } else if (connectionStatus.equals(getString(R.string.connecting))) {
            Log.i(TAG, "startPairing()connecting...........................................................");

            connectionStatus = getResources().getString(R.string.connect);
            mBleService.BleScanOff();
            Message msg = new Message();
            msg.what = MSG_LISTVIEW_CLR;
            mHandler.sendMessageDelayed(msg, 300); // Wait for preventing to display received ADVs on the list before finishing scan Off process
            Log.i(TAG, "[IN]CONNECTING... --> CONNECT");
        } else if (connectionStatus.equals(getString(R.string.disconnect))) {
            Log.i(TAG, "startPairing()disconnect...........................................................");
            mBleService.BleDisconnect();
            if (isPaired == true && isMSG_BPM_DATA_RECV == false && isFinish == false) {
                isMSG_BPM_DATA_RECV = false;
                errorShow(this.getString(R.string.connectivityError));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isFinish = true;
                        Intent intentwt = new Intent(getApplicationContext(), PressureEntry.class);
                        startActivity(intentwt);
                        overridePendingTransition(0, 0);
                        finish();
                    }
                }, 4000);
            }
            Log.i(TAG, "[IN]DISCONNECT --> CONNECT");
        }

    }

    private void save(String result) {
        flag = 1;

        try {
            SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
            int val = flow.getInt("flow", 0); // #1
            if (val == 1) {
                SharedPreferences section = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
                SharedPreferences.Editor editor_retake = section.edit();
                editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
                editor_retake.commit();
            }


            SharedPreferences settings1 = getSharedPreferences(CommonUtilities.USER_SP, 0);
            int PatientIdDroid = Integer.parseInt(settings1.getString("patient_id",
                    "-1"));
            String[] str = null;
            str = result.split(",");

            SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_TIMESLOT_SP, 0);
            String slot = settings.getString("timeslot", "AM");

            ClasssPressure pressure = new ClasssPressure();
            pressure.setPatient_Id("" + PatientIdDroid);

            pressure.setSystolic(Integer.parseInt(str[0]));
            pressure.setDiastolic(Integer.parseInt(str[1]));
            pressure.setPulse(Integer.parseInt(str[2]));
            pressure.setInputmode(0);
            pressure.setTimeslot(slot);
            pressure.setBody_movment(bodyMovement);
            pressure.setIrregular_pulse(irregularPulse);
            pressure.setBattery_level(batteryLevel);
            pressure.setMeasurement_position_flag(positionFlag);
            pressure.setCuff_fit_flag(cuffFitFlag);

            SharedPreferences settings2 = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
            String sectiondate = settings2.getString("sectiondate", "0");
            pressure.setSectionDate(sectiondate);
            pressure.setPrompt_flag("4");
            int lastinsert_id = dbcreatepressure.InsertPressure(pressure);
            Intent intent = new Intent(OmronBlsActivity.this, ShowPressureActivity.class);

            isFinish = true;

            intent.putExtra("sys", str[0]);
            intent.putExtra("dia", str[1]);
            intent.putExtra("pulse", str[2]);
            intent.putExtra("pressureid", lastinsert_id);

            startActivity(intent);
            OmronBlsActivity.this.finish();

        } catch (Exception e) {
            Log.e("A&D", "Exception inserting bp from in a and d continua");
            e.printStackTrace();
        }

    }

    private void setwelcome() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void Animations() {
        txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));
        AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, 0f);
        //AnimPlz.setDuration(1000);
        AnimPlz.start();

        AnimPlz.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                Log.e(TAG, "AnimPlz");
                txtReading.setVisibility(View.VISIBLE);
                rocketImage.setVisibility(View.VISIBLE);
            }
        });
    }

    private void CheckBlueToothState() {
        Log.d(TAG, "IN #CheckBlueToothState()");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
        String bluetooth_error = ERROR_MSG.getString("bluetooth_error", "-1");
        if (mBluetoothAdapter == null) {
            errorShow(bluetooth_error);
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startPairing();

                    }
                }, 5000);

            } else {
                try {
                    errorShow(bluetooth_error);
                    mBluetoothAdapter.enable();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "CALL #getPairedDevices()");
                            if (mBluetoothAdapter.isDiscovering()) {
                                mBluetoothAdapter.cancelDiscovery();
                            }
                            startPairing();
                        }
                    }, 9000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CheckBlueToothState();
                } else {
                    isFinish = true;
                    Intent intentwt = new Intent(getApplicationContext(),
                            PressureEntry.class);
                    startActivity(intentwt);
                    overridePendingTransition(0, 0);
                    finish();
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }
    private Date parseTimeToMilliseconds(String time){

        Date dateTime = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dateTime = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;

    }

    Handler readingCollectingHandler = new Handler();
    Runnable readingCollectingRunnable  = new Runnable() {
        @Override
        public void run() {
            if (sensorCurrentTime == null || mLatestReading == null || mLatestReadingTime == null) {
                return;
            }
            checkReadingIsTaken(mLatestReading, mLatestReadingTime);
            mLatestReadingTime = null;
            mLatestReadingTime = null;
        }
    };

    String mLatestReading;
    Date mLatestReadingTime;

    private void AddReadingAndWaitForTime(String data, Date time) {
        Log.d(TAG, "AddReadingAndWaitForTime: ");

        if(data != null) {
            Log.d(TAG, "AddReadingAndWaitForTime:  reading received");
            Log.d(TAG, "AddReadingAndWaitForTime: mLatestReading" + (mLatestReading == null ? "NULL" : mLatestReading));
            Log.d(TAG, "AddReadingAndWaitForTime: mLatestReadingTime" + (mLatestReadingTime == null ? "NULL" : mLatestReadingTime));

            Log.d(TAG, "AddReadingAndWaitForTime: data" + (data == null ? "NULL" : data));
            Log.d(TAG, "AddReadingAndWaitForTime: time" + (time == null ? "NULL" : time));

            if(mLatestReading == null || mLatestReadingTime == null){
                Log.d(TAG, "AddReadingAndWaitForTime: `Setting First Reading");
                mLatestReading = data;
                mLatestReadingTime = time;

            } else {
                if(mLatestReadingTime.getTime() < time.getTime()) {
                    Log.d(TAG, "AddReadingAndWaitForTime: Setting New Reading");
                    mLatestReading = data;
                    mLatestReadingTime = time;
                } else
                    Log.d(TAG, "AddReadingAndWaitForTime: Skipping Reading");
            }
        }
        if(readingCollectingHandler == null) {
            readingCollectingHandler = new Handler();
        }
        readingCollectingHandler.removeCallbacks(readingCollectingRunnable);
        readingCollectingHandler.postDelayed(readingCollectingRunnable,5000);
    }

    private void checkReadingIsTaken(String data, Date time ) {
        // check whether the activity that is currently shown on screen is in fact the session, rather than the test device page
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        String mClassName = cn.getClassName();
        if (!mClassName.equals(this.getClass().getCanonicalName())) {
            Log.d(TAG, "checkReadingIsTaken: reading was intercepted in the session page while the current top activity is not the session page.");
            return;
        }

        long sensorTime = sensorCurrentTime.getTime() - time.getTime();
        long tabletTime = tabletRecievedTime.getTime() - tabletPageOpenTime.getTime();

        Log.d(DEBUG_TAG,  "sensorCurrentTime is " + (sensorCurrentTime == null ? "NULL" : "Not NULL"));
        Log.d(DEBUG_TAG,  "mReceivedData is " + (data == null ? "NULL" : "Not NULL"));
        if(sensorCurrentTime == null || data == null ){
            return;
        }

        Log.d(DEBUG_TAG, "checkReadingIsTaken: sensorCurrentTime = " + sensorCurrentTime.toString());
        Log.d(DEBUG_TAG, "checkReadingIsTaken: sensorReadingTime = " + sensorReadingTime.toString());
        Log.d(DEBUG_TAG, "checkReadingIsTaken: tabletReceivedTime = " + tabletRecievedTime.toString());
        Log.d(DEBUG_TAG, "checkReadingIsTaken: tabletPageOpenTime = " + tabletPageOpenTime.toString());

        Log.i(TAG, "sensorTime: " + sensorTime);
        Log.i(TAG, "tabletTime: " + tabletTime);

        if(sensorTime > tabletTime) {
            Log.i(TAG, "Old data");
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startPairing();
                }
            }, 25000);
        } else {
            Log.i(TAG, "New data");
            save(data);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        rocketImage.setBackground(null);
    }

}
