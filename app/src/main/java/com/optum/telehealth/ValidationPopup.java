package com.optum.telehealth;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.optum.telehealth.util.CommonUtilities;

/**
 * Created on 4/27/2016.
 * ERROR MESSAGE SHOWING ACTIVITY FOR OTH APK
 */
public class ValidationPopup extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.validationpopup);
        TextView validationinfo = (TextView) findViewById(R.id.validationinfo);

        ImageButton versionclose = (ImageButton) findViewById(R.id.validationclose);
        versionclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences settings2 = getSharedPreferences(
                        CommonUtilities.CLUSTER_SP, 0);
                int isHomeEnable =  settings2.getInt("homeButton", 0);
                if(isHomeEnable == 1){

                    Intent i = new Intent(ValidationPopup.this,
                            Home.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                }else{

                    Intent i = new Intent(ValidationPopup.this,
                            HFPHomeActivity.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                }
//                Intent intent = new Intent(ValidationPopup.this, HFPHomeActivity.class);
//                startActivity(intent);
//                overridePendingTransition(0, 0);
//                finish();
            }
        });

        String message = getIntent().getExtras().getString("message");
        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        validationinfo.setTypeface(type, Typeface.NORMAL);
        validationinfo.setText(message);
    }
}
