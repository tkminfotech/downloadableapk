package com.optum.telehealth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;

import com.optum.telehealth.bean.ClassReminder;
import com.optum.telehealth.dal.Flow_Status_db;
import com.optum.telehealth.dal.Reminder_db;
import com.optum.telehealth.util.Alam_util;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Regular_Alam_util;
import com.optum.telehealth.util.SkipVitals;
import com.optum.telehealth.util.Skip_Reminder_Values_Upload;
import com.optum.telehealth.util.UploadVitalReadingsTask;


public class Reminder_Landing_Activity extends Titlewindow {

	GlobalClass appState;
	public Reminder_db reminder_db;
	public Flow_Status_db Flow_db;
	public int restsrt_flag=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reminder__landing_);
		appState = (GlobalClass) getApplicationContext();
		reminder_db = new Reminder_db(this);
		Flow_db = new Flow_Status_db(Reminder_Landing_Activity.this);

	//	SkipVitals.skip_patient_vitalse(getApplicationContext());
		SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		int flow_type = flowsp.getInt("is_reminder_flow", 0);

		if (flow_type == 1)// skip for reminder
		{
			Log.e("Reminder_Landing_Activity", "During reminder flow user click on START/HOM- Inserting skip data");
			SkipVitals.skip_patient_vitalse(getApplicationContext());
			SharedPreferences.Editor seditor = flowsp.edit();
			seditor.putInt("is_reminder_flow", 0);
			seditor.commit();
		} else {
			Log.e("Reminder_Landing_Activity", "User click on START/HOME in normal flow.");
			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
			String posturl = settings.getString("Server_post_url", "-1");
			new UploadVitalReadingsTask(Reminder_Landing_Activity.this, posturl).execute();

		}

		Regular_Alam_util.cancelAlarm(this);
		Alam_util.cancelAlarm(this);
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		SharedPreferences.Editor seditor = flow.edit();
		seditor.putInt("is_reminder_flow", 0);
		seditor.commit(); // reset reminder flow
		Flow_db.open();
		Cursor cursor1 = Flow_db.selectAll();
		Status = new String[cursor1.getCount()];
		int j = 0;

		while (cursor1.moveToNext()) {
			Status[j] = cursor1.getString(cursor1.getColumnIndex("Status"));

			Status_Val = Status[0];
			j += 1;
		}
		Flow_db.close();

		if (Status_Val.equals("0")) {
			Flow_db.open();
			Flow_db.deleteAll();
			Flow_db.insert("1");
			Flow_db.close();
			ClassReminder reminder = new ClassReminder();
			if (reminder_db.GetStartedAlarm() != null) {
				reminder = reminder_db.GetStartedAlarm();
				reminder.setStatus(2); // Set status to SUCCESS
				reminder_db.UpdateReminderStatus(reminder);
			}

		} else {
			Log.i("TitleWindow", "appState.isAlarmTriggered()--> " + Status_Val);
		}
		if (appState.isRegular_Alarm_Triggered() == true) {
			Regular_Alam_util.cancelAlarm(this);
			appState.setRegular_Alarm_Triggered(false);
			appState.setStart_Alarm_Count(0);
		}

		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);
		String serverurl = settings1.getString("Server_post_url", "-1");

		new Skip_Reminder_Values_Upload(this, serverurl).execute();

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(Reminder_Landing_Activity.this, FinalMainActivity.class);
				startActivity(intent);
				finish();
			}
		}, 3000);

	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//			Log.i("Reminder_Landing_Activity", " KEYCODE_BACK clicked");
//
//			Intent intent = new Intent(this, FinalMainActivity.class);
//			startActivity(intent);
//			this.finish();
//
//		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.i("Reminder_Landing_Activity", " onRestart");
		/*Log.e("Reminder_Landing_Activity", "onRestart starting page");

		Intent intent = new Intent(this, FinalMainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.finish();

		startActivity(intent);
		overridePendingTransition(0, 0);*/

		restsrt_flag=1;
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed()
	{
		// code here to show dialog
		//super.onBackPressed();  // optional depending on your needs
	}

}
