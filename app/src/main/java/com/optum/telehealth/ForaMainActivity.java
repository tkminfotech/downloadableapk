package com.optum.telehealth;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassTemperature;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.dal.Temperature_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Log;
import com.optum.telehealth.util.PINReceiver;
import com.optum.telehealth.util.Util;
import com.taidoc.pclinklibrary.android.bluetooth.util.BluetoothUtil;
import com.taidoc.pclinklibrary.connection.AndroidBluetoothConnection;
import com.taidoc.pclinklibrary.connection.util.ConnectionManager;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryConstant;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryEnum;
import com.taidoc.pclinklibrary.exceptions.NotSupportMeterException;
import com.taidoc.pclinklibrary.meter.AbstractMeter;
import com.taidoc.pclinklibrary.meter.record.AbstractRecord;
import com.taidoc.pclinklibrary.meter.record.TemperatureRecord;
import com.taidoc.pclinklibrary.meter.util.MeterManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class ForaMainActivity extends Titlewindow{
	private static final String TAG = "ForaMainActivity";

	private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
	public static final int REQUEST_DISCOVERABLE_CODE = 42;

	private static final int SENSOR_TYPE_BLE = 1;
	private static final int SENSOR_TYPE_CLASSIC = 0;
	private static final int SENSOR_TYPE_UNKNOWN = -1;

	private String pageName = "";
	private GlobalClass appClass;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz;
	TextView txtWelcome, txtReading;
	private TextView txt_errorMsg;
	private ProgressDialog progressDialog = null;
	boolean redirectFlag = false, btOFF = false;

	private static final String HEXES = "0123456789ABCDEF";
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private final BroadcastReceiver pinReceiver = new PINReceiver();
	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothSocket btSocket = null;
	private OutputStream outStream = null;
	private InputStream inStream = null;

	String macAddress = "";
	BluetoothDevice device = null;
	private BTConnector btFora;
	Temperature_db dbtemperature = new Temperature_db(this);
	Sensor_db dbSensor = new Sensor_db(this);

	private AndroidBluetoothConnection mConnection; // BLE connection (PCLink library)
	private AbstractMeter mForaMeter = null;

	// bluetooth recovery variables
	private boolean mBluetoothSocketTimedOut = false; // used to indicate that the BluetoothSocket connection has timed out
	private int failedBleConnectionCount = 0; // used to indicate how many BLE connections have failed

	int Dec = 0;
	int tempId;
	private int deviceType = -1;
	private String result = "0, 0";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aand_dreader);
		appClass = (GlobalClass) getApplicationContext();
		txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = false;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		//
		// get extras
		//
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		deviceType = extras.getInt("deviceType");
		pageName = "Temperature Sensor Page";
		macAddress = extras.getString("macaddress");

		txtWelcome = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		Typeface type = Typeface.createFromAsset(getAssets(), "fonts/FrutigerLTStd-Roman.otf");

		txtWelcome.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		try{
			rocketImage.setBackgroundResource(R.drawable.temp_animation);
            rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
            rocketAnimation.start();
            rocketImage.setVisibility(View.INVISIBLE);
            txtReading.setVisibility(View.INVISIBLE);
		} catch (OutOfMemoryError ex){
			Log.i("exception",ex.toString());
			rocketImage.setBackgroundResource(R.drawable.temperature009);
		}

		setWelcome();

		Button btnManual = (Button) findViewById(R.id.aAndDManuval);
		btnManual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				try {
					appClass.appTracking(pageName,"Click on Manual entry button");

					Intent intentTemp = new Intent(getApplicationContext(), TemperatureEntry.class);

					if (btFora != null && !btFora.isCancelled())
						btFora.cancel(true);

					startActivity(intentTemp);
					finish();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		// register the pairing request receiver
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(pinReceiver, filter);

		// Register for broadcasts on BluetoothAdapter state change
		IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
		registerReceiver(onOffReceiver, filter1);

		Log.d(TAG, "onCreate: Fora Activity");
	}

	@Override
	protected void onResume() {
		redirectFlag = false;

		// request location permission if on Marshmallow or over, and if it isn't already granted
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
				requestPermissions(new String[] { Manifest.permission.ACCESS_COARSE_LOCATION }, PERMISSION_REQUEST_COARSE_LOCATION);

				// enable location if on Android 7 or greater and the location service is turned off
				// for Android 7, the location service must be turned on for the device to be able to LE scan and receive scan results
			else if (Build.VERSION.SDK_INT >= 24 && !Util.isLocationOn(this)) {
				Util.displayLocationSettingsRequest(this);
			}

			else
				checkBluetoothState();

		} else {
			checkBluetoothState();
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause: called");
		super.onPause();
	}

	@Override
	public void onStop() {
		try {
			mBluetoothAdapter.cancelDiscovery();

			if (btFora != null) {
				redirectFlag = true;
				btFora.cancel(true);
				btFora = null;
			}

			if (btSocket != null) {
				btSocket.close();
				btSocket = null;
			}

			leDisconnect();

			try {
				unregisterReceiver(pinReceiver);

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				unregisterReceiver(onOffReceiver);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			Log.e(TAG, "onStop: unregister receiver exception", e);
		}
		Log.e(TAG, "-- ON STOP Fora activity --");
		super.onStop();
	}

	// step 1 - called in onResume.
	private void checkBluetoothState() {
		Log.d(TAG, "IN #checkBluetoothState()");

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
		String bluetooth_error  = ERROR_MSG.getString("bluetooth_error", "-1");

		if (mBluetoothAdapter == null) {
			errorShow(bluetooth_error);

		} else if (mBluetoothAdapter.isEnabled()) {
			enableBluetoothDiscovery(); // call step 2 (if bluetooth is enabled)

		} else {
			try {
				errorShow(bluetooth_error);
				mBluetoothAdapter.enable(); // enable bluetooth (step 2 will be called from the broadcast receiver)
				btOFF = false;

			} catch (Exception e) {
				Log.e(TAG, "checkBluetoothState: exception", e);
			}
		}
	}

	// step 2 - called in CheckBluetoothState
	// checks whether the Bluetooth is discoverable
	// start classic/LE connections
	public void enableBluetoothDiscovery() {
		int sensorBluetoothType = getSensorBluetoothType(macAddress);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
			// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
			if (sensorBluetoothType != SENSOR_TYPE_BLE)
				classicConnect();

			// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
			// we already checked that the API level is > 18
			if (sensorBluetoothType != SENSOR_TYPE_CLASSIC)
				leConnect();

		} else {
			BluetoothAdapter BA;
			BA = BluetoothAdapter.getDefaultAdapter();
			if (BA.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {

				Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
				turnOn.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
				startActivityForResult(turnOn, REQUEST_DISCOVERABLE_CODE);

			} else {
				// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
				if (sensorBluetoothType != SENSOR_TYPE_BLE)
					classicConnect();

				// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
				if (Build.VERSION.SDK_INT >= 18 && sensorBluetoothType != SENSOR_TYPE_CLASSIC)
					leConnect();
			}
		}
	}

	// step 3 (classic) - called in enableBluetoothDiscovery
	public void classicConnect() {
		Log.d(TAG, "classicConnect: macAddress ==" + macAddress);

		// getting the assigned BT device
		if (macAddress.trim().length() == 17) {
			device = mBluetoothAdapter.getRemoteDevice(macAddress);
		}

		if (device == null) {
			createOKProgressDialog(this.getString(R.string.Bluetooth), this.getString(R.string.unabletoconnectbluetoothAandD));
			finish();

		} else {
			Log.d(TAG, "classicConnect: execute BTConnector AsyncTask - BT device: " + device);
			btFora = (BTConnector) new BTConnector().execute();
		}
	}

	// step 4 (classic) called in classicConnect
	private class BTConnector extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... results) {
			try {
				for (Integer port = 1; port <= 1500; port++) {

					// break the loop if we navigate away from the activity (fail-safe to classicDisconnect)
					// or if the async task is cancelled (cancel is called)
					if (!Util.isActivityOnTop(ForaMainActivity.this) || isCancelled())
						break;

					if (isCancelled() || btOFF || redirectFlag) {
						Log.i(TAG, "cancelled in background");
						return "";
					}

					if (connect(device)) {
						Log.i(TAG, " Connection successful for " + port);
						return result;
					}
					try {
						Log.i(TAG, ": Waiting. port.." + port);
						Thread.sleep(2000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			// dismiss the dialog after the file was downloaded
			if (progressDialog != null)
				progressDialog.dismiss();

			if (result.trim().length() > 0) {
				try {
					Intent intent = new Intent(ForaMainActivity.this, ShowTemperatureActivity.class);
					intent.putExtra("temp", result);
					intent.putExtra("tempid", tempId);
					startActivity(intent);
					ForaMainActivity.this.finish();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		protected boolean connect(BluetoothDevice device) {
			byte[] data = new byte[34];
			result = "0, 0";

			// mBluetoothAdapter.startDiscovery(); changed this as follows
			mBluetoothAdapter.cancelDiscovery();

			SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
			int val = flow.getInt("flow", 0); // #1
			if (val == 1) {
				SharedPreferences section = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
				SharedPreferences.Editor editor_retake = section.edit();
				editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(ForaMainActivity.this));
				editor_retake.apply();
			}
			try {
				btSocket = device.createRfcommSocketToServiceRecord(MY_UUID); // connect to the remote BT device

			} catch (IOException e) {
				Log.e(TAG, "ON RESUME: Socket creation failed.", e);
			}

			if (btSocket == null) {
				return false;
			}

			try {
				try {
					// (from doc) This call will block until a connection is made or the connection fails
					btSocket.connect();

					Log.d(TAG, "BTConnector#connect: BT connection established, data transfer link open.");

				} catch (IOException e) {
					try {
						Log.e(TAG, "BTConnector#connect: btSocket.connect error.");
						e.printStackTrace();
						btSocket.close();

						mBluetoothSocketTimedOut = true;

						return false;

					} catch (IOException e2) {
						e.printStackTrace();
						Log.e(TAG, "BTConnector#connect: btSocket.close error.");
					}
				}

				// if we're able to successfully connect to the socket, it means that the sensor is classic bluetooth.
				// so we'll stop BLE stuff
				leDisconnect();
				// save the sensor type as BT classic
				saveSensorBluetoothType(macAddress, SENSOR_TYPE_CLASSIC);

				// attempt to connect to device
				try {

					outStream = btSocket.getOutputStream();
					inStream = btSocket.getInputStream();

					Log.i(TAG, "deviceType" + deviceType);

					int checkSum = (0x51 + 0x26 + 0x00 + 0x00 + 0x00 + 0x00 + 0xA3) & 0xFF;

					int cmd[] = {0x51, 0x26, 0x00, 0x00, 0x00, 0x00, 0xA3, checkSum};

					try {
						for (int aCmd : cmd) {
							outStream.write((aCmd & 0xFF));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					if (inStream.available() > 0) {
						inStream.read(data);
					}

					try {
						for (int aCmd : cmd) {
							outStream.write((aCmd & 0xFF));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(1500);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					byte H;
					byte L;
					try {
						System.out.print("Data received ");

						if (inStream.available() > 0) {
							inStream.read(data);

							System.out.println(getHex(data, data.length));

							H = data[3];
							L = data[2];
							String hex1 = Integer.toHexString(H & 0XFF);
							String hex2 = Integer.toHexString(L & 0XFF);
							String NewHex = hex1 + hex2;

							Log.i(TAG, "Temperature value from device hex " + NewHex);

							Dec = Integer.parseInt(NewHex, 16);
							Log.i(TAG, "Temperature value from device C " + Dec);

							float temp = ((float) Dec / 10);
							double finalValue = (temp * (1.8)) + 32;

							double tempValue;
							tempValue = Util.round(finalValue);

							result = tempValue + "";

							insertReading(tempValue); // inserts the reading to the SQLite database

							byte messageTurnOff[] = {81, 80, 0, 0, 0, 0, -93, 68};

							try {
								outStream.write(messageTurnOff);
							} catch (IOException e) {
								Log.e(TAG, "BTConnector#connect: Exception during turn off write cmd in temp device .", e);
								e.printStackTrace();
							}

							try {
								inStream.close();
								outStream.close();
								btSocket.close();
							} catch (IOException e2) {
								Log.e(TAG, "BTConnector#connect: IOException while closing the socket or stream after the reading was taken", e2);
								e2.printStackTrace();
							}

							return true;
						}

					} catch (IOException e) {
						Log.e(TAG, "BTConnector#connect: Exception during read.", e);
						e.printStackTrace();
					}

				} catch (Exception e) {
					Log.e(this.toString(), "BTConnector#connect: IOException " + e.getMessage());
					e.printStackTrace();
				}
			} catch (Exception ex) {
				Log.e(this.toString(), "BTConnector#connect: Exception " + ex.getMessage());
				ex.printStackTrace();
			}

			try {
				inStream.close();
				outStream.close();
				btSocket.close();

			} catch (Exception cl) {
				cl.printStackTrace();
			}

			return false;
		}
	}

	// step 3 (BLE) - called in enableBluetoothDiscovery
	private void leConnect() {
		Log.d(TAG, "leConnect()");

		setupAndroidBluetoothConnection();
		connectMeter();
	}

	// step 4.1 (BLE) - called in leConnect
	private void setupAndroidBluetoothConnection() {
		Log.d(TAG, "setupAndroidBluetoothConnection() - mConnection: " + mConnection);
		if (mConnection == null) {
			try {
				mConnection = ConnectionManager.createAndroidBluetoothConnection(mBTConnectionHandler);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// step 4.2 (BLE) - called in leConnect
	private void connectMeter() {
		Log.d(TAG, "#IN connectMeter()");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					Log.d(TAG, "connectMeter -> updatePairedList");
					updatePairedList();

					if (mConnection.getState() == AndroidBluetoothConnection.STATE_NONE) {
						Log.d(TAG, "connectMeter -> setLeConnectedListener");
						mConnection.setLeConnectedListener(mLeConnectedListener);

						Log.d(TAG, "connectMeter -> LeListen!!");
						mConnection.LeListen();
					}

				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);

				} finally {
					getTemperatureValue();
				}

				Looper.loop();
			}

		}).start();
	}

	// release BLE resources
	private void leDisconnect() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					Log.d(TAG, "leDisconnect: stopping BLE scanning/communication.");
					if (mConnection != null) {
						mConnection.setLeConnectedListener(null);

						// disconnecting caused connection errors on Android 7!
						if (Build.VERSION.SDK_INT < 24)
							mConnection.LeDisconnect();

						mConnection = null;
					}

				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
				}

				Looper.loop();
			}

		}).start();
	}

	// release BT classic resources
	private void classicDisconnect() {
		Log.d(TAG, "classicDisconnect: stopping BT classic scanning/communication.");
		if (btFora != null) {
			btFora.cancel(true);
			btFora = null;
		}

		if (btSocket != null) {
			try {
				btSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			btSocket = null;
		}
	}

	//region BLE connection handler
	private Handler mBTConnectionHandler = new Handler() {
		@TargetApi(Build.VERSION_CODES.KITKAT)
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
					case PCLinkLibraryConstant.MESSAGE_STATE_CHANGE:

						int mLeConnectionState = msg.arg1;
						Log.d(TAG, "mBTConnectionHandler#handleMessage -- MESSAGE_STATE_CHANGE: " + mLeConnectionState);

						switch (msg.arg1) {
							case AndroidBluetoothConnection.STATE_CONNECTED_BY_LISTEN_MODE:

								try {
									Log.d(TAG, "mBTConnectionHandler#handleMessage -> set mForaMeter. mConnection: " + mConnection);
									mForaMeter = MeterManager.detectConnectedMeter(mConnection);

								} catch (Exception e) {
									e.printStackTrace();
									throw new NotSupportMeterException();
								}
								if (mForaMeter == null) {
									throw new NotSupportMeterException();
								}
								break;

							case AndroidBluetoothConnection.STATE_CONNECTING:
								Log.d(TAG, "mBTConnectionHandler#handleMessage: connecting");
								break;

							case AndroidBluetoothConnection.STATE_SCANED_DEVICE:
								// LE sensor found, release BT classic resources
								classicDisconnect();
								// save the sensor type as BLE
								saveSensorBluetoothType(macAddress, SENSOR_TYPE_BLE);

								try {
									// Log.d(TAG, "mBTConnectionHandler#handleMessage -- connected device address: " + mConnection.getConnectedDeviceAddress());
									final BluetoothDevice device = BluetoothUtil.getPairedDevice(mConnection.getConnectedDeviceAddress());

									// connect on the UI thread
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											mConnection.LeConnect(getApplicationContext(), device);
										}
									});

								} catch (Exception e) {
									Log.e(TAG, "handleMessage - STATE_SCANED_DEVICE: exception: " + e.getMessage());
									e.printStackTrace();
								}

								break;

							case AndroidBluetoothConnection.STATE_LISTEN:
								break;

							case AndroidBluetoothConnection.STATE_NONE:
								break;

							case AndroidBluetoothConnection.STATE_BLUETOOTH_STACK_NULL_EXCEPTION:
								recoverBluetooth();
								break;
						} /* end of switch */
						break;

				} /* end of switch */

				Log.d(TAG, "mBTConnectionHandler#handleMessage -- Message: " + msg.what);

			} catch (NotSupportMeterException e) {
				Log.e(TAG, "not support meter", e);
			}
		}
	};

	// creates a Bluetooth bond with the Fora sensor (ANDROID 7 ONLY)
	// on Android 7, pairing was found to increase connection reliability.
	// without pairing, the connectGatt always returned with status 133!
	@TargetApi(Build.VERSION_CODES.KITKAT)
	private void pair(AbstractMeter foraMeter) {

		device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(macAddress);

		if (foraMeter != null && Build.VERSION.SDK_INT >= 24)
			device.createBond();
	}
	//endregion

	private AndroidBluetoothConnection.LeConnectedListener mLeConnectedListener = new AndroidBluetoothConnection.LeConnectedListener() {

		@Override
		public void onConnectionStateChange_Disconnect(BluetoothGatt gatt, int status, int newState) {
			mConnection.LeDisconnect();

			if (shouldRecoverBluetooth())
				recoverBluetooth();
		}

		@SuppressLint("NewApi")
		@Override
		public void onDescriptorWrite_Complete(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			Log.d(TAG , "onDescriptorWrite_Complete" );
			mConnection.LeConnected(gatt.getDevice());
		}

		@Override
		public void onCharacteristicChanged_Notify(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

			new Thread(new Runnable() {
				@TargetApi(Build.VERSION_CODES.KITKAT)
				@Override
				public void run() {
					Looper.prepare();

					try {
						mForaMeter = MeterManager.detectConnectedMeter(mConnection);
						Log.e(TAG, "mForaMeter reference: " + mForaMeter);
						Log.e(TAG, "mConnection reference: " + mConnection);

					} catch (Exception e) {
						e.printStackTrace();
						mConnection.LeDisconnect(); // disconnect and try again
					}

					Looper.loop();
				}
			}).start();
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {}
	};

	// updates the mConnection with the assigned MAC address
	private void updatePairedList() {
		Map<String, String> addrs = new HashMap<String, String>();
		String addrKey = "BLE_PAIRED_METER_ADDR_" + String.valueOf(0);
		addrs.put(addrKey, macAddress);

		Log.d(TAG, "updatePairedList -- set mConnection");

		if (mConnection == null) {
			mConnection = ConnectionManager.createAndroidBluetoothConnection(mBTConnectionHandler);
		}

		Log.d(TAG, "updatedPairedList -- MAC address: " + macAddress);
		mConnection.updatePairedList(addrs, 1);
	}

	public void getTemperatureValue() {
		try {
			if (mForaMeter == null) {

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						// check whether the activity that is currently shown on screen is in fact the session, rather than the test device page
						ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
						ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
						String mClassName = cn.getClassName();
						if (!mClassName.equals(ForaMainActivity.this.getClass().getCanonicalName())) {
							Log.d(TAG, "getTemperatureValue#run: trying to run the thread while outside of the activity.");
							return;
						}

						Log.d(TAG, "getTemperatureValue -> connectMeter (handler). mForaMeter is NULL mConnection isNull: " + (mConnection == null));

						setupAndroidBluetoothConnection();
						connectMeter();
					}
				}, 10000);

			} else {
				Log.e(TAG, "ForaMeter reference is NOT NULL");

				AbstractRecord record = mForaMeter.getStorageDataRecord(0, PCLinkLibraryEnum.User.CurrentUser);

				double valueInFahrenheit = ((TemperatureRecord) record).getObjectTemperatureValue();

				double finalValue = Util.round((valueInFahrenheit * (1.8)) + 32);
				result = String.valueOf(finalValue);

				// pair with the sensor if found - Android 7 ONLY!
				pair(mForaMeter);

				insertReading(finalValue);
				startResultActivity(result, tempId);

				try {
					mForaMeter.turnOffMeterOrBluetooth(0);
					leDisconnect();
					mForaMeter = null;

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//region bluetooth state receiver
	private final BroadcastReceiver onOffReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
				final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

				switch (state) {
					case BluetoothAdapter.STATE_TURNING_OFF:
						btOFF = true;
						if (btFora != null && !btFora.isCancelled()) {
							Log.e(TAG, "btFora not cancelled, CANCELLING");
							btFora.cancel(true);
						}

						break;

					case BluetoothAdapter.STATE_OFF:
						mBluetoothAdapter.enable();
						break;

					case BluetoothAdapter.STATE_ON:
						btOFF = false;
						Log.d(TAG, "onOffReceiver#onReceive: bluetooth is enabled");
						enableBluetoothDiscovery(); // start step 2
						break;
				}
			}
		}
	};
	//endregion

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case PERMISSION_REQUEST_COARSE_LOCATION: {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d("ss", "coarse location permission granted");
					checkBluetoothState();
				} else {
					Intent intentTemp = new Intent(getApplicationContext(), TemperatureEntry.class);
					startActivity(intentTemp);
					overridePendingTransition(0, 0);
					finish();
				}
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_DISCOVERABLE_CODE) {
			if (resultCode == RESULT_CANCELED) {
				errorShow(this.getString(R.string.connectivityError));
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (!redirectFlag) {
							Intent intentTemp = new Intent(getApplicationContext(), TemperatureEntry.class);
							startActivity(intentTemp);
							overridePendingTransition(0, 0);
							finish();
						}
					}
				}, 4000);
			} else {
				classicConnect();
			}
		}
	}

	//region Helpers
	public double round(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
	public String getHex(byte[] raw, int len) {
		Log.i(TAG, "Get Hex...");
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
					.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}
	static String byteArrayToHexString(byte in[]) {

		byte ch = 0x00;
		int i = 0;
		if (in == null || in.length <= 0)
			return null;

		String pseudo[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"A", "B", "C", "D", "E", "F"};

		StringBuffer out = new StringBuffer(in.length * 2);
		while (i < in.length) {
			ch = (byte) (in[i] & 0xF0); // Strip off high nibble
			ch = (byte) (ch >>> 4);
			// shift the bits down
			ch = (byte) (ch & 0x0F);
			// must do this is high order bit is on!
			out.append(pseudo[(int) ch]); // convert the nibble to a String
			// Character
			ch = (byte) (in[i] & 0x0F); // Strip off low nibble
			out.append(pseudo[(int) ch]); // convert the nibble to a String
			// Character
			i++;
		}

		String rslt = new String(out);

		return rslt;

	}

	private void insertReading(double value) {
		ClassTemperature temperature = new ClassTemperature();
		temperature.setTemperatureValue(value + "");
		temperature.setInputmode(0);
		SharedPreferences settings1 = getSharedPreferences( CommonUtilities.PREFS_NAME_date, 0);
		String sectionDate = settings1.getString("sectiondate", "0");

		SharedPreferences settings = getSharedPreferences( CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");

		temperature.setSectionDate(sectionDate);
		temperature.setTimeslot(slot);
		tempId = dbtemperature.InsertTemperatureMeasurement(temperature); // tempId is a member!
	}

	private void startResultActivity(String value, int id) {
		if (progressDialog != null)
			progressDialog.dismiss();

		if (value.trim().length() > 0) {
			try {
				Intent intent = new Intent(ForaMainActivity.this, ShowTemperatureActivity.class);
				intent.putExtra("temp", value);
				intent.putExtra("tempid", id);

				startActivity(intent);
				ForaMainActivity.this.finish();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void saveSensorBluetoothType(String mac, int type) {
		SharedPreferences preferences = getSharedPreferences("", 0);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(mac, type);
		editor.apply();
	}
	private int getSensorBluetoothType(String mac) {
		SharedPreferences preferences = getSharedPreferences("", 0);
		int type = preferences.getInt(mac, SENSOR_TYPE_UNKNOWN);

		Log.d(TAG, "getSensorBluetoothType: type: " + type);

		return type;
	}

	// this method is called when disconnecting from a BLE connection
	private boolean shouldRecoverBluetooth() {
		boolean shouldRecover = mBluetoothSocketTimedOut && getSensorBluetoothType(macAddress) != SENSOR_TYPE_CLASSIC;

		failedBleConnectionCount++;
		shouldRecover = shouldRecover || failedBleConnectionCount >= 2;

		Log.d(TAG, "shouldRecoverBluetooth: shouldRecover: " + shouldRecover);

		return shouldRecover;
	}
	private void recoverBluetooth() {
		Log.d(TAG, "recoverBluetooth: called.");
		if (mBluetoothAdapter != null) {
			mBluetoothAdapter.disable();

			// reinitialize recovery variables
			mBluetoothSocketTimedOut = false;
			failedBleConnectionCount = 0;
		}
	}
	//endregion

	//region UI methods
	private void setWelcome() {
		String deviceMessage;
		deviceMessage = this.getString(R.string.entertemperaturebluetooth);

		String Pin = dbSensor.SelectTempPIN();
		Constants.setPIN(Pin); // set pin for auto pairing

		txtWelcome.setText(deviceMessage);
		AnimPlz = ObjectAnimator.ofFloat(txtWelcome, "translationY", 0f, 0f);
		//AnimPlz.setDuration(1000);
		AnimPlz.start();
		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {
				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);
			}
		});
	}
	public void errorShow(String msg) {
		txt_errorMsg.setVisibility(View.VISIBLE);
		txt_errorMsg.setText(msg);
		final Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						txt_errorMsg.animate().alpha(0.0f);
						txt_errorMsg.setVisibility(View.GONE);
						timer.cancel();
					}
				});

			}
		}, 3000, 3000);
	}
	private void createOKProgressDialog(String title, String message) {
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setButton(DialogInterface.BUTTON_POSITIVE,
				this.getString(R.string.OK),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (!btFora.isCancelled())
							btFora.cancel(true); // Canceling AsyTask
						if (dialog != null)
							dialog.dismiss();
						progressDialog.dismiss();
						RedirectionActivity();

					}
				});
		progressDialog.show();
	}
	//endregion

	//region unused methods
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	public Boolean preference() {
		Boolean flag = null;
		try {

			String PREFS_NAME = "DroidPrefSc";
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduleStatus = settings.getInt("Scheduletatus", -1);
			Log.d("Droid", " SharedPreferences : " + getApplicationContext() + " is : " + scheduleStatus);

			flag = scheduleStatus == 1;

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return flag;
	}
	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.optum.telehealth.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	public BluetoothDevice getDevice(String devName) {

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if ((mBluetoothAdapter != null)) { // &&
			setProgressBarIndeterminateVisibility(true);
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					if (device.getName().contains(devName)) { // "ANIL-PC",
						// BlackBerry
						// 9800,
						// UC-321PBT,
						// NONIN
						return device;
					}
				}
			}
		}
		return null;
	}
	private void RedirectionActivity() {
		if (deviceType == 0) {

			Intent intent = new Intent(getApplicationContext(), UploadMeasurement.class);
			intent.putExtra("reType", 1);

			startActivity(intent);
			finish();
			// redirect to pulse
		} else if (deviceType == 1) {
			String macaddress = getForaMAC(dbSensor.SelectTempSensorName());
			Intent intentfr = new Intent(getApplicationContext(), ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", macaddress
			);
			startActivity(intentfr);
			finish();
		} else if (deviceType == 2) {
			Intent intent = new Intent(getApplicationContext(), GlucoseEntry.class);

			startActivity(intent);
			finish();
		}
	}
	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, "DROID : Connect to macAddress " + macAddress);

		// String macAddress = "00:1C:05:00:40:64"; //"";
		// find MAC address of oxymeter from the DB
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}
	@Override
	public void onBackPressed() {
		// code here to show dialog
		//super.onBackPressed();  // optional depending on your needs
	}
	//endregion

	@Override
	public void onDestroy(){
		super.onDestroy();

		rocketImage.setBackground(null);
	}

}
