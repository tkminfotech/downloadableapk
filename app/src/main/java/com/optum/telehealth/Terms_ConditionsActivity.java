package com.optum.telehealth;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Terms_ConditionsActivity extends Activity {
    private Button btn_accept;
    private String res_patient_id, flow_from, TAG = "Terms_Conditions", serviceUrl;
    private TextView txt_terms_conditions;
    private GlobalClass appClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appClass = (GlobalClass) getApplicationContext();
        appClass.setEnglishLanguage(this);
        appClass.isEllipsisEnable = true;
        appClass.isSupportEnable = true;
        setContentView(R.layout.activity_terms__conditions);


        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                res_patient_id = bundle.getString("patient_id");//this is for String pin_id
                flow_from = bundle.getString("flow_from");//this is for String message
                //Log.i(TAG, "flow_from = " + flow_from);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");

        txt_terms_conditions = (TextView) findViewById(R.id.txt_terms_conditions);
        txt_terms_conditions.setClickable(true);
        txt_terms_conditions.setMovementMethod(LinkMovementMethod.getInstance());

        if (appClass.isConnectingToInternet(getBaseContext())) {
            new mob_terms_conditions().execute();
        } else {
            AlertShow();
        }

        btn_accept = (Button) findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appClass.appTracking("Terms and Conditions Page","Click on Accept button");
                if (flow_from.equals("registration")) {
                    SharedPreferences settings = getSharedPreferences(CommonUtilities.TermesAndCondition_SP, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("ACCEPT", "yes");
                    editor.commit();
                    SharedPreferences Login_User = getSharedPreferences("Login_User", 0);
                    String username = Login_User.getString("username", "-1");
                    if (!username.equals("-1")) {
                        Intent i = new Intent(Terms_ConditionsActivity.this, AuthenticationActivity.class);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    } else {
                        Intent i = new Intent(Terms_ConditionsActivity.this, RegistrationActivity.class);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    }

//                    Intent i = new Intent(Terms_ConditionsActivity.this, RegisterCredentialActivity.class);
//                    i.putExtra("patient_id", res_patient_id);
//                    startActivity(i);
//                    overridePendingTransition(0, 0);
//                    finish();
                } else {
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    private class mob_terms_conditions extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        String res_message;

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_terms_conditions");
            try {
                progressDialog = new ProgressDialog(Terms_ConditionsActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_terms_conditions #doInBackground");
            String response = "";
            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_terms_conditions.ashx", new String[]{
                                "patient_id"},
                        new String[]{"1234567"});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                //str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                //Log.i(TAG, "mob_terms_conditions: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("terms_conditions");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("Success")) {
                        response = "success";
                        res_message = Util.getTagValue(nodes, i, "message");
                        return response;
                    } else {
                        response = "failed";
                        res_message = Util.getTagValue(nodes, i, "message");
                        return response;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("success")) {
                    // txt_terms_conditions.setText(res_message);
                    //String text = "<a href='https://othwebtest.portal724.us:8720'>"+ res_message +"</a>";
                    //txt_terms_conditions.setLinkTextColor(Color.parseColor("d"));
                    txt_terms_conditions.setText(Html.fromHtml(res_message));
                } else {
                    AlertShow();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    public void AlertShow() {
        new AlertDialog.Builder(Terms_ConditionsActivity.this)
                .setTitle("Connection Error")
                .setMessage(getResources().getString(R.string.internetFailed))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent i = new Intent(Terms_ConditionsActivity.this, RegistrationActivity.class);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    }
                }).create().show();
    }
}
