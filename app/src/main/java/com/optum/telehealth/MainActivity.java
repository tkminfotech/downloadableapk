package com.optum.telehealth;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.optum.telehealth.bean.ClassAdvice;
import com.optum.telehealth.bean.ClassMeasureType;
import com.optum.telehealth.bean.ClassSchedule;
import com.optum.telehealth.bean.ClassSensor;
import com.optum.telehealth.bean.ClassUser;
import com.optum.telehealth.dal.AdviceMessage_db;
import com.optum.telehealth.dal.Login_db;
import com.optum.telehealth.dal.MeasureType_db;
import com.optum.telehealth.dal.MydataSensor_db;
import com.optum.telehealth.dal.Questions_db;
import com.optum.telehealth.dal.Schedule_db;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.service.ScheduleService;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.ConnectionDetector;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.UploadTrackerDetails;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends Titlewindow {

    private Login_db login_db = new Login_db(this);

    private Sensor_db sensor_db = new Sensor_db(this); //
    private MydataSensor_db myDataSensor_db = new MydataSensor_db(this);
    MeasureType_db db_measureType = new MeasureType_db(this);
    private Schedule_db dbSchedule = new Schedule_db(this);

    private String TAG = "MainActiviy";
    private BluetoothAdapter mBluetoothAdapter = null;
    public static String serialNo = "";
    public static String imeilNo = "";
    private String NickName = "";
    public static String PostUrl = "";
    private static String port = "";
    private static String server = "";
    public static String PatientIdDroid;
    private ProgressDialog progressDialog;
    public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
    public static final String PREFS_NAME_TIME = "TimePrefSc";
    public static final String PREFS_NAME = "MyLogFileCreatedDateSierra";

    ProgressDialog pd = null;
    public int flag_login = 0;
    Dialog dialog;
    private String serviceUrl;
    private String patientId;
    private String token;
    boolean resumeFlag = false;
    Questions_db dbcreate1 = new Questions_db(this);
    AdviceMessage_db dbcreate = new AdviceMessage_db(this);
    String[] data_date = new String[]{};
String ContractCode = "";
    int[] userid, patientid, status;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "MainActivity started");
        if (getResources().getBoolean(R.bool.isTab)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_final_main);
        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = settings.getString("Server_post_url", "-1");
        SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
        token = tokenPreference.getString("Token_ID", "-1");
        patientId = tokenPreference.getString("patient_id", "-1");

        Log.e(TAG, "Started Main Activity");
        setprefference(0);
        setprefferenceForThanks("AA");
        setprefferenceWelcome();

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = (new ConnectionDetector(
                getApplicationContext())).isConnectingToInternet();
        Log.e(TAG, "Checking Config details in db");

        checkConfig();

        Log.i("Droid", "Device Serial Number : " + serialNo);

        // load patient details
        if (isInternetPresent) {
            Log.e(TAG, "Internet present downloading patient details started");

            // Toast.makeText(this, "  loadPatientDetails post started.",
            // Toast.LENGTH_LONG).show();
            Log.i(TAG, "Loading patient details...");

            //loadPatientDetails(); // if there is a connection , fetch
            // Patient
            // Details to local Sql_Lite table
            SharedPreferences patientSettings = getSharedPreferences(CommonUtilities.USER_SP, 0);
            PatientIdDroid = patientSettings.getString("patient_id", "0");
            ContractCode = patientSettings.getString("contract_code_name", "0");
            loadSensorDetails();
        } else {
            Log.e(TAG, "Internet not present loading patient details from db");
            /*Toast.makeText(this,
                    this.getString(R.string.turnonwifiorchecknetconnection),
					Toast.LENGTH_LONG).show();*/
            Log.i(TAG, "No net connection...");

            ClassUser classUser = new ClassUser();

            Login_db login_db1 = new Login_db(this);

            classUser = login_db1.SelectNickName();

            NickName = classUser.getNickName();
            PatientIdDroid = classUser.getPatientId();
            CommonUtilities.Login_UserName = NickName;
            Cursor Patientdetails = null;
            // login_db.SelectNickName();
            if (NickName == null) {
                // if (NickName.trim().length() == 0)
                {

                    Toast.makeText(this,
                            this.getString(R.string.connecttointernet),
                            Toast.LENGTH_LONG).show();

                    return;
                }
            } else {
                // userName.setText(NickName);
            }
            loginmyUser();
        }

        // loginmyUser();
    }

    @Override
    public void onStop() {
        super.onStop();

//        Util.WriteLog(TAG, PatientIdDroid + "");

        flag_login = 1;
        clickedhpf = 1;
        // this.finish();

        Log.i(TAG, "onStop on main activity");

        try {
//            check_logfile();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void check_logfile() {

        String Log_created_date = "";
        SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);

//        if (Util.WriteLog(TAG, PatientIdDroid + "")) // true, if log file newly
//        // created -- (false, if
//        // log file is already
//        // exists and re writing
//        // to the current file)
//        {
//            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
//            SharedPreferences.Editor editor = settings.edit();
//
//            java.util.Date d1 = null;
//            Calendar cal = Calendar.getInstance();
//            try {
//                d1 = dfDate.parse(dfDate.format(cal.getTime()));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            editor.putString("Logfile_created_date", d1.toString()); // saving
//            // log
//            // file
//            // created
//            // date
//            editor.commit();
//        } else // if, its Overwriting existing log file
//        {
//            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
//            Log_created_date = settings.getString("Logfile_created_date", "");
//
//            if (Log_created_date.trim().length() > 0) {
//
//                Calendar cal = Calendar.getInstance();
//                Date current_date = null;
//                Date Log_date = new Date();
//
//                try {
//                    Log_date = dfDate.parse(Log_created_date); // log created
//                    // date
//                } catch (ParseException e1) {
//                    e1.printStackTrace();
//                }
//
//                try {
//                    current_date = dfDate.parse(dfDate.format(cal.getTime())); // current
//                    // date
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//                int diffInDays = 0;
//
//                diffInDays = (int) ((current_date.getTime() - Log_date
//                        .getTime()) / (1000 * 60 * 60 * 24)); // difference in
//                // days
//
//                if (diffInDays > 2) {
//                    Util.ToFileClear(Constants.getdroidPatientid() + ".txt"); // clear
//                    // 3
//                    // day
//                    // older
//                    // log
//                    // files
//                }
//            }
//        }
    }

    public static void CancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx
                .getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    public void setprefferenceWelcome() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("welcome", 0);
        editor.commit();
    }

    public void setprefferenceForThanks(String n) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Scheduleid", n);
        editor.commit();
    }

    public void setprefference(int n) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("Scheduletatus", n);
        editor.commit();
    }

    public void setprefferenceForTimezone(String n) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME_TIME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("timezone", n);
        editor.commit();
    }

    public boolean checkDevice() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        mBluetoothAdapter.enable();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // e.printStackTrace();
        }

        return true;
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.ScheduleService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked on login page:");
//
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            finish();
//
//        }
        return super.onKeyDown(keyCode, event);
    }

    public void setNickName() {

        // load from database #
        // Cursor Patientdetails = login_db.SelectNickName();
        ClassUser classUser = new ClassUser();

        Login_db login_db1 = new Login_db(this);

        classUser = login_db1.SelectNickName();

        NickName = classUser.getNickName();
        SharedPreferences settings1 = getSharedPreferences(CommonUtilities.USER_SP, 0);
        PatientIdDroid = settings1.getString("patient_id", "-1");

        Constants.setPatientid(PatientIdDroid);
        // Patientdetails.close();
        // userName.setText(NickName);
        Constants.setpatienrName(NickName);

    }

//	public void loadPatientDetails() {
//		DownloadPatientDetailsTask task = new DownloadPatientDetailsTask();
//		task.execute(new String[] { PostUrl
//				+ "/droid_website/patient_authentication.ashx" });
//	}

    public void loginmyUser() {
        Log.i(TAG,
                "  loginmyUser called--------------");
        Log.i(TAG,
                "  flag_login--" + flag_login);
        SharedPreferences flowsp = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);
        int val = flowsp.getInt("flow", 0); // #1
        int flow_type = flowsp.getInt("reminder_path", 0); // #1
        System.out.println("SP VAL============flow_type==========>> " + flow_type);
        if (flow_type == 1) {
            flag_login = 0;
            clickedhpf = 0;
            Log.i(TAG,
                    "  reminder flow set login");
        }

        if (flag_login == 0) {
            setNickName();
            Log.i(TAG, "Reach loginmyUser ");
            final AlertDialog ad = new AlertDialog.Builder(this).create();
            int patientCount;
            Cursor c = login_db.UserIsPresent(login_db.Patient_Table);
            patientCount = c.getCount();
            c.close();
            login_db.cursorLogin.close();
            login_db.close();
            if (patientCount != 0) {

                chkService();

                CancelNotification(this, 0);
                if (clickedhpf == 0) {


                    if (val == 1) {

                        if (timeDifference() >= 7) {

                            Intent intentSc = new Intent(getApplicationContext(),
                                    MyDataActivity.class);
                            startActivity(intentSc);
                            overridePendingTransition(0, 0);
                            finish();
                            return;
                        }else{

                            Intent intentSc = new Intent(getApplicationContext(),
                                    Home.class);
                            startActivity(intentSc);
                            overridePendingTransition(0, 0);
                            finish();
                            return;
                        }

                    } else {

                        Intent intentSc = new Intent(getApplicationContext(),
                                Login.class);
                        intentSc.putExtra("startingwith", 0);
                        startActivity(intentSc);
                        overridePendingTransition(0, 0);
                        finish();
                    }
                } else {
                    Log.i(TAG,
                            "user click the tab multiple times, starting th eapp again ");
                }

            } else {
                Log.i(TAG,
                        "NOT ASSIGNED TO A PATIENT");
            }
        }

    }

    private String getForaMAC(String mac) {

        String macAddress = "";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length(); i = i + 2) {
            // macAddress.substring(i, i+2)
            sb.append(mac.substring(i, i + 2));
            sb.append(":");
        }

        macAddress = sb.toString();
        macAddress = macAddress.substring(0, macAddress.length() - 1);

        Log.i(TAG, "DROID : Connect to macAddress " + macAddress);

        // String macAddress = "00:1C:05:00:40:64"; //"";
        // find MAC address of oxymeter from the DB
        // #
        if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
            return macAddress;
        } else {
            return "";
        }
    }

    private void chkService() {

        // Lock service
        Log.e(TAG, "Checking service status");

        // Schedule service

        if (!isMyServiceRunning()) {

            Intent schedule = new Intent();
            schedule.setClass(MainActivity.this, ScheduleService.class);
            startService(schedule);

        } else {
            Log.i(TAG, "schedule Serice is already running");
        }

        // advice service


    }

    /*******************************
     * Download Schedule
     *************************************************/

    public void downloadSchedule() {

        Log.i(TAG, "medication message:-DownloadWebPageTask");
        downloadSchedule task = new downloadSchedule();
        // task.execute(new String[] { PostUrl + "/medication_handler.ashx" });
        task.execute(new String[]{""});

    }

    private class downloadSchedule extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "onPostExecute downloadSchedule");
            if (result.equalsIgnoreCase("AuthenticationError")) {
//                Toast.makeText(getApplicationContext(), R.string.authenticationError, Toast.LENGTH_LONG).show();
//                logoutClick();
            }
            try {
                progressDialog.dismiss();
                if (pd != null)
                    pd.dismiss();

            } catch (Exception e) {
                Log.i(TAG, "Exception in pd.dismiss()");
            }
            if (!isMyServiceRunning()) {// ##

                Intent schedule = new Intent();
                schedule.setClass(MainActivity.this, ScheduleService.class);
                startService(schedule);

            } else {
                Log.i(TAG, "schedule Serice is already running");
            }
            SharedPreferences settings = getSharedPreferences("Login_User", 0);
            SharedPreferences.Editor editor = settings.edit();
            String AuthFlow = settings.getString("AuthFlow", "-1");
            editor.putString("AuthFlow", "0");
            editor.commit();
            if (appClass.isConnectingToInternet(getBaseContext())) {
                loginmyUser();
            } else {
                if (AuthFlow.equals("1")) {
                    Log.i(TAG, "AUTH FLOW EQUALS 1");
                    Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
                    startActivity(authenticationIntent);
                    overridePendingTransition(0, 0);
                    finish();
                } else {
                    Log.i(TAG, "AUTH FLOW NOT EQUALS 1");
                    loginmyUser();
                }
            }


            // setProgressBarIndeterminateVisibility(false);

        }

        protected String doInBackground(String... urls) {
            String response = "";

            //String n = Constants.getdroidPatientid();
            String id = Constants.getdroidPatientid();

            SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.US);
            Date d = new Date();
            String dayOfTheWeek = sdf.format(d).toUpperCase();

            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_schedule_handler.ashx",
                        new String[]{"authentication_token", "patient_id", "device_day"}, new String[]{token, patientId, dayOfTheWeek});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return "";
                }

                String str = Util
                        .readStream(response1.getEntity().getContent());

                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                str.toString();
                //Log.i("response:", "schedule_handler_response:" + str);

                if (str.length() < 10) {
                    Log.i(TAG, "Schedule data null");
                    return "";
                } else {
                    Log.i(TAG, "Schedule data get from server");
                    // dbSchedule.DeletPatint();
                    dbSchedule.DeletAllSchedule();
                }

                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("schedule_group");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    response = Util.getTagValue(AF_Nodes, i, "mob_response");
                }
                if (response.equals("AuthenticationError")) {

                    return response;
                }
                NodeList nodes = doc.getElementsByTagName("schedule");
                // NodeList timeNode = doc.getElementsByTagName("times");
                ClassSchedule classSchedule = new ClassSchedule();
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "MM/dd/yyyy hh:mm:ss aa", Locale.US);
                String currentDateandTime = dateFormat.format(new Date());

                for (int i = 0; i < nodes.getLength(); i++) {

                    classSchedule.setScheduleId((Util.getTagValue(nodes, i,
                            "schedule_id")));
                    classSchedule.setPatientId(Util.getTagValue(nodes, i, "patient_id"));
                    classSchedule.setLastUpdateDate((Util.getTagValue(nodes, i,
                            "last_updateDate")));
                    classSchedule.setDownloadDate(currentDateandTime);
                    classSchedule.setStatus(0);

                    Constants.setPatientid(classSchedule.getPatientId());

                    classSchedule.setTimeout(Integer.parseInt(Util.getTagValue(
                            nodes, i, "late_minutes")));

                    Element advice_idelement = (Element) nodes.item(i);
                    NodeList timeNode = advice_idelement
                            .getElementsByTagName("times");
                    NodeList actvivitiesNode = advice_idelement
                            .getElementsByTagName("actvivities");

                    for (int k = 0; k < timeNode.getLength(); k++) {
                        classSchedule.setScheduleTime(Util.getTagValue(
                                timeNode, k, "time"));
                        for (int j = 0; j < actvivitiesNode.getLength(); j++) {

                            classSchedule.setMeasureTypeId(Integer
                                    .parseInt(Util.getTagValue(actvivitiesNode,
                                            j, "measure_type_id")));
                            Log.e(TAG, "MEASURETYPE ID:" + Integer
                                    .parseInt(Util.getTagValue(actvivitiesNode,
                                            j, "measure_type_id")));
                            dbSchedule.insertSchedule(classSchedule);
                        }

                    }
                }

            } catch (Exception e) {

                Log.e(TAG,
                        "Error schedule downloading and saving  "
                                + e.getMessage());
                // e.printStackTrace();

                SharedPreferences settings = getSharedPreferences(
                        PREFS_NAME_SCHEDULE, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("Scheduletatus", 0);
                editor.commit();
            }

            return response;
        }

    }

    private class DownloadSensorDetailsTask extends
            AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "onPostExecute SensorDetails");
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            } else {
                DownloadMydataSensor();
            }
        }

        protected String doInBackground(String... urls) {

            String response = "";
            try {
                //sensor_db.deleteplayorder();//
                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                        + "/droid_website/mob_sensor_details.ashx", new String[]{
                        "authentication_token", "patient_id"}, new String[]{token,
                        patientId});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    sensor_db.delete();
                    sensor_db.deleteplayorder();//
                    return ""; // process
                }
                // Log.e("Optum_Sierra","serialNo"+serialNo);

                String str = Util
                        .readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG, " nnull Sensor details");
                    sensor_db.delete();
                    sensor_db.deleteplayorder();//
                    return "";
                }
                str = str.replace("&", "");
               // Log.i("response:", "sensor_response:" + str);
                DocumentBuilder dbr = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = dbr.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("patient_sensor_details");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {
                    response = Util.getTagValue(AF_Nodes, i, "mob_response");
                    String nick_name = Util.getTagValue(AF_Nodes, i, "nick_name");
                    //Log.i(TAG, "nick_name:" + nick_name);
                    CommonUtilities.Login_UserName = nick_name;
                    // save uname and id to sp
                    SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("patient_name", nick_name);
                    editor.commit();
                }
                if (response.equals("AuthenticationError")) {

                    return response;
                }
                NodeList nodes = doc.getElementsByTagName("sensor");

                sensor_db.delete(); // deleting entire table
                sensor_db.deleteplayorder();//

                db_measureType.delete();
                ClassSensor sensor = new ClassSensor();

                String s_time = TestDate.getCurrentTime();

                for (int i = 0; i < nodes.getLength(); i++) {

                    sensor.setPatientId(Util.getTagValue(
                            nodes, i, "patient_id"));
                    sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes,
                            i, "sensor_id")));
                    sensor.setSensorName(Util.getTagValue(nodes, i,
                            "sensor_name"));
                    sensor.setMeasureTypeId(Integer.parseInt(Util.getTagValue(
                            nodes, i, "measure_type_id")));
                    sensor.setMeasureTypeName(Util.getTagValue(nodes, i,
                            "measure_type_name"));
                    sensor.setMacId((Util.getTagValue(nodes, i, "id")));
                    sensor.setPin((Util.getTagValue(nodes, i, "sn")));

                    /*int temperature_unit_id = (Integer.parseInt(Util.getTagValue(nodes, i, "temperature_unit_id")));
                    Log.i(TAG, "temperature_unit_id:" +temperature_unit_id);*/

                    Element advice_idelement = (Element) nodes.item(i);
                    NodeList advice_idname = advice_idelement
                            .getElementsByTagName("weight_unit_id");

                    if (advice_idname.getLength() > 0) {
                        sensor.setWeightUnitId(Integer.parseInt(Util
                                .getTagValue(nodes, i, "weight_unit_id")));
                    } else {
                        sensor.setWeightUnitId(0);
                    }

                    NodeList temp_id = advice_idelement
                            .getElementsByTagName("temperature_unit_id");

                    if (temp_id.getLength() > 0) {
                        int temperature_unit_id = (Integer.parseInt(Util
                                .getTagValue(nodes, i, "temperature_unit_id")));

                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("temperature_unit_id", temperature_unit_id);
                        editor.commit();

                        Log.i(TAG, "temperature_unit_id:" + temperature_unit_id);
                    } else {
                        //sensor.setWeightUnitId(0);
                        Log.i(TAG, "temperature_unit_id: null");
                    }

                    Element advice_idelement1 = (Element) nodes.item(i);
                    NodeList advice_idname1 = advice_idelement1
                            .getElementsByTagName("weight_unit_name");

                    if (advice_idname1.getLength() > 0) {
                        sensor.setWeightUnitName(Util.getTagValue(nodes, i,
                                "weight_unit_name"));
                    } else {
                        sensor.setWeightUnitName("");
                    }

                    ContentValues values = new ContentValues();
                    ContentValues valuesorder = new ContentValues();

                    values.put("Patient_Id", sensor.getPatientId());

                    values.put("Sensor_id", sensor.getSensorId());
                    values.put("Sensor_name", sensor.getSensorName());
                    values.put("Measure_type_id", sensor.getMeasureTypeId());
                    values.put("Measure_type_name", sensor.getMeasureTypeName());
                    values.put("Weight_unit_id", sensor.getWeightUnitId());
                    values.put("Weight_unit_name", sensor.getWeightUnitName());
                    values.put("Mac_id", sensor.getMacId());
                    values.put("Status", 0);
                    values.put("pin", sensor.getPin());
                    //Log.d(TAG, "IN #pin" + sensor.getPin());

                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss", Locale.US);
                    Date date = new Date();
                    values.put("Download_Date", dateFormat.format(date));

                    sensor_db.insertSensor(values);// inserting downloaded date

                    int order = orderofvital(sensor.getMeasureTypeId());
                    int homeorder = homeOrderofvital(sensor.getMeasureTypeId());
                    ClassMeasureType sensor1 = new ClassMeasureType();
                    sensor1.setMeasuretypeId(sensor.getMeasureTypeId());
                    sensor1.setPatientId(sensor.getPatientId());
                    sensor1.setStatus(0);
                    sensor1.setDate(s_time);

                    db_measureType.insert(sensor1);

                    valuesorder.put("Patient_Id", sensor.getPatientId());
                    valuesorder.put("Measure_type_name",
                            sensor.getMeasureTypeName());
                    valuesorder.put("Measure_type_id",
                            sensor.getMeasureTypeId());
                    valuesorder.put("playorder", order);
                    valuesorder.put("Status", 0);
                    valuesorder.put("homeOrder", homeorder);

                    sensor_db.insertSensor_play_order(valuesorder);

                    response = "Success";
                    /*
                     * Log.i(TAG, "Sensor details...SensorId " +
					 * sensor.getSensorId() + " Meassure_type " +
					 * sensor.getMeasureTypeName() + " Sensor_name " +
					 * sensor.getSensorName() + " Patient_Id " +
					 * sensor.getPatientId() + " Status " + sensor.getStatus());
					 */

                }
                // close();
                sensor_db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }
    }


    public void loadSensorDetails() {
        SharedPreferences flowsp = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);
        int flow_type = flowsp.getInt("reminder_path", 0); // #1
        System.out.println("SP VAL============flow_type==========>> " + flow_type);
        if (flow_type == 1) {

            Log.i(TAG,
                    "isR_Flow = true");
        } else {
            Log.e(TAG, "isR_Flow = false");
            SharedPreferences settings1 = getSharedPreferences(
                    CommonUtilities.PREFS_NAME_date, 0);
            SharedPreferences.Editor editor_retake = settings1.edit();
            editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(MainActivity.this));
            editor_retake.commit();
        }
        DownloadSensorDetailsTask task = new DownloadSensorDetailsTask();
        task.execute(new String[]{serviceUrl
                + "/droid_website/mob_sensor_details.ashx"});

    }

    public void DownloadMydataSensor() {
        DownloadMyDataSensorTask taskUpdate = new DownloadMyDataSensorTask();
        taskUpdate.execute(new String[]{serviceUrl
                + "/droid_website/mob_sensor_details_mydata.ashx"});
    }

    private class DownloadMyDataSensorTask extends
            AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "onPostExecute SensorDetails");
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            } else {
                new QuestionFetch().execute();
            }
        }

        protected String doInBackground(String... urls) {

            String response = "";
            try {
                //sensor_db.deleteplayorder();//
                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                        + "/droid_website/mob_sensor_details_mydata.ashx", new String[]{
                        "authentication_token", "patient_id"}, new String[]{token,
                        patientId});

                if (response1 == null) {

                    Log.e(TAG, "Connection Failed!");

                    return ""; // process
                }
                // Log.e("Optum_Sierra","serialNo"+serialNo);

                String str = Util
                        .readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG, " nnull Sensor details");
                    //sensor_db.delete();
                    return "";
                }
                str = str.replace("&", "");
                //Log.i("response:", "sensor_response:" + str);
                DocumentBuilder dbr = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = dbr.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("patient_sensor_details");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {
                    response = Util.getTagValue(AF_Nodes, i, "mob_response");

                }
                if (response.equals("AuthenticationError")) {

                    return response;
                }
                NodeList nodes = doc.getElementsByTagName("sensor");
                myDataSensor_db.delete(); // deleting entire table
                ClassSensor sensor = new ClassSensor();

                for (int i = 0; i < nodes.getLength(); i++) {

                    sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes, i, "sensor_id")));
                    sensor.setSensorName(Util.getTagValue(nodes, i, "vital_name"));
                    ContentValues values = new ContentValues();
                    values.put("Sensor_id", sensor.getSensorId());
                    values.put("Vital_name", sensor.getSensorName());

                    myDataSensor_db.insertMydataSensor(values);// inserting downloaded date
                    response = "Success";
                }
                myDataSensor_db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }
    }

    // login page order
    private int orderofvital(int measure_type_id) {
        int a = 0;

        if (measure_type_id == 7) {
            a = 1;
        } else if (measure_type_id == 101) {
            a = 2;
        } else if (measure_type_id == 2) {
            a = 3;
        } else if (measure_type_id == 3) {
            a = 4;
        } else if (measure_type_id == 6) {
            a = 5;
        } else if (measure_type_id == 1) {
            a = 6;

        } else {
            a = 0;

        }

        return a;
    }

    // home page order
    private int homeOrderofvital(int measure_type_id) {
        int a = 0;

        if (measure_type_id == 7) {
            a = 1;
        } else if (measure_type_id == 101) {
            a = 3;
        } else if (measure_type_id == 2) {
            a = 5;
        } else if (measure_type_id == 3) {
            a = 4;
        } else if (measure_type_id == 6) {
            a = 2;
        } else if (measure_type_id == 1) {
            a = 6;

        }

        return a;
    }

    public void upload_log_toserver() {
        Upload_LogTask taskLogUpdate = new Upload_LogTask();
        taskLogUpdate.execute(new String[]{serviceUrl
                + "/droid_website/mob_patient_log.ashx"});
    }

    private class Upload_LogTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {

			/*
             * try { Thread.sleep(4000); Intent intent = new
			 * Intent(Questions.this, BlackActivity.class);
			 * Questions.this.finish(); startActivity(intent); } catch
			 * (InterruptedException e) {
			 * e.printStackTrace(); }
			 */
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            String response = "";
            // String query =
            // "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber,a.Item_Content from HFP_DMP_User_Response a where a.Status=0";

            String complete_log = Util.grtLogData(PatientIdDroid + "");

            // Log.i(TAG, "finalxml ..." + s);
            String finalxml = "";

            finalxml = "<logmain>" + complete_log + "</logmain>";

            // Log.i(TAG, "finalxml ..." + finalxml.toString());

            if (!complete_log.equals("")) {

                try {

                    HttpClient client = new DefaultHttpClient();
                    Log.i(TAG, "Updating serverurl" + serviceUrl
                            + "/droid_website/mob_patient_log.ashx");
                    HttpPost post = new HttpPost(serviceUrl
                            + "/droid_website/mob_patient_log.ashx");
                    StringEntity se = new StringEntity(finalxml, HTTP.UTF_8);
                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    post.setHeader("authentication_token", token);
                    post.setHeader("patient_id", patientId);
                    // post.setHeader("mode", "2");

                   // Log.i("Droid", "Header values in log upload"
                    //        + PatientIdDroid);

                    post.setEntity(se);
                    HttpResponse response1 = client.execute(post);
                    int a = response1.getStatusLine().getStatusCode();
                    InputStream in = response1.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        str.append(line + "\n");
                    }
                    in.close();
                    str.toString();
                    //Log.i("response:", "patientLog_response:" + str);
                    if (str.length() > 0) {

                        DocumentBuilder db = DocumentBuilderFactory
                                .newInstance().newDocumentBuilder();
                        InputSource is1 = new InputSource();
                        is1.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is1);

                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                        }

                    }

					/*
                     * db_log.cursorLog.close(); db_log.close();
					 */

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }

            }

            return response;
        }

    }

    /***************************************
     * Upload log Response to Server end
     ********************************************/

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");

		/*Intent intent = new Intent(getApplicationContext(),
                MainActivity.class);
		this.finish();
		startActivity(intent);*/

    }

    private void checkConfig() {

        {

            SharedPreferences settingswifi = getSharedPreferences(
                    CommonUtilities.WiFi_SP, 0);
            serialNo = settingswifi.getString("wifi", "-1");

            imeilNo = settingswifi.getString("imei_no", "");
            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.SERVER_URL_SP, 0);
            server = settings.getString("Server_url", "-1");
            port = settings.getString("Server_port", "-1");
            PostUrl = settings.getString("Server_post_url", "-1");
            String server4 = settings.getString("Server_url", "-1");

            CommonUtilities.SERVER_URL = PostUrl;
            Constants.setParams(server, port, PostUrl, serialNo);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        resumeFlag = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onPause" + resumeFlag);
        if (resumeFlag) {
            Intent intent = new Intent(getApplicationContext(),
                    FinalMainActivity.class);
            SharedPreferences flowsp = getSharedPreferences(
                    CommonUtilities.USER_FLOW_SP, 0);
            int flow_type = flowsp.getInt("reminder_path", 0); // #1
            System.out.println("SP VAL============flow_type==========>> " + flow_type);
            if (flow_type == 1) {
                intent.putExtra("Flow", "true");
                Log.i(TAG,
                        "isR_Flow = true");
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }

    private class QuestionFetch extends AsyncTask<String, Void, String> {

        protected String getTagValue(NodeList nodes, int i, String tag) {
            return getCharacterDataFromElement((org.w3c.dom.Element) (((org.w3c.dom.Element) nodes
                    .item(i)).getElementsByTagName(tag)).item(0));

        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "onPostExecute");
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            } else {
                UpdateAdviceMessage();
            }
        }

        protected HttpResponse connect(String url, String[] header,
                                       String[] value) {
            try {
                InputStream content = null;
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                post.setEntity(new UrlEncodedFormEntity(pairs));
                for (int i = 0; i < header.length; i++) {
                    post.setHeader(header[i], value[i]);
                }
                return client.execute(post);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
            return null;
        }

        public Boolean preference() {
            Boolean flag = null;
            try {

                String PREFS_NAME = "DroidPrefSc";
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                int scheduletatus = settings.getInt("Scheduletatus", -1);
                Log.i("Droid", " SharedPreferences : "
                        + getApplicationContext() + " is : " + scheduletatus);
                if (scheduletatus == 1)
                    flag = true;
                else
                    flag = false;
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            return flag;
        }

        protected String readStream(InputStream in) {
            try {

                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));
                StringBuilder str = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    str.append(line + "\n");
                }
                in.close();
                return str.toString();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            return "";
        }

        protected String doInBackground(String... urls) {
            String response = "";
            SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
            String token = tokenPreference.getString("Token_ID", "-1");
            String patientId = tokenPreference.getString("patient_id", "-1");
            SharedPreferences settings = getSharedPreferences("DroidPrefSc", 0);
            String language;
            if (getResources().getBoolean(R.bool.isSpanish)) {
                language = "SPANISH";
            } else {
                language = "ENGLISH";
            }

            String scheduletatus = settings.getString("Scheduleid", "AA");
            //int patientId = Constants.getdroidPatientid();
            // Log.i(TAG, " Scheduleid"+scheduletatus+"");
            try {
                HttpResponse response1 = null;
                if (preference()) {

                    Log.i(TAG, "download question with Scheduleid"
                            + scheduletatus + "");

                    Log.i(TAG,
                            "Updating serverurl"
                                    + serviceUrl
                                    + "/droid_website/mob_patient_schedule_question.ashx");
                    response1 = connect(
                            serviceUrl
                                    + "/droid_website/mob_patient_schedule_question.ashx",
                            new String[]{"authentication_token", "schedule_id", "patient_id", "language"},
                            new String[]{token, scheduletatus, patientId, language});
                } else {
                    Log.i(TAG, "Updating serverurl" + serviceUrl
                            + "/droid_website/mob_patient_questions.ashx");
                    response1 = connect(
                            serviceUrl
                                    + "/droid_website/mob_patient_questions.ashx",
                            new String[]{"authentication_token", "mode", "patient_id", "language"},
                            new String[]{token, "0", patientId, language});
                }
                int Statuscode = response1.getStatusLine().getStatusCode(); //
                // checking for 200 success code

                String str = readStream(response1.getEntity().getContent());
                System.out.println(str);
                response = str;
                if (Statuscode == 200) {
                    Log.i(TAG, "Question status 200 from the server");

                    dbcreate1.Cleartable();
                }

                if (str.trim().length() == 0) {
                    Log.i(TAG, "No Question from the server");
                    return "";
                }
                Log.i(TAG, "Question from the server downloaded");
                str = str.replaceAll("&", "and");
                // str.replaceAll(" & ", " and ");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                // str=str.replaceAll("\\s","");
                Log.i("response:", "patientQuestion_response:" + str.toString());
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);

                String patient_id = "";
                String question_id = "";
                String pattern_name = "";
                String top_node_number = "";
                String node_number = "";
                String sequence_number = "";
                String item_number = "";
                String child_node_number = "";
                String guidance = "";
                String sequence_number1 = "";
                String explanation = "";
                String kind = "";
                String sequence_number2 = "";
                String item_number1 = "";
                String item_content = "";


                NodeList AF_Nodes = doc.getElementsByTagName("whole_tree");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    if (Util.getTagValue(AF_Nodes, i, "mob_response").equalsIgnoreCase("AuthenticationError")) {

                        return "AuthenticationError";
                    }
                }

                NodeList nodes = doc.getElementsByTagName("tree");
                for (int i = 0; i < nodes.getLength(); i++) {
                    patient_id = getTagValue(nodes, i, "patient_id");
                    question_id = getTagValue(nodes, i, "question_id");
                    pattern_name = getTagValue(nodes, i, "pattern_name");
                    top_node_number = getTagValue(nodes, i, "top_node_number");
                    dbcreate1.InsertDmp(Integer.parseInt(patient_id),
                            Integer.parseInt(question_id), pattern_name,
                            Integer.parseInt(top_node_number));
                }
                NodeList branch_tree = doc.getElementsByTagName("branch_tree");

                for (int i = 0; i < branch_tree.getLength(); i++) {
                    // b = getTagValue(nodes, i, "question_id");
                    // Node item=branch_tree.item(i) ;
                    NodeList branch = doc.getElementsByTagName("branch");

                    for (int j = 0; j < branch.getLength(); j++) {

                        node_number = getTagValue(branch, j, "node_number");
                        sequence_number = getTagValue(branch, j,
                                "sequence_number");
                        item_number = getTagValue(branch, j, "item_number");
                        child_node_number = getTagValue(branch, j,
                                "child_node_number");

                        if (item_number.equals("")) {
                            item_number = "0";
                        }

                        dbcreate1.InsertDmpBranching(
                                Integer.parseInt(patient_id), node_number,
                                child_node_number,
                                Integer.parseInt(sequence_number),
                                Integer.parseInt(item_number), "",
                                Integer.parseInt(question_id));
                        // dbcreate1.InsertDmpBranching(Patient_Code,
                        // Node_Number, Child_Node_Number, Sequence_Number,
                        // Item_Number, Last_Update_Date, Question_Id)
                    }
                    Constants.setPatientid(patient_id);

                }

                NodeList interview = doc.getElementsByTagName("interview");
                for (int i = 0; i < interview.getLength(); i++) {
                    // b = getTagValue(nodes, i, "question_id");
                    // Node item=branch_tree.item(i) ;
                    NodeList interview_content = doc
                            .getElementsByTagName("interview_content");

                    for (int j = 0; j < interview_content.getLength(); j++) {

                        sequence_number1 = getTagValue(interview_content, j,
                                "sequence_number");
                        guidance = getTagValue(interview_content, j, "guidance");
                        explanation = getTagValue(interview_content, j,
                                "explanation");
                        kind = getTagValue(interview_content, j, "kind");

                        dbcreate1.InsertDmpQuestion(
                                Integer.parseInt(sequence_number1),
                                explanation, guidance, Integer.parseInt(kind),
                                "");

                    }

                }

                NodeList interview_response = doc
                        .getElementsByTagName("interview_response");
                for (int i = 0; i < interview_response.getLength(); i++) {
                    // b = getTagValue(nodes, i, "question_id");
                    // Node item=branch_tree.item(i) ;
                    NodeList answer = doc.getElementsByTagName("response");

                    for (int j = 0; j < answer.getLength(); j++) {

                        sequence_number2 = getTagValue(answer, j,
                                "sequence_number");
                        item_number1 = getTagValue(answer, j, "item_number");
                        item_content = getTagValue(answer, j, "item_content");
                        dbcreate1.InsertDmpAnswers(
                                Integer.parseInt(sequence_number2),
                                Integer.parseInt(item_number1), item_content);

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    public String getCharacterDataFromElement(org.w3c.dom.Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    public void DownloadAdviceMessage() {
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(new String[]{serviceUrl + "/droid_website/mob_rm_feedback.ashx"});
    }

    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            String response = "";
            try {
                dbcreate.Cleartable();
                Log.i(TAG, "Checking device registration MAC-" + serialNo
                        + " IMEI-" + imeilNo);
                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                        + "/droid_website/mob_rm_feedback.ashx", new String[]{
                        "authentication_token", "mode", "patient_id"}, new String[]{
                        token, "0", patientId});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return ""; // process
                }

                String str = Util
                        .readStream(response1.getEntity().getContent());

                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                str.toString();
                //Log.i("response:", "handler_response:" + str);

                if (str.length() < 10) {
                    Log.i(TAG, "Advice message data null");
                    return "";
                }

                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("advice_message");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    response = Util.getTagValue(AF_Nodes, i, "mob_response");
                }
                if (response.equals("AuthenticationError")) {

                    return response;
                }
                NodeList nodes = doc.getElementsByTagName("advice");

                ClassAdvice classAdvice = new ClassAdvice();

                for (int i = 0; i < nodes.getLength(); i++) {

                    classAdvice.setPatientId(Util.getTagValue(
                            nodes, i, "patient_id"));
                    classAdvice.setAdviceId(Integer.parseInt(Util.getTagValue(
                            nodes, i, "advice_id")));
                    classAdvice.setUserId(Integer.parseInt(Util.getTagValue(
                            nodes, i, "user_id")));
                    classAdvice.setAdviceSubject(Util.getTagValue(nodes, i,
                            "advice_subject"));
                    classAdvice.setAdviceText(Util.getTagValue(nodes, i,
                            "advice_text"));
                    classAdvice.setMessageDate(Util.getTagValue(nodes, i,
                            "create_date"));
                    //Log.i(TAG, "create_date DOWN =>" + Util.getTagValue(nodes, i,
                    //        "create_date"));

                    // saving advice to db
                    Constants.setPatientid(classAdvice.getPatientId());
                    dbcreate.InsertAdviceMessage(classAdvice);
                }
            } catch (Exception e) {

            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            } else {
                setPic();
            }
        }
    }

    /*****************************************************************
     * update advicemessage to server start
     ****************************************************/
    public void UpdateAdviceMessage() {
        Update taskUpdate = new Update();
        taskUpdate.execute(new String[]{serviceUrl
                + "/droid_website/mob_rm_feedback.ashx"});
    }

    private class Update extends AsyncTask<String, Void, String> {
        @Override
        protected void onPostExecute(String result) {
            //Log.i("onPostExecute", result);
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            } else {
                DownloadAdviceMessage();
            }
        }

        protected String doInBackground(String... urls) {
            String response = "";
            try {
                Cursor c = dbcreate.SelectAdviceMessageForupload();
                data_date = new String[c.getCount()];

                userid = new int[c.getCount()];
                patientid = new int[c.getCount()];
                status = new int[c.getCount()];
                c.moveToFirst();
                if (c.getCount() > 0) {
                    while (c.isAfterLast() == false) {
                        Log.i(TAG,
                                "Uploading advice message to server started ...");
                        SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        String token = tokenPreference.getString("Token_ID", "-1");
                        String patientId = tokenPreference.getString("patient_id", "-1");
                        HttpClient client = new DefaultHttpClient();
                        Log.i(TAG, "UploadAdviceMessage url ..." + serviceUrl
                                + "/droid_website/mob_rm_feedback.ashx");
                        HttpPost post = new HttpPost(serviceUrl
                                + "/droid_website/mob_rm_feedback.ashx");
                        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                        post.setHeader("authentication_token", token);
                        post.setEntity(new UrlEncodedFormEntity(pairs));
                        post.setHeader("mode", "1");
                        post.setHeader("user_id", c.getString(5));
                        // Log.i("Droid","Uploading user_id ..."+c.getString(2));
                        post.setHeader("patient_id", c.getString(2));
                        post.setHeader("create_date",
                                c.getString(1).replace("/", "-"));
                        Log.i(TAG, "create_date UP =>" + c.getString(1).replace("/", "-"));
                        // Log.i("Droid","Uploading create_date ..."+c.getString(1));
                        HttpResponse response1 = client.execute(post);

                        String str = Util.readStream(response1.getEntity()
                                .getContent());

                        str.toString();
                       // Log.i("response:", "handler_response:" + str);
                        if (str.length() > 0) {
                            DocumentBuilder db = DocumentBuilderFactory
                                    .newInstance().newDocumentBuilder();
                            InputSource is1 = new InputSource();
                            is1.setCharacterStream(new StringReader(str
                                    .toString()));
                            Document doc = db.parse(is1);
                            NodeList nodes = doc.getElementsByTagName("optum");
                            for (int i = 0; i < nodes.getLength(); i++) {

                                response = Util.getTagValue(nodes, i, "response");
                            }
                            if (response.equals("Success")) {
                                // /// if success delete the value from the
                                // tablet db
                                Log.i(TAG,
                                        "Deleted adviceid ..." + c.getString(0));
                                if (Integer.parseInt(c.getString(7)) == 1) {
                                    dbcreate.DeleteMessageByid(Integer
                                            .parseInt(c.getString(0)));
                                } else if (Integer.parseInt(c.getString(7)) == 3) {
                                    dbcreate.Delete(Integer.parseInt(c
                                            .getString(0)));
                                }
                            }

                        }
                        c.moveToNext();

                    }
                    c.close();
                    dbcreate.cursorAdviceMessage.close();

                }
            } catch (Exception e) {
                // e.printStackTrace();
            }
            // DownloadAdviceMessage();
            return response;
        }
    }

    private int timeDifference() {
        SharedPreferences mydata = getSharedPreferences("mydata", 0);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        Date lastClickDate = null;
        Date currentDate = null;
        try {
            lastClickDate = simpleDateFormat.parse(mydata.getString("click_time", appClass.get_myData_date()));
            currentDate = simpleDateFormat.parse(appClass.get_myData_date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long milliseconds = currentDate.getTime() - lastClickDate.getTime();
        int days = (int) (milliseconds / (1000 * 60 * 60 * 24));
        return days;
    }

    private void setPic() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 141);
                } else{
                    picRedirect();
                }
            } else{
                picRedirect();
            }
        } catch (Exception e) {
            Log.i(TAG, "Exception in set profile picture");
        }

    }

    public void picRedirect(){
        try{
            Log.i(TAG, "started downloading Support image ");

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.USER_SP, 0);

            String contractcode = settings.getString("contract_code_name", "0");

            String PatientIdDroid = Constants.getdroidPatientid();

            String mCurrentPhotoPath = Environment
                    .getExternalStorageDirectory().toString()
                    + "/versions/HFP/Images/";

            File directoryFile = new File(mCurrentPhotoPath);

            File myFile = new File(directoryFile.getAbsolutePath() + "/"
                    + contractcode + ".png");
            Log.i(TAG, "contractcode-" + contractcode);

            if (myFile.exists()) {

                // myFile.delete();
                downloadSchedule();

                Log.i(TAG, "contractcode already exists");

            } else {
                DownloadProfilePicServer();
                Log.i(TAG, "contractcode not  exists");
            }
            /*
             * ActivityProfilepicture apf=new ActivityProfilepicture();
			 * apf.DownloadProfilePicServer
			 * (PatientIdDroid,serverurl,ContractCode); Thread.sleep(1000);
			 */
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void DownloadProfilePicServer() {

		/*
         * patientId=id; serverurl=PostUrl; contratCode=ContratCode;
		 */
        Log.i(TAG, "Download image Server started ");
        DownloadImageTask task = new DownloadImageTask();
        task.execute(new String[]{PostUrl + "droid_website/GetSounFile.ashx"});

    }

    private class DownloadImageTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {
            downloadSchedule();
        }

        @SuppressWarnings("unused")
        protected HttpResponse connect(String url, String[] header,
                                       String[] value) {
            try {

                HttpClient client = new DefaultHttpClient();
                Log.i(TAG, url);
                HttpPost post = new HttpPost(url);
                List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                post.setEntity(new UrlEncodedFormEntity(pairs));
                for (int i = 0; i < header.length; i++) {
                    post.setHeader(header[i], value[i]);
                }
                return client.execute(post);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
            return null;
        }

        @Override
        protected String doInBackground(String... arg0) {
            // TODO Auto-generated method stub
            String response = "";

            try {

                URL url;
                File file;

                if (ContractCode.equals("")) {
                    url = new URL(serviceUrl + "/droid_website/user_images/"
                            + PatientIdDroid + ".jpg");
                    file = new File(PatientIdDroid + ".png");
                   // Log.d(TAG, "" + url);
                } else {
                    url = new URL(serviceUrl + "/droid_website/contract_images/"
                            + ContractCode + ".jpg");
                    file = new File(ContractCode + ".png");
                   // Log.d(TAG, "" + url);
                }

                if (!PatientIdDroid.equals("0")) {
                    /* Open a connection to that URL. */
                   // Log.d(TAG, "" + url);
                    URLConnection con = url.openConnection();

                    InputStream is = con.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is,
                            1024 * 50);
                    String SDCardRoot = Environment
                            .getExternalStorageDirectory().toString()
                            + "/versions/HFP/Images/";

                    File directory = new File(SDCardRoot);
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                    FileOutputStream fos = new FileOutputStream(SDCardRoot
                            + file);
                    byte[] buffer = new byte[1024 * 50];

                    int current = 0;
                    while ((current = bis.read(buffer)) != -1) {
                        fos.write(buffer, 0, current);
                    }

                    fos.flush();
                    fos.close();
                    bis.close();
                }
                Log.d(TAG, "download contract code completed");

            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "download contract code exception");
            }

            return response;
        }

    }
}