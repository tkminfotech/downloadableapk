package com.optum.telehealth;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Html;
import com.optum.telehealth.util.Log;

import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.bean.ClassUser;
import com.optum.telehealth.dal.AdviceMessage_db;
import com.optum.telehealth.dal.Flow_Status_db;
import com.optum.telehealth.dal.Login_db;
import com.optum.telehealth.dal.MyDataTracker_db;
import com.optum.telehealth.dal.Questions_db;
import com.optum.telehealth.dal.Reminder_db;
import com.optum.telehealth.service.AlarmService;
import com.optum.telehealth.util.Alam_util;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Regular_Alam_util;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class AuthenticationActivity extends Activity implements View.OnClickListener {

    private GlobalClass appClass;
    private Button btn_login;
    private TextView txt_error_message, txt_forgotuname, txt_forgotpword;
    private EditText edt_username;
    private EditText edt_password;
    private String userName;
    private String password;
    private String serviceUrl;
    private String contractCode;
    private String chartStatus = "0";
    String log_status = "0";
    private String time_zone_id;
    private Login_db login_db = new Login_db(this);
    String ApkType;
    public String authentication_Token;
    public static String imeilNo;
    public boolean reminder_flow;
    Flow_Status_db Flow_db;
    String Shared_username;
    private String serverAppVersion, Shared_Status;
    private int analyticLogCount = 5;
    private Reminder_db reminder_db = new Reminder_db(this);
    Questions_db dbcreate1 = new Questions_db(this);
    String TAG = "AuthenticationActivity";
    AdviceMessage_db dbcreate = new AdviceMessage_db(this);
    private Button newMember;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 3452;
    private CheckBox rememberUser;
    ProgressDialog progressDialog;
    private int isHomeEnable = 0;                   //0-no home button, 1-show home button
    private int isSupportEnable = 0;            //0-no support button, 1-show support button
    String device_status = "", pin_id = "", res_message = "";
    private String isTempLogin = "0";        // 0- normal password, 1 - temperory password
    private String isTempTimeout = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appClass = (GlobalClass) getApplicationContext();
        SharedPreferences user = getSharedPreferences("Login_User", 0);
        String username = user.getString("username", "-1");
        if (appClass.isAuthenticationLanguage_english && username.equals("-1"))
            appClass.setEnglishLanguage(this);
        else
            appClass.changeLanguage(this);
        setContentView(R.layout.activity_authentication);

        Log.i("AuthenticationActivity", "onCreate");
        appClass.myDataTracker_db = new MyDataTracker_db(getApplicationContext());
        serverAppVersion = getResources().getString(R.string.version);
        appClass.isAuthenticationLanguage_english = false;
        readPhoneState();

        Typeface type1 = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        txt_forgotuname = (TextView) findViewById(R.id.txt_forgotuname);
        txt_forgotpword = (TextView) findViewById(R.id.txt_forgotpword);

        txt_forgotuname.setText(R.string.forgotuname);
        txt_forgotpword.setText(R.string.forgotpwordWithUnderline);

        rememberUser = (CheckBox) findViewById(R.id.rememberUser);

        edt_username = (EditText) findViewById(R.id.edt_userName);
        edt_password = (EditText) findViewById(R.id.edt_password);

        edt_username.setTypeface(type1, Typeface.NORMAL);
        edt_password.setTypeface(type1, Typeface.NORMAL);

        txt_error_message = (TextView) findViewById(R.id.txt_errorMessage);
        btn_login = (Button) findViewById(R.id.btn_login);

        txt_forgotuname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AuthenticationActivity.this, RequestUsernameActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });



        SharedPreferences Login_User = getSharedPreferences("Login_User", 0);
        Shared_username = Login_User.getString("CheckUsername", "-1");
        Shared_Status = Login_User.getString("CheckStatus", "-1");

        if (!Shared_username.equals("-1")) {
            edt_username.setText(Shared_username);
        }
        if (Shared_Status.equals("true") || Shared_Status.equals("-1")) {
            rememberUser.setChecked(true);
        } else {
            rememberUser.setChecked(false);
        }
        try {
            Flow_db = new Flow_Status_db(getApplicationContext());
        } catch (Exception e) {
            Log.i("AuthenticationActivity", "Flow_db Exception:" + e);
        }
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String R_ID = bundle.getString("R_ID");//this is for String
                Log.i("AuthenticationActivity", "R_ID value = " + R_ID);
                if (R_ID.equals("0") || R_ID.equals("")) {
                    reminder_flow = false;
                    Log.i("AuthenticationActivity", "Normal Flow");
                } else {
                    SharedPreferences sharedpreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("R_STATUS", 0);
                    editor.commit();
                    reminder_flow = true;
                    Log.i("AuthenticationActivity", "Reminder Flow");
                }
            } else {
                reminder_flow = false;
                Log.i("AuthenticationActivity", "Normal Flow");
            }
        } catch (Exception e) {
            Log.i("AuthenticationActivity", "R_ID Exception:" + e);
        }

        txt_forgotpword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AuthenticationActivity.this, RequestPasswordActivity.class);
                i.putExtra("reminder_flow",reminder_flow);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });

        btn_login.setOnClickListener(this);
        try {

            Alam_util.cancelAlarm(this);
            Regular_Alam_util.cancelAlarm(this);
        } catch (Exception e) {
            Log.e("AuthenticationActivity", "exception while closing alam in final main act... ");
        }
//        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.putString("Server_url", "https://othdevold.portal724.us");
//        editor.putString("Server_port", "8725");
//        editor.putString("Server_post_url", "https://othdevold.portal724.us:8725");
//        editor.commit();

        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");

        newMember = (Button) findViewById(R.id.newMember);
        newMember.setText(R.string.newmember);
        newMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AuthenticationActivity.this, RegistrationActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                //startRequest();
                validateAuthentication();
                break;
            default:

                break;
        }
    }


    private void validateAuthentication() {
        userName = edt_username.getText().toString().trim();
        password = edt_password.getText().toString().trim();
        if (userName.equals("") || password.equals("")) {
            SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
            String WrongCredentials = ERROR_MSG.getString("WrongCredentials", "-1");
            setNotificationMessage(WrongCredentials);
        } else {
            if (appClass.isConnectingToInternet(getBaseContext())) {
                new User_Authentication().execute();

            } else {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String NoConnectionError = ERROR_MSG.getString("NoConnectionError", "-1");
                setNotificationMessage(NoConnectionError);
            }
        }
    }

    private class User_Authentication extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute Authentication");
            try {
                progressDialog = new ProgressDialog(AuthenticationActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                int R_STATUS = 1;
                // Log.i("onPostExecute", result);

                String username = edt_username.getText().toString().trim();
                boolean checkStatus = rememberUser.isChecked();
                //edt_username.setText("");
                edt_password.setText("");
                if (result.equals("success")) {
//
//                SharedPreferences settings2 = getSharedPreferences(
//                        CommonUtilities.CLUSTER_SP, 0);
//                ApkType = settings2.getString("clustorNo", "-1"); // #1

                    SharedPreferences settings = getSharedPreferences("Login_User", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("username", username);
                    if (checkStatus) {
                        editor.putString("CheckUsername", username);
                        editor.putString("CheckStatus", "true");
                    } else {
                        editor.putString("CheckUsername", "-1");
                        editor.putString("CheckStatus", "false");
                    }
                    editor.putString("AuthFlow", "1");
                    editor.commit();

                    SharedPreferences skipPreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
                    SharedPreferences.Editor S_editor = skipPreferences.edit();
                    S_editor.putInt("R_LOGOUT", 1);
                    S_editor.commit();

                    if (!getResources().getString(R.string.version).equals(serverAppVersion)) {

                        //versionUpdateNotification();
                    }
                    SharedPreferences sharedpreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
                    SharedPreferences.Editor reditor = sharedpreferences.edit();
                    int R_ID = sharedpreferences.getInt("R_ID", 0);
                    R_STATUS = sharedpreferences.getInt("R_STATUS", 1);

                    if (!Shared_username.equals(username)) {
                        try {
                            reminder_db.deleteAll();
                            dbcreate.clearAllMsg();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //dbcreate1.Cleartable();
                        if (R_STATUS == -1) {
                            try {
                                reditor.putInt("R_STATUS", 0);
                                reditor.commit();
                                Log.e(TAG, "CANCEL NOTIFICATION");
                                NotificationManager notifManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                notifManager.cancel(R_ID);
                            } catch (Exception e) {
                                Log.e(TAG, "Exception Notification Cancel");
                            }
                        }
                    }
                    try {
                        final Intent reminder = new Intent();
                        reminder.setClass(AuthenticationActivity.this, AlarmService.class);
                        if (!isMyReminderServiceRunning()) {
                            Log.e(TAG, " starting  AlarmService ");
                            startService(reminder);
                        } else {
                            Log.e(TAG, " Already   AlarmService started ");
                            if (!reminder_flow || R_STATUS == -1) {
                                stopService(reminder);
                                Thread.sleep(400);
                                startService(reminder);
                            }
                        }
                        new mob_fetch_error_msg().execute();


                    } catch (Exception e) {
                        Log.e(TAG, "exception while starting or closing alam... ");
                    }
                } else if (isTempLogin.equals("1")) {
                    progressDialog.dismiss();
                    showPopup();

                } else {
                    progressDialog.dismiss();
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String WrongCredentials = ERROR_MSG.getString("WrongCredentials", "-1");
                    setNotificationMessage(WrongCredentials);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        protected String doInBackground(String... urls) {
            Log.i("doInBackground", "doInBackground");
            String response = "";
            String language = "0";
            String session_time = "";
            try {
                SharedPreferences settings_n = getSharedPreferences(
                        CommonUtilities.USER_SP, 0);
                String lngID = settings_n.getString("language_id", "0");
                readPhoneState();
                //Log.i("loc imeilNo", imeilNo);
                //Log.i("loc userName", userName);
                //Log.i("loc password", password);
                //Log.i("loc version", getResources().getString(R.string.version));
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_patient_authentication.ashx", new String[]{
                                "user_name", "user_password", "version_number", "imei_no","language_id"},

                        new String[]{userName, password, getResources().getString(R.string.version), imeilNo,lngID});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    return "Connection Failed!"; // process
                }

                login_db.delete_patient();
                String str = Util.readStream(response1.getEntity().getContent());

                if (str.trim().length() == 0) {
                    Log.i("No aPK details",
                            "No aPK details for the current serial number from server");
                    return "No aPK details";
                } else {

                    login_db.delete_patient();
                }

                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i("response:", "authentication_response:" + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                ClassUser classUser = new ClassUser();
                NodeList nodes = doc.getElementsByTagName("patient");
                for (int i = 0; i < nodes.getLength(); i++) {
                    if (Util.getTagValue(nodes, i, "mob_response").equals("Success")) {
                        classUser.setPatientId(Util.getTagValue(nodes, i, "patient_id"));
                        classUser.setFullName("-");
                        classUser.setNickName(Util.getTagValue(nodes, i, "nick_name"));
                        classUser.setDbo("-");
                        classUser.setSex("-");
                        // classUser.setType("AM"); // testing
                        classUser.setType(Util.getTagValue(nodes, i, "ampm"));
                        classUser.setContractcode(Util.getTagValue(nodes, i, "contract_code_name"));
                        session_time = Util.getTagValue(nodes, i, "session_timeout");

                        device_status = Util.getTagValue(nodes, i, "device_status");
                        pin_id = Util.getTagValue(nodes, i, "pin_id");
                        res_message = Util.getTagValue(nodes, i, "message");
                        chartStatus = Util.getTagValue(nodes, i, "chart_status");
                        if (session_time.equals("")) {
                            session_time = "15";
                            classUser.setSession_time(session_time);
                        } else {
                            classUser.setSession_time(session_time);
                        }
                        appClass.startTime = Long.parseLong(session_time) * 60 * 1000;
                        contractCode = Util.getTagValue(nodes, i, "contract_code_name");
                        ApkType = Util.getTagValue(nodes, i, "cluster_id");
                        isHomeEnable = Integer.parseInt(Util.getTagValue(nodes, i, "is_home_enable"));
                        isSupportEnable = Integer.parseInt(Util.getTagValue(nodes, i, "is_support_enable"));
                        time_zone_id = Util.getTagValue(nodes, i, "time_zone_id");
                        authentication_Token = Util.getTagValue(nodes, i, "token_uuid");
                        log_status = Util.getTagValue(nodes, i, "log_status");
                        serverAppVersion = Util.getTagValue(nodes, i, "Android_version");
                        analyticLogCount = Integer.parseInt(Util.getTagValue(nodes, i, "analytic_log_count"));
                        Constants.setPatientid(classUser.getPatientId());
                        language = Util.getTagValue(nodes, i, "language_id");
                        isTempLogin = Util.getTagValue(nodes, i, "is_temp_password");
//                    if (getResources().getBoolean(R.bool.isSpanish)) {
//                        language = "11";
//                    } else {
//                        language = "0";
//                    }
                        String ServerTime = Util.getTagValue(nodes, i, "server_time");
                        setprefferenceForTimezone(ServerTime);

                        int year = Integer.parseInt(ServerTime.substring(6, 10));
                        int month = Integer.parseInt(ServerTime.substring(0, 2));
                        int day = Integer.parseInt(ServerTime.substring(3, 5));
                        int hour = Integer.parseInt(ServerTime.substring(11, 13));
                        int minute = Integer.parseInt(ServerTime.substring(14, 16));
                        int seconds = Integer.parseInt(ServerTime.substring(17, 19));

                        //TestDate.setCalender(year, month, day, hour, minute, seconds);

                        CommonUtilities.Login_UserName = classUser.getNickName();
                        login_db.insert_Patient_Data(classUser);


                        // save uname and id to sp
                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("patient_name", classUser.getNickName());
                        editor.putString("patient_id", classUser.getPatientId() + "");
                        editor.putString("contract_code_name", contractCode);
                        editor.putString("language_id", language);
                        editor.putString("Token_ID", authentication_Token);
                        editor.putString("Session_Time", session_time);
                        editor.putString("chart_status", chartStatus);
                        editor.commit();
                        appClass.token = authentication_Token;
                        appClass.patientId = classUser.getPatientId() + "";

                        SharedPreferences clusterSsettings = getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
                        SharedPreferences.Editor clusterEditor = clusterSsettings.edit();
                        clusterEditor.putString("clustorNo", ApkType);
                        clusterEditor.putInt("homeButton", isHomeEnable);
                        clusterEditor.putInt("supportButton", isSupportEnable);
                        clusterEditor.putInt("AnalyticCount", analyticLogCount);
                        clusterEditor.putInt("time_zone_id", Integer.parseInt(time_zone_id));
                        clusterEditor.commit();
                        return "success";
                    } else if (Util.getTagValue(nodes, i, "mob_response").equals("Failed")) {
                        isTempLogin = Util.getTagValue(nodes, i, "is_temp_password");
                        String failedMessage = Util.getTagValue(nodes, i, "message");
                        return failedMessage;
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }
    }

    public static final String PREFS_NAME_TIME = "TimePrefSc";

    public void setprefferenceForTimezone(String n) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME_TIME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("timezone", n);
        editor.commit();
    }


    public void setNotificationMessage(String msg) {
        txt_error_message.setVisibility(View.VISIBLE);
        txt_error_message.setText(msg);
        btn_login.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_error_message.animate().alpha(0.0f);
                        txt_error_message.setVisibility(View.GONE);
                        btn_login.setEnabled(true);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i("AuthenticationActivity", "DROID KEYCODE_BACK clicked on HOME page:");
//            this.finishAffinity();
//        }
        return super.onKeyDown(keyCode, event);
    }

    /*private void versionUpdateNotification() {

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.icon_name))
                .setContentText(getResources().getString(R.string.updateNotificationMsg))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(getResources().getString(R.string.updateNotificationMsg)));
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(1, notificationBuilder.build());
    }*/

    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }

    private boolean isMyReminderServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.AlarmService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void readPhoneState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getApplicationContext().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_COARSE_LOCATION);
            } else {
                readerImei();
            }
        } else {
            readerImei();
        }
    }

    public void readerImei() {
        try {
            String android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
//            TelephonyManager tManager = (TelephonyManager) this
//                    .getSystemService(Context.TELEPHONY_SERVICE);
            imeilNo = android_id;
            Log.i(TAG, "imeilNo= " + imeilNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PERMISSION_REQUEST_COARSE_LOCATION) {
//            if (resultCode == RESULT_CANCELED) {
//                readPhoneState();
//            } else {
//                readerImei();
//            }
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    readerImei();
                } else {
                    readPhoneState();
                }
                return;
            }
        }
    }

    public class IntentServ extends IntentService {

        public IntentServ() {
            super("IntentServ");
        }

        @Override
        protected void onHandleIntent(Intent intent) {

        }
    }

    private class mob_fetch_error_msg extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_fetch_error_msg");

        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_fetch_error_msg #doInBackground");
            String response = "";
            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_get_error_message.ashx", new String[]{
                                "imei_no"},
                        new String[]{"00000"});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                //str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i(TAG, "mob_fetch_error_msg: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("error_message_details");
                for (int i = 0; i < nodes.getLength(); i++) {
                    Element error_element = (Element) nodes.item(i);
                    NodeList error_message = error_element
                            .getElementsByTagName("error_message");
                    for (int j = 0; j < error_message.getLength(); j++) {
                        String error_code = Util.getTagValue(error_message, j, "error_code");
                        String error_description = Util.getTagValue(error_message, j, "error_description");
                        String error_description_spanish = Util.getTagValue(error_message, j, "error_description_spanish");
                        //Log.i(TAG, "#onPostExecute error_code: " + error_code);
                        //Log.i(TAG, "#onPostExecute error_description: " + error_description);
                        //Log.i(TAG, "#onPostExecute error_description_spanish: " + error_description_spanish);
                        SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        SharedPreferences settings_n = getSharedPreferences(
                                CommonUtilities.USER_SP, 0);
                        int ln = Integer.parseInt(settings_n.getString("language_id", "0"));

                        if (ln == 11) {
                            //Spanish Error Msg
                            editor.putString(error_code, error_description_spanish);
                            editor.commit();
                        } else {
                            //English Error Msg
                            editor.putString(error_code, error_description);
                            editor.commit();
                        }
                    }
                    response = "success";
                    return response;
                }
            } catch (Exception e) {
                e.printStackTrace();
                response = "error";
                return response;
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("error")) {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String connection_error = ERROR_MSG.getString("connection_error", "-1");
                    setNotificationMessage(connection_error);
                } else {
                    String username = edt_username.getText().toString().trim();
                    if (isTempLogin.equals("1")) {

                        Intent i = new Intent(AuthenticationActivity.this, NewPasswordActivity.class);

                        i.putExtra("DeviceStatus", device_status);
                        i.putExtra("pin_id", pin_id);
                        i.putExtra("message", res_message);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    } else {

                        if (reminder_flow && Shared_username.equals(username)) {
                            Log.i("onPostExecute", "reminder_flow && Shared_username.equals(username)");
                            Intent intent = new Intent(getApplicationContext(),
                                    FinalMainActivity.class);
                            intent.putExtra("Flow", "true");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                            finish();
                        } else {
                            Log.i("onPostExecute", "NOT reminder_flow && Shared_username.equals(username)");
                            if (device_status.equals("0")) {
                                Intent i = new Intent(AuthenticationActivity.this, PinValidationActivity.class);
                                i.putExtra("pin_id", pin_id);
                                i.putExtra("message", res_message);
                                i.putExtra("class_status", "true");
                                startActivity(i);
                                overridePendingTransition(0, 0);
                                finish();
                            } else if (device_status.equals("2")) {
                                Intent i = new Intent(AuthenticationActivity.this, ConfigErrorActivity.class);
                                i.putExtra("message_desc", res_message);
                                i.putExtra("message", "-1");
                                i.putExtra("class_name", "com.optum.telehealth.RegistrationActivity");
                                startActivity(i);
                                overridePendingTransition(0, 0);
                                finish();
                            } else {
                                Intent intent = new Intent(getApplicationContext(),
                                        FinalMainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                                finish();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    private void showPopup() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reset_password_popup);
        dialog.setTitle("Custom Alert Dialog");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgview_resetPopupClose);
        TextView txtPopupHeader = (TextView) dialog.findViewById(R.id.txt_resetPopupHeader);
        TextView txtError = (TextView) dialog.findViewById(R.id.txt_resetPasswordPopupError);
        TextView txtErrorNumber = (TextView) dialog.findViewById(R.id.txt_resetPasswordPopupErrorNumber);
        Button btnClose = (Button) dialog.findViewById(R.id.btn_resetPopupClose);
        txtErrorNumber.setVisibility(View.GONE);
        btnClose.setVisibility(View.GONE);
        txtPopupHeader.setText(R.string.invalidPassword);
        txtError.setText(R.string.invalidPasswordDescAuthentication);

        dialog.show();
        imgClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });

    }

}