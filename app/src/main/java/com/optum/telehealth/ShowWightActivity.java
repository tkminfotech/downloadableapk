package com.optum.telehealth;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.optum.telehealth.dal.MeasureType_db;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.dal.Weight_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.FontsOverride;
import com.optum.telehealth.util.NumberToString;
import com.optum.telehealth.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ShowWightActivity extends Titlewindow {

    Sensor_db dbSensor = new Sensor_db(this);
    Weight_db dbCreateWeight = new Weight_db(this);
    private Button NextButton = null, RetryButton = null;
    private String TAG = "ShowWeightActivity";
    public int vala = 0;
    MeasureType_db dbsensor1 = new MeasureType_db(this);
    private GlobalClass appClass;
    private int redirectFlag = 0;
    TextView msg;
    private TextView txt_errorMsg;
    boolean resumeFlag = false;

    @Override
    public void onStop() {
        redirectFlag = 1;
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, " KEYCODE_BACK clicked:");
//
//            Intent intent = new Intent(this, FinalMainActivity.class);
//
//            this.finish();
//            startActivity(intent);
//
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_wight);
        appClass = (GlobalClass) getApplicationContext();
        appClass.isSupportEnable = true;
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        msg = (TextView) findViewById(R.id.t);

        try {
            vala = extras.getInt("type");
            String unit = dbSensor.measureunitName();
            int u = 0;
            if (unit.contains("Kilogram")) {
                u = 1;
            }
            if (extras.getString("text").trim().length() < 10) {
                String result_re;
                if (u == 1) {
                    if (vala == 1)//kg
                    {
                        result_re = ""
                                + (Double) Util.round((((Double) Double
                                .parseDouble(extras.getString("text")
                                        .trim()))));
                    } else {
                        result_re = ""
                                + (Double) Util.round((((Double) Double
                                .parseDouble(extras.getString("text")
                                        .trim()))) / 2.204);
                    }
                } else {
                    result_re = ""
                            + (Double) Util.round((((Double) Double
                            .parseDouble(extras.getString("text")
                                    .trim()))));
                }
                displyText(result_re);

            } else {
                displyText(extras.getString("text"));
            }
            final ViewGroup mContainer = (ViewGroup) findViewById(
                    android.R.id.content).getRootView();
            FontsOverride.overrideFonts(getApplicationContext(), mContainer);
        } catch (Exception e) {
            Log.i(TAG, " Exception show wt on create ");
        }
        addListenerOnButton();
        Log.i(TAG, "--oncreate completed show wt---- ");
        redirector();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void addListenerOnButton() {

        NextButton = (Button) findViewById(R.id.btnaccept);

        NextButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                appClass.appTracking("Weight reading display page","Click on Accept button");
                redirect();
            }
        });
        RetryButton = (Button) findViewById(R.id.btnwtretake);

        Log.i(TAG, "RetryButton clicked wt page- ");
        SharedPreferences settingsNew = getSharedPreferences(
                CommonUtilities.RETAKE_SP, 0);

        int retake = settingsNew.getInt("retake_status", 0);

        /*if (retake == 1) {
            RetryButton.setVisibility(View.INVISIBLE);
        }*/

        RetryButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                appClass.appTracking("Weight reading display page","Click on Retake button");
                Util.Stopsoundplay();
                delete_last_meassurement();

                // set retak sp
                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.RETAKE_SP, 0);
                SharedPreferences.Editor editor_retake = settings.edit();
                editor_retake.putInt("retake_status", 1);
                editor_retake.commit();
                redirectFlag = 1;
                RedirectWeight();
            }

        });
    }

    private void delete_last_meassurement() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        dbCreateWeight.delete_weight_data(extras.getInt("measureId"));

    }

    private void RedirectWeight() {

        Cursor cursorsensor = dbSensor.SelectWeightSensorName();
        String Sensor_name = "", Mac = "";
        Log.i("Droid", "First SensorName : " + Sensor_name);
        cursorsensor.moveToFirst();
        if (cursorsensor.getCount() > 0) {
            Sensor_name = cursorsensor.getString(3);
            Mac = getForaMAC(cursorsensor.getString(9));
        }
        cursorsensor.close();
        dbSensor.cursorsensor.close();
        dbSensor.close();

        Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
        if (Sensor_name.contains("A and D")) {
            Intent intentwt = new Intent(getApplicationContext(),
                    AandDReader.class);
            // Intent intentwt = new
            // Intent(getApplicationContext(),TemperatureEntry.class);
            // intentwt.putExtra("macaddress", "00:1C:05:00:40:64");
            intentwt.putExtra("deviceType", 0);
            startActivity(intentwt);
            this.finish();
            overridePendingTransition(0, 0);
        } else if (Sensor_name.contains("FORA")) {
            Intent intentfr = new Intent(getApplicationContext(),
                    ForaMainActivity.class);
            // intentfr.putExtra("macaddress", "00:12:A1:B0:86:FB");
            intentfr.putExtra("deviceType", 0);
            // intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
            intentfr.putExtra("macaddress", Mac);
            startActivity(intentfr);
            this.finish();
            overridePendingTransition(0, 0);
        } else if (Sensor_name.contains("P724")) {
            Intent intentfr = new Intent(getApplicationContext(),
                    P724Activity.class);
            // intentfr.putExtra("macaddress", "00:12:A1:B0:86:FB");
            intentfr.putExtra("deviceType", 0);
            // intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
            intentfr.putExtra("macaddress", Mac);
            startActivity(intentfr);
            this.finish();
            overridePendingTransition(0, 0);
        } else {
            final Context context = this;
            // Intent intent = new Intent(context,ThanksActivity.class); #
            // change for skip vitals
            Intent intent = new Intent(context, WeightEntry.class);
            startActivity(intent);
            this.finish();
            overridePendingTransition(0, 0);
        }

    }

    private String getForaMAC(String mac) {

        String macAddress = "";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length(); i = i + 2) {
            // macAddress.substring(i, i+2)
            sb.append(mac.substring(i, i + 2));
            sb.append(":");
        }

        macAddress = sb.toString();
        macAddress = macAddress.substring(0, macAddress.length() - 1);

        Log.i(TAG, " : Connect to macAddress " + macAddress);
        // #
        if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
            return macAddress;
        } else {
            return "";
        }
    }

    private void playSound(int unit_type) {
        Log.i(TAG, "play sound started ");
        try {
            NumberToString ns = new NumberToString();
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                appClass.setBundle(extras);
            } else {
                extras = appClass.getBundle();
            }
            String no_to_string = "";
            Long final_value1;
            String Result;

            if (unit_type == 1)

            {
                Result = ""
                        + Util.round((((Double) Double.parseDouble(extras
                        .getString("text").trim()))) / 2.204);
            } else {
                Result = extras.getString("text").trim();
            }

            if (vala == 1)// data from manual entry
            {
                Result = extras.getString("text").trim();
            }

            if (Result.contains(".")) {
                Double final_value;

                final_value = Double.parseDouble(Result);

                no_to_string = ns.getNumberToString(final_value);
            } else {

                final_value1 = Long.parseLong(Result);
                no_to_string = ns.getNumberToStringLong(final_value1);

            }

            ArrayList<String> final_list = new ArrayList<String>();
            final_list.add("Messages/WeightIs.wav");
            List<String> sellItems = Arrays.asList(no_to_string.split(" "));

            for (String item : sellItems) {
                if (item.toString().length() > 0) {
                    final_list.add("Numbers/" + item.toString() + ".wav");
                }
            }

            if (final_list.get(final_list.size() - 1).toString()
                    .equals("Numbers/zero.wav")) // remove if last item is zero
            {
                final_list.remove(final_list.size() - 1);
            }
            if (unit_type == 0) {
                final_list.add("Messages/pounds.wav");
            } else {
                final_list.add("Messages/kilograms.wav");
            }

            Util.playSound4FileList(final_list, getApplicationContext());
        } catch (Exception e) {
            Log.e(TAG, "Play sound Exception in show wt page");
        }
    }

    private void redirect() {
        Bundle extras = getIntent().getExtras();
        dbsensor1.updateStatus(7);// update the vital as taken
        dbCreateWeight.UpdateMeasureData_as_valid(extras.getInt("measureId"));
        Log.i(TAG, "next  clicked wt page- ");
        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.RETAKE_SP, 0);
        SharedPreferences.Editor editor_retake = settings1.edit();
        editor_retake.putInt("retake_status", 0);
        editor_retake.commit();
        Util.Stopsoundplay();
        // if (appClass.isConnectingToInternet(getApplicationContext())) {

        Intent intent = new Intent(ShowWightActivity.this,
                UploadMeasurement.class);
        intent.putExtra("reType", 1);

        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
//        } else {
//
//            errorShow(this.getString(R.string.netConnectionErrorMsg));
//        }

    }

    public void displyText(String disText) {
        try {
            String unit = dbSensor.measureunitName();
            int language = Constants.getLanguage();
            if (unit.contains("Kilogram")) {
                if (language == 11) {
                    msg.setText(Html.fromHtml("Su peso es de <b>" + disText + "</b> kilogramos."));
                } else {
                    msg.setText(Html.fromHtml("Your weight is <b>" + disText + "</b> kilograms."));
                }
            } else {
                if (language == 11) {
                    msg.setText(Html.fromHtml("Su peso es de <b>" + disText + "</b> libras."));
                } else {
                    msg.setText(Html.fromHtml("Your weight is <b>" + disText + "</b> pounds."));
                }
            }
        } catch (Exception e) {

        }
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        NextButton.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        NextButton.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    public void redirector() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivityManager am = (ActivityManager) getApplicationContext()
                        .getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                if (cn.getClassName().equals(
                        "com.optum.telehealth.ShowWightActivity")) {
                    if (redirectFlag == 0) {
                        redirect();
                    }
                }
            }
        }, 15000);
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        resumeFlag = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onPause" + resumeFlag);
        if (resumeFlag) {
            redirectFlag = 0;
            redirector();
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
