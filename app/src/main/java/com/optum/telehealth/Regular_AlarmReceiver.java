package com.optum.telehealth;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.util.Regular_Alam_util;

public class Regular_AlarmReceiver extends BroadcastReceiver {

	private GlobalClass appstate;
	String TAG = "Regular_AlarmReceiver";

	@Override
	public void onReceive(Context context, Intent arg1) {

		appstate = ((GlobalClass) context.getApplicationContext());

		appstate.setStart_Alarm_Count(appstate.getStart_Alarm_Count() + 1);
		Log.e(TAG,
				"Count===========================>"
						+ appstate.getStart_Alarm_Count());
		if (appstate.getStart_Alarm_Count() == 7) {
			Regular_Alam_util.cancelAlarm(context);
			appstate.setRegular_Alarm_Triggered(false);
			appstate.setStart_Alarm_Count(0);
			Log.d(TAG, "CancelAlarm");

			Intent intent = new Intent(context, Reminder_Landing_Activity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
			/*
			 * android.provider.Settings.System.putInt(context.getContentResolver
			 * (), Settings.System.SCREEN_OFF_TIMEOUT, (60000 * 5));
			 */

		} else if (appstate.getStart_Alarm_Count() < 7) {
			if (android.os.Build.VERSION.SDK_INT >= 19) {
				if (appstate.getStart_Alarm_Count() == 1) {
					appstate.setStart_Alarm_Count(2);
				}
				createIntent(context);
			} else {
				if (appstate.getStart_Alarm_Count() > 1)
					createIntent(context);
			}

		}
	}

	/*
	 * Call intent to which ever activity is currently in the foreground state.
	 */
	private void createIntent(Context context) {

		if (android.os.Build.VERSION.SDK_INT >= 19) {
			Regular_Alam_util.startAlarm(context);
		} else {
			// Not android.os.Build.VERSION.SDK_INT >= 19
		}
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

		String mClassName = cn.getClassName();
		System.out.println("CLASSS RECEIVIED===========>>> " + mClassName);
			if (mClassName.contains("com.optum.telehealth")) {
				Class<?> c = null;
				if (mClassName != null) {
					try {
						c = Class.forName(mClassName);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
				SharedPreferences sharedpreferences = context.getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedpreferences.edit();
				editor.putInt("LOAD_STATUS", 1);
				editor.commit();

				Intent intent = new Intent(context, c);

				if (appstate.getBundle() != null) {
					intent.putExtras(appstate.getBundle());
				}
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);

			} else {
				Log.e("Alarm_Receiver", "**********Optum Is Closed**********");
			}
	}

}