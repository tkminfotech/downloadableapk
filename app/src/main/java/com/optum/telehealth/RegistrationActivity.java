package com.optum.telehealth;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import com.optum.telehealth.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.bean.ClassAdvice;
import com.optum.telehealth.bean.State;
import com.optum.telehealth.dal.MyDataTracker_db;
import com.optum.telehealth.dal.State_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.ConnectionDetector;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class RegistrationActivity extends Activity {

    private Button alreadyMember, btn_submit, reg_datepicker;
    int year, month, day;
    private String TAG = "RegistrationActivity";
    String serviceUrl;
    private EditText reg_fName, reg_lName;
    String temp_fname = "", temp_lname = "", temp_state = "", temp_dob = "";
    private TextView txt_errorMessage;
    private GlobalClass appClass;
    private boolean fnameStatus = false, lnameStatus = false, stateStatus = false, dobStatus = false;
    private Spinner spn_state;
    private List<State> statesListFromDB;
    private ProgressDialog progressDialog;
    State_db state_db = new State_db(this);
    String res_pin, res_message;
    private boolean isRequestPin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appClass = (GlobalClass) getApplicationContext();
        appClass.setEnglishLanguage(this);
        setContentView(R.layout.activity_registration);
        reg_fName = (EditText) findViewById(R.id.reg_fName);
        reg_lName = (EditText) findViewById(R.id.reg_lName);
        spn_state = (Spinner) findViewById(R.id.spn_stateList);
        appClass.myDataTracker_db = new MyDataTracker_db(getApplicationContext());
        alreadyMember = (Button) findViewById(R.id.alreadyMember);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        reg_datepicker = (Button) findViewById(R.id.reg_datepicker);
        isRequestPin = false;
        txt_errorMessage = (TextView) findViewById(R.id.txt_errorMessage);

        //Disable Submit button While loading the Activity.
        button_Disable();
        btn_submit.setText("   " + getResources().getString(R.string.sendpin) + "   ");

        reg_fName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (reg_fName.getText().length() == 1) {
                    if (reg_fName.getText().toString().equals(" ")) {
                        reg_fName.setText("");
                    } else {
                        fnameStatus = true;
                        button_Enable();
                    }
                } else if (reg_fName.getText().length() > 1) {
                    fnameStatus = true;
                    button_Enable();
                } else {
                    fnameStatus = false;
                    button_Disable();
                }
            }
        });
        reg_lName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (reg_lName.getText().length() == 1) {
                    if (reg_lName.getText().toString().equals(" ")) {
                        reg_lName.setText("");
                    } else {
                        lnameStatus = true;
                        button_Enable();
                    }
                } else if (reg_lName.getText().length() > 1) {
                    lnameStatus = true;
                    button_Enable();
                } else {
                    lnameStatus = false;
                    button_Disable();
                }
            }
        });
//        reg_state.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                if (reg_state.getText().length() == 1) {
//                    if (reg_state.getText().toString().equals(" ")) {
//                        reg_state.setText("");
//                    } else {
//                        stateStatus = true;
//                        button_Enable();
//                    }
//                } else if (reg_state.getText().length() > 1) {
//                    stateStatus = true;
//                    button_Enable();
//                } else {
//                    stateStatus = false;
//                    button_Disable();
//                }
//            }
//        });
        spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    stateStatus = false;
                    button_Disable();
                } else {
                    temp_state = statesListFromDB.get(position).getStateName();
                    stateStatus = true;
                    button_Enable();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");

        reg_datepicker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showTimePickerDialog();
                reg_datepicker.requestFocus();
                return false;
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                temp_fname = reg_fName.getText().toString().trim();
                temp_lname = reg_lName.getText().toString().trim();
                //temp_state = reg_state.getText().toString().trim();
                if (appClass.isConnectingToInternet(getBaseContext())) {
                    new mob_request_pin().execute();
                } else {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String NoConnectionError = ERROR_MSG.getString("NoConnectionError", "-1");
                    setNotificationMessage(NoConnectionError);
                }
            }
        });
        alreadyMember.setText(R.string.alreadymember);
        alreadyMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appClass.isAuthenticationLanguage_english = true;
                Intent i = new Intent(RegistrationActivity.this, AuthenticationActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });
        Boolean isInternetPresent = (new ConnectionDetector(
                getApplicationContext())).isConnectingToInternet();
        if (isInternetPresent) {

            downloadStateDetails();
        } else {

            populateSpinner();
        }
    }

    private void button_Enable() {
        if (fnameStatus && lnameStatus && stateStatus && dobStatus) {
            btn_submit.setEnabled(true);
            if (getResources().getBoolean(R.bool.isTab)) {
                btn_submit.setBackground(getResources().getDrawable(R.drawable.orange));
            } else {
                btn_submit.setBackground(getResources().getDrawable(R.drawable.button_orange));
            }
        }
    }

    private void button_Disable() {
        btn_submit.setEnabled(false);
        if (getResources().getBoolean(R.bool.isTab)) {
            btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable_tab));
        } else {
            btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable));
        }
    }

    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    public void showTimePickerDialog() {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialogSpinner datePicker = new DatePickerDialogSpinner(this, pickerListener, year, month, day);

            datePicker.updateDate(year, month, day);
            return datePicker;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth + 1;
            day = selectedDay;

            String s_month = String.format("%02d", month);
            String s_day = String.format("%02d", day);

            reg_datepicker.setText(s_month + "/" + s_day + "/" + year);
            dobStatus = true;
            reg_datepicker.setTextColor(getResources().getColor(R.color.black));
            temp_dob = s_day + "/" + s_month + "/" + year;
            button_Enable();
            Log.i(TAG, "year=" + year + " month=" + month + " day=" + day);
        }
    };

    private class mob_request_pin extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_request_pin");
            try {
                progressDialog = new ProgressDialog(RegistrationActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_request_pin #doInBackground");
            String response = "";
            String language = "0";
            isRequestPin = false;
            try {
                SharedPreferences settings_n = getSharedPreferences(
                        CommonUtilities.USER_SP, 0);
                String lngID = settings_n.getString("language_id", "0");
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_request_pin.ashx", new String[]{
                                "f_name", "l_name", "dob", "state","language_id"},
                        new String[]{temp_fname, temp_lname, temp_dob, temp_state,lngID});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                //str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i(TAG, "mob_request_pin: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("request_pin");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("Success")) {
                        response = "success";
                        res_pin = Util.getTagValue(nodes, i, "pin_id");
                        language = Util.getTagValue(nodes, i, "language_id");
                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("language_id", language);
                        editor.commit();
                        //Log.i(TAG, "==========SUCCESS==========" + res_pin);
                        return response;
                    } else {
                        response = "failed";

                        res_message = Util.getTagValue(nodes, i, "message");
                        language = Util.getTagValue(nodes, i, "language_id");
                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("language_id", language);
                        editor.commit();
                        //Log.i(TAG, "==========FAILED==========" + res_message);
                        return response;
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                //          progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("success")) {
                    isRequestPin = true;
                    new mob_fetch_error_msg().execute();

                } else if (s.equals("failed")) {
                    isRequestPin = false;
                    new mob_fetch_error_msg().execute();


                } else {
                    progressDialog.dismiss();
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String connection_error = ERROR_MSG.getString("connection_error", "");
                    setNotificationMessage(connection_error);
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    public void setNotificationMessage(String msg) {
        txt_errorMessage.setVisibility(View.VISIBLE);
        txt_errorMessage.setText(msg);
        btn_submit.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMessage.animate().alpha(0.0f);
                        txt_errorMessage.setVisibility(View.GONE);
                        btn_submit.setEnabled(true);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }

    private void downloadStateDetails() {

        DownloadStateList task = new DownloadStateList();
        task.execute(new String[]{serviceUrl
                + "/droid_website/mob_get_state.ashx"});
    }

    private class DownloadStateList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(RegistrationActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String response = "";
            List<State> stateList = new ArrayList<State>();
            try {

                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_get_state.ashx", new String[]{},
                        new String[]{});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return ""; // process
                }
                String str = Util
                        .readStream(response1.getEntity().getContent());

                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                str.toString();
                //Log.i("response:", "handler_response:" + str);

                if (str.length() < 10) {
                    Log.i(TAG, "Advice message data null");
                    return "";
                }
                state_db.clearTable();
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("state_list");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    NodeList nodes = doc.getElementsByTagName("state");

                    for (int j = 0; j < nodes.getLength(); j++) {
                        State state = new State();
                        state.setStateId(Integer.parseInt(Util.getTagValue(
                                nodes, j, "id")));
                        state.setStateName(Util.getTagValue(
                                nodes, j, "name"));

                        stateList.add(state);
                    }
                }
                state_db.insertState(stateList);
            } catch (Exception e) {

            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            //loadApkDetails();
            try {
                progressDialog.dismiss();
                populateSpinner();

            } catch (Exception ex) {
                Log.i("onPostExecute", ex.getMessage());
            }
        }
    }

    private void populateSpinner() {
        statesListFromDB = new ArrayList<State>();
        List<State> states = state_db.getAllStates();
        State state = new State();
        state.setStateName("-Select State-");
        statesListFromDB.add(state);
        for (int i = 0; i < states.size(); i++) {

            State state1 = states.get(i);
            statesListFromDB.add(state1);

        }
        SpinnerAdapter adapter = new SpinnerAdapter(RegistrationActivity.this, android.R.layout.simple_spinner_item, statesListFromDB);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_state.setAdapter(adapter);
    }


    private class mob_fetch_error_msg extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_fetch_error_msg");
            try {
//                progressDialog = new ProgressDialog(RegistrationActivity.this);
//                progressDialog.setMessage("Please wait...");
//                progressDialog.show();
//                progressDialog.setCancelable(false);
//                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_fetch_error_msg #doInBackground");
            String response = "";
            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_get_error_message.ashx", new String[]{
                                "imei_no"},
                        new String[]{"00000"});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                //str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i(TAG, "mob_fetch_error_msg: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("error_message_details");
                for (int i = 0; i < nodes.getLength(); i++) {
                    Element error_element = (Element) nodes.item(i);
                    NodeList error_message = error_element
                            .getElementsByTagName("error_message");
                    for (int j = 0; j < error_message.getLength(); j++) {
                        String error_code = Util.getTagValue(error_message, j, "error_code");
                        String error_description = Util.getTagValue(error_message, j, "error_description");
                        String error_description_spanish = Util.getTagValue(error_message, j, "error_description_spanish");
                        //Log.i(TAG, "#onPostExecute error_code: " + error_code);
                        //Log.i(TAG, "#onPostExecute error_description: " + error_description);
                        //Log.i(TAG, "#onPostExecute error_description_spanish: " + error_description_spanish);
                        SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        SharedPreferences settings_n = getSharedPreferences(
                                CommonUtilities.USER_SP, 0);
                        int ln = Integer.parseInt(settings_n.getString("language_id", "0"));


                        if (ln == 11) {
                            //Spanish Error Msg
                            editor.putString(error_code, error_description_spanish);
                            editor.commit();
                        } else {
                            //English Error Msg
                            editor.putString(error_code, error_description);
                            editor.commit();
                        }
                    }
                    response = "success";
                    return response;
                }
            } catch (Exception e) {
                e.printStackTrace();
                response = "error";
                return response;
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("error")) {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String connection_error = ERROR_MSG.getString("connection_error", "");
                    setNotificationMessage(connection_error);
                } else {
                    if (isRequestPin == false) {

                        Intent i = new Intent(RegistrationActivity.this, ConfigErrorActivity.class);
                        i.putExtra("message_desc", res_message);
                        i.putExtra("message", "-1");
                        i.putExtra("class_name", "com.optum.telehealth.RegistrationActivity");
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    } else {


                        Intent i = new Intent(RegistrationActivity.this, PinValidationActivity.class);
                        i.putExtra("pin_id", res_pin);
                        i.putExtra("message", "-1");
                        i.putExtra("class_status", "false");
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
}
