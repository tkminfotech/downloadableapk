package com.optum.telehealth;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.optum.telehealth.bean.ClasssPressure;
import com.optum.telehealth.dal.Pressure_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

import java.util.Timer;
import java.util.TimerTask;

public class PressureEntry extends Titlewindow implements View.OnFocusChangeListener {
    Pressure_db dbcreatepressure = new Pressure_db(this);
    int pressureId;
    private String TAG = "PressureEntry";
    Dialog dialog;
    private Button Pressure_Next;
    private TextView txt_errorMsg;
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            nextClick();
        }
    };
    private EditText edtsystolic;
    private EditText edtdiastolic;
    private EditText edtupulse;
    private boolean isSysFocus = false;
    private boolean isDiaFocus = false;
    private boolean isPulseFocus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pressure_entry);

        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        appClass.isSupportEnable = true;
        Pressure_Next = (Button) findViewById(R.id.btn_bloodPressureNext);
        edtsystolic = (EditText) findViewById(R.id.edtsys);
        edtdiastolic = (EditText) findViewById(R.id.edtdia);
        edtupulse = (EditText) findViewById(R.id.edtpulse);
        edtsystolic.setOnFocusChangeListener(this);
        edtdiastolic.setOnFocusChangeListener(this);
        edtupulse.setOnFocusChangeListener(this);
        Pressure_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appClass.appTracking("Blood Pressure manual entry page","Click on Next button");
                if(isSysFocus){
                    edtdiastolic.requestFocus();
                }else if(isDiaFocus){
                    edtupulse.requestFocus();
                }else if(isPulseFocus){
                    nextClick();
                }else{
                    nextClick();
                }

            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            this.finish();
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStop() {
        //Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
        super.onStop();
    }

    private void nextClick() {

        String PatientIdDroid = Constants.getdroidPatientid();


        try {
            String s_val = edtsystolic.getText().toString();
            double d_val = 0;
            if (!s_val.equals("") && !s_val.equals(".")) {
                d_val = Double.parseDouble(s_val); // Make use of autoboxing.  It's also easier to read.
            }
            String s_d_val = edtdiastolic.getText().toString();
            double d_d_val = 0;
            if (!s_d_val.equals("") && !s_d_val.equals(".")) {
                d_d_val = Double.parseDouble(s_d_val); // Make use of autoboxing.  It's also easier to read.
            }
            String s_P_val = edtupulse.getText().toString();
            double d_P_val = 0;
            if (!s_P_val.equals("") && !s_P_val.equals(".")) {
                d_P_val = Double.parseDouble(s_P_val); // Make use of autoboxing.  It's also easier to read.
            }
            Log.i(TAG, "VALUE:" + d_val + " " + d_P_val);

            if ((edtsystolic.getText().toString()).length() >= 1
                    && (edtdiastolic.getText().toString()).length() >= 1
                    && (edtupulse.getText().toString()).length() >= 1
                    && !(edtsystolic.getText().toString()).contains(".")
                    && !(edtdiastolic.getText().toString()).contains(".")
                    && !(edtupulse.getText().toString()).contains(".")
                    && !(edtupulse.getText().toString().length() > 3)
                    && !(edtsystolic.getText().toString().length() > 3)
                    && !(edtdiastolic.getText().toString().length() > 3)
                    && d_val != 0 && d_P_val != 0 && d_d_val != 0) {
                SharedPreferences flow = getSharedPreferences(
                        CommonUtilities.USER_FLOW_SP, 0);
                int val = flow.getInt("flow", 0); // #1
                if (val == 1) {

                    SharedPreferences section = getSharedPreferences(
                            CommonUtilities.PREFS_NAME_date, 0);
                    SharedPreferences.Editor editor_retake = section.edit();
                    editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(PressureEntry.this));
                    editor_retake.commit();
                }
                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.USER_TIMESLOT_SP, 0);
                String slot = settings.getString("timeslot", "AM");

                ClasssPressure pressure = new ClasssPressure();
                pressure.setPatient_Id(PatientIdDroid);
                pressure.setSystolic(Integer.parseInt(edtsystolic.getText()
                        .toString()));
                pressure.setDiastolic(Integer.parseInt(edtdiastolic.getText()
                        .toString()));
                pressure.setPulse(Integer.parseInt(edtupulse.getText()
                        .toString()));
                pressure.setInputmode(1);
                pressure.setTimeslot(slot);
                Log.e(TAG, "Slot >>>" + slot);

                SharedPreferences settings1 = getSharedPreferences(
                        CommonUtilities.PREFS_NAME_date, 0);
                String sectiondate = settings1.getString("sectiondate", "0");
                pressure.setSectionDate(sectiondate);
                pressure.setPrompt_flag("4");
                Log.e(TAG, "Pressure value saving to db");
                pressureId = dbcreatepressure.InsertPressure(pressure);
                /*Toast.makeText(getApplicationContext(), "Saved Successfully",
                        Toast.LENGTH_SHORT).show();*/

                Intent intent = new Intent(PressureEntry.this,
                        ShowPressureActivity.class);
                intent.putExtra("sys", edtsystolic.getText().toString());
                intent.putExtra("dia", edtdiastolic.getText().toString());
                intent.putExtra("pulse", edtupulse.getText().toString());
                intent.putExtra("pressureid", pressureId);

                edtsystolic.setText("");
                edtdiastolic.setText("");
                edtupulse.setText("");
                Log.e(TAG, "Redirecting to show Pressure Page");
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);

            } else {
                Log.e(TAG, "Invalid Pressure value entered by user");
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String PressureValidationErrorMessage  = ERROR_MSG.getString("PressureValidationErrorMessage", "-1");
                errorShow(PressureValidationErrorMessage);
                return;
            }
        } catch (Exception e) {
            // p did not contain a valid double
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        Pressure_Next.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        Pressure_Next.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edtsys:
                if (hasFocus) {
                    isSysFocus = true;
                    isDiaFocus = false;
                    isPulseFocus = false;
                }
                break;
            case R.id.edtdia:
                if (hasFocus) {
                    isSysFocus = false;
                    isDiaFocus = true;
                    isPulseFocus = false;
                }
                break;
            case R.id.edtpulse:
                if (hasFocus) {
                    isSysFocus = false;
                    isDiaFocus = false;
                    isPulseFocus = true;
                }
                break;

        }
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
