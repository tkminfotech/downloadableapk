package com.optum.telehealth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import com.optum.telehealth.util.Log;
import android.widget.Toast;

import com.optum.telehealth.bean.ClassAdvice;
import com.optum.telehealth.dal.AdviceMessage_db;
import com.optum.telehealth.dal.MeasureType_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.ConnectionDetector;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Regular_Alam_util;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Login extends Titlewindow {
    private String TAG = "Login";
    private String serviceUrl = "";
    private String serialNo = "";
    private String imeilNo = "";
    private GlobalClass appClass;
    AdviceMessage_db dbcreate = new AdviceMessage_db(this);
    MeasureType_db db_measureType = new MeasureType_db(this);
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_final_main);
        Log.e("Home", "ONCREATE");
        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = settings.getString("Server_post_url", "-1");

        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.WiFi_SP, 0);
        serialNo = settings1.getString("wifi", "-1");
        imeilNo = settings1.getString("imei_no", "");
        appClass = (GlobalClass) getApplicationContext();
        call_alams();
        Cursor c = dbcreate.SelectAdviceMessageForDisplay();
        c.moveToFirst();
        if (c.getCount() > 0) {
            try {
//                while (c.isAfterLast() == false) {
//                    dbcreate.UpdateAdviceMessageByDate(c.getInt(8));
//                    c.moveToNext();
//                }
                Log.i(TAG, "Redirection start");
                Intent intent = new Intent(Login.this,
                        PopActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //intent.putExtra("subject", c.getString(3));
//                intent.putExtra("message",
//                        c.getString(4));
                startActivity(intent);
                finish();

            } catch (NullPointerException e) {
                Log.i(TAG, "IllegalArgumentException");
            }
        } else {
            Intent intent = new Intent(Login.this, Welcome_Activity.class);
            startActivity(intent);
            finish();
        }
    }

    /*public void DownloadAdviceMessage() {
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(new String[]{serviceUrl + "/droid_website/mob_handler.ashx"});

    }

    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        boolean fala_adv = false;

        protected String doInBackground(String... urls) {
            String response = "";

            try {
                Log.i(TAG, "Checking device registration MAC-" + serialNo
                        + " IMEI-" + imeilNo);
                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                        + "/droid_website/mob_handler.ashx", new String[]{
                        "authentication_token", "mode", "patient_id"}, new String[]{
                        token, "0", patientId});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return ""; // process
                }

                String str = Util
                        .readStream(response1.getEntity().getContent());

                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                str.toString();
                Log.i("response:", "handler_response:" + str);

                if (str.length() < 10) {
                    Log.i(TAG, "Advice message data null");
                    return "";
                }

                dbcreate.Cleartable();
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("advice_message");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    response = Util.getTagValue(AF_Nodes, i, "mob_response");
                }
                if (response.equals("AuthenticationError")) {

                    return response;
                }
                NodeList nodes = doc.getElementsByTagName("advice");

                ClassAdvice classAdvice = new ClassAdvice();

                for (int i = 0; i < nodes.getLength(); i++) {

                    classAdvice.setPatientId(Util.getTagValue(
                            nodes, i, "patient_id"));
                    classAdvice.setUserId(Integer.parseInt(Util.getTagValue(
                            nodes, i, "user_id")));
                    classAdvice.setAdviceSubject(Util.getTagValue(nodes, i,
                            "advice_subject"));
                    classAdvice.setAdviceText(Util.getTagValue(nodes, i,
                            "advice_text"));
                    classAdvice.setMessageDate(Util.getTagValue(nodes, i,
                            "create_date"));

                    // saving advice to db
                    Constants.setPatientid(classAdvice.getPatientId());

                    if (dbcreate.InsertAdviceMessage(classAdvice)) // if there i
                    // a new
                    // advice
                    // msg
                    {
                        fala_adv = true;
                        // notification();

                        dbcreate.UpdateAdviceMessageByDate(classAdvice
                                .getMessageDate());

                        try {
                            Log.i(TAG, "Redirection start");
                            Intent intent = new Intent(Login.this,
                                    PopActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("subject", classAdvice.getAdviceSubject());
                            intent.putExtra("message",
                                    classAdvice.getAdviceText());
                            startActivity(intent);

                        } catch (NullPointerException e) {
                            // TODO Auto-generated catch block
                            // e.printStackTrace();
                            Log.i(TAG, "IllegalArgumentException");
                            fala_adv = false;
                        }
                    }

                }

            } catch (Exception e) {
                // e.printStackTrace();
                fala_adv = false;
            }

            return response;
        }

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(Login.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            //loadApkDetails();
            progressDialog.dismiss();
            if (result.equalsIgnoreCase("AuthenticationError")) {
                Toast.makeText(getApplicationContext(), R.string.authenticationError, Toast.LENGTH_LONG).show();
                logoutClick();
            } else if (!fala_adv) {
                Intent intent = new Intent(Login.this, Welcome_Activity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        }
    }*/

    private void call_alams() {
        SharedPreferences flowsp = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, Context.MODE_PRIVATE);
        int val = flowsp.getInt("start_flow", 5); // #1

        if (val == 1) {
            Log.i(TAG, "starting Regular alam---");

            Regular_Alam_util.cancelAlarm(getApplicationContext());
            appClass.setRegular_Alarm_Triggered(false);
            appClass.setStart_Alarm_Count(0);
            Regular_Alam_util.startAlarm(getApplicationContext());// start
            // Regular
            // alarm
            // to
            // set
            // the
            // five
            // minutes
            // time
            // out
            appClass.setRegular_Alarm_Triggered(true);
            SharedPreferences.Editor editor = flowsp.edit();
            editor.putInt("start_flow", 0);// for reloading 5 minutes
            editor.commit();

            SharedPreferences settings1 = getSharedPreferences(
                    CommonUtilities.PREFS_NAME_date, 0);
            SharedPreferences.Editor editor_retake = settings1.edit();
            editor_retake
                    .putString("measure_time_skip", Util.get_server_time());
            editor_retake.putString("sectiondate",
                    Util.get_patient_time_zone_time(Login.this));

            editor_retake.commit();

        } else {
            Log.i(TAG, "Cancel Regular alam---");

            Regular_Alam_util.cancelAlarm(getApplicationContext());

        }
        Log.i(TAG,
                "Util.get_server_time()---------------"
                        + Util.get_server_time());
        db_measureType.updateStatusforAll(1);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
