package com.optum.telehealth;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassWeight;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.dal.Weight_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.PINReceiver;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.Util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class P724Activity extends Titlewindow{
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private final BroadcastReceiver mybroadcast = new PINReceiver();
    private String TAG = "P724Activity Sierra";
    public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
    Weight_db dbcreateweight = new Weight_db(this);
    Sensor_db dbSensor = new Sensor_db(this);
    private BluetoothAdapter mBluetoothAdapter = null;
    private static final String DEVICE_NAME = "Electronic Scale";
    private String result_re = "0, 0";
    public int back = 0;
    TextView textView = null;
    private ProgressDialog progressDialog = null;
    String macAddress = "";
    private static final String HEXES = "0123456789ABCDEF";
    private String pageName = "Weight Sensor Page";
    BluetoothSocket socket = null;
    BluetoothDevice device = null;
    private int deviceType = 0;
    ImageView rocketImage;
    int measureId;
    AnimationDrawable rocketAnimation;
    BackgroundThread backgroundThread;
    private static final UUID MY_UUID_SECURE = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");

    BTConnector btc = new BTConnector();
    private GlobalClass appClass;
    ObjectAnimator AnimPlz, ObjectAnimator;
    public boolean cancel_flag = false;
    TextView txtwelcom, txtReading;
    private TextView txt_errorMsg;
    boolean isPaired = false;
    public static final int REQUEST_DISCOVERABLE_CODE = 42;
    boolean redirectFlag = false;
    boolean isSocketConnect = false;
    private boolean isStop = false;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "TIME_DIFF onCreate: called");

        setContentView(R.layout.activity_aand_dreader);
        appClass = (GlobalClass) getApplicationContext();
        appClass.isEllipsisEnable = true;
        appClass.isSupportEnable = false;
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        Log.e(TAG, "P724 BT page started");
        // tts = new TextToSpeech(this, this);
        Button btnmanual = (Button) findViewById(R.id.aAndDManuval);
        txtwelcom = (TextView) findViewById(R.id.txtwelcome);
        txtReading = (TextView) findViewById(R.id.takereading);
        rocketImage = (ImageView) findViewById(R.id.loading);
        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        // txtwelcom.setText("Please step on the scale.");
        txtwelcom.setTypeface(type, Typeface.NORMAL);
        txtReading.setTypeface(type, Typeface.NORMAL);
        try {
            rocketImage.setBackgroundResource(R.drawable.weight_animation);
            rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
            rocketAnimation.start();
            rocketImage.setVisibility(View.INVISIBLE);
            txtReading.setVisibility(View.INVISIBLE);
        } catch (OutOfMemoryError ex){
            android.util.Log.i("exception",ex.toString());
            rocketImage.setBackgroundResource(R.drawable.scale0013);
        }

        try {
            String Pin = dbSensor.SelectWTPIN();
            Constants.setPIN(Pin);

        } catch (Exception e) {

        }

        //Constants.setPIN(Pin);// set pin for auto pairing
        btnmanual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                appClass.appTracking(pageName,"Click on Manual entry button");
                Intent intent = new Intent(P724Activity.this, WeightEntry.class);
                btc.cancel(true);
                cancel_flag = true;
                finish();
                startActivity(intent);
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
            } else {
                CheckBlueToothState();
            }
        } else {
            CheckBlueToothState();
        }
        textView = new TextView(this);
        //stopService(new Intent(P724Activity.this, AdviceService.class));

        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        // setMessage("Searching for device configuration...");


        backgroundThread = new BackgroundThread();
        backgroundThread.setRunning(true);
        backgroundThread.start();
        Log.i(TAG, "-------------------oncreate--------------  p724  activity");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                    CheckBlueToothState();
                } else {
                    Intent intentwt = new Intent(getApplicationContext(),
                            WeightEntry.class);
                    startActivity(intentwt);
                    overridePendingTransition(0, 0);
                    finish();
                }
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
//        back = 1;
//        btc.cancel(true);
//        try {
//            if (socket != null)
//                socket.close();
//        } catch (IOException e) {
//        }
//        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, " KEYCODE_BACK clicked:");
//            back = 1;
//            btc.cancel(true);
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            try {
//                if (socket != null)
//                    socket.close();
//            } catch (IOException e) {
//                // e.printStackTrace();
//            }
//            startActivity(intent);
//            finish();
//        }
        return super.onKeyDown(keyCode, event);
    }

    public void setMessage(String message) {
        textView.setTextSize(20);
        textView.setText(message);
        setContentView(textView);
    }

	/*-----------------*/

    public class BTConnector extends AsyncTask<BluetoothDevice, String, String> {

        String progress = "";

        boolean mIsTaskCancelled = false;

        void setIsTaskCancelled(boolean isCancelled) {
            Log.d(TAG, "setIsTaskCancelled: loop is set to be cancelled");
            mIsTaskCancelled = isCancelled;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            // showDialog(PROGRESS_BAR_TYPE);
        }

        /**
         * Downloading file in background thread
         */
        protected String doInBackground(BluetoothDevice... device) {
            try {
                Log.d(TAG, "TIME_DIFF doInBackground: BTConnector task started");

                String res = "";
                BluetoothDevice device1 = null;

                Log.e(TAG, "Background started to connect scale");

                for (int i = 0; i <= 500000; i++) {

                    // if the task was cancelled manually, break the loop
                    if (mIsTaskCancelled) {
                        Log.d(TAG, "doInBackground: loop is broken.");
                        break;
                    }

                    if (mBluetoothAdapter == null)
                        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                    if (macAddress.trim().length() == 17) { // getting a
                        // configured device
                        device1 = mBluetoothAdapter.getRemoteDevice(macAddress);
                        Log.i(TAG, " P724 macAddress" + macAddress);
                    }

                    if (connect(device1)) {
                        Log.i(TAG, "Optum : Connection successful for 1 "
                        );
                        return result_re;
                    }

                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            return "";
        }

        @SuppressLint("SimpleDateFormat")
        @Override
        protected void onPostExecute(String result) {

            if (cancel_flag) {
                Log.i(TAG, "return on post exec)");
                return;
            }

            Log.i(TAG, "onPostExecute()");
            SharedPreferences flow = getSharedPreferences(
                    CommonUtilities.USER_FLOW_SP, 0);
            int val = flow.getInt("flow", 0); // #1
            if (val == 1) {
                SharedPreferences section = getSharedPreferences(
                        CommonUtilities.PREFS_NAME_date, 0);
                SharedPreferences.Editor editor_retake = section.edit();
                editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(P724Activity.this));
                editor_retake.commit();
            }

            if (result_re.contains("-")) {

                Log.i(TAG, "return on post, exit due to negative reading.");
                return;

            } else {

                if (back == 1) {
                    back = 0;
                } else {
                    // progressDialog.dismiss();
                    Log.i(TAG, "PROGRESS_BAR_TYPE end ");
                    ContentValues values = new ContentValues();
                    String PatientIdDroid = Constants.getdroidPatientid();

                    result = result_re;
                    Log.i(TAG, "onPostExecute()");
                    String message = result;
                    String ttmessage = "";

                    if (result.trim().length() == 0) {
                        // message=Resources.getSystem().getString(R.string.unabletoconnectbluetooth);
                        message = getApplicationContext().getString(
                                R.string.unabletoconnectbluetooth);
                        ttmessage = getApplicationContext().getString(
                                R.string.pleasetry);
                    } else {
                        try {

                            if (TestDate.getCurrentTime().length() == 2) {
                                getSavedTime1();
                            }

                            if (deviceType == 0) { // weight
                                // scale

                                String wt = "";
                                ClassWeight weight = new ClassWeight();
                                weight.setPatientID(PatientIdDroid);
                                weight.setWeightInKg(result_re);
                                weight.setInputmode(0);
                                SharedPreferences settingswt = getSharedPreferences(
                                        CommonUtilities.USER_TIMESLOT_SP, 0);
                                String slot = settingswt.getString("timeslot",
                                        "AM");
                                weight.setTimeslot(slot);
                                SharedPreferences settings1 = getSharedPreferences(
                                        CommonUtilities.PREFS_NAME_date, 0);
                                String sectiondate = settings1.getString("sectiondate", "0");
                                weight.setSectionDate(sectiondate);
                                measureId = dbcreateweight.InsertWeight(weight);

                            }
                            if (deviceType == 0) {
                                Intent intent = new Intent(P724Activity.this,
                                        ShowWightActivity.class);

                                intent.putExtra("text", result_re);
                                intent.putExtra("measureId", measureId);

                                startActivity(intent);
                                overridePendingTransition(0, 0);
                                P724Activity.this.finish();
                                return;

                            }

                        } catch (Exception e) {
                            message = getApplicationContext().getString(
                                    R.string.invaliddata);
                        }
                    }
                    if (deviceType == 0) {
                        Intent intent = new Intent(P724Activity.this,
                                ShowWightActivity.class);
                        intent.putExtra("text", result_re);
                        intent.putExtra("measureId", measureId);
                        intent.putExtra("type", 0);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        P724Activity.this.finish();
                        return;
                    }
                }
            }
            Log.i(TAG, "onPostExecute() end");

        }

        protected boolean connect(BluetoothDevice device) {
            isSocketConnect = false;
            InputStream inStream = null;
            OutputStream outStream = null;

            // The documents tell us to cancel the discovery process.
            mBluetoothAdapter.cancelDiscovery();
            boolean finished = false;

            try {
                socket = device
                        .createInsecureRfcommSocketToServiceRecord(MY_UUID_SECURE);

                Log.i(TAG, " : Connection socket " + device.getName());
                if (socket == null) {
                    Log.i(TAG, " : Connection socket failed" + device.getName());
                    return false;
                }

                // attempt to connect to device
                try {
                    Thread.sleep(3000);

                    socket.connect();

                    // try to pair the device (if the device is already paired, it won't be an issue)
                    try {

                        if (Build.VERSION.SDK_INT >= 19)
                            device.createBond();

                        else
                            BluetoothDevice.class.getMethod("createBond").invoke(device);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        Log.i(TAG, " : Connected socket " + device.getName());
                        isSocketConnect = true;
                        inStream = socket.getInputStream();
                        outStream = socket.getOutputStream();
                    } catch (IOException exx) {
                        exx.printStackTrace();
                        Log.e(TAG, " : Conenction Failed...\n");
                        socket.close();
                        return false;
                    }
                    if (inStream == null || outStream == null) {
                        Log.e(TAG, " : Stream is null in the scale ");
                        try {
                            inStream.close();
                            outStream.close();
                            socket.close();
                        } catch (Exception cl) {
                            cl.printStackTrace();
                        }
                        return false;
                    }
                    Log.i(TAG, " : Connected to device " + device.getName()
                            + "\n");

                    byte[] buffer = new byte[1024];
                    int bytes;

                    bytes = inStream.read(buffer);
                    Log.i(TAG, "< " + getHex(buffer, bytes));

                    byte a = 0;
                    byte b = 0;
                    a = buffer[5];
                    b = buffer[4];

                    DecimalFormat formatter = new DecimalFormat("00");

                    String hex1 = Integer.toHexString(a & 0XFF);
                    String hex2 = Integer.toHexString(b & 0XFF);

                    if (hex1.trim().length() == 1) {
                        hex1 = "0" + hex1;
                    }
                    if (hex2.trim().length() == 1) {
                        hex2 = "0" + hex2;
                    }

                    Log.i(TAG,
                            "Got wt kg..."
                                    + ((float) Integer.parseInt(
                                    hex2 + hex1, 16)) / 10);

                    Log.i(TAG,
                            "Got wt old lbs..."
                                    + (float) Util.round((((float) Integer
                                    .parseInt(hex2 + hex1, 16)) / 10) * 2.204));

                    String bin = Integer.toHexString(Integer.parseInt(""
                            + (Integer.parseInt(hex2 + hex1, 16)) * 10));

                    Log.i(TAG, " hex val in kg bin.." + bin);

                    String val = bin;
                    String val2 = "2B0F";
                    String val3 = "C350";
                    String res = ""
                            + (float) (((Integer.parseInt(val, 16)
                            * Integer.parseInt(val2, 16) / Integer
                            .parseInt(val3, 16)) + 1) / 2 * 2)
                            / 10;

                    Log.i(TAG, "Got wt new hex formula  lbs wt..." + res);

                    /*
                     * result_re = "" + (float) round((((float)
                     * Integer.parseInt(hex2 + hex1, 16)) / 10) * 2.204);
                     */
                    result_re = res;
                    // 2.2046226218487758072297380134503 new lbs conversion
                    // 2.204 old lbs

                    Log.i(TAG, "Got wt pound..." + result_re);
                    finished = true;

                    return finished;
                } catch (Exception e) {
                    Log.e(this.toString(), "IOException 1 " + e.getMessage());
                    e.printStackTrace();
                }
            } catch (Exception ex) {
                Log.e(this.toString(), "Exception 1" + ex.getMessage());
                ex.printStackTrace();
            }
            try {
                inStream.close();
                outStream.close();
                socket.close();
            } catch (Exception cl) {
                cl.printStackTrace();
            }

            return false;
        }
    }




    private static BigDecimal truncateDecimal(double x, int numberofDecimals) {
        if (x > 0) {
            return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals,
                    BigDecimal.ROUND_FLOOR);
        } else {
            return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals,
                    BigDecimal.ROUND_CEILING);
        }
    }

    public Boolean preferenceWelcome(int mode) {
        Boolean flag = null;
        try {
            SharedPreferences settings = getSharedPreferences(
                    PREFS_NAME_SCHEDULE, 0);
            int scheduletatus = settings.getInt("welcome", -1);
            if (mode == 1) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("welcome", 1);
                editor.commit();
            }
            if (scheduletatus == 0)

                flag = true;
            else
                flag = false;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return flag;
    }

    private static final String PREFS_NAME_TIME = "TimePrefSc";

    public void getSavedTime1() {

        SharedPreferences settings = getSharedPreferences(PREFS_NAME_TIME, 0);
        String savedTime = settings.getString("timezone", "00");

        Log.i(TAG, "inside getSavedTime fn; rest servertime " + savedTime);

        int year = Integer.parseInt(savedTime.substring(6, 10));
        int month = Integer.parseInt(savedTime.substring(0, 2));
        int day = Integer.parseInt(savedTime.substring(3, 5));
        int hour = Integer.parseInt(savedTime.substring(11, 13));
        int minute = Integer.parseInt(savedTime.substring(14, 16));
        int seconds = Integer.parseInt(savedTime.substring(17, 19));
        TestDate.setCalender(year, month, day, hour, minute, seconds);

    }

    public double round(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.#");
        return Double.valueOf(twoDForm.format(d));
    }

    public String getHex(byte[] raw, int len) {
        Log.i(TAG, "Get Hex...");
        if (raw == null) {
            return null;
        }
        final StringBuilder hex = new StringBuilder(3 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F))).append(" ");
            if (--len == 0)
                break;
        }
        return hex.toString();
    }

    public class BackgroundThread extends Thread {
        volatile boolean running = false;
        int cnt;

        void setRunning(boolean b) {
            running = b;
            cnt = 6;
        }

        @Override
        public void run() {
            while (running) {
                try {
                    sleep(60);
                    if (cnt-- == 0) {
                        running = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            handler.sendMessage(handler.obtainMessage());
        }
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            setProgressBarIndeterminateVisibility(false);
            boolean retry = true;
            while (retry) {
                try {
                    backgroundThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            setwelcome();
        }

    };

    private void setwelcome() {

        String th = "";
        String deviceMessage = "";

        if (preferenceWelcome(0)) {
            if (deviceType == 0) {
                if (preferenceWelcome(1)) {
                    deviceMessage = this
                            .getString(R.string.enterweightbluetooth);
                    txtwelcom.setText(deviceMessage);

                }

            } else if (deviceType == 1) {
                deviceMessage = this.getString(R.string.checkbloodpressure);
                txtwelcom.setText(deviceMessage);
            }
        } else {
            if (deviceType == 0) {
                deviceMessage = this.getString(R.string.enterweightbluetooth);
                txtwelcom.setText(deviceMessage);
            } else if (deviceType == 1) {
                th = this.getString(R.string.checkbloodpressure);
                txtwelcom.setText(th);
            }
        }

        if (deviceType == 0) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // e.printStackTrace();
            }

        } else if (deviceType == 1) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
            }
        }
        AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, 0f);
        //AnimPlz.setDuration(1500);
        AnimPlz.start();
        AnimPlz.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                Log.e(TAG, "AnimPlz");
                txtReading.setVisibility(View.VISIBLE);
                rocketImage.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        timer.cancel();

                    }
                });

            }
        }, 3000, 3000);
    }

    private void CheckBlueToothState() {
        Log.d(TAG, "IN #CheckBlueToothState()");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
        String bluetooth_error  = ERROR_MSG.getString("bluetooth_error", "-1");
        if (mBluetoothAdapter == null) {
            errorShow(bluetooth_error);
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
                BTDiscoverable();
            } else {
                try {
                    errorShow(bluetooth_error);
                    mBluetoothAdapter.enable();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mBluetoothAdapter.isDiscovering()) {
                                mBluetoothAdapter.cancelDiscovery();
                            }
                            BTDiscoverable();
                        }
                    }, 2000);
                } catch (Exception e) {
                }
            }
        }
    }

    public void BTDiscoverable() {
        Log.d(TAG, "CALL #getPairedDevices()");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        if (extras.getString("macaddress").length() > 1)
            macAddress = extras.getString("macaddress");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            getPairedDevices(macAddress);
        } else {
            BluetoothAdapter BA;
            BA = BluetoothAdapter.getDefaultAdapter();
            if (BA.getScanMode() !=
                    BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                turnOn.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
                startActivityForResult(turnOn, REQUEST_DISCOVERABLE_CODE);
            } else {
                getPairedDevices(macAddress);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_DISCOVERABLE_CODE) {
            if (resultCode == RESULT_CANCELED) {
                errorShow(this.getString(R.string.connectivityError));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!redirectFlag) {
                            Intent intentwt = new Intent(getApplicationContext(),
                                    WeightEntry.class);
                            startActivity(intentwt);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }
                }, 4000);
            } else {
                Log.d(TAG, "CALL #getPairedDevices()");
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    appClass.setBundle(extras);
                } else {
                    extras = appClass.getBundle();
                }
                if (extras.getString("macaddress").length() > 1)
                    macAddress = extras.getString("macaddress");
                getPairedDevices(macAddress);
            }
        }
    }

    private void getPairedDevices(String macAddr) {
        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(onoffReceiver, filter1);

        Set<BluetoothDevice> pairedDevice = mBluetoothAdapter
                .getBondedDevices();
        if (pairedDevice.size() > 0) {
            Log.d(TAG, "get paired devices list");
            for (BluetoothDevice device : pairedDevice) {
                try {
                    if (device.getAddress().equals(macAddr)) {
                        isPaired = true;
                        btc.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, device);
                        Log.d(TAG, "Device Paired");
                    }
                } catch (Exception e) {
                }
            }
        }
        if (!isPaired) {

            IntentFilter Bfilter = new IntentFilter();
            Bfilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
            registerReceiver(mybroadcast, Bfilter);

            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(ActionFoundReceiver, filter);

            if (mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.cancelDiscovery();
            }
            mBluetoothAdapter.startDiscovery();
        }
    }

    private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    final BluetoothDevice device = intent
                            .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device.getAddress().equals(macAddress)) {
                        try {
                            Log.d("pairDevice()", "Start Pairing...");
                            Method m = device.getClass().getMethod("createBond", (Class[]) null);
                            m.invoke(device, (Object[]) null);
                            Log.d("pairDevice()", "Pairing finished.");
                            isPaired = true;

                            if (btc != null && btc.getStatus() == AsyncTask.Status.PENDING)
                                btc.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, device);

                        } catch (Exception e) {
                            Log.i(TAG,
                                    "Exception in Device paired with the sensor");
                        }
                    }
                }
                if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    if (!isPaired) {
                        if (mBluetoothAdapter.isDiscovering()) {
                            mBluetoothAdapter.cancelDiscovery();
                        }
                        mBluetoothAdapter.startDiscovery();
                    }
                }
            } catch (Exception e) {
                Log.e(TAG,
                        "Exception  onReceive in Device paired with the sensor");
            }
        }
    };

    private final BroadcastReceiver onoffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (!mBluetoothAdapter.isEnabled()) {
                                        if (!redirectFlag) {
                                            CheckBlueToothState();
                                        } else {
                                            unregisterReceiver(onoffReceiver);
                                        }
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }, 5000);
                        break;
                }
            }
        }
    };

    @Override
    protected void onResume() {

        isStop = false;
        IntentFilter filter1 = new IntentFilter();
        filter1.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(ActionFoundReceiver, filter1);

        if (mBluetoothAdapter == null)
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // restart discovery if the AsyncTask hasn't run, or finished running
        if (btc == null || btc.getStatus() == AsyncTask.Status.PENDING)
            mBluetoothAdapter.startDiscovery();

        super.onResume();


    }

    @Override
    protected void onPause() {
        if (ActionFoundReceiver != null)
            unregisterReceiver(ActionFoundReceiver);
        super.onPause();
    }

    @Override
    public void onStop() {
        btc.cancel(true);
        cancel_flag = true;
        redirectFlag = true;
        isStop = true;

        // break the loop of the async task
        if (btc != null) {
            btc.setIsTaskCancelled(true);
        }

        try {
            unregisterReceiver(mybroadcast);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            unregisterReceiver(onoffReceiver);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.cancelDiscovery();
            }
            if (socket != null)
                socket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i(TAG, "-------------------onstop--------------  p724  activity");
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        rocketImage.setBackground(null);
    }

}