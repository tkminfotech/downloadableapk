package com.optum.telehealth.dal;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.bean.ClassTemperature;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.Util;

public class Temperature_db extends SQLiteOpenHelper {

	public static final String dbName = "OTPC_Temperature_v2.db";
	public final String temperatureTable = "_Temperature";

	public Cursor cursorTemperature;
	public Context context;

	public Temperature_db(Context context) {

		super(context, dbName, null, 1);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_TEMPARATURE_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ temperatureTable + "("
				+ "Temperature_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "TemperatureValue TEXT," + "LastUpdatedDate TEXT,"
				+ "Status  INTEGER," + "Patient_Id INTEGER,"
				+ " InputMode INTEGER," + " sectionDate TEXT,"
				+ " timeslot TEXT,"  + "is_reminder INTEGER DEFAULT 0" + ")";
		db.execSQL(CREATE_TEMPARATURE_NAME_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public int InsertTemperatureMeasurement(ClassTemperature temp) {
		SQLiteDatabase db = this.getWritableDatabase();
		int temp_id = 0;
		try {

			ContentValues values = new ContentValues();
			String Patientid = Constants.getdroidPatientid();

			values.put("TemperatureValue", temp.getTemperatureValue());
			values.put("LastUpdatedDate", TestDate.getCurrentTime());
			values.put("Status", 1);
			values.put("Patient_Id", Patientid);
			values.put("InputMode", temp.getInputmode());
			values.put("timeslot", temp.getTimeslot());
			values.put("sectionDate", temp.getSectionDate());
			db.insert(temperatureTable, null, values);

			cursorTemperature = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorTemperature.moveToFirst()) {
				do {

					temp_id = cursorTemperature.getInt(0);

				} while (cursorTemperature.moveToNext());
			}
			cursorTemperature.close();
			db.close();

		} catch (SQLException e) {

		}
		return temp_id;
	}

	public int InsertTemperatureMeasurement_skip(ClassTemperature temp) {
		SQLiteDatabase db = this.getWritableDatabase();
		int temp_id = 0;
		try {

			ContentValues values = new ContentValues();
			String Patientid = Constants.getdroidPatientid();

			values.put("TemperatureValue", temp.getTemperatureValue());
			values.put("LastUpdatedDate", TestDate.getCurrentTime());
			values.put("Status", 0);
			values.put("Patient_Id", Patientid);
			values.put("InputMode", temp.getInputmode());
			values.put("timeslot", temp.getTimeslot());
			values.put("sectionDate", temp.getSectionDate());
			values.put("is_reminder", 1);
			db.insert(temperatureTable, null, values);

			cursorTemperature = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorTemperature.moveToFirst()) {
				do {

					temp_id = cursorTemperature.getInt(0);

				} while (cursorTemperature.moveToNext());
			}
			cursorTemperature.close();
			db.close();

		} catch (SQLException e) {

		}
		return temp_id;
	}

	public void InsertTemperatureMeasurementService(ClassTemperature temp) {
		SQLiteDatabase db = this.getWritableDatabase();

		try {

			ContentValues values = new ContentValues();
			String Patientid = Constants.getdroidPatientid();

			values.put("TemperatureValue", temp.getTemperatureValue());
			values.put("LastUpdatedDate", temp.getMeassureDate());
			values.put("Status", 0);
			values.put("Patient_Id", Patientid);
			values.put("InputMode", temp.getInputmode());
			values.put("timeslot", temp.getTimeslot());
			values.put("sectionDate", temp.getSectionDate());
			db.insert(temperatureTable, null, values);
			db.close();

		} catch (SQLException e) {

		}
	}

	public Cursor ExecTempQuery() {
		SQLiteDatabase db = this.getReadableDatabase();
		String Patientid = Constants.getdroidPatientid();

		cursorTemperature = db.rawQuery("SELECT * FROM " + temperatureTable
				+ " where Patient_Id=" + Patientid, null);

		return cursorTemperature;
	}

	public Cursor SelectMeasuredTemperature() {
		String patientId = null;
		SharedPreferences tokenPreference = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
		patientId = tokenPreference.getString("patient_id", "-1");
		SQLiteDatabase db = this.getReadableDatabase();
		cursorTemperature = db.rawQuery("SELECT * FROM " + temperatureTable
				+ " where Status=0 and Patient_Id!=0", null);

		return cursorTemperature;

	}

	public void UpdatetemparatureData(int temparatureid) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + temperatureTable + " Set Status=1"
				+ "  WHERE Temperature_Id=" + temparatureid);
		db.close();
	}

	public void UpdatetemparatureData_as_valid(int temparatureid) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + temperatureTable + " Set Status=0"
				+ "  WHERE Temperature_Id=" + temparatureid);
		db.close();
	}

	public void delete_pulse_data(int id) {

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("DELETE  FROM " + temperatureTable
				+ " where Temperature_Id=" + id);

		dbPressure.close();

	}

	public int GetCurrentDateValues(int type, String currentTime,
			Context context) {

		// type = 1 AMPM patient ; 0 = Either AM or PM

		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		String patientidNew = Constants.getdroidPatientid();

		// String s = "SELECT MessageDate,timeslot FROM " + _Table
//		String s = "SELECT sectionDate, timeslot FROM "
//				+ temperatureTable
//				+ " where  Patient_Id="
//				+ patientidNew
//				+ " and sectionDate !='0' and TemperatureValue != '-101' and timeslot != 'AP' " +
//				"order by Temperature_Id DESC LIMIT 1";
//		cursorTemperature = dbPressure.rawQuery(s, null);


		String[] columnsToReturn = { "sectionDate", "timeslot" };
		String selection = "Patient_Id = ? AND sectionDate != ? AND TemperatureValue != ? AND timeslot != ?";
		String[] selectionArgs = { patientidNew, "0" , "-101" , "AP" }; // matched to "?" in selection
		String orderBy = "Temperature_Id DESC";
		String limit = "1";
		cursorTemperature = dbPressure.query(temperatureTable, columnsToReturn,
				selection, selectionArgs, null, null, orderBy,limit);

		cursorTemperature.moveToFirst();

		while (cursorTemperature.isAfterLast() == false) {

			DatefromDB = cursorTemperature.getString(0).substring(0, 10); // get
																			// the
																			// last
																			// weight
																			// reading
																			// date
			timeslot = cursorTemperature.getString(1);

			TimeFromDB = cursorTemperature.getString(0).substring(
					cursorTemperature.getString(0).length() - 2,
					cursorTemperature.getString(0).length()); // get the last
																// weight
																// reading time
																// (AM or PM)
			cursorTemperature.moveToNext();

		}
		cursorTemperature.close();

		String patient_time = Util.get_patient_time_zone_time(context);
		patient_time = patient_time.substring(0, 10);

		// SimpleDateFormat dateFormat1 = new
		// SimpleDateFormat("MM/dd/yyyy");
		// Date date1 = new Date();
		Log.e("LOg Temperature>>>>", "" + type);
		if (type == 0) // AM or PM
		{

			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																			// current
				// at with last
				// massurement date
				no = 1;
			else
				no = 0;
		} else {
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
				if (currentTime.trim().equals(timeslot)) {
					no = 1;
				} else {
					no = 0;
				}
			} else {
				no = 0;
			}

			/*
			 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
			 * }else { no=0; }
			 */
		}

		dbPressure.close();

		return no;

	}

}
