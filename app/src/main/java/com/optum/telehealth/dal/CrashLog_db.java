package com.optum.telehealth.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.optum.telehealth.bean.CrashLog;
import java.sql.SQLException;

/**
 * Created by tkmif-ws011 on 5/11/2016.
 */
public class CrashLog_db {

    public static final String dbName = "CrashLog.db";
    public static final String _Table = "tpm_CrashLog";

    public Cursor cursorCrashLog;
    private final Context mCtx;

    private DatabaseHelper mDbHelper;
    private SQLiteDatabase dbCrashLog;

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {

            super(context, dbName, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            String CREATE_msg_TABLE = "CREATE TABLE IF NOT EXISTS " + _Table
                    + "(" + "CrashLog_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + " Patient_Id TEXT," + " Device_type TEXT,"
                    + " Android_version  TEXT," + " Application_version TEXT,"
                    + " Crash_details TEXT," + " Exception_time TEXT," + " Status int" + ")";
            db.execSQL(CREATE_msg_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

    }

    public CrashLog_db open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        closeOpenWTdbConnection();
        mDbHelper = new DatabaseHelper(mCtx);
        dbCrashLog = mDbHelper.getWritableDatabase();
        return this;

    }

    public CrashLog_db(Context ctx) {
        this.mCtx = ctx;
    }

    public void closeOpenWTdbConnection() {
        mDbHelper.close();
    }

    public String InsertCrashLog(CrashLog crashLog) {

        try {
            open();
        } catch (SQLException e) {
            // e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put("Patient_Id", crashLog.getPatient_id());
        values.put("Device_type", crashLog.getDevice_type());
        values.put("Android_version", crashLog.getAndroid_version());
        values.put("Application_version", crashLog.getApplication_version());
        values.put("Crash_details", crashLog.getCrash_details());
        values.put("Exception_time", crashLog.getException_time());
        values.put("Status", 0);

        dbCrashLog.insert(_Table, null, values);
        String crashLog_id = "0";
        cursorCrashLog = dbCrashLog.rawQuery("SELECT last_insert_rowid()", null);

        if (cursorCrashLog.moveToFirst()) {
            do {

                crashLog_id = ""+cursorCrashLog.getInt(0);

            } while (cursorCrashLog.moveToNext());
        }
        cursorCrashLog.close();

        closeOpenWTdbConnection();

        return crashLog_id;

    }





    public Cursor SelectCrashLog(String patientId) {
        try {
            open();
        } catch (SQLException e) {
            // e.printStackTrace();
        }
        // Cursor c = db.query(msg_Table, null, null, null, null, null, null);

        String[] columnsToReturn = { "CrashLog_Id", "Patient_Id","Device_type",
                "Android_version","Application_version", "Crash_details","Exception_time", "Status" };
        String selection = "Patient_Id = ?";
        String[] selectionArgs = { patientId }; // matched to "?" in selection
        cursorCrashLog = dbCrashLog.query(_Table, columnsToReturn, selection, selectionArgs, null, null, null);


//        cursorCrashLog = dbCrashLog.rawQuery("SELECT * FROM " + _Table
//                + " where Patient_Id="+patientId, null);

        return cursorCrashLog;

    }

    public void UpdateCrashLogStatus(int crashLog_Id) {
        try {
            open();
            // db.delete(msg_Table, null, null);
            dbCrashLog.execSQL("UPDATE  " + _Table + " Set Status=1"
                    + "  WHERE CrashLog_Id=" + crashLog_Id);
            closeOpenWTdbConnection();
        } catch (SQLException e) {
            // e.printStackTrace();
        }
    }



    public void delete_CrashLog_ById(String id) {

        try {

            open();
            dbCrashLog.execSQL("DELETE  FROM " + _Table + " where CrashLog_Id=" + id);
            closeOpenWTdbConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
