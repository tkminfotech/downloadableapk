package com.optum.telehealth.dal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.bean.ClasssPressure;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.Util;

public class Pressure_db extends SQLiteOpenHelper {

	public static final String dbName = "OPTC_Pressure_v2.db";
	public static final String dmp_Table = "_Pressure1";
	public Cursor cursorPreesure;
public Context context;
	public Pressure_db(Context context) {
		super(context, dbName, null, 1);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase dbPressure) {

		/*
		 * String CREATE_pressure_TABLE =
		 * "CREATE TABLE IF NOT EXISTS "+dmp_Table+"(PressureId " +
		 * "INTEGER PRIMARY KEY AUTOINCREMENT,PatientId INTEGER,Systolic INTEGER,Diastolic INTEGER"
		 * + "Pulse INTEGER,MeasureDate TEXT,Status INTEGER)";
		 */

		String CREATE_pressure_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ dmp_Table + "("
				+ "PressureId INTEGER PRIMARY KEY AUTOINCREMENT," + ""
				+ "PatientId INTEGER," + "Systolic INTEGER,"
				+ "Diastolic INTEGER" + "," + "Pulse INTEGER" + ","
				+ "MeasureDate TEXT" + "," + "Status INTEGER,"
				+ "InputMode INTEGER," + " sectionDate TEXT," + "timeslot TEXT,"
				+ "prompt_flag TEXT," + "body_movment TEXT," + "irregular_pulse TEXT,"
				+ "battery_level TEXT," + "measurement_position_flag TEXT,"
				+ "cuff_fit_flag TEXT,"  + "is_reminder INTEGER DEFAULT 0" + ")";

		dbPressure.execSQL(CREATE_pressure_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

	@SuppressLint("SimpleDateFormat")
	public int InsertPressure(ClasssPressure pressure) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa", Locale.US);
		String currentDateandTime = dateFormat.format(new Date());

		ContentValues values = new ContentValues();
		values.put("PatientId", pressure.getPatient_Id());
		values.put("Systolic", pressure.getSystolic());
		values.put("Diastolic", pressure.getDiastolic());
		values.put("Pulse", pressure.getPulse());
		values.put("MeasureDate", TestDate.getCurrentTime());
		values.put("InputMode", pressure.getInputmode());
		values.put("timeslot", pressure.getTimeslot());
		values.put("sectionDate", pressure.getSectionDate());
		values.put("prompt_flag", pressure.getPrompt_flag());
		values.put("Status", 1);
		values.put("body_movment", pressure.getBody_movment());
		values.put("irregular_pulse", pressure.getIrregular_pulse());
		values.put("battery_level", pressure.getBattery_level());
		values.put("measurement_position_flag", pressure.getMeasurement_position_flag());
		values.put("cuff_fit_flag", pressure.getCuff_fit_flag());
		//Log.i(dmp_Table,
		//		"values inserted:-" + pressure.getPatient_Id()
		//				+ pressure.getSystolic() + pressure.getDiastolic()
		//				+ pressure.getPulse() + currentDateandTime);

		// dbPressure.insert("ConfigurationSettings", null, values_Settings);

		dbPressure.insert(dmp_Table, null, values);
		int pressure_id = 0;
		cursorPreesure = dbPressure
				.rawQuery("SELECT last_insert_rowid()", null);

		if (cursorPreesure.moveToFirst()) {
			do {

				pressure_id = cursorPreesure.getInt(0);

			} while (cursorPreesure.moveToNext());
		}
		cursorPreesure.close();

		dbPressure.close();

		return pressure_id;

	}

	@SuppressLint("SimpleDateFormat")
	public int InsertPressure_skip(ClasssPressure pressure) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa", Locale.US);
		String currentDateandTime = dateFormat.format(new Date());

		ContentValues values = new ContentValues();
		values.put("PatientId", pressure.getPatient_Id());
		values.put("Systolic", pressure.getSystolic());
		values.put("Diastolic", pressure.getDiastolic());
		values.put("Pulse", pressure.getPulse());
		values.put("MeasureDate", TestDate.getCurrentTime());
		values.put("InputMode", pressure.getInputmode());
		values.put("timeslot", pressure.getTimeslot());
		values.put("sectionDate", pressure.getSectionDate());
		values.put("prompt_flag", pressure.getPrompt_flag());
		values.put("Status", 0);
		values.put("body_movment", pressure.getBody_movment());
		values.put("irregular_pulse", pressure.getIrregular_pulse());
		values.put("battery_level", pressure.getBattery_level());
		values.put("measurement_position_flag", pressure.getMeasurement_position_flag());
		values.put("cuff_fit_flag", pressure.getCuff_fit_flag());
		values.put("is_reminder", 1);
		//Log.i(dmp_Table,
		//		"values inserted:-" + pressure.getPatient_Id()
		//				+ pressure.getSystolic() + pressure.getDiastolic()
		//				+ pressure.getPulse() + currentDateandTime);

		// dbPressure.insert("ConfigurationSettings", null, values_Settings);

		dbPressure.insert(dmp_Table, null, values);
		int pressure_id = 0;
		cursorPreesure = dbPressure
				.rawQuery("SELECT last_insert_rowid()", null);

		if (cursorPreesure.moveToFirst()) {
			do {

				pressure_id = cursorPreesure.getInt(0);

			} while (cursorPreesure.moveToNext());
		}
		cursorPreesure.close();

		dbPressure.close();

		return pressure_id;

	}

	public Cursor SelectDBValues(String query) {
		SQLiteDatabase dbPressure = this.getReadableDatabase();
		cursorPreesure = dbPressure.rawQuery(query, null);
		return cursorPreesure;
	}

	public Cursor ExecQuery() {
		SQLiteDatabase dbPressure = this.getReadableDatabase();

		String Patientid = Constants.getdroidPatientid();

		cursorPreesure = dbPressure.rawQuery("SELECT * FROM " + dmp_Table
				+ " where PatientId=" + Patientid, null);

		// cursorPreesure = dbPressure.query(dmp_Table, null, null, null,null,
		// null, null);
		return cursorPreesure;
	}

	public Cursor SelectMeasuredpressure() {
		String patientId = null;
		Log.i("SelectMeasuredpressure","");
		SharedPreferences tokenPreference = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
		patientId = tokenPreference.getString("patient_id", "-1");
		Log.i("SelectMeasuredpressure","patientId=>"+patientId);
		SQLiteDatabase dbPressure = this.getReadableDatabase();
		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
		cursorPreesure = dbPressure.rawQuery("SELECT * FROM " + dmp_Table
				+ " where Status=0 and PatientId!=0", null);

		return cursorPreesure;

	}

	public void UpdatepressureData(int pressureid) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("UPDATE  " + dmp_Table + " Set Status=1"
				+ "  WHERE PressureId=" + pressureid);
		dbPressure.close();
	}

	public void delete_pressure_data(int id) {

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("DELETE  FROM " + dmp_Table + " where PressureId="
				+ id);

		dbPressure.close();

	}

	public void UpdatepressureData_as_valid(int pressureid) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("UPDATE  " + dmp_Table + " Set Status=0"
				+ "  WHERE PressureId=" + pressureid);
		dbPressure.close();
	}

	public int GetCurrentDateValues(int type, String currentTime,Context context) {

		// type = 1 AMPM patient ; 0 = Either AM or PM

		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		String patientidNew = Constants.getdroidPatientid();

		// String s = "SELECT MessageDate,timeslot FROM " + _Table
//		String s = "SELECT sectionDate,timeslot FROM " + dmp_Table
//				+ " where  PatientId=" + patientidNew
//				+ " and sectionDate !='0' and Systolic != '-101' " +
//				"and timeslot != 'AP' order by PressureId DESC LIMIT 1";
//		cursorPreesure = dbPressure.rawQuery(s, null);


		String[] columnsToReturn = { "sectionDate", "timeslot" };
		String selection = "PatientId = ? AND sectionDate != ? AND Systolic != ? AND timeslot != ?";
		String[] selectionArgs = { patientidNew, "0" , "-101" , "AP" }; // matched to "?" in selection
		String orderBy = "PressureId DESC";
		String limit = "1";
		cursorPreesure = dbPressure.query(dmp_Table, columnsToReturn,
				selection, selectionArgs, null, null, orderBy,limit);


		cursorPreesure.moveToFirst();

		while (cursorPreesure.isAfterLast() == false) {

			DatefromDB = cursorPreesure.getString(0).substring(0, 10); // get
																		// the
																		// last
																		// weight
																		// reading
																		// date
			timeslot = cursorPreesure.getString(1);

			TimeFromDB = cursorPreesure.getString(0).substring(
					cursorPreesure.getString(0).length() - 2,
					cursorPreesure.getString(0).length()); // get the last
															// weight
															// reading time
															// (AM or PM)
			cursorPreesure.moveToNext();

		}
		cursorPreesure.close();

		String patient_time = Util.get_patient_time_zone_time(context);
		patient_time = patient_time.substring(0, 10);

		// SimpleDateFormat dateFormat1 = new
		// SimpleDateFormat("MM/dd/yyyy");
		// Date date1 = new Date();
		Log.e("LOg Pressure>>>>", ""+type);
		if (type == 0) // AM or PM
		{

			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																			// current
				// at with last
				// massurement date
				no = 1;
			else
				no = 0;
		} else {
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
				if (currentTime.trim().equals(timeslot)) {
					no = 1;
				} else {
					no = 0;
				}
			} else {
				no = 0;
			}

			/*
			 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
			 * }else { no=0; }
			 */
		}

		dbPressure.close();

		return no;

	}

}
