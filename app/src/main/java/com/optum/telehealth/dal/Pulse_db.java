package com.optum.telehealth.dal;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.bean.ClassMeasurement;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.Util;

public class Pulse_db extends SQLiteOpenHelper {

	public static final String dbName = "OPTC_Pulse_v2.db";
	public final String oxygen_Table = "Droid_Measurement_BloodOxygen";
	public final String pressure_Table = "Droid_Measurement_pressure";

	public Cursor cursorMeasurement;
public Context context;
	public Pulse_db(Context context) {
		super(context, dbName, null, 1);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_OXYGEN_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ oxygen_Table + "("
				+ "Measurement_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "oxygenValue INTEGER," + "PressureValue INTEGER,"
				+ "LastUpdatedDate TEXT," + "Status  INTEGER,"
				+ "Patient_Id INTEGER," + " InputMode int,"
				+ " sectionDate TEXT," + " timeslot TEXT," + "is_reminder INTEGER DEFAULT 0" + ")";
		db.execSQL(CREATE_OXYGEN_NAME_TABLE);

		/*
		 * CREATE TABLE IF NOT EXISTS oxygen_Table ( Measurement_Id INTEGER
		 * PRIMARY KEY AUTOINCREMENT, oxygenValue intger,PressureValue intger,
		 * LastUpdatedDate TEXT,Status intger,Patient_Id intger)
		 */

		String CREATE_Pressure_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ pressure_Table + "(" + "Pressure_Reade_Id int,"
				+ "Value int," + "LastUpdatedDate TEXT," + "Status  int" + ")";
		db.execSQL(CREATE_Pressure_NAME_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public int InsertOxymeterMeasurement(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		int pulse_id = 0;
		try {

			ContentValues values = new ContentValues();
			String Patientid = Constants.getdroidPatientid();

			values.put("oxygenValue", measurement.getOxygen_Value());
			values.put("PressureValue", measurement.getPressure_Value());
			values.put("LastUpdatedDate", TestDate.getCurrentTime());
			values.put("Status", 1);
			values.put("Patient_Id", Patientid);
			values.put("timeslot", measurement.getTimeslot());
			values.put("sectionDate", measurement.getSectionDate());
			values.put("InputMode", measurement.getInputmode());
			db.insert(oxygen_Table, null, values);

			cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorMeasurement.moveToFirst()) {
				do {

					pulse_id = cursorMeasurement.getInt(0);

				} while (cursorMeasurement.moveToNext());
			}
			cursorMeasurement.close();

			db.close();

		} catch (SQLException e) {

		}
		return pulse_id;

	}

	public int InsertOxymeterMeasurement_skip(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		int pulse_id = 0;
		try {

			ContentValues values = new ContentValues();
			String Patientid = Constants.getdroidPatientid();

			values.put("oxygenValue", measurement.getOxygen_Value());
			values.put("PressureValue", measurement.getPressure_Value());
			values.put("LastUpdatedDate", TestDate.getCurrentTime());
			values.put("Status", 0);
			values.put("Patient_Id", Patientid);
			values.put("timeslot", measurement.getTimeslot());
			values.put("sectionDate", measurement.getSectionDate());
			values.put("InputMode", measurement.getInputmode());
			values.put("is_reminder", 1);
			db.insert(oxygen_Table, null, values);

			cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorMeasurement.moveToFirst()) {
				do {

					pulse_id = cursorMeasurement.getInt(0);

				} while (cursorMeasurement.moveToNext());
			}
			cursorMeasurement.close();

			db.close();

		} catch (SQLException e) {

		}
		return pulse_id;

	}

	public void InsertOxymeterMeasurementService(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();

		try {

			ContentValues values = new ContentValues();
			String Patientid = Constants.getdroidPatientid();

			values.put("oxygenValue", measurement.getOxygen_Value());
			values.put("PressureValue", measurement.getPressure_Value());
			values.put("LastUpdatedDate", measurement.getMeassure_Date());
			values.put("timeslot", measurement.getTimeslot());
			values.put("Status", measurement.getStatus());
			values.put("Patient_Id", Patientid);
			db.insert(oxygen_Table, null, values);
			db.close();

		} catch (SQLException e) {

		}
	}

	public void InsertPressureMeasurement(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put("Pressure_Reade_Id", measurement.getPressure_Read_Id());
		values.put("Value", measurement.getValue());
		values.put("LastUpdatedDate", measurement.last_Update_Date);
		values.put("Status", measurement.getStatus());

		db.insert(pressure_Table, null, values);
		db.close();

	}

	public Cursor ExecQuery() {
		SQLiteDatabase db = this.getReadableDatabase();
		String Patientid = Constants.getdroidPatientid();

		cursorMeasurement = db.rawQuery("SELECT * FROM " + oxygen_Table
				+ " where Patient_Id=" + Patientid, null);

		// cursorMeasurement = dbMeasuremnt.query(oxygen_Table, null, null,
		// null,null, null, null);
		return cursorMeasurement;
	}

	public void Deletemeasurement() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(oxygen_Table, null, null);
		db.close();
	}

	public Cursor SelectMeasuredData() {
		String patientId = null;

		SharedPreferences tokenPreference = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
		patientId = tokenPreference.getString("patient_id", "-1");
		SQLiteDatabase db = this.getReadableDatabase();
		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
		cursorMeasurement = db.rawQuery("SELECT * FROM " + oxygen_Table
				+ " where Status=0 and Patient_Id!=0", null);

		return cursorMeasurement;

	}

	public void UpdateMeasureData(int Measurement_Id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + oxygen_Table + " Set Status=1"
				+ "  WHERE Measurement_Id=" + Measurement_Id);
		db.close();

	}

	public void UpdateMeasureData_as_valid(int Measurement_Id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + oxygen_Table + " Set Status=0"
				+ "  WHERE Measurement_Id=" + Measurement_Id);
		db.close();

	}

	public void delete_pulse_data(int id) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("DELETE  FROM " + oxygen_Table
				+ " where Measurement_Id=" + id);

		dbPressure.close();

	}

	public int GetCurrentDateValues(int type, String currentTime,
			Context context) {

		// type = 1 AMPM patient ; 0 = Either AM or PM

		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		String patientidNew = Constants.getdroidPatientid();

		// String s = "SELECT MessageDate,timeslot FROM " + _Table
//		String s = "SELECT sectionDate, timeslot FROM "
//				+ oxygen_Table
//				+ " where  Patient_Id="
//				+ patientidNew
//				+ " and sectionDate !='0' and oxygenValue != '-101' " +
//				"and timeslot != 'AP' order by Measurement_Id DESC LIMIT 1";
//		cursorMeasurement = dbPressure.rawQuery(s, null);


		String[] columnsToReturn = { "sectionDate", "timeslot" };
		String selection = "Patient_Id = ? AND sectionDate != ? AND oxygenValue != ? AND timeslot != ?";
		String[] selectionArgs = { patientidNew, "0" , "-101" , "AP" }; // matched to "?" in selection
		String orderBy = "Measurement_Id DESC";
		String limit = "1";
		cursorMeasurement = dbPressure.query(oxygen_Table, columnsToReturn,
				selection, selectionArgs, null, null, orderBy,limit);

		cursorMeasurement.moveToFirst();
		while (cursorMeasurement.isAfterLast() == false) {

			DatefromDB = cursorMeasurement.getString(0).substring(0, 10); // get
																			// the
																			// last
																			// weight
																			// reading
																			// date
			timeslot = cursorMeasurement.getString(1);

			TimeFromDB = cursorMeasurement.getString(0).substring(
					cursorMeasurement.getString(0).length() - 2,
					cursorMeasurement.getString(0).length()); // get the last
																// weight
																// reading time
																// (AM or PM)
			cursorMeasurement.moveToNext();

		}
		cursorMeasurement.close();

		String patient_time = Util.get_patient_time_zone_time(context);
		patient_time = patient_time.substring(0, 10);

		// SimpleDateFormat dateFormat1 = new
		// SimpleDateFormat("MM/dd/yyyy");
		// Date date1 = new Date();
		Log.e("LOg Temperature>>>>", "" + type);
		if (type == 0) // AM or PM
		{

			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																			// current
				// at with last
				// massurement date
				no = 1;
			else
				no = 0;
		} else {
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
				if (currentTime.trim().equals(timeslot)) {
					no = 1;
				} else {
					no = 0;
				}
			} else {
				no = 0;
			}

			/*
			 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
			 * }else { no=0; }
			 */
		}

		dbPressure.close();

		return no;

	}

}
