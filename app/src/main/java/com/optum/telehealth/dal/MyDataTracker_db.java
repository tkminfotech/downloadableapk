package com.optum.telehealth.dal;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.UploadTrackerDetails;


/**
 * Created by tkmif-ws011 on 10/19/2016.
 */
public class MyDataTracker_db extends SQLiteOpenHelper {

    public static final String dbName = "Mydata_Tracker_v2.db";
    public static final String myDataTracker_Table = "Droid_MydataTrackerDetails";

    private static final String TAG = "MyDataTrackerDb Sierra";
    public Cursor cursorsensor;
    Context ctx;

    public MyDataTracker_db(Context context) {
        super(context, dbName, null, 1);
        ctx = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_MydataSensor_TABLE = "CREATE TABLE IF NOT EXISTS "
                + myDataTracker_Table + "("
                + "Tracker_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "Patient_Id int," + "Date_Time TEXT," + "Device TEXT,"
                + "OS TEXT," + "Version TEXT," + "Page TEXT,"
                + "Action TEXT," + "Status int" + ")";
        db.execSQL(CREATE_MydataSensor_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void delete() {
        Log.i(TAG, "Deleted sucesfully");
        SQLiteDatabase db_sensor = this.getWritableDatabase();
        db_sensor.delete(myDataTracker_Table, null, null);
        db_sensor.close();

    }


    public void insertMyDataTracker(ContentValues values) {
        long insertId = -1;
        SQLiteDatabase db_sensor = this.getWritableDatabase();
        insertId = db_sensor.insert(myDataTracker_Table, null, values);
        db_sensor.close();
        Cursor tracker = SelectAllMyDataTracker();
        int count = tracker.getCount();
        SharedPreferences clusterSsettings = ctx.getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
        int analyticCount = clusterSsettings.getInt("AnalyticCount",5);
        if(count >= analyticCount){

            SharedPreferences settings = ctx.getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);

            String serviceUrl = settings.getString("Server_post_url", "-1");
            new UploadTrackerDetails(ctx, serviceUrl).execute();
        }

        tracker.close();
        // Log.i(TAG, "inserted succesfully");
    }


    public Cursor SelectAllMyDataTracker() {
        SQLiteDatabase db_sensor = this.getReadableDatabase();
        cursorsensor = db_sensor.rawQuery("SELECT * FROM " + myDataTracker_Table, null);

        return cursorsensor;
    }

//    public long UpdateTrackEvent(int trackerId, int value) {
//        long updateId = 0;
//        SQLiteDatabase db_sensor = this.getWritableDatabase();
//        try {
//
//            db_sensor.execSQL("UPDATE  " + myDataTracker_Table + " Set Value=" + value
//                    + "  WHERE Tracker_Id=" + trackerId);
//            db_sensor.close();
//        } catch (Exception e) {
//            updateId = -1;
//            db_sensor.close();
//        }
//        return updateId;
//    }

//    public long GetMatchingTrackEvent(ContentValues values) {
//        long isSuccess = -1;
//        SQLiteDatabase db_sensor = this.getReadableDatabase();
//
//        int trackId=0;
//        int value = 0;
//        Cursor cursor = db_sensor.rawQuery("SELECT * " +
//                "FROM " + myDataTracker_Table + " WHERE Vital='" + values.get("Vital")
//                + "' AND Mode='" + values.get("Mode")
//                + "' AND Patient_Id='" + values.get("Patient_Id")
//                + "' AND Version='" + values.get("Version")
//                + "' AND Event='" + values.get("Event") + "'", null);
//        if (cursor.getCount() > 0) {
//            if (cursor.moveToFirst()) {
//
//                trackId = cursor.getInt(0);
//                value = cursor.getInt(9);
//            }
//            isSuccess = UpdateTrackEvent(trackId, value + 1);
//        } else {
//
//            isSuccess = insertMyDataTracker(values);
//        }
//        cursor.close();
//        db_sensor.close();
//
//        return isSuccess;
//    }
}
