package com.optum.telehealth.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.bean.ClassAdvice;
import com.optum.telehealth.bean.State;
import com.optum.telehealth.util.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tkmif-ws011 on 2/22/2017.
 */

public class State_db extends SQLiteOpenHelper {

    private static final String TAG = "StateDb";
    public static final String dbName = "DroidDB_State.db";
    public static final String state_Table = "Droid_State";



    public State_db(Context context) {

        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_msg_TABLE = "CREATE TABLE IF NOT EXISTS " + state_Table
                + "(" + "State_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "State_Name TEXT," + "IdFromServer int)";
        db.execSQL(CREATE_msg_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        db.execSQL("DROP TABLE IF EXISTS " + state_Table);
    }


    public void clearTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(state_Table, null, null);
        db.close();
    }

    public void insertState(List<State> stateList) {
        SQLiteDatabase db = this.getWritableDatabase();

        if (stateList.size() > 0) {

            for (int i = 0; i < stateList.size(); i++) {
                State state = stateList.get(i);
                ContentValues values = new ContentValues();
                values.put("State_Name", state.getStateName());
                values.put("IdFromServer", state.getStateId());
                db.insert(state_Table, null, values);
            }
        }
        db.close();

    }

    public List<State> getAllStates() {
        List<State> stateList = new ArrayList<State>();
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "SELECT * FROM " + state_Table;
        Cursor cursor = db.rawQuery(Query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                State state = new State();
                state.setStateName(cursor.getString(1));
                state.setStateId(cursor.getInt(2));
                stateList.add(state);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return contact list
        return stateList;
    }

}
