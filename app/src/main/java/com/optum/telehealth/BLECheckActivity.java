package com.optum.telehealth;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassMeasurement;
import com.optum.telehealth.dal.Pulse_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

@SuppressLint("NewApi")
public class BLECheckActivity extends Titlewindow{
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    BluetoothManager manager;
    private String pageName = "Blood Oxygen Sensor Page";
    BluetoothGatt mBluetoothGatt;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothDevice deviceL;
    TextView txtwelcom, txtReading;
    ImageView rocketImage;
    AnimationDrawable rocketAnimation;
    ObjectAnimator AnimPlz, ObjectAnimator;
    int pulseId;
    Pulse_db dbcreate1 = new Pulse_db(this);
    private String result = "0, 0";
    int flag = 0;
    int a, b;
    private GlobalClass appClass;
    String print, print1;
    TextView text;
    String macAddress = "";
    private TextView txt_errorMsg;
    public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
            .fromString("46A970E0-0D5F-11E2-8B5E-0002A5D5C51B");
    public static final int REQUEST_DISCOVERABLE_CODE = 42;
    boolean redirectFlag = false, mScanning = false;
    String TAG = "BLECheckActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aand_dreader);

        appClass = (GlobalClass) getApplicationContext();
        appClass.isSupportEnable = false;
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);

        Button btnmanual = (Button) findViewById(R.id.aAndDManuval);

        txtwelcom = (TextView) findViewById(R.id.txtwelcome);
        txtReading = (TextView) findViewById(R.id.takereading);

        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");

        txtwelcom.setTypeface(type, Typeface.NORMAL);
        txtReading.setTypeface(type, Typeface.NORMAL);

        rocketImage = (ImageView) findViewById(R.id.loading);
        try {
            rocketImage.setBackgroundResource(R.drawable.oxygen_animation);
            rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
            rocketAnimation.start();
            rocketImage.setVisibility(View.INVISIBLE);
            txtReading.setVisibility(View.INVISIBLE);
        }catch (OutOfMemoryError ex){
           Log.i("exception",ex.toString());
            rocketImage.setBackgroundResource(R.drawable.oxymeter0027);
        }

        setwelcome();
        Animations();

        manager = (BluetoothManager) getBaseContext().getSystemService(
                Context.BLUETOOTH_SERVICE);

        mBluetoothAdapter = manager.getAdapter();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        if (extras.getString("macaddress").length() > 1) {
            macAddress = extras.getString("macaddress");
        }
        // Log.i("Optum", "Optum : LE macAddress " + macAddress);
        btnmanual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                appClass.appTracking(pageName,"Click on Manual entry button");
                Intent intent = new Intent(BLECheckActivity.this,
                        PulseEntry.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void BTDiscoverable() {
        // Register for broadcasts on BluetoothAdapter state change

        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(onoffReceiver, filter1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            search();
        } else {
            BluetoothAdapter BA;
            BA = BluetoothAdapter.getDefaultAdapter();
            if (BA.getScanMode() !=
                    BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                turnOn.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
                startActivityForResult(turnOn, REQUEST_DISCOVERABLE_CODE);
            } else {
                search();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_DISCOVERABLE_CODE) {
            if (resultCode == RESULT_CANCELED) {
                errorShow(this.getString(R.string.connectivityError));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!redirectFlag) {
                            Intent intentwt = new Intent(getApplicationContext(),
                                    PulseEntry.class);
                            startActivity(intentwt);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }
                }, 4000);
            } else {
                search();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("ss", "coarse location permission granted");
                    search();
                } else {
                    Intent intentwt = new Intent(getApplicationContext(),
                            PulseEntry.class);
                    startActivity(intentwt);
                    overridePendingTransition(0, 0);
                    finish();
                }
                return;
            }
        }
    }

    private void setwelcome() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }

    private void Animations() {
        txtwelcom.setText(this.getString(R.string.enterpulsebluetooth));
        AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, 0f);
        //AnimPlz.setDuration(1000);
        AnimPlz.start();
        AnimPlz.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                Log.e("", "AnimPlz");
                txtReading.setVisibility(View.VISIBLE);
                rocketImage.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i("Optum", "Optum KEYCODE_BACK clicked:");
//
//            Intent intent = new Intent(this, Home.class);
//            startActivity(intent);
//            finish();
//        }
        return super.onKeyDown(keyCode, event);
    }

    public void search() {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
        scanLeDevice();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (flag == 0) {
                    search();
                }
            }
        }, 18000);
    }

    private void scanLeDevice() {
        // Stops scanning after a pre-defined scan period.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mScanning) {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(vLeScanCallback);
                }
            }
        }, 15000);
        mScanning = true;
        mBluetoothAdapter.startLeScan(vLeScanCallback);
    }

    private BluetoothAdapter.LeScanCallback vLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("Foundddd", device.toString());
                            deviceL = device;
                            if (device.toString().toUpperCase().equals(macAddress.toUpperCase())) {
                                mBluetoothAdapter.stopLeScan(vLeScanCallback);
                                connect();
                            }
                        }
                    });
                }
            };


    public void save(String result) {
        flag = 1;
        SharedPreferences flow = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);
        int val = flow.getInt("flow", 0); // #1
        if (val == 1) {
            SharedPreferences section = getSharedPreferences(
                    CommonUtilities.PREFS_NAME_date, 0);
            SharedPreferences.Editor editor_retake = section.edit();
            editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
            editor_retake.commit();
        }
        int oxyreading = 0, HeartRate = 0;
        String[] separated = result.split(",");
        oxyreading = Integer.parseInt(separated[0].trim());
        HeartRate = Integer.parseInt(separated[1].trim());

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MM/dd/yyyy hh:mm:ss aa", Locale.US);
        String currentDateandTime = dateFormat
                .format(new Date());
        ClassMeasurement measurement = new ClassMeasurement();
        measurement.setStatus(0);
        measurement.setOxygen_Value(Integer
                .parseInt(separated[0].trim()));
        measurement.setLast_Update_Date(currentDateandTime);
        measurement.setPressure_Value(Integer
                .parseInt(separated[1].trim()));
        measurement.setInputmode(0);
        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.PREFS_NAME_date, 0);
        String sectiondate = settings1.getString("sectiondate", "0");

        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.USER_TIMESLOT_SP, 0);
        String slot = settings.getString("timeslot", "AM");

        measurement.setSectionDate(sectiondate);
        measurement.setTimeslot(slot);

        if ((measurement.getOxygen_Value() + "").trim()
                .length() > 1) {
            pulseId = dbcreate1
                    .InsertOxymeterMeasurement(measurement);
        }
        Intent intent = new Intent(BLECheckActivity.this,
                ShowPulseActivity.class);
        intent.putExtra("pulse", oxyreading + "");
        intent.putExtra("pulseid", pulseId);
        startActivity(intent);


    }

    /**********************************************************************************************/
    @SuppressLint("NewApi")
    private void connect() {
        mBluetoothGatt = deviceL.connectGatt(getBaseContext(), false,
                mGattCallback);
    }

    @SuppressLint("NewApi")
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                            int newState) {
            // String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i("zzzzzzz", "Attempting to start service discovery:"
                        + mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i("zzzzzzzzz", "Disconnected from GATT server.");
            }

        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            if (status == BluetoothGatt.GATT_SUCCESS) {
                try {
                    BluetoothGattService service = mBluetoothGatt
                            .getService(UUID_HEART_RATE_MEASUREMENT);
                    Log.e("zzzzzzsssz", "uiiiid" + service.getUuid().toString());
                    update(service);
                } catch (NullPointerException e) {
                    Log.e("zzzzzzz", "no uiiiid");
                }
            } else {
                Log.w("zzzzzzzz", "onServicesDiscovered received: " + status);
            }
        }

        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            // TODO Auto-generated method stub
            broadcastUpdate(characteristic);
            super.onCharacteristicChanged(gatt, characteristic);
        }

        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(characteristic);
                Log.w("zzzzzzzz", "characteristic sent");
            }
        }
    };

    public void update(BluetoothGattService service) {

        List<BluetoothGattCharacteristic> list = service.getCharacteristics();

        for (int i = 0; i < list.size(); i++) {
            Log.e("charactaristic", list.get(i).toString());
            mBluetoothGatt.readCharacteristic(list.get(i));

            if (list.get(i)
                    .getUuid()
                    .equals(UUID
                            .fromString("0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b"))) {
                mBluetoothGatt.setCharacteristicNotification(list.get(i), true);
                BluetoothGattDescriptor descriptor = list
                        .get(i)
                        .getDescriptor(
                                UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
                descriptor
                        .setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                mBluetoothGatt.writeDescriptor(descriptor);
            }

        }
    }

    private void broadcastUpdate(
            final BluetoothGattCharacteristic characteristic) {

        byte[] data = characteristic.getValue();
        if (data != null && data.length > 0) {
            StringBuilder stringBuilder = new StringBuilder("");
            StringBuilder stringBuilder2 = new StringBuilder("");

            Byte PalseRate = (Byte) data[9];
            Byte spo = (Byte) data[7];

            stringBuilder.append(spo.intValue() + " ");
            stringBuilder2.append(PalseRate.intValue() + " ");


            print = stringBuilder.toString();
            print1 = stringBuilder2.toString();
            a = spo.intValue();
            b = PalseRate.intValue();

            Log.d("sa", print);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    result = print + ", " + print1;
                    if (flag == 0) {

                        if (a > 0 && b > 0) {

                            save(result);
                        }
                    }
                }
            });
        }
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        timer.cancel();

                    }
                });

            }
        }, 3000, 3000);
    }

    private void CheckBlueToothState() {
        Log.d(TAG, "IN #CheckBlueToothState()");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
        String bluetooth_error  = ERROR_MSG.getString("bluetooth_error", "-1");
        if (mBluetoothAdapter == null) {
            errorShow(bluetooth_error);
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                /*mBluetoothAdapter.disable();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBluetoothAdapter.enable();
                    }
                }, 2000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "CALL #getPairedDevices()");*/
                BTDiscoverable();
                    /*}
                }, 4000);*/
            } else {
                try {
                    errorShow(bluetooth_error);
                    mBluetoothAdapter.enable();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "CALL #getPairedDevices()");
                            if (mBluetoothAdapter.isDiscovering()) {
                                mBluetoothAdapter.cancelDiscovery();
                            }
                            BTDiscoverable();
                        }
                    }, 2000);
                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private final BroadcastReceiver onoffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (!mBluetoothAdapter.isEnabled()) {
                                        if (!redirectFlag) {
                                            CheckBlueToothState();
                                        } else {
                                            unregisterReceiver(onoffReceiver);
                                        }
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }, 5000);
                        break;
                }
            }
        }
    };

    @Override
    protected void onResume() {
        try {
            redirectFlag = false;
            flag = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);

                    // enable location if on Android 7 or greater and the location service is turned off
                    // for Android 7, the location service must be turned on for the device to be able to LE scan and receive scan results
                } else if (Build.VERSION.SDK_INT >= 24 && !Util.isLocationOn(this)) {
                    Util.displayLocationSettingsRequest(this);

                } else {
                    CheckBlueToothState();
                }
            } else {
                CheckBlueToothState();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        try {
            if (onoffReceiver != null)
                unregisterReceiver(onoffReceiver);
            flag = 1;
            redirectFlag = true;
            if (mScanning) {
                mBluetoothAdapter.stopLeScan(vLeScanCallback);
                mScanning = false;
            }
            if (mBluetoothGatt != null) {
                mBluetoothGatt.close();
                mBluetoothGatt = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    public void onStop() {
        try {
            if (onoffReceiver != null)
                unregisterReceiver(onoffReceiver);
        } catch (Exception e) {

        }
        flag = 1;
        redirectFlag = true;
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        rocketImage.setBackground(null);
    }

}

