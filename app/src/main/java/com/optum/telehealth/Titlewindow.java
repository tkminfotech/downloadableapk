package com.optum.telehealth;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import com.optum.telehealth.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.Toast;

import com.optum.telehealth.bean.ClassReminder;
import com.optum.telehealth.bean.ClassSensor;
import com.optum.telehealth.dal.Flow_Status_db;
import com.optum.telehealth.dal.MydataSensor_db;
import com.optum.telehealth.dal.Reminder_db;
import com.optum.telehealth.service.AlarmService;
import com.optum.telehealth.util.Alam_util;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.SkipVitals;
import com.optum.telehealth.util.UploadVitalReadingsTask;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Titlewindow extends Activity {
    String ApkType;

    public int appstatus, clickedhome, clickedhpf = 0;
    public String title_apk = "";
    private String serverurl = "";
    private Reminder_db reminder_db = new Reminder_db(this);
    private MydataSensor_db myDataSensor_db = new MydataSensor_db(this);
    Flow_Status_db Flow_db = new Flow_Status_db(this);
    String[] Status;
    String Status_Val;
    GlobalClass appClass;
    private String serviceUrl;
    private String chartStatus;
    private int clickedhome1;
    private int isHomeEnable;                   //0-no home button, 1-show home button
    private int isSupportEnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setConfig();


        this.getWindow()
                .addFlags(
                        /*WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD*/
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        appClass = (GlobalClass) getApplicationContext();
        resetSessionTime();

        setLanguage();
        /*requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main);*/

        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.CLUSTER_SP, 0);

        ApkType = settings.getString("clustorNo", "-1"); // #1
        isHomeEnable = settings.getInt("homeButton", -1); // #1
        isSupportEnable = settings.getInt("supportButton", -1); // #1
        title_apk = ApkType;
        Log.i("Optum", "Titlewindow oncreate");

        if (!checkConfigValues()) {

            // for live build call the below function
            // ********************************
            // insConfig();

            // for local testing build call the below function
            // ****************************
            // insConfig_local();
        }

        Util.stopPlaySingle();
        Util.Stopsoundplay();
        // Sierra
        //if (ApkType.equals("1000000")) {
            setContentView(R.layout.window_title_sierra);
//        } else if (ApkType.equals("1000001")) // HFP
//        {
//            setContentView(R.layout.window_title_hfp);
//        } else {
//            setContentView(R.layout.window_title_hfp);
//            appstatus = 1;
//        }

    }

    private boolean checkConfigValues() {

        boolean flag = false;
        String serialNo = "-1";

        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.SERVER_URL_SP, 0);
        String server1 = settings.getString("Server_url", "-1");
        String server2 = settings.getString("Server_port", "-1");
        String server3 = settings.getString("Server_post_url", "-1");
        String server4 = settings.getString("Server_url", "-1");

        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.USER_SP, 0);
        String PatientIdDroid = settings1.getString("patient_id", "-1");

        SharedPreferences settingseifi = getSharedPreferences(
                CommonUtilities.WiFi_SP, 0);
        serialNo = settingseifi.getString("wifi", "");
        String imei = settingseifi.getString("imei_no", "");

        if (server3.contains("-1")) {
            flag = false;
        }

        Constants.setPatientid(PatientIdDroid);
        Constants.setIMEI_No(imei);
        Constants.setParams(server1, server2, server3, serialNo);
        return flag;
    }

    private void insConfig() {
        try {

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.SERVER_URL_SP, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("Server_url", "optumtelehealth.com");
            editor.putString("Server_port", "443");
            editor.putString("Server_post_url",
                    "https://www.optumtelehealth.com");
            editor.commit();

            checkConfigValues();

        } catch (Exception e) {

        }
    }

    private void insConfig_local() {
        try {

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.SERVER_URL_SP, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("Server_url", "www2.optumtelehealth.com");
            editor.putString("Server_port", "443");
            editor.putString("Server_post_url",
                    "https://www2.optumtelehealth.com:443");
            editor.commit();

            checkConfigValues();

        } catch (Exception e) {

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    private void setConfig() {

        {
            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.SERVER_URL_SP, 0);

            serverurl = settings.getString("Server_post_url", "-1");
            CommonUtilities.SERVER_URL = serverurl;

        }
    }

    public void insert_skip_values() {

        SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
        int flow_type = flowsp.getInt("is_reminder_flow", 0);

        if (flow_type == 1)// skip for reminder
        {
            Log.e("TitleWindow", "During reminder flow user click on START/HOM- Inserting skip data");
            SkipVitals.skip_patient_vitalse(getApplicationContext());
            SharedPreferences.Editor seditor = flowsp.edit();
            seditor.putInt("is_reminder_flow", 0);
            seditor.commit();
        } else {
            Log.e("TitleWindow", "User click on START/HOME in normal flow.");
            SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
            String posturl = settings.getString("Server_post_url", "-1");
            new UploadVitalReadingsTask(Titlewindow.this, posturl).execute();

        }

    }

    private void set_flow(int a) {
        SharedPreferences flow = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);

        SharedPreferences.Editor editor = flow.edit();
        editor.putInt("start_flow", a);
        editor.commit();
    }

    // Initiating Menu XML file (menu.xml)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        if (chartStatus.equals("0"))
            menu.findItem(R.id.menu_myData).setVisible(false);
        else
            menu.findItem(R.id.menu_myData).setVisible(true);

        if (getResources().getBoolean(R.bool.isTab)) {
            menu.findItem(R.id.menu_support).setVisible(false);
//            if (ApkType.equals("1000000")) {                                //serra
//                menu.findItem(R.id.menu_Support).setVisible(false);
//            } else if (ApkType.equals("1000001")) {                         //hfpa
//                menu.findItem(R.id.menu_Home).setVisible(false);
//            } else {
//                menu.findItem(R.id.menu_Home).setVisible(false);
//                menu.findItem(R.id.menu_Support).setVisible(false);
//            }
            if(isHomeEnable == 0)
                menu.findItem(R.id.menu_Home).setVisible(false);
                if(isSupportEnable == 0)
                    menu.findItem(R.id.menu_Support).setVisible(false);

        } else {
            menu.findItem(R.id.menu_Support).setVisible(false);
//            if (ApkType.equals("1000000")) {
//                menu.findItem(R.id.menu_support).setVisible(false);
//                menu.findItem(R.id.menu_Start).setVisible(false);
//            } else if (ApkType.equals("1000001")) {
//                menu.findItem(R.id.menu_Home).setVisible(false);
//            } else {
//                menu.findItem(R.id.menu_Home).setVisible(false);
//            }

            if(isHomeEnable == 0)
                menu.findItem(R.id.menu_Home).setVisible(false);
            if(isSupportEnable == 0)
                menu.findItem(R.id.menu_support).setVisible(false);
        }
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                final View homeView = findViewById(R.id.menu_Home);
                final View menuOptionView = findViewById(R.id.menu_options);
                final View menuSupportView = findViewById(R.id.menu_Support);
                final View menuStartView = findViewById(R.id.menu_Start);
                if (homeView != null) {
                    homeView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            return false;
                        }
                    });
                }
                if (menuSupportView != null) {
                    menuSupportView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            return false;
                        }
                    });
                }
                if (menuOptionView != null) {
                    menuOptionView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            return false;
                        }
                    });
                }
                if (menuStartView != null) {
                    menuStartView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            return false;
                        }
                    });
                }
            }
        });


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        try {
            MenuItem item = menu.findItem(R.id.menu_options);
            if(item != null) {
                if (appClass.isEllipsisEnable) {
                    item.setEnabled(true);
                    item.getIcon().setAlpha(255);
                    // You can also use something like:
                    // menu.findItem(R.id.example_foobar).setEnabled(false);
                } else {

                    item.setEnabled(false);
                    item.getIcon().setAlpha(130);
                }
            }
        }catch (Exception ex){

            Log.i("TitleWindow", ex.getMessage());
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e("TitleWindow", "onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.menu_options:
                appClass.appTracking("Application Menu","Click on ellipsis button");
                return true;
            case R.id.menu_Start:
                Log.i("TitleWindow", "menu_Start Click");
                appClass.appTracking("Application Menu","Click on Start button");
                startSession();
                return true;
            case R.id.menu_Home:
                Log.i("TitleWindow", "menu_Home Click");
                appClass.appTracking("Application Menu","Click on Home button");
                homeClick();
                return true;
            case R.id.menu_feedback:
                Log.i("TitleWindow", "menu_feedback Click");
                appClass.appTracking("Application Menu","Click on Feedback button");
                feedbackClick();
                return true;
            case R.id.menu_myData:
                Log.i("TitleWindow", "menu_myData Click");
                appClass.appTracking("Application Menu","Click on My Data button");
                myDataClick();
                return true;
            case R.id.menu_support:
                Log.i("TitleWindow", "menu_support Click");
                appClass.appTracking("Application Menu","Click on Support button");
                if(appClass.isSupportEnable)
                    suportClick();
                return true;
            case R.id.menu_Support:
                Log.i("TitleWindow", "menu_Support Click");
                appClass.appTracking("Application Menu","Click on Support button");
                suportClick();
                return true;
            case R.id.menu_about:
                Log.i("TitleWindow", "menu_about Click");
                appClass.appTracking("Application Menu","Click on About button");
                versionClick();
                return true;
            case R.id.menu_terms:
                Log.i("TitleWindow", "menu_terms Click");
                appClass.appTracking("Application Menu","Click on Terms & Conditions button");
                termsClick();
                return true;
            case R.id.menu_logout:
                Log.i("TitleWindow", "menu_logout Click");
                appClass.appTracking("Application Menu","Click on Logout button");
                logoutClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startSession() {

        appClass.isEllipsisEnable = false;
        appClass.isSupportEnable = false;
        Log.e("TitleWindow", "START SESSION");
//        android.provider.Settings.System
//                .putInt(getContentResolver(),
//                        Settings.System.SCREEN_OFF_TIMEOUT,
//                        (60000 * 4 * 9));
        //start.setEnabled(false);

        clickedhome1 = 1;
        set_flow(1); // set flow as start click for alarm
        insert_skip_values();

        Flow_db.open();
        Cursor cursor1 = Flow_db.selectAll();
        Status = new String[cursor1.getCount()];
        int j = 0;

        while (cursor1.moveToNext()) {
            Status[j] = cursor1.getString(cursor1
                    .getColumnIndex("Status"));

            Status_Val = Status[0];
            j += 1;
        }
        Flow_db.close();

        if (Status_Val.equals("0")) {
            Alam_util.cancelAlarm(Titlewindow.this);

            Flow_db.open();
            Flow_db.deleteAll();
            Flow_db.insert("1");
            Flow_db.close();

        }

					/*
                     * Handler tim = new Handler(); tim.postDelayed(new
					 * Runnable() {
					 *
					 * @Override public void run() { if
					 * (appState.isRegular_Alarm_Triggered()) {
					 * Regular_Alam_util .cancelAlarm(getApplicationContext());
					 * appState.setRegular_Alarm_Triggered(false); }
					 * appState.setStart_Alarm_Count(0);
					 * Regular_Alam_util.startAlarm(getApplicationContext());//
					 * start // Regular // alarm
					 * appState.setRegular_Alarm_Triggered(true); } }, 60 *
					 * 1000);
					 */

        // uploadSensordetails();

					/*
                     * Toast.makeText(getApplicationContext(),
					 * "Connecting to server please wait..", Toast.LENGTH_LONG)
					 * .show();
					 */
        SharedPreferences flow = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);

        SharedPreferences.Editor editor = flow.edit();
        editor.putInt("flow", 0);
        editor.commit();

        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.CLUSTER_SP, 0);
        ApkType = settings.getString("clustorNo", "-1"); // #1

//        if (ApkType.equals("1000000")) {
//            Log.e("TitleWindow", "REDIRECT MAINACTIVITY");
//            Intent i = new Intent(Titlewindow.this,
//                    MainActivity.class);
//            startActivity(i);
//            overridePendingTransition(0, 0);
//        } else if (ApkType.equals("1000001")) // HFP
//        {
            Intent i = new Intent(Titlewindow.this,
                    HFPMainActivity.class);
            startActivity(i);
            overridePendingTransition(0, 0);
//
//        } else {
//
//        }
        startReminderService();
    }

    public void homeClick() {
        appClass.isEllipsisEnable = false;
        appClass.isSupportEnable = true;
        Log.e("TitleWindow", "HOME START SESSION");
//        android.provider.Settings.System
//                .putInt(getContentResolver(),
//                        Settings.System.SCREEN_OFF_TIMEOUT,
//                        (60000 * 5 * 2));
        //start.setEnabled(false);
        // uploadSensordetails();
        insert_skip_values();
        Flow_db.open();
        Cursor cursor1 = Flow_db.selectAll();
        Status = new String[cursor1.getCount()];
        int j = 0;

        while (cursor1.moveToNext()) {
            Status[j] = cursor1.getString(cursor1
                    .getColumnIndex("Status"));

            Status_Val = Status[0];
            j += 1;
        }
        Flow_db.close();
        if (Status_Val.equals(null)) {
            Status_Val = "1";
        }

        if (Status_Val.equals("0")) {
            Alam_util.cancelAlarm(Titlewindow.this);

            Flow_db.open();
            Flow_db.deleteAll();
            Flow_db.insert("1");
            Flow_db.close();

            ClassReminder reminder = new ClassReminder();
            if (reminder_db.GetStartedAlarm() != null) {
                reminder = reminder_db.GetStartedAlarm();
                reminder.setStatus(3); // Set status to SUCCESS
                reminder_db.UpdateReminderStatus(reminder);
            }
        } else {
            Log.i("TitleWindow", "appState.isAlarmTriggered()--> "
                    + Status_Val);
        }

        //home.setEnabled(false);
        clickedhome = 1;

        SharedPreferences flow = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);
        SharedPreferences.Editor editor = flow.edit();
                    editor.putInt("flow", 1);
            editor.commit();

//        if (ApkType.equals("1000000")) {
//            SharedPreferences.Editor editor = flow.edit();
//            editor.putInt("flow", 1);
//            editor.commit();
//
//            Intent i = new Intent(Titlewindow.this, MainActivity.class);
//            startActivity(i);
//            overridePendingTransition(0, 0);
//        } else if (ApkType.equals("1000001")) // HFP
//        {
            appClass.setHfpClicked(true);
            Intent i = new Intent(Titlewindow.this,
                    HFPMainActivity.class);
            startActivity(i);
        finish();
            overridePendingTransition(0, 0);
//        }

        startReminderService();
    }

    public void myDataClick() {

        DownloadMydataSensor();


    }

    public void suportClick() {
        setLanguage();
        if(appClass.isSupportEnable) {

            Intent i = new Intent(Titlewindow.this, supportinfo.class);
            startActivity(i);
            overridePendingTransition(0, 0);
        }
    }

    public void versionClick() {
        setLanguage();
        Intent i = new Intent(Titlewindow.this, versionInfo.class);
        startActivity(i);
        overridePendingTransition(0, 0);
    }

    public void logoutClick() {
        clickedhome = 1;
        SharedPreferences skipPreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor S_editor = skipPreferences.edit();
        S_editor.putInt("R_LOGOUT", 0);
        S_editor.commit();
        insert_skip_values();
        Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
        startActivity(authenticationIntent);
        overridePendingTransition(0, 0);
    }

    public void feedbackClick() {
        insert_skip_values();
        clickedhome = 1;
        Intent feedbackIntent = new Intent(getApplicationContext(), FeedbackActivity.class);
        feedbackIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        feedbackIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(feedbackIntent);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onUserInteraction() {

        super.onUserInteraction();
        Log.e("TitleWindow", "IN #onUserInteraction");
        SharedPreferences skipPreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = skipPreferences.edit();
        int LOAD_STATUS = skipPreferences.getInt("LOAD_STATUS", -1);
        if (LOAD_STATUS == 1) {
            Log.e("TitleWindow", "IN #onUserInteraction LOAD_STATUS = 1");
            editor.putInt("LOAD_STATUS", 0);
            editor.commit();
        } else {
            Log.e("TitleWindow", "IN #onUserInteraction LOAD_STATUS = 0 or -1");
            //Reset the timer on user interaction...
            appClass.countDownTimer.cancel();
            appClass.countDownTimer.start();
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void termsClick() {
        Intent i = new Intent(Titlewindow.this, Terms_ConditionsActivity.class);
        i.putExtra("patient_id", "1234567");
        i.putExtra("flow_from", "titlewindow");
        startActivity(i);
        overridePendingTransition(0, 0);
    }


    private void startReminderService(){

        final Intent reminder = new Intent();
        reminder.setClass(Titlewindow.this, AlarmService.class);
        if (!isMyReminderServiceRunning()) {
            Log.e("TitleWindow", " starting  AlarmService ");
            startService(reminder);
        } else {
            Log.e("TitleWindow", " Already   AlarmService started ");
            //if (!reminder_flow || R_STATUS == -1) {
                stopService(reminder);
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startService(reminder);
           // }
        }
    }

    private boolean isMyReminderServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.AlarmService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    private void DownloadMydataSensor() {
        if (appClass.isConnectingToInternet(this)) {
            clickedhome = 1;
            SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
            serviceUrl = settings.getString("Server_post_url", "-1");
            DownloadMyDataSensorTask taskUpdate = new DownloadMyDataSensorTask();
            taskUpdate.execute(new String[]{serviceUrl
                    + "/droid_website/mob_sensor_details_mydata.ashx"});

        } else {

//            Cursor c = myDataSensor_db.SelectMydataSensors();
//            int size = c.getCount();
//
//            if (size > 0) {
//
//                Intent myDataIntent = new Intent(getApplicationContext(), MyDataActivity.class);
//                startActivity(myDataIntent);
//            } else {

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internetFailed), Toast.LENGTH_SHORT).show();

            //           }
        }

    }

    private class DownloadMyDataSensorTask extends
            AsyncTask<String, Void, String> {
        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(Titlewindow.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if(progressDialog != null)
                 progressDialog.dismiss();
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");

                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            } else if(result.equals("Success")){

                Cursor c = myDataSensor_db.SelectMydataSensors();
                if(c.getCount() > 0) {

                    Intent myDataIntent = new Intent(getApplicationContext(), MyDataActivity.class);
                    startActivity(myDataIntent);
                }else{

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();

                }

            }else{
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();

            }
        }

        protected String doInBackground(String... urls) {

            String response = "";
            try {
                //sensor_db.deleteplayorder();//
                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                        + "/droid_website/mob_sensor_details_mydata.ashx", new String[]{
                        "authentication_token", "patient_id"}, new String[]{token,
                        patientId});

                if (response1 == null) {


                    return ""; // process
                }
                // Log.e("Optum_Sierra","serialNo"+serialNo);

                String str = Util
                        .readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {

                    //sensor_db.delete();
                    return "";
                }
                str = str.replace("&", "");
                //Log.i("response:", "sensor_response:" + str);
                DocumentBuilder dbr = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = dbr.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("patient_sensor_details");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {
                    response = Util.getTagValue(AF_Nodes, i, "mob_response");

                }
                if (response.equals("AuthenticationError")) {

                    return response;
                }
                NodeList nodes = doc.getElementsByTagName("sensor");
                myDataSensor_db.delete(); // deleting entire table
                ClassSensor sensor = new ClassSensor();

                for (int i = 0; i < nodes.getLength(); i++) {

                    sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes, i, "sensor_id")));
                    sensor.setSensorName(Util.getTagValue(nodes, i, "vital_name"));
                    ContentValues values = new ContentValues();
                    values.put("Sensor_id", sensor.getSensorId());
                    values.put("Vital_name", sensor.getSensorName());
                    myDataSensor_db.insertMydataSensor(values);// inserting downloaded date
                    response = "Success";
                }
                myDataSensor_db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }
    }


    private void setLanguage(){
        int ln = 0;
        SharedPreferences settings_n = getSharedPreferences(
                CommonUtilities.USER_SP, 0);
        ln = Integer.parseInt(settings_n.getString("language_id", "0"));
        chartStatus = settings_n.getString("chart_status", "0");
        Constants.setLanguage(ln);
        if (ln == 11) {
            Locale locale = new Locale("es");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

        } else {
            Locale.setDefault(Locale.US);
            Configuration config = new Configuration();
            config.locale = Locale.US;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
    }


    private void resetSessionTime(){

        SharedPreferences settings_n = getSharedPreferences("activity_name", 0);
        String className = settings_n.getString("class_name", "0");

        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        String mClassName = cn.getClassName();

        if(!className.equals(mClassName)){
            appClass.setRegular_alarm_count(0);
            appClass.setStart_Alarm_Count(0);

        }
        SharedPreferences settings = getSharedPreferences("activity_name", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("class_name", mClassName);
        editor.commit();

    }

}