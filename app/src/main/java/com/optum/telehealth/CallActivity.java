package com.optum.telehealth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

public class CallActivity extends Titlewindow {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call);
		Log.e("CallActivity", "onCreate");
		String ApkType = "";
		//SkipVitals.skip_patient_vitalse(getApplicationContext());
		SharedPreferences settings2 = getSharedPreferences(
				CommonUtilities.CLUSTER_SP, 0);
		ApkType = settings2.getString("clustorNo", "-1");

		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);

		SharedPreferences.Editor seditor = flow.edit();
		seditor.putInt("flow", 0);
		seditor.putInt("reminder_path", 1);
		seditor.putInt("is_reminder_flow", 1);
		seditor.commit();

//		SharedPreferences settings1 = getSharedPreferences(
//				CommonUtilities.PREFS_NAME_date, 0);
//		SharedPreferences.Editor editor_retake = settings1.edit();
//		editor_retake.putString("sectiondate",
//				Util.get_patient_time_zone_time(this));
//		editor_retake.commit();

//		if (ApkType.equals("1000000")) // Sierra//
//		{
//			Intent intent = new Intent(this, MainActivity.class);
//			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			startActivity(intent);
//			finish();
//		} else if (ApkType.equals("1000001")) // HFP//
//		{
			Intent intent = new Intent(this, HFPMainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
//		}

	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
