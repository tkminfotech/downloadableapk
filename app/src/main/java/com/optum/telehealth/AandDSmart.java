package com.optum.telehealth;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.optum.telehealth.bean.ClasssPressure;
import com.optum.telehealth.dal.Pressure_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.GattByteBufferAandDsmart;
import com.optum.telehealth.util.GattUtilsAndSmart;
import com.optum.telehealth.util.PairAandDSmart;
import com.optum.telehealth.util.Util;

import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

@SuppressLint("NewApi")
public class AandDSmart extends Titlewindow{

    private static final int API_LEVEL = Build.VERSION.SDK_INT;
    private static final boolean FORCE_PAIRING_POPUP_TO_FOREGROUND = API_LEVEL <= 19; // will force pairing popup to foreground instead of notification center

    private Timer timer = null;
    private boolean timer_statu = false;
    private String pageName = "Blood Pressure Sensor Page";
    BluetoothManager manager;
    BluetoothGatt mBluetoothGatt;
    BluetoothAdapter mBluetoothAdapter;

    BluetoothDevice deviceL = null;
    private BroadcastReceiver pnr_receiver = null;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    String print = "", data_save;
    public static final long leastSigBits = 0x800000805f9b34fbL;
    // service
    public static final UUID BLOOD_PRESSURE = new UUID(
            (0x1810L << 32) | 0x1000, leastSigBits);

    public static final UUID CLIENT_CHARACTERISTIC_CONFIGURATION = new UUID(
            (0x2902L << 32) | 0x1000, leastSigBits);

    public static final UUID BLOOD_PRESSURE_MEASUREMENT = new UUID(
            (0x2A35L << 32) | 0x1000, leastSigBits);
    public static final UUID BLOOD_PRESSURE_FEATURE = new UUID(
            (0x2A49L << 32) | 0x1000, leastSigBits);
    public static final UUID INTERMEDIATE_CUFF_PRESSURE = new UUID(
            (0x2A36L << 32) | 0x1000, leastSigBits);

    boolean redirectFlag = false;
    int flag = 0;
    TextView text;
    Button btnmanual;
    TextView txtwelcom, txtReading, txt_errorMsg;
    ImageView rocketImage;
    AnimationDrawable rocketAnimation;
    ObjectAnimator AnimPlz;
    String macAddress = "", TAG = "******************AandDSmart***************";
    int a1, b11, c1;
    int lastinsert_id = 0;
    Pressure_db dbcreatepressure = new Pressure_db(this);
    public String sys, dia, pulse;
    private GlobalClass appClass;
    public static final int REQUEST_DISCOVERABLE_CODE = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aand_dreader);
        appClass = (GlobalClass) getApplicationContext();
        appClass.isEllipsisEnable = true;
        appClass.isSupportEnable = false;
        btnmanual = (Button) findViewById(R.id.aAndDManuval);
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        txtwelcom = (TextView) findViewById(R.id.txtwelcome);
        txtReading = (TextView) findViewById(R.id.takereading);

        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");

        txtwelcom.setTypeface(type, Typeface.NORMAL);
        txtReading.setTypeface(type, Typeface.NORMAL);

        rocketImage = (ImageView) findViewById(R.id.loading);
        try{
            rocketImage.setBackgroundResource(R.drawable.pressure_animation);
            rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
            rocketAnimation.start();
            rocketImage.setVisibility(View.INVISIBLE);
            txtReading.setVisibility(View.INVISIBLE);
        } catch (OutOfMemoryError ex){
            Log.i("exception",ex.toString());
            rocketImage.setBackgroundResource(R.drawable.bloodpressure0011);
        }
        setwelcome();
        Animations();

        addListenerOnButton();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CheckBlueToothState();
                } else {
                    Intent intentwt = new Intent(getApplicationContext(),
                            PressureEntry.class);
                    startActivity(intentwt);
                    overridePendingTransition(0, 0);
                    finish();
                }
                return;
            }
        }
    }

    private void CheckBlueToothState() {
        Log.d(TAG, "IN #CheckBlueToothState()");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
        String bluetooth_error  = ERROR_MSG.getString("bluetooth_error", "-1");
        if (mBluetoothAdapter == null) {
            errorShow(bluetooth_error);
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                BTDiscoverable();
            } else {
                try {
                    errorShow(bluetooth_error);
                    mBluetoothAdapter.enable();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "CALL #getPairedDevices()");
                            if (mBluetoothAdapter.isDiscovering()) {
                                mBluetoothAdapter.cancelDiscovery();
                            }
                            BTDiscoverable();
                        }
                    }, 2000);
                } catch (Exception e) {
                	e.printStackTrace();
                }
            }
        }
    }

    public void BTDiscoverable() {
        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(onoffReceiver, filter1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            search();
        } else {
            BluetoothAdapter BA;
            BA = BluetoothAdapter.getDefaultAdapter();
            if (BA.getScanMode() !=
                    BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                turnOn.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
                startActivityForResult(turnOn, REQUEST_DISCOVERABLE_CODE);
            } else {
                search();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_DISCOVERABLE_CODE) {
            if (resultCode == RESULT_CANCELED) {
                errorShow(this.getString(R.string.connectivityError));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!redirectFlag) {
                            Intent intentwt = new Intent(getApplicationContext(),
                                    PressureEntry.class);
                            startActivity(intentwt);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }
                }, 4000);
            } else {
                search();
            }
        }
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    private void addListenerOnButton() {
        btnmanual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                appClass.appTracking(pageName,"Click on Manual entry button");
                Intent intentpl = new Intent(getApplicationContext(),
                        PressureEntry.class);
                startActivity(intentpl);
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }

    private void setwelcome() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }

    private void Animations() {
        txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));
        AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, 0f);
        //AnimPlz.setDuration(1000);
        AnimPlz.start();

        AnimPlz.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                Log.e("", "AnimPlz");
                txtReading.setVisibility(View.VISIBLE);
                rocketImage.setVisibility(View.VISIBLE);
            }
        });
    }

    public void search() {
        Log.e("Foundddd", "inside search");
        boolean x = mBluetoothAdapter.startLeScan(mLeScanCallback);
        if (x) {
            Log.e("scan status", "true");
        } else {
            Log.e("scan status", "false");
        }

    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Found=>" + device.toString());

                    // workaround to force showing the pairing popup to foreground instead of notification center
                    if(FORCE_PAIRING_POPUP_TO_FOREGROUND) {
                        mBluetoothAdapter.startDiscovery();
                        mBluetoothAdapter.cancelDiscovery();
                    }

                    deviceL = device;

                    // connect GATT to the sensor if the MAC addresses match
                    if (device.toString().toUpperCase().equals((macAddress.toUpperCase()))) {
	                    connect(device);
                    }
                }
            });
        }
    };

    public void connect(BluetoothDevice device) {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }

        mBluetoothAdapter.stopLeScan(mLeScanCallback);
        mBluetoothGatt = device.connectGatt(getBaseContext(), false, mGattCallback);
        Log.i("mBluetoothGatt", "connect");
    }

    public BluetoothDevice getDevice() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
                .getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().contains("UA-651BLE")) {
                    return device;
                }
            }
        }
        return null;
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            if (mBluetoothGatt == null)
                mBluetoothGatt = gatt;

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "Connected to GATT server.");
                Log.i(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from GATT server. GATT status: " + status);

                // clean up everything if the GATT disconnects with status 19 without any readings being taken
	            // disconnecting with status 19 was causing subsequent connections to error out on Android 7
	            // the clean up is done by restarting the entire activity!
                if (status == 19 && print.equals("") && Build.VERSION.SDK_INT >= 24) {
                	Intent restartActivityIntent = new Intent(getApplicationContext(), AandDSmart.class);
                	startActivity(restartActivityIntent);

                	finish();
                	return;
                }

                mBluetoothGatt.close();
                mBluetoothGatt = null;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "reconnecting");

                        if (mBluetoothAdapter == null)
                            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                        search();
                    }
                });
            }
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                List<BluetoothGattService> services = gatt.getServices();

                int i;
                for (i = 0; i < services.size(); i++){
                    Log.d(TAG, "----------$$$$$$$$$$$$---------"+services.get(i).getUuid().toString());
                }
                if (mBluetoothGatt == null)
                    mBluetoothGatt = gatt;

                try {
                    BluetoothGattService service = mBluetoothGatt.getService(BLOOD_PRESSURE);
                    update(service);

                } catch (NullPointerException e) {
                    Log.e("service error", "no uuid");
                }

            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

            if (mBluetoothGatt == null)
                mBluetoothGatt = gatt;

            broadcastUpdate(characteristic);
            super.onCharacteristicChanged(gatt, characteristic);
        }

        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            if (mBluetoothGatt == null)
                mBluetoothGatt = gatt;

            broadcastUpdate(characteristic);
            Log.w(TAG, "characteristic read");
        }
    };

    public void update(BluetoothGattService service) {

        List<BluetoothGattCharacteristic> list = service.getCharacteristics();

        for (int i = 0; i < list.size(); i++) {
            Log.e("characteristic", list.get(i).toString());
            mBluetoothGatt.readCharacteristic(list.get(i));

            if (list.get(i).getUuid().equals(INTERMEDIATE_CUFF_PRESSURE)) {
                Log.e("char found", list.get(i).getUuid().toString());

                mBluetoothGatt.setCharacteristicNotification(list.get(i), true);

                BluetoothGattDescriptor descriptor = list.get(i).getDescriptor(CLIENT_CHARACTERISTIC_CONFIGURATION);
                Log.e("dis found", descriptor.toString());

                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                mBluetoothGatt.writeDescriptor(descriptor);
            }

            if (list.get(i).getUuid().equals(BLOOD_PRESSURE_MEASUREMENT)) {
                Log.e("read char", "second if");

                mBluetoothGatt.setCharacteristicNotification(list.get(i), true);

                BluetoothGattDescriptor descriptor = list.get(i).getDescriptor(CLIENT_CHARACTERISTIC_CONFIGURATION);
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                mBluetoothGatt.writeDescriptor(descriptor);
            }
            if (list.get(i).getUuid().equals(BLOOD_PRESSURE_FEATURE)) {
                Log.e("read char", "third if");
                mBluetoothGatt.readCharacteristic(list.get(i));
            }
        }
    }

    private void broadcastUpdate(final BluetoothGattCharacteristic characteristic) {
        byte[] data = characteristic.getValue();
        if (data != null && data.length > 0) {
            Log.d("data", data.toString() + "  " + data.length);
            GattByteBufferAandDsmart bb = GattByteBufferAandDsmart.wrap(data);

            byte flags = bb.getInt8();

            float a, b, c;

            a = GattUtilsAndSmart.getFloatValue(data, GattUtilsAndSmart.FORMAT_SFLOAT, 1);
            a1 = (int) a;
            Log.e("data", data.toString() + "  " + a + "int value " + a1);

            b = GattUtilsAndSmart.getFloatValue(data,
                    GattUtilsAndSmart.FORMAT_SFLOAT, 3);
            b11 = (int) b;
            Log.e("data", data.toString() + "  " + b + "int value " + b11);
            boolean x = Time_Stamp_Flag(flags);

            Log.e("results", x + "  " + a + "  " + b);
            if (Time_Stamp_Flag(flags)) {
                c = GattUtilsAndSmart.getFloatValue(data, GattUtilsAndSmart.FORMAT_SFLOAT, 14);
                c1 = (int) c;
            } else {
                c = GattUtilsAndSmart.getFloatValue(data, GattUtilsAndSmart.FORMAT_SFLOAT, 7);
                c1 = (int) c;

            }
            Log.e("data", data.toString() + "  " + c + "int value " + c1);
            if ((a1 > 0) && (b11 > 0) && (c1 > 0)) {

                if (a1 < 999 && b11 < 999 && c1 < 999) {
                    print = a1 + "," + b11 + "," + c1;
                }
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("your blood pressure ", "values-" + print);
                    if ((a1 > 0) && (b11 > 0) && (c1 > 0)) {
                        if (a1 < 999 && b11 < 999 && c1 < 999) {
                            data_save = print;
                            calle_timer();
                        }
                    }
                }
            });
        }
    }

    private void calle_timer() {
        TimerTask task = new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d("mg", "save data after the delay to get last data");
                        save(data_save);
                        timer.cancel();
                        timer_statu = false;
                    }
                });
            }
        };
        if (!timer_statu) {
            timer = new Timer();
            timer.schedule(task, 10000, 1500);
            timer_statu = true;
        }
    }

    private void save(String result) {
        flag = 1;
        try {
            SharedPreferences flow = getSharedPreferences(
                    CommonUtilities.USER_FLOW_SP, 0);
            int val = flow.getInt("flow", 0); // #1
            if (val == 1) {
                SharedPreferences section = getSharedPreferences(
                        CommonUtilities.PREFS_NAME_date, 0);
                SharedPreferences.Editor editor_retake = section.edit();
                editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
                editor_retake.commit();
            }
            SharedPreferences settings1 = getSharedPreferences(
                    CommonUtilities.USER_SP, 0);
            String PatientIdDroid = settings1.getString("patient_id",
                    "-1");
            String[] str = null;
            str = print.split(",");

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.USER_TIMESLOT_SP, 0);
            String slot = settings.getString("timeslot", "AM");

            ClasssPressure pressure = new ClasssPressure();
            pressure.setPatient_Id(PatientIdDroid);

            pressure.setSystolic(Integer.parseInt(str[0]));
            pressure.setDiastolic(Integer.parseInt(str[1]));
            pressure.setPulse(Integer.parseInt(str[2]));
            pressure.setInputmode(0);
            pressure.setTimeslot(slot);

            SharedPreferences settings2 = getSharedPreferences(
                    CommonUtilities.PREFS_NAME_date, 0);
            String sectiondate = settings2.getString("sectiondate", "0");
            pressure.setSectionDate(sectiondate);
            pressure.setPrompt_flag("4");
            lastinsert_id = dbcreatepressure.InsertPressure(pressure);
            Intent intent = new Intent(AandDSmart.this, ShowPressureActivity.class);
            sys = "" + str[0];
            dia = "" + str[1];
            pulse = "" + str[2];
            intent.putExtra("sys", sys);
            intent.putExtra("dia", dia);
            intent.putExtra("pulse", pulse);
            intent.putExtra("pressureid", lastinsert_id);
            startActivity(intent);
            AandDSmart.this.finish();
        } catch (Exception e) {
            Log.i("A&D",
                    "Exception inserting bp from in a and d continua");
        }
    }

    private boolean Time_Stamp_Flag(byte flags) {
	    return (flags & GattUtilsAndSmart.SECOND_BITMASK) != 0;
    }

    @Override
    public void onStop() {
        redirectFlag = true;
        super.onStop();
    }

    @Override
    protected void onPause() {
        try {
            Log.e(TAG, "-------------------onPause-----------------");
            if (onoffReceiver != null) {
                unregisterReceiver(onoffReceiver);
            }
            unregisterReceiver(pnr_receiver);

            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mBluetoothGatt.close();
            mBluetoothGatt = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    protected void onResume() {
        resumeCaller();
        super.onResume();
    }

    public void resumeCaller() {
        try {
            Log.e(TAG, "-------------------onResume-----------------");
            if (Build.VERSION.SDK_INT >= 18) {
                mBluetoothAdapter = ((BluetoothManager) getBaseContext().getSystemService(
                        Context.BLUETOOTH_SERVICE)).getAdapter();
            } else {
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            }
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                appClass.setBundle(extras);
            } else {
                extras = appClass.getBundle();
            }
            macAddress = extras.getString("macaddress");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);

                    // enable location if on Android 7 or greater and the location service is turned off
                    // for Android 7, the location service must be turned on for the device to be able to LE scan and receive scan results
                } else if (Build.VERSION.SDK_INT >= 24 && !Util.isLocationOn(this)) {
                    Util.displayLocationSettingsRequest(this);

                } else {
                    CheckBlueToothState();
                }
            } else {
                CheckBlueToothState();
            }

            // IMPORTANT: At least on Samsung S7 Android 7, the broadcast receiver is not used to pair the sensor! the sensor is paired automatically
            if (pnr_receiver == null)
                pnr_receiver = new PairAandDSmart(this);
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
            registerReceiver(pnr_receiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final BroadcastReceiver onoffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        try {
                            Log.e(TAG, "-------------------onPause-----------------");
                            unregisterReceiver(pnr_receiver);
                            mBluetoothAdapter.stopLeScan(mLeScanCallback);
                            mBluetoothGatt.close();
                            mBluetoothGatt = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (!redirectFlag) {
                                        resumeCaller();
                                    } else {
                                        unregisterReceiver(onoffReceiver);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 5000);
                        break;
                }
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Log.i(TAG, " KEYCODE_BACK clicked: A&D SMART");
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        rocketImage.setBackground(null);
    }

}
