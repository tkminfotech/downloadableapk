package com.optum.telehealth.service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.AuthenticationActivity;
import com.optum.telehealth.FinalMainActivity;
import com.optum.telehealth.GlobalClass;
import com.optum.telehealth.R;
import com.optum.telehealth.ReminderCallActivity;
import com.optum.telehealth.bean.ClassReminder;
import com.optum.telehealth.bean.ClassUser;
import com.optum.telehealth.dal.Flow_Status_db;
import com.optum.telehealth.dal.Login_db;
import com.optum.telehealth.dal.Reminder_db;
import com.optum.telehealth.dal.Skip_Reminder_Value_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.ConnectionDetector;
import com.optum.telehealth.util.Skip_Reminder_Values_Upload;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

@SuppressLint("SimpleDateFormat")
public class AlarmService extends Service {

    private String serviceUrl = "";
    private String token;
    private String patient_Id;
    /**
     * interface for clients that bind
     */
    IBinder mBinder;
    /**
     * indicates whether onRebind should be used
     */
    boolean mAllowRebind;
    /**
     * flag to indicates whether there are reminder data available
     */
    private String TAG = "Alarm Service";
    private Timer timer;
    private int i = 0;
    private String PatientIdDroid = "";
    private String patientId = "";
    public Context contxt;
    public static String serialNo = "";
    public static String imeilNo = "";
    private ArrayList<ClassReminder> reminders = new ArrayList<ClassReminder>();
    private Reminder_db reminder_db = new Reminder_db(this);
    public Skip_Reminder_Value_db reminder_skip_db = new Skip_Reminder_Value_db(
            this);
    private GlobalClass appClass;
    String[] Status;
    String Status_Val;
    Flow_Status_db Flow_db = new Flow_Status_db(this);
    public Context context;
    Boolean isInternetPresent;
    int time_zone = 0;
    String apk_type = "";
    String time_slot = "";
    private Login_db login_db = new Login_db(this);
    String patient_time;
    int R_ID, intervalLimit;
    Calendar now;
    SimpleDateFormat df;

    /**
     * Called when the service is being created.
     **/
    @Override
    public void onCreate() {

        Log.e(TAG, "AlarmService Created");
        appClass = (GlobalClass) getApplicationContext();
        contxt = getApplicationContext();
        //appClass.acquireWakelock();
        context = getApplicationContext();

        timer = new Timer();

        timer.schedule(updateTask, 0L, 1 * 60 * 1000L); // 1 min

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

/*    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startId) {


        super.onStart(intent, startId);
    }*/

    /**
     * A client is binding to the service with bindService()
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /**
     * Called when The service is no longer used and is being destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        appClass.releaseWakelock();
        Log.i(TAG, "ReminderService destroying");
        timer.cancel();
        timer = null;
    }

    TimerTask updateTask = new TimerTask() {
        @Override
        public void run() {
            Log.d(TAG, "------->  i=" + i + "  <-------");
           appClass.acquireWakelock();
            alarmTimeCheck();
        }
    };

    public void alarmTimeCheck() {
        try {
            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.USER_SP, 0);
            PatientIdDroid = settings.getString("patient_id", "");

            SharedPreferences settings1 = getSharedPreferences(
                    CommonUtilities.SERVER_URL_SP, 0);
            serviceUrl = settings1.getString("Server_post_url", "-1");
            SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
            token = tokenPreference.getString("Token_ID", "-1");
            patient_Id = tokenPreference.getString("patient_id", "-1");
            SharedPreferences cluster_sp = getSharedPreferences(
                    CommonUtilities.CLUSTER_SP, 0);

            time_zone = cluster_sp.getInt("time_zone_id", 0);

            Log.i(TAG, "Paient id: " + PatientIdDroid + "  Time_zone: "
                    + time_zone);


            //if (reminders.size() == 0) {
            reminders = reminder_db.GetAllLocalReminders();
            //}
            patient_time = Util.get_patient_time_zone_time(this);

            now = Calendar.getInstance();
            // AM/PM format
            df = new SimpleDateFormat("hh:mm aa", Locale.US);
            System.out.println("Before: " + df.format(now.getTime()));

            SharedPreferences skipPreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
            SharedPreferences.Editor S_editor = skipPreferences.edit();
            int R_ID_S = skipPreferences.getInt("R_ID", 0);
            int R_STATUS_S = skipPreferences.getInt("R_STATUS", 1);
            String R_SKIP_TIME = skipPreferences.getString("R_SKIP_TIME", "-1");
            String sectiondate = skipPreferences.getString("sectiondate", "-1");
            Log.e(TAG, "R_STATUS_S:    " + R_STATUS_S);
            if (R_SKIP_TIME.equals(df.format(now.getTime())) && R_STATUS_S == -1) {
                try {
                    S_editor.putInt("R_STATUS", 0);
                    S_editor.commit();
                    Log.e(TAG, "CANCEL NOTIFICATION");
                    NotificationManager notifManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notifManager.cancel(R_ID_S);
                    Log.e(TAG, "Skip Alarm ");
                    SkipReminder(sectiondate);
                } catch (Exception e) {
                    Log.e(TAG, "Exception Reminder Full Skip");
                }
            }
            // int mHours = calendar.get(Calendar.HOUR);
            // int mMinutes = calendar.get(Calendar.MINUTE);
            // int year1 = Integer.parseInt(time.substring(6, 10));
            // int month1 = Integer.parseInt(time.substring(0, 2));
            // int day1 = Integer.parseInt(time.substring(3, 5));
            int mHours = Integer.parseInt(patient_time.substring(11, 13));
            int mMinutes = Integer.parseInt(patient_time.substring(14, 16));
            // int sec = Integer.parseInt(time.substring(14, 16));
            String sysAmPm = patient_time.substring(20, 22);

            System.out
                    .println("==================================================");
            Log.e(TAG,
                    "Time_zone Time:");
            Log.e(TAG, "HOUR:" + mHours + " MIN:" + mMinutes + " AM/PM:" + sysAmPm);
            Log.d(TAG,
                    "Alarm Time(Count=" + reminders.size() + "):");
            for (int i = 0; i < reminders.size(); i++) {

                String time1 = reminders.get(i).getReminderTime();
                intervalLimit = reminders.get(i).getDuration();
                Log.d(TAG, "Duration:" + intervalLimit);

                int hr = Integer.parseInt(time1.substring(0, 2));
                int mn = Integer.parseInt(time1.substring(3, 5));
                String RmAmPm = time1.substring(6, 8);

               // Log.d(TAG, "HOUR:" + hr + " MIN:" + mn + " AM/PM:" + RmAmPm);
                if ((mHours == hr && mMinutes == mn)
                        && (sysAmPm.equals(RmAmPm))) {

                    Flow_db.open();

                    Cursor cursor1 = Flow_db.selectAll();
                    Status = new String[cursor1.getCount()];
                    int j = 0;

                    while (cursor1.moveToNext()) {
                        Status[j] = cursor1.getString(cursor1
                                .getColumnIndex("Status"));

                        Status_Val = Status[0];

                        j += 1;

                    }

                    Flow_db.close();

                    if (Status_Val.equals("1")) {

                        SharedPreferences sharedpreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
                        R_ID = sharedpreferences.getInt("R_ID", 0);
                        int R_STATUS = sharedpreferences.getInt("R_STATUS", 1);

                        if (R_STATUS == -1) {
                            Log.i("R_STATUS", "-1");
                            SkipReminder(patient_time);
                        } else if (R_STATUS == 1 || R_STATUS == 0) {
                            Log.i("R_STATUS", "1 OR 0");

                            ActivityManager am = (ActivityManager) context
                                    .getSystemService(Context.ACTIVITY_SERVICE);
                            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

                            String mClassName = cn.getClassName();

                            Log.i("mClassName", mClassName);
                            if ((mClassName
                                    .equals("com.optum.telehealth.FinalMainActivity")
                                    || mClassName
                                    .equals("com.optum.telehealth.Home")
                                    || mClassName
                                    .equals("com.optum.telehealth.AuthenticationActivity")
                                    || mClassName
                                    .equals("com.optum.telehealth.HFPHomeActivity")
                                    || mClassName
                                    .equals("com.optum.telehealth.Reminder_Landing_Activity") || mClassName
                                    .equals("com.optum.telehealth.ThanksActivity") || mClassName
                                    .equals("com.optum.telehealth.FeedbackActivity") || mClassName
                                    .equals("com.optum.telehealth.MyDataActivity"))
                                    || !mClassName
                                    .contains("com.optum.telehealth")) {
                                reminders.get(i).setStatus(1);
                                reminder_db.UpdateReminderStatus(reminders.get(i));
                                new AuthCheck().execute();
                            } else {
                                Log.i(TAG, "SkipReminder 1" + patient_time);
                                SkipReminder(patient_time);
                            }

                        } else {
                            Log.i(TAG, "SkipReminder 2");
                            SkipReminder(patient_time);
                        }

                    } else {
                        Log.i(TAG, "SkipReminder 3");
                        SkipReminder(patient_time);
                    }
                }
            }
            if (i == 0) {
                isInternetPresent = (new ConnectionDetector(
                        getApplicationContext())).isConnectingToInternet();
                if (isInternetPresent) {

                    SharedPreferences settingseifi = getSharedPreferences(
                            CommonUtilities.WiFi_SP, 0);
                    serialNo = settingseifi.getString("wifi", "-1");
                    imeilNo = settingseifi.getString("imei_no", "");

                    downloadReminderData();
                }
                i++;
            } else if (i == 18) {
                i = 0;

            } else {
                i++;
            }
            System.out
                    .println("==================================================");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void downloadReminderData() {

        Log.i(TAG, "downloadReminderData");
        DownloadReminderData task = new DownloadReminderData();
        task.execute();

    }

    /**
     * Async Task to download the Reminder data and store in DB.
     */
    private class DownloadReminderData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "onPostExecute Alarm Service");
            if (result.equalsIgnoreCase("AuthenticationError")) {
                //Toast.makeText(getApplicationContext(), R.string.authenticationError, Toast.LENGTH_LONG).show();
                //logoutClick();
            }
            //alarmTimeCheck();
        }

        protected String doInBackground(String... urls) {

            String response = "";

            try {
                /*
                 * Log.e(TAG, "post url--" + serverurl +
				 * "/droid_website/reminder.ashx" + "PatientIdDroid-" +
				 * PatientIdDroid); HttpResponse response1 =
				 * Util.connect(serverurl + "/droid_website/reminder.ashx", //
				 * "http://115.111.179.7:8016/droid_website/reminder.ashx", new
				 * String[] { "patient_id" }, new String[] { PatientIdDroid });
				 */

                Log.i(TAG, "Checking device registration MAC-" + serialNo
                        + " IMEI-" + imeilNo);

                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_get_patient_reminder_details.ashx",
                        new String[]{"authentication_token", "patient_id"}, new String[]{
                                token, patient_Id});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return "";
                }

                String str = Util
                        .readStream(response1.getEntity().getContent());

                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                str.toString();
               // Log.i("response:", "Reminder Response :" + str);
                if (str.length() < 10) {
                    Log.i(TAG, "Reminder data null");
                    return "";
                }

                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("patient_reminder");
                for (int i = 0; i < nodes.getLength(); i++) {

                    response = Util.getTagValue(nodes, i, "mob_response");
                }
                if (response.equals("AuthenticationError")) {

                    return response;
                }
                NodeList nodesReminder = doc.getElementsByTagName("reminder");
                NodeList nodePatientDetails = doc
                        .getElementsByTagName("patient_details");

                if (nodes.getLength() > 0) {
                    reminders.clear();
                    reminder_db.deleteAll();
                    // ----FIRST TIME ENTRY
                    for (int i = 0; i < nodesReminder.getLength(); i++) {
                        ClassReminder reminder = new ClassReminder();

                        reminder.setPatientId(Integer.parseInt(Util
                                .getTagValue(nodePatientDetails, 0,
                                        "patient_id")));
                        reminder.setReminderId(Integer.parseInt(Util
                                .getTagValue(nodesReminder, i, "reminder_id")));
                        reminder.setReminderTime(Util.getTagValue(
                                nodesReminder, i, "reminder_time"));
                        reminder.setDuration(Integer.parseInt(Util.getTagValue(
                                nodesReminder, i, "reminder_duration")));
                        Log.i("AlarmService:", "reminder_duration :" + Integer.parseInt(Util.getTagValue(
                                nodesReminder, i, "reminder_duration")));
                        reminder.setCreatedDateAndTime(Util.getTagValue(
                                nodesReminder, i, "created_date"));

                        reminder.setStatus(0);

                        reminders.add(reminder);

                    }
                    // NodeList nodePatientDetails =
                    // doc.getElementsByTagName("patient_details");
                    time_zone = (Integer.parseInt(Util.getTagValue(
                            nodePatientDetails, 0, "time_zone")));
                    time_slot = Util.getTagValue(nodePatientDetails, 0,
                            "patient_time_slot");
                    apk_type = (Util.getTagValue(nodePatientDetails, 0,
                            "cluster"));
                    patientId = Util.getTagValue(nodePatientDetails, 0,
                            "patient_id");

                    ClassUser classUser = new ClassUser();
                    classUser.setPatientId(patientId);
                    classUser.setFullName("");
                    classUser.setNickName("");

                    classUser.setDbo("");
                    classUser.setSex("");

                    // classUser.setType("AM"); // testing
                    classUser.setType(time_slot);
                    classUser.setContractcode("");
                    login_db.insert_Patient_Data(classUser);

                    SharedPreferences settings = getSharedPreferences(
                            CommonUtilities.CLUSTER_SP, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("clustorNo", apk_type);
                    editor.putInt("time_zone_id", time_zone);
                    editor.commit();

                    SharedPreferences contract_cod = getSharedPreferences(
                            CommonUtilities.USER_SP, 0);
                    SharedPreferences.Editor contract_editor = contract_cod
                            .edit();
                    contract_editor.putString("patient_id", patientId);
                    contract_editor.commit();

                    // Sort the reminders on ascending order
                    Collections.sort(reminders,
                            new Comparator<ClassReminder>() {
                                DateFormat f = new SimpleDateFormat("hh:mm a", Locale.US);

                                @Override
                                public int compare(ClassReminder o1,
                                                   ClassReminder o2) {
                                    try {
                                        if (o1.getReminderTime() == null
                                                || o2.getReminderTime() == null)
                                            return 0;
                                        return f.parse(o1.getReminderTime())
                                                .compareTo(
                                                        f.parse(o2
                                                                .getReminderTime()));
                                    } catch (ParseException e) {
                                        throw new IllegalArgumentException(e);
                                    }
                                }
                            });

                    // insert the sorted values to Reminder DB
                    for (int i = 0; i < reminders.size(); i++) {

                        DateFormat df = new SimpleDateFormat("M/dd/yyyy", Locale.US);// FORMAT:
                        // 4/22/2015
                        String mReminderInsertTime = df.format(Calendar
                                .getInstance().getTime());

                        reminder_db.insertReminder(reminders.get(i),
                                mReminderInsertTime);

                    }

                }

            } catch (Exception e) {

                e.printStackTrace();
                Log.e(TAG, "Error Reminder downloading and saving  " + e);

            }

            return response;
        }

    }

    public void logoutClick() {
        Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
        authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(authenticationIntent);
    }

    public void SkipReminder(String patient_time) {
        Log.e(TAG,
                "Skip Alarm 'Activity || ScreenOn' or not in a valid screen Reason..........");
        reminder_skip_db.open();
        reminder_skip_db.insert(PatientIdDroid,
                patient_time, "0");
        reminder_skip_db.close();

        new Skip_Reminder_Values_Upload(contxt, serviceUrl)
                .execute();
    }

    private class AuthCheck extends AsyncTask<String, Void, String> {

        public void notificationBuilder() {
            Log.e(TAG, "notificationBuilder----------=>");
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.drawable.nico)
                            .setContentTitle(getString(R.string.icon_name))
                            .setContentText(getString(R.string.notification))
                            .setOngoing(true);
            // Creates an explicit intent for an Activity in your app
            Intent resultIntent;

            resultIntent = new Intent(getApplicationContext(),
                    FinalMainActivity.class);

            resultIntent.putExtra("Flow", "true");

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
            stackBuilder.addParentStack(AuthenticationActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                resultPendingIntent =stackBuilder.getPendingIntent(0,
                                PendingIntent.FLAG_CANCEL_CURRENT);
            } else{

                resultPendingIntent =stackBuilder.getPendingIntent(0,
                        PendingIntent.FLAG_UPDATE_CURRENT);
            }

            mBuilder.setContentIntent(resultPendingIntent);
            mBuilder.setAutoCancel(true);
            mBuilder.setVisibility(1);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(alarmSound);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

//            SkipVitals
//                    .skip_patient_vitalse(getApplicationContext());

            SharedPreferences settings2 = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
            SharedPreferences.Editor editor_retake = settings2.edit();
            editor_retake.putString("measure_time_skip", Util.get_server_time());
            editor_retake.putString("sectiondate", patient_time);

            editor_retake.commit();

            R_ID += 1;
            now.add(Calendar.MINUTE, intervalLimit);
            System.out.println("After: " + df.format(now.getTime()));

            SharedPreferences sharedpreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt("R_STATUS", -1);
            editor.putString("measure_time_skip",
                    Util.get_server_time());
            editor.putString("sectiondate", patient_time);
            editor.putInt("R_ID", R_ID);
            editor.putString("R_SKIP_TIME", df.format(now.getTime()));
            editor.commit();
            mNotificationManager.notify(R_ID, mBuilder.build());
            // mId allows you to update the notification later on.
        }

        @Override
        protected void onPostExecute(String result) {
            try {

               //Log.e(TAG, "onPostExecute=>" + result);
                if (!result.equals("Invalid AuthenticationID")) {
                    appClass.acquireWakelock();
                    ActivityManager am = (ActivityManager) context
                            .getSystemService(Context.ACTIVITY_SERVICE);
                    ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

                    String mClassName = cn.getClassName();

                    Log.i("onPostExecute-----", mClassName);
                    if ((mClassName
                            .equals("com.optum.telehealth.FinalMainActivity")
                            || mClassName
                            .equals("com.optum.telehealth.Home")
                            || mClassName
                            .equals("com.optum.telehealth.HFPHomeActivity")
                            || mClassName
                            .equals("com.optum.telehealth.Reminder_Landing_Activity") || mClassName
                            .equals("com.optum.telehealth.ThanksActivity") || mClassName
                            .equals("com.optum.telehealth.MyDataActivity") || mClassName
                            .equals("com.optum.telehealth.FeedbackActivity"))) {
                        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                        if(pm.isScreenOn() == false){
                            Log.i("onPostExecute-------", "screen off");
                            notificationBuilder();
                        } else {
//                            SkipVitals
//                                    .skip_patient_vitalse(getApplicationContext());

                            SharedPreferences sharedpreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("sectiondate", patient_time);
                            editor.commit();

                            SharedPreferences settings2 = getSharedPreferences(
                                    CommonUtilities.PREFS_NAME_date, 0);
                            SharedPreferences.Editor editor_retake = settings2
                                    .edit();
                            editor_retake.putString("measure_time_skip",
                                    Util.get_server_time());
                            editor_retake
                                    .putString("sectiondate", patient_time);

                            editor_retake.commit();
                            Log.i("onPostExecute--------", "screen on");
                            // Creates an explicit intent for an Activity in your app
                            Intent resultIntent = new Intent(getApplicationContext(),
                                    ReminderCallActivity.class);
                            //Toast.makeText(getApplicationContext(),"notification happend",Toast.LENGTH_SHORT).show();
                            resultIntent.putExtra("Flow", "true");
                            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(resultIntent);
                            Log.i("onPostExecute--------", "screen on finished");
//                        }


//                        if (appClass.paused) {
//                            Log.i("onPostExecute------", "paused");
//                            notificationBuilder();
                       }
                    } else {
                        notificationBuilder();
                    }
                } else {
                    reminder_db.deleteAll();
                }
            } catch (Exception e) {
                Log.i("onPostExecute--------", e.getMessage());
                e.printStackTrace();
            }
        }

        protected String doInBackground(String... urls) {

            String response = "";

            try {
                Log.i(TAG, "Checking device registration MAC-" + serialNo
                        + " IMEI-" + imeilNo);
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_authentication_checking.ashx",
                        new String[]{"authentication_token", "patient_id"}, new String[]{
                                token, patient_Id});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return "";
                }
                String str = Util
                        .readStream(response1.getEntity().getContent());
                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                str.toString();
                //Log.i("response:", "Authentication Response :" + str);
                if (str.length() < 10) {
                    Log.i(TAG, "Authentication Response null");
                    return "";
                }
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("optum");
                for (int i = 0; i < nodes.getLength(); i++) {
                    response = Util.getTagValue(nodes, i, "response");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Error Reminder downloading and saving  " + e);

            }
            return response;
        }
    }
}