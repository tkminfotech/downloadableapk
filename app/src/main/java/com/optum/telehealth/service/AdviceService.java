package com.optum.telehealth.service;

import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import com.optum.telehealth.util.Log;
import android.widget.Toast;

import com.optum.telehealth.AuthenticationActivity;
import com.optum.telehealth.PopActivity;
import com.optum.telehealth.R;
import com.optum.telehealth.bean.ClassAdvice;
import com.optum.telehealth.bean.ClassUser;
import com.optum.telehealth.dal.AdviceMessage_db;
import com.optum.telehealth.dal.Login_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

public class AdviceService extends Service {

    Login_db login_db1 = new Login_db(this);
    // Droid_Schedule_db db = new Droid_Schedule_db(this);
    AdviceMessage_db dbcreate = new AdviceMessage_db(this);
    private String TAG = "AdviceService Sierra";
    private String PatientIdDroid = "0";
    private String serviceUrl;

    @Override
    public void onCreate() {

        Log.i(TAG, "ADvice creating");
        super.onCreate();

    }

    @Override
    public IBinder onBind(Intent intent) {

        Log.i(TAG, "ADvice onBind");
        return null;
    }

    private Timer timer;

    private TimerTask updateTask = new TimerTask() {

        @Override
        public void run() {

            //DownloadAdviceMessage();

        }

    };


    @Override
    public void onDestroy() {

        super.onDestroy();

        Log.i(TAG, "Adicce destroying");

        this.stopSelf();

        timer.cancel();
        timer = null;
    }

    private Boolean setUrl() {

        Boolean flag = false;

        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = settings.getString("Server_post_url", "-1");

        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.WiFi_SP, 0);


        if (!serviceUrl.equals("-1")) {

            ClassUser classUser = new ClassUser();
            classUser = login_db1.SelectNickName();
            PatientIdDroid = classUser.getPatientId();

            flag = true;

        }
        //Log.i(TAG, "Constan variables :" + serviceUrl + PatientIdDroid);

        return flag;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startId) {

        super.onStart(intent, startId);

        ClassUser classUser = new ClassUser();
        Login_db login_db1 = new Login_db(this);
        classUser = login_db1.SelectNickName();
        PatientIdDroid = classUser.getPatientId(); // getting patient id

        setUrl(); // get values from sp



        timer = new Timer();
        //timer.schedule(updateTask, 0L, 10* 60 * 10000L);
        timer.schedule(updateTask, 60L, 60 * 1000L);// 10 sec*/

    }

//    public void DownloadAdviceMessage() {
//        DownloadWebPageTask task = new DownloadWebPageTask();
//        task.execute(new String[]{serviceUrl + "/droid_website/mob_handler.ashx"});
//
//    }

//    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
//
//        protected String doInBackground(String... urls) {
//
//            SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
//            String token = tokenPreference.getString("Token_ID", "-1");
//            String patientId = tokenPreference.getString("patient_id", "-1");
//            String response = "";
//
//            try {
//
//
//                HttpResponse response1 = Util.connect(serviceUrl
//                        + "/droid_website/mob_rm_feedback.ashx", new String[]{
//                        "authentication_token", "mode", "patient_id"}, new String[]{
//                        token, "0", patientId});
//
//                if (response1 == null) {
//                    Log.e(TAG, "Connection Failed!");
//                    return ""; // process
//                }
//
//                String str = Util
//                        .readStream(response1.getEntity().getContent());
//
//                str = str.replaceAll("&", "and");
//                str = str.replaceAll("\r\n", "");
//                str = str.replaceAll("\n", "");
//                str = str.replaceAll("\r", "");
//
//                str.toString();
//                //Log.i("response:", "handler_response:" + str);
//                if (str.length() < 10) {
//                    Log.i(TAG, "Advice message data null");
//                    return "";
//                }
//
//                // dbcreate.Cleartable();
//                DocumentBuilder db = DocumentBuilderFactory.newInstance()
//                        .newDocumentBuilder();
//                InputSource is = new InputSource();
//                is.setCharacterStream(new StringReader(str.toString()));
//                Document doc = db.parse(is);
//                NodeList AF_Nodes = doc.getElementsByTagName("whole_tree");
//                for (int i = 0; i < AF_Nodes.getLength(); i++) {
//
//                    response = Util.getTagValue(AF_Nodes, i, "advice_message");
//                }
//                if(response.equals("AuthenticationError")){
//
//                    return response;
//                }
//                NodeList nodes = doc.getElementsByTagName("advice");
//
//                ClassAdvice classAdvice = new ClassAdvice();
//
//                for (int i = 0; i < nodes.getLength(); i++) {
//
//                    classAdvice.setPatientId(Util.getTagValue(
//                            nodes, i, "patient_id"));
//                    classAdvice.setAdviceId(Integer.parseInt(Util.getTagValue(
//                            nodes, i, "advice_id")));
//                    classAdvice.setUserId(Integer.parseInt(Util.getTagValue(
//                            nodes, i, "user_id")));
//                    classAdvice.setAdviceSubject(Util.getTagValue(nodes, i,
//                            "advice_subject"));
//                    classAdvice.setAdviceText(Util.getTagValue(nodes, i,
//                            "advice_text"));
//                    classAdvice.setMessageDate(Util.getTagValue(nodes, i,
//                            "create_date"));
//
//                    // saving advice to db
//                    Constants.setPatientid(classAdvice.getPatientId());
//
//                    if (dbcreate.InsertAdviceMessage(classAdvice)) // if there i
//                    // a new
//                    // advice
//                    // msg
//                    {
//                        // notification();
//
//                        dbcreate.UpdateAdviceMessageByDate(classAdvice
//                                .getMessageDate());
//
//                        try {
//                            String message = classAdvice.getAdviceText();
//                            Log.i(TAG, "Redirection start");
//                            Intent intent = new Intent(AdviceService.this,
//                                    PopActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            intent.putExtra("subject", "subject");
//                            intent.putExtra("message", message);
//                            startActivity(intent);
//
//                        } catch (NullPointerException e) {
//                            // TODO Auto-generated catch block
//                            // e.printStackTrace();
//                            Log.i(TAG, "IllegalArgumentException");
//                        }
//                    }
//
//                }
//
//            } catch (Exception e) {
//                // e.printStackTrace();
//            }
//
//            return response;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            Log.i(TAG, "onPostExecute");
//            if (result.equalsIgnoreCase("AuthenticationError")) {
//                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
//                String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error", "-1");
//                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
//                logoutClick();
//            }
//        }
//    }

    public void logoutClick() {

//        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.remove("patient_name");
//        editor.remove("patient_id");
//        editor.remove("contract_code_name");
//        editor.remove("language_id");
//        editor.remove("Token_ID");
//        editor.commit();
//        SharedPreferences clusterSsettings = getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
//        SharedPreferences.Editor clusterEditor = clusterSsettings.edit();
//        clusterEditor.remove("clustorNo");
//        clusterEditor.remove("time_zone_id");
//        clusterEditor.commit();

        Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
        authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(authenticationIntent);

    }

}
