package com.optum.telehealth.service;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.AuthenticationActivity;
import com.optum.telehealth.MainActivity;
import com.optum.telehealth.R;
import com.optum.telehealth.bean.ClassSchedule;
import com.optum.telehealth.bean.ClassUser;
import com.optum.telehealth.dal.Login_db;
import com.optum.telehealth.dal.Schedule_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ScheduleService extends Service {

	Schedule_db dbSchedule = new Schedule_db(this);
	Login_db login_db1 = new Login_db(this);
	public static final String PREFS_NAME = "DroidPrefSc";
	String PostUrl = "";
	private String TAG = "ScheduleService Sierra";

	@Override
	public void onCreate() {

		Log.i(TAG, "ScheduleService creating");

		/*
		 * timer = new Timer();
		 * 
		 * //timer.schedule(updateTask, 0L, 10* 60 * 10000L);
		 * 
		 * timer.schedule(updateTask, 0L, 10 * 1000L); //10sec
		 */

		super.onCreate();

	}

	@Override
	public IBinder onBind(Intent intent) {

		Log.i(TAG, "ScheduleService onBind");
		return null;
	}

	private Timer timer;
	private int i = 12;

	private TimerTask updateTask = new TimerTask() {

		@Override
		public void run() {
			Log.i(TAG, "i=" + i + " download");
			if (i == 12) {
				
				
				//Log.e(TAG, " Paient id1 : " + PatientIdDroid + "PostUrl1 : " + PostUrl);
				
				downloadSchedule();
				i = 0;

			} else {
				Log.i(TAG, "check_schedule for this time");
				check_schedule();
				i++;
			}

		}

	};

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void notification() {
		Intent intent = new Intent(ScheduleService.this, MainActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(ScheduleService.this,
				0, intent, 0);

		Uri soundUri = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		Notification noti = new Notification.Builder(ScheduleService.this)
				.setContentTitle("Schedule Notification:- Please check")
				// .setContentText(cn.getSubject()+" at "+cn.getTime())
				.setSmallIcon(R.drawable.ic_launcher).setContentIntent(pIntent)
				.setSound(soundUri).build();

		// .addAction(R.drawable.ic_launcher, "And more", pIntent).build();

		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		noti.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, noti);
	}

	private String PatientIdDroid = "0";

	@Override
	public void onDestroy() {

		super.onDestroy();

		Log.i(TAG, "ScheduleService destroying");

		this.stopSelf();

		timer.cancel();
		timer = null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);
		PostUrl = settings.getString("Server_post_url", "-1");

		/*SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.WiFi_SP, 0);
		PostUrl = settings1.getString("wifi", "-1");*/

		if (!PostUrl.equals("-1")) {

			ClassUser classUser = new ClassUser();
			classUser = login_db1.SelectNickName();
			PatientIdDroid = classUser.getPatientId();

		}

		//Log.e(TAG, " schedule Paient id : " + PatientIdDroid + "PostUrl : " + PostUrl);

		timer = new Timer();

		// timer.schedule(updateTask, 0L, 10* 60 * 10000L); 1hr

		timer.schedule(updateTask, 0L, 5 * 60 * 1000L); // 5 min
		Log.e(TAG, " schedule timer started");

		// timer.schedule(updateTask, 0L, 10* 1000L); //10 sec

		super.onStart(intent, startId);

	}

	public void downloadSchedule() {

		Log.i(TAG, "Scedule message:-DownloadWebPageTask in service");
		downloadSchedule task = new downloadSchedule();
		// task.execute(new String[] { PostUrl + "/medication_handler.ashx" });
		task.execute(new String[] { "" });

	}

	@SuppressLint("SimpleDateFormat")
	private class downloadSchedule extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {

			Log.i(TAG, "onPostExecute Scheduleservice");
			if (result.equalsIgnoreCase("AuthenticationError")) {
//				Toast.makeText(getApplicationContext(), R.string.authenticationError, Toast.LENGTH_LONG).show();
//				logoutClick();
			}
			check_schedule();

		}

		protected String doInBackground(String... urls) {

			String response = "";

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.US);
				Date d = new Date();
				String dayOfTheWeek = sdf.format(d).toUpperCase();
				Log.e("Title Window", dayOfTheWeek);
				SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
				String serviceUrl = settings.getString("Server_post_url", "-1");
				SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
				String  token = tokenPreference.getString("Token_ID", "-1");
				String patientId = tokenPreference.getString("patient_id", "-1");
				HttpResponse response1 = Util.connect(serviceUrl
						+ "/droid_website/mob_schedule_handler.ashx",
						new String[] { "authentication_token","patient_id", "device_day" },
						new String[] { token, patientId, dayOfTheWeek});

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return "";
				} else {
					dbSchedule.DeletAllSchedule();
				}

				String str = Util
						.readStream(response1.getEntity().getContent());

				str = str.replaceAll("&", "and");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				str.toString();
				//Log.i("response:", "schedule_handler_response:" + str);

				if (str.length() < 10) {
					Log.i(TAG, "Schedule data null");
					return "";
				} else {

					// dbSchedule.DeletPatint();
				}

				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);
				NodeList AF_Nodes = doc.getElementsByTagName("schedule_group");
				for (int i = 0; i < AF_Nodes.getLength(); i++) {

					response = Util.getTagValue(AF_Nodes, i, "mob_response");
				}
				if(response.equals("AuthenticationError")){

					return response;
				}
				NodeList nodes = doc.getElementsByTagName("schedule");
				// NodeList timeNode = doc.getElementsByTagName("times");
				ClassSchedule classSchedule = new ClassSchedule();
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss aa", Locale.US);
				String currentDateandTime = dateFormat.format(new Date());
				for (int i = 0; i < nodes.getLength(); i++) {

					classSchedule.setScheduleId((Util.getTagValue(nodes, i,
							"schedule_id")));
					classSchedule.setPatientId(Util
							.getTagValue(nodes, i, "patient_id"));
					classSchedule.setLastUpdateDate((Util.getTagValue(nodes, i,
							"last_updateDate")));
					classSchedule.setDownloadDate(currentDateandTime);
					classSchedule.setStatus(0);

					classSchedule.setTimeout(Integer.parseInt(Util.getTagValue(
							nodes, i, "late_minutes")));

					Constants.setPatientid(classSchedule.getPatientId());

					Element advice_idelement = (Element) nodes.item(i);
					NodeList timeNode = advice_idelement
							.getElementsByTagName("times");
					NodeList actvivitiesNode = advice_idelement
							.getElementsByTagName("actvivities");

					// SimpleDateFormat dateFormat = new
					// SimpleDateFormat("hh:mm");
					// Time date = dateFormat.parse(time);

					for (int k = 0; k < timeNode.getLength(); k++) {
						classSchedule.setScheduleTime(Util.getTagValue(
								timeNode, k, "time"));
						for (int j = 0; j < actvivitiesNode.getLength(); j++) {

							classSchedule.setMeasureTypeId(Integer
									.parseInt(Util.getTagValue(actvivitiesNode,
											j, "measure_type_id")));

						

							dbSchedule.insertSchedule(classSchedule);

							

						}

					}
				}
				

			} catch (Exception e) {

				Log.e(TAG,
						"Error schedule downloading and saving  "
								+ e.getMessage());
				// e.printStackTrace();

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt("Scheduletatus", 0);
				editor.commit();
			}

			return response;
		}

	}

	private void check_schedule() {
		Calendar c = Calendar.getInstance();
		String timeNow = c.get(Calendar.HOUR_OF_DAY) + ":00";
		//Log.i(TAG, "Timer Now : " + timeNow + " Pid=" + PatientIdDroid);
		if (!PatientIdDroid.equals("0")) {
			ClassSchedule classSchedule = dbSchedule.selectSchedule(timeNow,
					PatientIdDroid);
			//Log.i(TAG, "Type ID : " + classSchedule.getMeasureTypeId()
			//		+ " Time : " + classSchedule.getScheduleTime() + " pId : "
			//		+ classSchedule.getPatientId());

			/*if (classSchedule.getMeasureTypeId() != 0) {
				notification();
			}*/
		}
	}
	public void logoutClick() {

//		SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
//		SharedPreferences.Editor editor = settings.edit();
//		editor.remove("patient_name");
//		editor.remove("patient_id");
//		editor.remove("contract_code_name");
//		editor.remove("language_id");
//		editor.remove("Token_ID");
//		editor.commit();
//		SharedPreferences clusterSsettings = getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
//		SharedPreferences.Editor clusterEditor = clusterSsettings.edit();
//		clusterEditor.remove("clustorNo");
//		clusterEditor.remove("time_zone_id");
//		clusterEditor.commit();

		Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
		authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(authenticationIntent);

	}
}
