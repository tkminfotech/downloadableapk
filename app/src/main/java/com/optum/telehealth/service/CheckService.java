package com.optum.telehealth.service;

import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.app.Service;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.os.IBinder;
import com.optum.telehealth.util.Log;

public class CheckService extends Service {

	private String TAG = "CheckService";

	@Override
	public void onCreate() {

		super.onCreate();

	}

	@Override
	public IBinder onBind(Intent intent) {

		Log.i("Check Log--", "TimeZoneService onBind");
		return null;
	}

	private Timer timer;

	private TimerTask BTdateTask = new TimerTask() {

		@Override
		public void run() {

			try {
				if (!isMyReminderServiceRunning()) {
					Log.e(TAG, " Starting  AlarmService from CheckService -result ");

					Intent reminder = new Intent();
					reminder.setClass(CheckService.this, AlarmService.class);
					startService(reminder);

				} else {

					Log.e(TAG, " Already Alarm Service working CheckService -result");
				}

			} catch (Exception e) {
				Log.e(TAG,
						"exception while starting or closing alam...CheckService -result ");
			}

			// code
		}

	};

	@Override
	public void onDestroy() {

		super.onDestroy();

		Log.i("Check Log", "Timer service destroying");

		this.stopSelf();

		timer.cancel();
		timer = null;
	}

	private boolean isMyReminderServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.optum.telehealth.service.AlarmService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {

		timer = new Timer();

		// timer.schedule(updateTask, 0L, 10* 60 * 10000L); 1hr

		timer.schedule(BTdateTask, 0L, 1 * 60 * 1000L); // 2 min

		super.onStart(intent, startId);

	}

}
