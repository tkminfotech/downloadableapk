package com.optum.telehealth.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.bean.ClassUser;
import com.optum.telehealth.dal.Login_db;
import com.optum.telehealth.util.CommonUtilities;


public class TimeZoneService extends Service {

	Login_db login_db1 = new Login_db(this);
	private String TAG="TimeZoneService Sierra";
	public static final String PREFS_NAME = "TimePrefSc";
	public String PatientIdDroid = "0";
	public String serviceUrl = "";
	public String serverTime = "";

	@Override
	public void onCreate() {
		
		super.onCreate();

	}

	@Override
	public IBinder onBind(Intent intent) {

		Log.i(TAG, "TimeZoneService onBind");
		return null;
	}

	private Timer timer;

	private TimerTask BTdateTask = new TimerTask() {

		@Override
		public void run() {
			
			if (setUrl())
			{
			UpdateTimeServer();
			}
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}

		}

	};

	
	@Override
	public void onDestroy() {

		super.onDestroy();

		Log.i(TAG, "Timer service destroying");

		this.stopSelf();

		timer.cancel();
		timer = null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		
		

		
		timer = new Timer();

		// timer.schedule(updateTask, 0L, 10* 60 * 10000L); 1hr

		timer.schedule(BTdateTask, 0L, 2 * 60 * 1000L); // 2 min
		
		super.onStart(intent, startId);

	}

	

	
	
	/*******************************************************************GRT TIME**********************************************************/
	
	public void UpdateTimeServer() {

		Log.e(TAG, " UpdateTimeServer started ");
		UpdateTime taskUpdate = new UpdateTime();
		taskUpdate.execute(new String[] { serviceUrl
				+ "/droid_website/mob_handler.ashx" });

	}
	
	private class UpdateTime extends AsyncTask<String, Void, String> {

		@SuppressLint("SimpleDateFormat")
		protected String doInBackground(String... urls) {

			String response = "";

			try {
						

						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(serviceUrl
								+ "/droid_website/mob_Server_time.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();

						post.setEntity(new UrlEncodedFormEntity(pairs));					

						HttpResponse response1 = client.execute(post);
						InputStream in = response1.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(in));
						StringBuilder str = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							str.append(line + "\n");
						}
						in.close();
				//Log.i(TAG, "mob_Server_time.ashx" + str.toString());
						serverTime=	str.toString().replace("\n", "");
						if(serverTime.trim().length()<5)
						{
							Log.e(TAG, " Server time is null set the default ");
							SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aa", Locale.US);
							serverTime = dateFormat.format(new Date());
						}
						setprefferenceForTimezone(serverTime);
						
			} catch (Exception e) {
				//e.printStackTrace();
			}

			// DownloadAdviceMessage();
			return response;

		}

	}
	
	public void setprefferenceForTimezone(String n)
	{
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("timezone", n);
        editor.commit();      
	}
	
	
	
	/*******************************************************************GRT TIME**********************************************************/

	
	private Boolean setUrl() {

	
		
		Boolean flag = false;

		SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
		serviceUrl = settings.getString("Server_post_url", "-1");
		
		if(!serviceUrl.equals("-1"))
		{
		
				
				ClassUser classUser = new ClassUser();				
				classUser = login_db1.SelectNickName();
				PatientIdDroid = classUser.getPatientId();

				flag = true;

		}
		//Log.i(TAG, "Constan variables :" + serviceUrl + PatientIdDroid);

		return flag;
	}

}
