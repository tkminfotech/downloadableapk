package com.optum.telehealth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.optum.telehealth.util.CommonUtilities;

public class SettingsActivity extends Activity {
    EditText server, port;
    Button save;
    Dialog dialog;
    RelativeLayout error_notify;
    TextView error_display;
    String servername;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        server = (EditText) findViewById(R.id.server);
        port = (EditText) findViewById(R.id.port);
        save = (Button) findViewById(R.id.btnsave);
        error_notify = (RelativeLayout) findViewById(R.id.error_notify);
        error_display = (TextView) findViewById(R.id.error_display);
        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }
        error_notify.setVisibility(View.INVISIBLE);
        SharedPreferences reminderPreferences = getSharedPreferences("REMINDER_STATUS", Context.MODE_PRIVATE);
        SharedPreferences.Editor reminderEditor = reminderPreferences.edit();
        reminderEditor.putBoolean("REMINDER_STATUS", false);
        reminderEditor.commit();
        addListenerOnButton();

        //###-----CALL THIS FOR LIVE BUILD-----###
        //liveBuild();

        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        servername = settings.getString("Server_url", "-1");
        SharedPreferences Login_User = getSharedPreferences("Login_User", 0);
        String username = Login_User.getString("username", "-1");





        if (!servername.equals("-1")) {
            Intent i = new Intent(SettingsActivity.this, Config_Errors_Fetch_Activity.class);
            startActivity(i);
            overridePendingTransition(0, 0);
            finish();
        }
        TextView server = (TextView) findViewById(R.id.textserver);
        TextView textport = (TextView) findViewById(R.id.textport);

        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        server.setTypeface(type, Typeface.NORMAL);
        textport.setTypeface(type, Typeface.NORMAL);
    }

    public void liveBuild() {
        try {

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.SERVER_URL_SP, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("Server_url", "optumtelehealth.com");
            editor.putString("Server_port", "443");
            editor.putString("Server_post_url",
                    "https://www.optumtelehealth.com");
            editor.commit();


//            SharedPreferences settings = getSharedPreferences(
//                    CommonUtilities.SERVER_URL_SP, 0);
//            SharedPreferences.Editor editor = settings.edit();
//            editor.putString("Server_url", "othwebtest.portal724.us");
//            editor.putString("Server_port", "8723");
//            editor.putString("Server_post_url",
//                    "https://othwebtest.portal724.us:8723");
//            editor.commit();
        } catch (Exception e) {
        }
    }



    private void addListenerOnButton() {
        save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if ((server.getText().toString()).length() != 0
                        && (port.getText().toString()).length() != 0) {
                    SharedPreferences settings = getSharedPreferences(
                            CommonUtilities.SERVER_URL_SP, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("Server_url", server.getText().toString().trim());
                    editor.putString("Server_port", port.getText().toString().trim());
                    editor.putString("Server_post_url",
                            "https://" + server.getText().toString().trim() + ':' + port.getText().toString().trim());
                    editor.commit();

                    Intent i = new Intent(SettingsActivity.this, Config_Errors_Fetch_Activity.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                } else {
                    errorShow();
                }
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void errorShow() {
        error_notify.setVisibility(View.VISIBLE);
        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        error_display.setTypeface(type, Typeface.NORMAL);
        error_display.setText(this.getString(R.string.invalidserver));
        error_display.setTypeface(type, Typeface.NORMAL);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                error_display.setText("");
                error_notify.setVisibility(View.INVISIBLE);
            }
        }, 5000);
    }
}
