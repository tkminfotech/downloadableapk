package com.optum.telehealth;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class NewPasswordActivity extends Activity implements View.OnClickListener {

    private EditText edt_newPassword;
    private EditText edt_reEnterNewPassword;
    private Button btn_back;
    private Button btn_submit;
    private String TAG = "NewPasswordActivity";
    private String password;
    private boolean isCloseClick = false;
    private TextView txt_errorMessage;
    private boolean reminderFlow;
    private String deviceStatus;
    private String pin_id;
    private String pin_message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        txt_errorMessage = (TextView) findViewById(R.id.txt_errorMessage);
        edt_newPassword = (EditText)findViewById(R.id.edt_enterNewPassword);
        edt_reEnterNewPassword = (EditText)findViewById(R.id.edt_reEnterPassword);
        btn_back = (Button)findViewById(R.id.btn_newPasswordBack);
        btn_submit = (Button)findViewById(R.id.btn_newPasswordSubmit);
        btn_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);

        SharedPreferences remPreferences = getSharedPreferences("REMINDER_STATUS", Context.MODE_PRIVATE);
        reminderFlow = remPreferences.getBoolean("REMINDER_STATUS",false);

        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {

                deviceStatus = bundle.getString("DeviceStatus","-1");
                pin_id = bundle.getString("pin_id","-1");
                pin_message = bundle.getString("message","");
            }
        } catch (Exception e) {
            Log.i("AuthenticationActivity", "R_ID Exception:" + e);
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_newPasswordBack:
                backClick();
                break;
            case R.id.btn_newPasswordSubmit:
                validatePassword();
                break;
        }
    }


private void backClick(){

    Intent i = new Intent(getApplicationContext(), AuthenticationActivity.class);
    startActivity(i);
    overridePendingTransition(0, 0);
    finish();
}

    private void clearAlldata() {

        edt_newPassword.setText("");
        edt_reEnterNewPassword.setText("");

    }

    private void validatePassword(){
        String newPassword = edt_newPassword.getText().toString().trim();
        String reEnterPassword = edt_reEnterNewPassword.getText().toString().trim();
        if(newPassword.length() < 4 || reEnterPassword.length() < 4)
            showPopup("reEnter");
        else if(!newPassword.equals(reEnterPassword))
            showPopup("invalid");
        else{
            password = edt_newPassword.getText().toString().trim();
            new createNewPassword().execute();

        }


    }

    private void showPopup(final String header) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reset_password_popup);
        dialog.setTitle("Custom Alert Dialog");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgview_resetPopupClose);
        TextView txtPopupHeader = (TextView) dialog.findViewById(R.id.txt_resetPopupHeader);
        TextView txtError = (TextView) dialog.findViewById(R.id.txt_resetPasswordPopupError);
        TextView txtErrorNumber = (TextView) dialog.findViewById(R.id.txt_resetPasswordPopupErrorNumber);
        Button btnClose = (Button) dialog.findViewById(R.id.btn_resetPopupClose);
        txtErrorNumber.setVisibility(View.GONE);
        btnClose.setVisibility(View.GONE);
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing()) {
                    if(isCloseClick ==  false) {
                        dialog.dismiss();
                        if(deviceStatus.equals("0")){

                            Intent i = new Intent(NewPasswordActivity.this, PinValidationActivity.class);
                            i.putExtra("pin_id", pin_id);
                            i.putExtra("message", pin_message);
                            i.putExtra("class_status", "true");
                            startActivity(i);
                            overridePendingTransition(0, 0);
                            finish();
                        }else{

                            Intent intent = new Intent(getApplicationContext(),
                                    FinalMainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            if(reminderFlow)
                                intent.putExtra("Flow", "true");
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                            finish();
                        }

                    }
                }
            }
        };

        if (header.equals("reEnter")) {
            txtPopupHeader.setText(R.string.reEnterPassword);
            txtError.setText(R.string.reEnterPasswordDesc);
        } else if (header.equals("success")) {
            txtPopupHeader.setText(R.string.passwordChanged);
            txtError.setText(R.string.passwordChangedDesc);
            handler.postDelayed(runnable, 5000);

        } else {
            txtPopupHeader.setText(R.string.invalidPassword);
            txtError.setText(R.string.invalidPasswordDesc);

        }

        dialog.show();
        imgClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();


                    if (header.equals("success")){
                        isCloseClick = true;
                        if(deviceStatus.equals("0")){

                            Intent i = new Intent(NewPasswordActivity.this, PinValidationActivity.class);
                            i.putExtra("pin_id", pin_id);
                            i.putExtra("message", pin_message);
                            i.putExtra("class_status", "true");
                            startActivity(i);
                            overridePendingTransition(0, 0);
                            finish();
                        }else{

                            Intent intent = new Intent(getApplicationContext(),
                                    FinalMainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            if(reminderFlow)
                                intent.putExtra("Flow", "true");
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }
                    clearAlldata();

            }
        });

    }


    private class createNewPassword extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        String serviceUrl = urlSettings.getString("Server_post_url", "-1");
        SharedPreferences Login_User = getSharedPreferences("Login_User", 0);
        String username = Login_User.getString("username", "-1");
        SharedPreferences settings1 = getSharedPreferences(CommonUtilities.USER_SP, 0);
        String patientId = settings1.getString("patient_id", "-1");
        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_request_pin");
            try {
                progressDialog = new ProgressDialog(NewPasswordActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_request_pin #doInBackground");
            String response = "";
            String language = "";
            SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
            String token = tokenPreference.getString("Token_ID", "-1");
            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_update_password.ashx", new String[]{
                                "patient_id", "username", "password","authentication_token"},

                        new String[]{patientId,username,password,token});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                //str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i(TAG, "mob_request_pin: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("update_password");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("AuthenticationError")) {

                        return mob_response;
                    }else if (mob_response.equals("Success")) {
                        response = "success";
                        return response;
                    } else {
                        response = "failed";
                        String res_message = Util.getTagValue(nodes, i, "message");
                        //Log.i(TAG, "==========FAILED==========" + res_message);
                        return response;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equalsIgnoreCase("AuthenticationError")) {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                    Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                    logoutClick();
                }else if (s.equals("success")) {

                    showPopup("success");
                } else if (s.equals("failed")) {
                    //showPopup("invalid");
                } else {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String connection_error = ERROR_MSG.getString("connection_error", "-1");
                    setNotificationMessage(connection_error);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    public void setNotificationMessage(String msg) {
        txt_errorMessage.setVisibility(View.VISIBLE);
        txt_errorMessage.setText(msg);
        btn_submit.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMessage.animate().alpha(0.0f);
                        txt_errorMessage.setVisibility(View.GONE);
                        btn_submit.setEnabled(true);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }

    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    public void logoutClick() {

        Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
        startActivity(authenticationIntent);
        finish();

    }
}
