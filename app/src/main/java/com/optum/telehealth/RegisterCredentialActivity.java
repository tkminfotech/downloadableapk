package com.optum.telehealth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class RegisterCredentialActivity extends Activity {

    private EditText edt_password, edt_userName;
    private TextView txt_header;
    private RelativeLayout password_Parent;
    private ImageButton password_show;
    boolean show_password = false;
    private Button btn_register;
    private String temp_uname, temp_password, TAG = "RegisterCredential", serviceUrl;
    private TextView txt_errorMessage;
    private String patient_id = "";
    private GlobalClass appClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_credential);
        edt_password = (EditText) findViewById(R.id.edt_password);
        txt_header = (TextView)findViewById(R.id.txt_regCredentialHeader);
        password_Parent = (RelativeLayout) findViewById(R.id.password_Parent);
        edt_userName = (EditText) findViewById(R.id.edt_userName);
        password_show = (ImageButton) findViewById(R.id.password_show);
        txt_errorMessage = (TextView) findViewById(R.id.txt_errorMessage);
        appClass = (GlobalClass) getApplicationContext();
        SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);
        txt_header.setText(settings.getString("msg_register_complete",""));
        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");

        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                patient_id = bundle.getString("patient_id");//this is for String
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        password_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (show_password) {
                    show_password = false;
                    edt_password.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    show_password = true;
                    edt_password.setTransformationMethod(null);
                }
            }
        });
        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                temp_uname = edt_userName.getText().toString().trim();
                temp_password = edt_password.getText().toString().trim();
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                if (temp_uname.length() < 8 && temp_password.length() < 4) {
                    String msg_input_error  = ERROR_MSG.getString("msg_input_error", "-1");
                    setNotificationMessage(msg_input_error );
                } else if (temp_uname.length() < 8) {
                    String msg_input_error  = ERROR_MSG.getString("msg_input_uname_error", "");
                    setNotificationMessage(msg_input_error );
                } else if (temp_password.length() < 4) {
                    String msg_input_error  = ERROR_MSG.getString("msg_input_pwd_error", "");
                    setNotificationMessage(msg_input_error );
                } else {
                    if (appClass.isConnectingToInternet(getBaseContext())) {
                        new mob_user_registration().execute();
                    } else {
                        String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                        setNotificationMessage(NoConnectionError);
                    }
                }
            }
        });

        edt_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                GradientDrawable border = new GradientDrawable();
                boolean deviceModel = getResources().getBoolean(R.bool.isTab);
                int dp;
                if (deviceModel) {
                    border.setColor(0x00000000); //Black background
                    dp = 1;
                } else {
                    border.setColor(0xFFFFFFFF); //white background
                    dp = 2;
                }

                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
                if (hasFocus) {
                    if (deviceModel) {
                        border.setStroke(px, getResources().getColor(R.color.black));
                    } else {
                        border.setStroke(px, getResources().getColor(R.color.Mob_Orange_Dark));
                    }
                } else {
                    if (deviceModel) {
                        border.setStroke(px, getResources().getColor(R.color.black));
                    } else {
                        border.setStroke(px, getResources().getColor(R.color.Mob_EditText_Border));
                    }
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    password_Parent.setBackgroundDrawable(border);
                } else {
                    password_Parent.setBackground(border);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    public void setNotificationMessage(String msg) {
        txt_errorMessage.setVisibility(View.VISIBLE);
        txt_errorMessage.setText(msg);
        btn_register.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMessage.animate().alpha(0.0f);
                        txt_errorMessage.setVisibility(View.GONE);
                        btn_register.setEnabled(true);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }

    private class mob_user_registration extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        String res_message;

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_resend_pin");
            try {
                progressDialog = new ProgressDialog(RegisterCredentialActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_user_registration #doInBackground");
            String response = "";
            SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
            String token = tokenPreference.getString("Token_ID", "-1");
            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_user_registration.ashx", new String[]{
                                "patient_id", "username", "password","authentication_token"},
                        new String[]{patient_id, temp_uname, temp_password,token});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details from server");
                    response = "error";
                    return response;
                }
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
               // Log.i(TAG, "mob_resend_pin: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("register_user");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("AuthenticationError")) {

                        return mob_response;
                    }else if (mob_response.equals("Success")) {
                        response = "success";
                        res_message = Util.getTagValue(nodes, i, "message");
                        return response;
                    } else if(mob_response.equalsIgnoreCase("failed")){
                        response = "failed";
                        res_message = Util.getTagValue(nodes, i, "message");
                        return response;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                if (s.equalsIgnoreCase("AuthenticationError")) {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                    Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                    logoutClick();
                }else if (s.equals("success")) {
                    setNotificationMessage(res_message);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(RegisterCredentialActivity.this, AuthenticationActivity.class);
                            startActivity(i);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }, 3500);
                } else if (s.equals("failed")){
                    setNotificationMessage(res_message);
                } else {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                    setNotificationMessage(NoConnectionError);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }


    public void logoutClick() {

        Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
        startActivity(authenticationIntent);
        finish();

    }
}
