package com.optum.telehealth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class RequestUsernameActivity extends Activity {
    private Button btn_back, btn_send;
    private EditText edt_userName;
    String temp_uname, TAG = "RequestUsernameActivity", serviceUrl;
    private TextView txt_errorMessage;
    private GlobalClass appClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_username);

        appClass = (GlobalClass) getApplicationContext();

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_send = (Button) findViewById(R.id.btn_send);
        edt_userName = (EditText) findViewById(R.id.edt_userName);
        txt_errorMessage = (TextView) findViewById(R.id.txt_errorMessage);

        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                temp_uname = edt_userName.getText().toString();
                if(temp_uname.length()<1){
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String NoConnectionError = ERROR_MSG.getString("msg_no_entry", "-1");
                    setNotificationMessage(NoConnectionError);
                } else{
                    if (appClass.isConnectingToInternet(getBaseContext())) {
                        new mob_request_username().execute();
                    } else {
                        SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                        String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                        setNotificationMessage(NoConnectionError);
                    }
                }
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RequestUsernameActivity.this, AuthenticationActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    public void setNotificationMessage(String msg) {
        txt_errorMessage.setVisibility(View.VISIBLE);
        txt_errorMessage.setText(msg);
        btn_send.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMessage.animate().alpha(0.0f);
                        txt_errorMessage.setVisibility(View.GONE);
                        btn_send.setEnabled(true);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }

    private class mob_request_username extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        String res_message;

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_resend_pin");
            try {
                progressDialog = new ProgressDialog(RequestUsernameActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_resend_pin #doInBackground");
            String response = "";
            try {
                SharedPreferences settings_n = getSharedPreferences(
                        CommonUtilities.USER_SP, 0);
                String lngID = settings_n.getString("language_id", "0");
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_forgot_username.ashx", new String[]{
                                "user_input","language_id"},
                        new String[]{temp_uname,lngID});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
               // Log.i(TAG, "mob_resend_pin: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("forgot_username");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("Success")) {
                        response = "success";
                        res_message = Util.getTagValue(nodes, i, "message");
                        return response;
                    } else {
                        response = "failed";
                        res_message = Util.getTagValue(nodes, i, "message");
                        return response;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("success")) {
                    setNotificationMessage(res_message);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(RequestUsernameActivity.this, AuthenticationActivity.class);
                            startActivity(i);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }, 3500);
                } else if (s.equals("error")) {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                    setNotificationMessage(NoConnectionError);
                } else {
                    setNotificationMessage(res_message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
}
