package com.optum.telehealth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Config_Errors_Fetch_Activity extends Activity {

    private GlobalClass appClass;
    private LinearLayout view_InternetError;
    private ImageButton img_btn_Internet;
    private String TAG = "Config_Errors", serviceUrl;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appClass = (GlobalClass) getApplicationContext();
        appClass.changeLanguage(this);
        setContentView(R.layout.activity_config_error_fetch);

        SharedPreferences Login_User = getSharedPreferences("Login_User", 0);
        username = Login_User.getString("username", "-1");

        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");


        view_InternetError = (LinearLayout) findViewById(R.id.view_InternetError);
        img_btn_Internet = (ImageButton) findViewById(R.id.img_btn_Internet);

        img_btn_Internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InternetChecker();
            }
        });

        InternetChecker();
    }

    private void InternetChecker() {
        if (appClass.isConnectingToInternet(getBaseContext())) {
            if (view_InternetError.getVisibility() == View.VISIBLE) {
                view_InternetError.setVisibility(View.GONE);
            }
            new mob_fetch_error_msg().execute();
        } else {
            view_InternetError.setVisibility(View.VISIBLE);
        }
    }

    private class mob_fetch_error_msg extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_fetch_error_msg");
            try {
                progressDialog = new ProgressDialog(Config_Errors_Fetch_Activity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_fetch_error_msg #doInBackground");
            String response = "";
            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_get_error_message.ashx", new String[]{
                                "imei_no"},
                        new String[]{"00000"});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                //str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
               // Log.i(TAG, "mob_fetch_error_msg: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("error_message_details");
                for (int i = 0; i < nodes.getLength(); i++) {
                    Element error_element = (Element) nodes.item(i);
                    NodeList error_message = error_element
                            .getElementsByTagName("error_message");
                    for (int j = 0; j < error_message.getLength(); j++) {
                        String error_code = Util.getTagValue(error_message, j, "error_code");
                        String error_description = Util.getTagValue(error_message, j, "error_description");
                        String error_description_spanish = Util.getTagValue(error_message, j, "error_description_spanish");
                        //Log.i(TAG, "#onPostExecute error_code: " + error_code);
                        //Log.i(TAG, "#onPostExecute error_description: " + error_description);
                        //Log.i(TAG, "#onPostExecute error_description_spanish: " + error_description_spanish);
                        SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        if (getResources().getBoolean(R.bool.isSpanish)) {
                            //Spanish Error Msg
                            editor.putString(error_code, error_description_spanish);
                            editor.commit();
                        } else {
                            //English Error Msg
                            editor.putString(error_code, error_description);
                            editor.commit();
                        }
                    }
                    response = "success";
                    return response;
                }
            } catch (Exception e) {
                e.printStackTrace();
                response = "error";
                return response;
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("error")) {
                    view_InternetError.setVisibility(View.VISIBLE);
                } else {
                    SharedPreferences installed_User = getSharedPreferences(CommonUtilities.TermesAndCondition_SP, 0);
                    String isAccept = installed_User.getString("ACCEPT", "no");
                    if (isAccept.equals("yes")) {

                        if (!username.equals("-1")) {
                            Intent i = new Intent(Config_Errors_Fetch_Activity.this, AuthenticationActivity.class);
                            startActivity(i);
                            overridePendingTransition(0, 0);
                            finish();
                        } else {
                            Intent i = new Intent(Config_Errors_Fetch_Activity.this, RegistrationActivity.class);
                            startActivity(i);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    } else {

                        Intent i = new Intent(Config_Errors_Fetch_Activity.this, Terms_ConditionsActivity.class);
                        i.putExtra("patient_id", "1234567");
                        i.putExtra("flow_from", "registration");
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    }


//                    if (!username.equals("-1")) {
//                        Intent i = new Intent(Config_Errors_Fetch_Activity.this, AuthenticationActivity.class);
//                        startActivity(i);
//                        overridePendingTransition(0, 0);
//                        finish();
//                    } else {
//                        Intent i = new Intent(Config_Errors_Fetch_Activity.this, RegistrationActivity.class);
//                        startActivity(i);
//                        overridePendingTransition(0, 0);
//                        finish();
//                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
}
