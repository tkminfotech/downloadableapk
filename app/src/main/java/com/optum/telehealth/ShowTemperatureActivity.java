package com.optum.telehealth;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.optum.telehealth.dal.MeasureType_db;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.dal.Temperature_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.NumberToString;
import com.optum.telehealth.util.Util;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.text.Html;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ShowTemperatureActivity extends Titlewindow {

    private Button NextButton = null, retryButon = null;
    private GlobalClass appClass;
    private Sensor_db dbSensor = new Sensor_db(this);
    private String TAG = "ShowTemperatureActivity";
    private Temperature_db dbtemparature = new Temperature_db(this);
    MeasureType_db dbsensor1 = new MeasureType_db(this);
    private int redirectFlag = 0;
    private TextView txt_errorMsg;
    boolean resumeFlag = false;
    private String unit;

    private double tempValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_temperature);
        appClass = (GlobalClass) getApplicationContext();
        appClass.isSupportEnable = true;
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");

        TextView msg = (TextView) findViewById(R.id.t);

        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
        int manualEntry = settings.getInt("manualEntry", 0);
        tempValue = Double.parseDouble(extras.getString("temp"));
        unit = getResources().getText(R.string.unittemperature).toString();
       // if(manualEntry == 1) {
            int temperature_unit_id = settings.getInt("temperature_unit_id", -1);
            if (temperature_unit_id == 1) {
                unit = getResources().getText(R.string.unittemperature_C).toString();
                if(manualEntry == 1){

                }else{

                    tempValue = Util.round((tempValue -32)/1.8);
                }
            }
    //    }


        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("manualEntry", 0);
        editor.commit();
        try {
            if (extras.getString("temp").trim().length() >= 1) {
                int language = Constants.getLanguage();
                if (language == 11) {
                    msg.setText(Html.fromHtml("Su temperatura es de <b>" + tempValue + "</b> "+unit+"."));
                } else {
                    msg.setText(Html.fromHtml("Your temperature is <b>" + tempValue + "</b> "+unit+"."));
                }
                playSound();

            } else {
                msg.setText(this.getString(R.string.yourtemperature));
            }

        } catch (Exception e) {
            // Log.e("Droid Error",e.getMessage());
            Log.i(TAG, " Exception show show tepm  on create ");
        }
        addListenerOnButton();
       redirector();
    }

    public Boolean preference() {
        Boolean flag = null;
        try {

            String PREFS_NAME = "DroidPrefSc";
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            int scheduletatus = settings.getInt("Scheduletatus", -1);
            Log.i("Droid", " SharedPreferences : " + getApplicationContext()
                    + " is : " + scheduletatus);
            if (scheduletatus == 1)
                flag = true;
            else
                flag = false;
        } catch (Exception e) {
            // Log.e("Droid Error",e.getMessage());
            Log.i(TAG, "preference() Exception show glucose----------- ");
        }
        return flag;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onStop() {

        Log.i(TAG, "----------on stop temp----------- ");
        /*
		 * Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		 * Util.Stopsoundplay();
		 */
        redirectFlag = 1;
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, " KEYCODE_BACK clicked: show glucose");
//
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            finish();
//
//        }
        return super.onKeyDown(keyCode, event);
    }

    private void addListenerOnButton() {

        NextButton = (Button) findViewById(R.id.btnacceptTemp);

        NextButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                appClass.appTracking("Temperature reading display page","Click on Accept button");
                redirect();
            }
        });
        retryButon = (Button) findViewById(R.id.btnwtretaketemp);

        SharedPreferences settingsNew = getSharedPreferences(
                CommonUtilities.RETAKE_SP, 0);

        int retake = settingsNew.getInt("retake_status", 0);

        /*if (retake == 1) {
            retryButon.setVisibility(View.INVISIBLE);
        }*/

        retryButon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "retry clicked");
                appClass.appTracking("Temperature reading display page","Click on Retake button");
                delete_last_meassurement();

                // set retak sp
                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.RETAKE_SP, 0);
                SharedPreferences.Editor editor_retake = settings.edit();
                editor_retake.putInt("retake_status", 1);
                editor_retake.commit();
                redirectFlag = 1;
                RedirectTemperature();

            }

        });

    }

    private void RedirectTemperature() {
        String tmpMac = dbSensor.SelectTempSensorName();
        dbSensor.close();

        Log.i(TAG, "tmpMac" + tmpMac);

        if (tmpMac.trim().length() > 4) {
            Intent intentfr = new Intent(getApplicationContext(),
                    ForaMainActivity.class);
            intentfr.putExtra("deviceType", 2);
            // intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
            intentfr.putExtra("macaddress", getForaMAC(tmpMac));
            startActivity(intentfr);
            this.finish();

        } else {
            Intent intentfr = new Intent(getApplicationContext(),
                    TemperatureEntry.class);
            startActivity(intentfr);
            this.finish();
        }

    }

    private String getForaMAC(String mac) {

        String macAddress = "";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length(); i = i + 2) {
            // macAddress.substring(i, i+2)
            sb.append(mac.substring(i, i + 2));
            sb.append(":");
        }

        macAddress = sb.toString();
        macAddress = macAddress.substring(0, macAddress.length() - 1);

        Log.i(TAG, " : Connect to macAddress " + macAddress);
        // #
        if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
            return macAddress;
        } else {
            return "";
        }
    }

    private void delete_last_meassurement() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        dbtemparature.delete_pulse_data(extras.getInt("tempid"));

    }

    private String patient_id;

    public void UpdateMeasurementtoServer() {

        patient_id = Constants.getdroidPatientid() + "";
        Log.e(TAG, " UpdateMeasurementtoServer Temperature : ");
        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        String serviceUrl = settings.getString("Server_post_url", "-1");
        Update taskUpdate = new Update();
        taskUpdate.execute(new String[]{serviceUrl
                + "/droid_website/mob_handler.ashx"});

    }

    private class Update extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {

            String response = "";
            Log.i(TAG, "uploading started.");

            /********************************* Temperature **************************/
            try {

                Cursor ctemperature = dbtemparature.SelectMeasuredTemperature();
                ctemperature.moveToFirst();
                if (ctemperature.getCount() > 0) {

                    while (ctemperature.isAfterLast() == false) {
                        Log.i(TAG,
                                "Uploading temparature data to server started ...");
                        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
                        String serviceUrl = settings.getString("Server_post_url", "-1");
                        SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        String token = tokenPreference.getString("Token_ID", "-1");
                        String patientId = tokenPreference.getString("patient_id", "-1");
                        HttpClient client = new DefaultHttpClient();
                        HttpPost post = new HttpPost(
                                serviceUrl
                                        + "/droid_website/mob_add_tpm_body_temperature.ashx");

                        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                        post.setEntity(new UrlEncodedFormEntity(pairs));
                        post.setHeader("authentication_token", token);
                        post.setHeader("login_patient_id", patientId);
                        post.setHeader("patient_id", ctemperature.getString(4));
                        post.setHeader("transmit_date", Util.get_patient_time_zone_time(getApplicationContext()));
                        post.setHeader("measure_date",
                                ctemperature.getString(2));
                        post.setHeader("fahrenheit", ctemperature.getString(1));
                        post.setHeader("input_mode", ctemperature.getString(5));
                        post.setHeader("section_date",
                                ctemperature.getString(6));
                        post.setHeader("is_reminder", ctemperature.getString(8));
                        HttpResponse response1 = client.execute(post);
                        int a = response1.getStatusLine().getStatusCode();
                        InputStream in = response1.getEntity().getContent();
                        BufferedReader reader = new BufferedReader(
                                new InputStreamReader(in));
                        StringBuilder str = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            str.append(line + "\n");
                        }

                        in.close();
                        str.toString();
                        //Log.i("response:", "bodyTemp_response:" + str);
                        if (str.length() > 0) {
                            DocumentBuilder db = DocumentBuilderFactory
                                    .newInstance().newDocumentBuilder();
                            InputSource is1 = new InputSource();
                            is1.setCharacterStream(new StringReader(str
                                    .toString()));
                            Document doc = db.parse(is1);
                            NodeList nodes = doc.getElementsByTagName("optum");
                            for (int i = 0; i < nodes.getLength(); i++) {

                                response = Util.getTagValue(nodes, i, "response");
                            }
                            if (response.equals("Success")) {
                                // /// if success delete the value from the
                                // tablet db
                                Log.i(TAG,
                                        "Update temperature data with id ..."
                                                + ctemperature.getString(0));
                                dbtemparature.UpdatetemparatureData(Integer
                                        .parseInt(ctemperature.getString(0)));
                            }
                        }
                        ctemperature.moveToNext();
                    }
                }
                ctemperature.close();
                dbtemparature.cursorTemperature.close();
                dbtemparature.close();

            } catch (Exception e) {
                // e.printStackTrace();
            }
            // DownloadAdviceMessage();
            return response;

        }


        @Override
        protected void onPostExecute(String result) {
            //Log.i("onPostExecute", result);
           if (result.equalsIgnoreCase("AuthenticationError")) {
               SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
               String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            }


        }

    }

    ArrayList<String> final_list = new ArrayList<String>();

    private void playSound() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        final_list.add("Messages/TempIs.wav");
        add_to_finalList(extras.getString("temp"));
        final_list.add("Messages/degreesF.wav");
    }

    private void add_to_finalList(String vitalValue) {
        String no_to_string = "";
        NumberToString ns = new NumberToString();

        if (vitalValue.contains(".")) {
            Double final_value = Double.parseDouble(vitalValue);
            no_to_string = ns.getNumberToString(final_value);
        } else {
            Long final_value = Long.parseLong(vitalValue);
            no_to_string = ns.getNumberToStringLong(final_value);
        }

        List<String> sellItems = Arrays.asList(no_to_string.split(" "));

        for (String item : sellItems) {
            if (item.toString().length() > 0) {
                final_list.add("Numbers/" + item.toString() + ".wav");
            }
        }

        if (final_list.get(final_list.size() - 1).toString()
                .equals("Numbers/zero.wav")) // remove if last item is zero
        {
            final_list.remove(final_list.size() - 1);
        }
    }

    private void redirect() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appClass.setBundle(extras);
        } else {
            extras = appClass.getBundle();
        }
        dbsensor1.updateStatus(6); // update the vitals as taken
        dbtemparature.UpdatetemparatureData_as_valid(extras.getInt("tempid"));
        Log.i(TAG, "accept clicked");
        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.RETAKE_SP, 0);
        SharedPreferences.Editor editor_retake = settings1.edit();
        editor_retake.putInt("retake_status", 0);
        editor_retake.commit();

        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);
        int val = settings.getInt("flow", 0); // #1
        if (val == 1) {
           // if (appClass.isConnectingToInternet(this)) {
                Intent intentSc = new Intent(getApplicationContext(),
                        UploadMeasurement.class);
                startActivity(intentSc);
                finish();

                overridePendingTransition(0, 0);
//            } else {
//                errorShow(this.getString(R.string.netConnectionErrorMsg));
//            }


        } else if (preference()) {
           // if (appClass.isConnectingToInternet(this)) {
                Intent intentSc = new Intent(getApplicationContext(),
                        UploadMeasurement.class);
                intentSc.putExtra("reType", 1);

                startActivity(intentSc);
                finish();
                Log.e(TAG, "redirect to upload temp page");
//            } else {
//                errorShow(this.getString(R.string.netConnectionErrorMsg));
//            }

        } else {
            if (appClass.isConnectingToInternet(this)) {
                UpdateMeasurementtoServer();
            } else {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                errorShow(NoConnectionError);
            }


            // String tmpMac = dbSensor.SelectGlucoName();
            String sensor_name = dbSensor.SelectGlucose_sensor_Name();
            dbSensor.close();

            if (sensor_name.contains("One Touch Ultra")) {

                Intent intent = new Intent(getApplicationContext(),
                        GlucoseReader.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);

            } else if (sensor_name.contains("Bayer")) {

                Intent intent = new Intent(getApplicationContext(),
                        GlucoseReader.class);

                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);

            } else if (sensor_name.contains("Accu-Chek")) {

                if (Build.VERSION.SDK_INT >= 18) {
                    Intent intent = new Intent(getApplicationContext(),
                            GlucoseAccuChek.class);
                    finish();
                    startActivity(intent);
                } else {

                    Toast.makeText(getBaseContext(), "not support ble",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),
                            GlucoseEntry.class);
                    startActivity(intent);
                    finish();

                }

            } else if (sensor_name.contains("Taidoc")) {

                if (Build.VERSION.SDK_INT >= 18) {
                    Intent intent = new Intent(getApplicationContext(),
                            GlucoseTaidoc.class);
                    startActivity(intent);
                    finish();
                } else {

                    Toast.makeText(getBaseContext(), "not support ble",
                            Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplicationContext(),
                            GlucoseEntry.class);
                    startActivity(intent);
                    finish();

                }

            } else if (sensor_name.trim().length() > 0) {
                Intent intent = new Intent(getApplicationContext(),
                        GlucoseEntry.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            } else {
                Intent intentfr = new Intent(getApplicationContext(),
                        ThanksActivity.class);

                startActivity(intentfr);
                overridePendingTransition(0, 0);
                finish();
                // Toast.makeText(getApplicationContext(),
                // "Please assign glucose.",
                // Toast.LENGTH_LONG).show();
            }
        }
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        NextButton.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        NextButton.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }
    private void redirector() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivityManager am = (ActivityManager) getApplicationContext()
                        .getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                if (cn.getClassName().equals(
                        "com.optum.telehealth.ShowTemperatureActivity")) {
                    if (redirectFlag == 0) {
                        redirect();
                    }
                }
            }
        }, 15000);
    }
    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        resumeFlag = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onPause" + resumeFlag);
        if (resumeFlag) {
            redirectFlag = 0;
            redirector();
        }
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
