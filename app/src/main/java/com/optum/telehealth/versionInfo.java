package com.optum.telehealth;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created on 4/25/2016.
 * JUST A VERSION INFO ACTIVITY FOR OTH APK
 */
public class versionInfo extends Activity{
    private TextView txt_udiHeader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.versioninfo);
        txt_udiHeader = (TextView)findViewById(R.id.txt_UDIHeader);
        ImageButton versionclose = (ImageButton) findViewById(R.id.versionclose);
        versionclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Typeface type1 = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        txt_udiHeader.setTypeface(type1, Typeface.BOLD);
    }
}
