package com.optum.telehealth;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.dal.Flow_Status_db;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.util.Alam_util;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Regular_Alam_util;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Home extends Titlewindow implements OnClickListener {

    private String TAG = "Optum_Home";
    Sensor_db dbSensor = new Sensor_db(this);
    ObjectAnimator AnimPlz, ObjectAnimator;
    private Sensor_db sensor_db = new Sensor_db(this);
    private Flow_Status_db Flow_db = new Flow_Status_db(this);
    private RelativeLayout layout_weight;
    private RelativeLayout layout_bloodPressure;
    private RelativeLayout layout_bloodGlucose;
    private RelativeLayout layout_bloodOxygen;
    private RelativeLayout layout_temperature;
    private RelativeLayout layout_question;

    private RelativeLayout layout_bloodPressure1;
    private RelativeLayout layout_question1;
    private RelativeLayout layout_temp1;

    private LinearLayout layout_firstRow;
    private LinearLayout layout_secondRow;
    private Button btn_start_session;
    private ImageButton btn_weight;
    private ImageButton btn_temp;
    private ImageButton btn_question;
    private ImageButton btn_bp;
    private ImageButton btn_bo;
    private ImageButton btn_bg;

    private ImageButton btn_bp1;
    private ImageButton btn_question1;
    private ImageButton btn_temp1;
    private GlobalClass globalClass;

    private TextView txt_weightLastReading;
    private TextView txt_tempLastReading;
    private TextView txt_questionLastReading;
    private TextView txt_pressureLastReading;
    private TextView txt_oxygenLastReading;
    private TextView txt_glucoseLastReading;
    private TextView txt_pressureLastReading1;
    private TextView txt_questionLastReading1;
    private TextView txt_tempLastReading1;
    private String serviceUrl;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        globalClass = (GlobalClass) getApplicationContext();
        globalClass.isEllipsisEnable = true;
        appClass.isSupportEnable = true;
        Log.e("Home", "ONCREATE");
        layout_weight = (RelativeLayout) findViewById(R.id.layout_weight);
        layout_bloodGlucose = (RelativeLayout) findViewById(R.id.layout_bloodGlucose);
        layout_bloodPressure = (RelativeLayout) findViewById(R.id.layout_bloodPressure);
        layout_bloodOxygen = (RelativeLayout) findViewById(R.id.layout_bloodOxygen);
        layout_temperature = (RelativeLayout) findViewById(R.id.layout_temperature);
        layout_question = (RelativeLayout) findViewById(R.id.layout_question);
        layout_weight.setOnClickListener(this);
        layout_bloodPressure.setOnClickListener(this);
        layout_bloodOxygen.setOnClickListener(this);
        layout_bloodGlucose.setOnClickListener(this);
        layout_temperature.setOnClickListener(this);
        layout_question.setOnClickListener(this);
        layout_question1 = (RelativeLayout) findViewById(R.id.layout_question1);
        layout_bloodPressure1 = (RelativeLayout) findViewById(R.id.layout_bloodPressure1);
        layout_temp1 = (RelativeLayout) findViewById(R.id.layout_temperature1);
        layout_bloodPressure1.setOnClickListener(this);
        layout_question1.setOnClickListener(this);
        layout_temp1.setOnClickListener(this);
        btn_weight = (ImageButton) findViewById(R.id.btn_weight);
        btn_temp = (ImageButton) findViewById(R.id.btn_temp);
        btn_question = (ImageButton) findViewById(R.id.btn_qs);
        btn_bp = (ImageButton) findViewById(R.id.btn_Bp);
        btn_bo = (ImageButton) findViewById(R.id.btn_bloodOxygen);
        btn_bg = (ImageButton) findViewById(R.id.btn_bloodGlucose);
        btn_weight.setOnClickListener(this);
        btn_temp.setOnClickListener(this);
        btn_question.setOnClickListener(this);
        btn_bp.setOnClickListener(this);
        btn_bo.setOnClickListener(this);
        btn_bg.setOnClickListener(this);
        btn_bp1 = (ImageButton) findViewById(R.id.btn_Bp1);
        btn_question1 = (ImageButton) findViewById(R.id.btn_question1);
        btn_temp1 = (ImageButton) findViewById(R.id.btn_temperature1);
        btn_bp1.setOnClickListener(this);
        btn_question1.setOnClickListener(this);
        btn_temp1.setOnClickListener(this);
        layout_firstRow = (LinearLayout) findViewById(R.id.layout_firstRow);
        layout_secondRow = (LinearLayout) findViewById(R.id.layout_secondRow);

        txt_weightLastReading = (TextView)findViewById(R.id.txt_weightLastReading);
        txt_tempLastReading = (TextView)findViewById(R.id.txt_tempLastReading);
        txt_questionLastReading = (TextView)findViewById(R.id.txt_questionLastReading);
        txt_pressureLastReading = (TextView)findViewById(R.id.txt_pressureLastReading);
        txt_oxygenLastReading = (TextView)findViewById(R.id.txt_oxygenLastReading);
        txt_glucoseLastReading = (TextView)findViewById(R.id.txt_glucoseLastReading);
        txt_pressureLastReading1 = (TextView)findViewById(R.id.txt_pressureLastReading1);
        txt_questionLastReading1 = (TextView)findViewById(R.id.txt_questionLastReading1);
        txt_tempLastReading1 = (TextView)findViewById(R.id.txt_temperatureLastReading1);
        SharedPreferences serviceSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = serviceSettings.getString("Server_post_url", "-1");

        if(appClass.isConnectingToInternet(getApplicationContext())){

            downloadSensorLastReading();
        }else{

            SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
            txt_weightLastReading.setText(" " + lastStatus.getString("weight_lastReading",""));
            txt_tempLastReading.setText(" " + lastStatus.getString("temperature_lastReading",""));
            txt_questionLastReading.setText(" " + lastStatus.getString("question_lastReading",""));
            txt_pressureLastReading.setText(" " + lastStatus.getString("pressure_lastReading",""));
            txt_oxygenLastReading.setText(" " + lastStatus.getString("oxygen_lastReading",""));
            txt_glucoseLastReading.setText(" " + lastStatus.getString("glucose_lastReading",""));
            txt_pressureLastReading1.setText(" " + lastStatus.getString("pressure_lastReading",""));
            txt_questionLastReading1.setText(" " + lastStatus.getString("question_lastReading",""));
            txt_tempLastReading1.setText(" " + lastStatus.getString("temperature_lastReading",""));
        }



        btn_start_session = (Button) findViewById(R.id.btn_start_session);
        btn_start_session.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                globalClass.appTracking("Home Page","Click on Start button");
                startSession();
            }
        });
        //txtwelcom = (TextView) findViewById(R.id.animtext);
        SharedPreferences flow = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);

        SharedPreferences.Editor editor = flow.edit();
        editor.putInt("flow", 1);
        editor.putInt("start_flow", 0);// for reloading 5 minutes
        editor.commit();
        //anim();
        getUI();

        Flow_db.open();
        Flow_db.deleteAll();
        Flow_db.insert("1");
        Flow_db.close();

        // clear retak sp
        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.RETAKE_SP, 0);
        SharedPreferences.Editor editor_retake = settings.edit();
        editor_retake.putInt("retake_status", 0);
        editor_retake.commit();

        try {
            Log.i(TAG, "canceling all the alams");
            Regular_Alam_util.cancelAlarm(Home.this);
            Alam_util.cancelAlarm(Home.this);

        } catch (Exception e) {
            // TODO: handle exception
        }

        Log.i(TAG, "---------on create completed home----------");
    }


    private void getUI() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = (displaymetrics.heightPixels);
        int width = (displaymetrics.widthPixels / 4) - 120;
        Log.i(TAG, " screen width/4- 120 -" + width);
        // Log.i(TAG, "split_width-"+split_width);

        LinearLayout row1 = (LinearLayout) findViewById(R.id.button_layout1);
        LinearLayout row2 = (LinearLayout) findViewById(R.id.button_layout2);
        // row2.setVisibility(View.GONE);
        Cursor c = sensor_db.SelecthomeOrder();
        c.moveToFirst();
        int i = 3;
        Log.i(TAG, "---------creating dynamic button----------");
        if (c.getCount() > 0) {

            while (c.isAfterLast() == false) {

                int a = Integer.parseInt(c.getString(6));
                //int measuretype = Integer.parseInt(c.getString(3));

                c.moveToNext();

                //Button ivBowl = new Button(this);

                //if (i < 3)

                //{
                if (a == 1) {
                    i--;
                    layout_weight.setVisibility(View.VISIBLE);
                    //ivBowl.setBackgroundResource(R.drawable.wtclick);
                } else if (a == 2) {
                    i--;
                    layout_bloodOxygen.setVisibility(View.VISIBLE);
                    //ivBowl.setBackgroundResource(R.drawable.tempclick);
                } else if (a == 3) {
                    i--;
                    layout_bloodGlucose.setVisibility(View.VISIBLE);

                    //ivBowl.setBackgroundResource(R.drawable.qsclick);
                } else if (a == 4) {
                    if (i > 0) {
                        i--;
                        layout_bloodPressure.setVisibility(View.VISIBLE);
                    } else {

                        layout_bloodPressure1.setVisibility(View.VISIBLE);
                    }
                    // ivBowl.setBackgroundResource(R.drawable.bpclick);
                } else if (a == 5) {
                    if (i > 0) {
                        i--;
                        layout_question.setVisibility(View.VISIBLE);


                    } else {

                        layout_question1.setVisibility(View.VISIBLE);
                    }
                    //ivBowl.setBackgroundResource(R.drawable.boclick);
                } else if (a == 6) {
                    if (i > 0) {
                        i--;
                        layout_temperature.setVisibility(View.VISIBLE);
                    } else {

                        layout_temp1.setVisibility(View.VISIBLE);
                    }
                    //ivBowl.setBackgroundResource(R.drawable.bgclick);
                }
//if(i == 0) {
//    ViewTreeObserver vto = layout_firstRow.getViewTreeObserver();
//    vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//        @Override
//        public void onGlobalLayout() {
//            layout_firstRow.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//            int width = layout_firstRow.getMeasuredWidth();
//            int height = layout_firstRow.getMeasuredHeight();
//            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout_secondRow.getLayoutParams();
//            int wd = layout_firstRow.getMeasuredWidth();
//            params.width = width;
//            layout_secondRow.setGravity(Gravity.LEFT);
//            layout_secondRow.setLayoutParams(params);
//        }
//    });
//
//}
//                    ivBowl.setId(measuretype);
//                    ivBowl.setOnClickListener(this);
//                    // ivBowl.setBackgroundResource(R.drawable.wtclick);
//                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
//                            LayoutParams.WRAP_CONTENT,
//                            LayoutParams.WRAP_CONTENT);
//
//                    // cp.update(0, 0, 350, 350);
//                    // cp.update(0, 0, 750, 500);
//                    // cp.update(0, 0, width,height);
//
//                    // layoutParams.setMargins(155, 45, 0, 0); // left, top,
//                    // right,
//                    // bottom
//                    if (i == 0) {
//                        if (width > 160) {
//                            layoutParams.setMargins((int) (width / 2.5), 45, 0,
//                                    0);
//                        } else {
//                            layoutParams.setMargins(width, 45, 0, 0);
//
//                        }
//                    } else {
//                        layoutParams.setMargins(width, 45, 0, 0);
//                    }
//
//                    ivBowl.setLayoutParams(layoutParams);
//                    row1.addView(ivBowl);
//                } else {
//
//                    if (a == 1)
//                        ivBowl.setBackgroundResource(R.drawable.wtclick);
//                    else if (a == 2)
//                        ivBowl.setBackgroundResource(R.drawable.tempclick);
//                    else if (a == 3)
//                        ivBowl.setBackgroundResource(R.drawable.qsclick);
//                    else if (a == 4)
//                        ivBowl.setBackgroundResource(R.drawable.bpclick);
//                    else if (a == 5)
//                        ivBowl.setBackgroundResource(R.drawable.boclick);
//                    else if (a == 6)
//                        ivBowl.setBackgroundResource(R.drawable.bgclick);
//                    ivBowl.setId(measuretype);
//                    ivBowl.setOnClickListener(this);
//                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
//                            LayoutParams.WRAP_CONTENT,
//                            LayoutParams.WRAP_CONTENT);
//                    // layoutParams.setMargins(width, 25, 0, 0); // left, top,
//                    // right,
//                    // bottom
//
//                    if (i == 3) {
//                        if (width > 160) {
//
//                            layoutParams.setMargins((int) (width / 2.5), 45, 0,
//                                    0);
//
//                        } else {
//                            layoutParams.setMargins(width, 45, 0, 0);
//
//                        }
//                    } else {
//
//                        layoutParams.setMargins(width, 45, 0, 0);
//
//                    }
//
//                    ivBowl.setLayoutParams(layoutParams);
//                    row2.addView(ivBowl);
//                }
//                i++;
            }
        }
        c.close();
        sensor_db.cursorsensor.close();
        sensor_db.close();

    }

    public void onClick(View v) {

        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.PREFS_NAME_date, 0);
        SharedPreferences.Editor editor_retake = settings1.edit();
        editor_retake.putString("sectiondate",
                Util.get_patient_time_zone_time(this));
        editor_retake.commit();
        switch (v.getId()) {

            case R.id.layout_weight:
                globalClass.appTracking("Home Page","Click on Weight button");
                redirectWT(); // wt
                break;
            case R.id.layout_question:
                globalClass.appTracking("Home Page","Click on Questions button");
                redirectToQuestion();
                break;
            case R.id.layout_bloodOxygen:
                globalClass.appTracking("Home Page","Click on Blood Oxygen button");
                redirectPulse();// puls
                break;
            case R.id.layout_bloodPressure:
                globalClass.appTracking("Home Page","Click on Blood Pressure button");
                redirectBP(); // bp
                break;
            case R.id.layout_temperature:
                globalClass.appTracking("Home Page","Click on Temperature button");
                redirectTemp();// temp
                break;
            case R.id.layout_bloodGlucose:
                globalClass.appTracking("Home Page","Click on  Blood Glucose button");
                redirectToGlucose();
                break;
            case R.id.layout_question1:
                globalClass.appTracking("Home Page","Click on Questions button");
                redirectToQuestion();
                break;
            case R.id.layout_bloodPressure1:
                globalClass.appTracking("Home Page","Click on Blood Pressure button");
                redirectBP(); // bp
                break;
            case R.id.layout_temperature1:
                globalClass.appTracking("Home Page","Click on Temperature button");
                redirectTemp();// temp
                break;
            case R.id.btn_weight:
                globalClass.appTracking("Home Page","Click on Weight button");
                redirectWT(); // wt
                break;
            case R.id.btn_qs:
                globalClass.appTracking("Home Page","Click on Questions button");
                redirectToQuestion();
                break;
            case R.id.btn_bloodOxygen:
                globalClass.appTracking("Home Page","Click on Blood Oxygen button");
                redirectPulse();// puls
                break;
            case R.id.btn_Bp:
                globalClass.appTracking("Home Page","Click on Blood Pressure button");
                redirectBP(); // bp
                break;
            case R.id.btn_temp:
                globalClass.appTracking("Home Page","Click on Temperature button");
                redirectTemp();// temp
                break;
            case R.id.btn_bloodGlucose:
                globalClass.appTracking("Home Page","Click on  Blood Glucose button");
                redirectToGlucose();
                break;
            case R.id.btn_question1:
                globalClass.appTracking("Home Page","Click on Questions button");
                redirectToQuestion();
                break;
            case R.id.btn_Bp1:
                globalClass.appTracking("Home Page","Click on Blood Pressure button");
                redirectBP(); // bp
                break;
            case R.id.btn_temperature1:
                globalClass.appTracking("Home Page","Click on Temperature button");
                redirectTemp();// temp
                break;
            default:

                break;

        }
//        if (v.getId() == 7) {
//            redirectWT(); // wt
//        } else if (v.getId() == 101) {
//            redirectToQuestion();
//        } else if (v.getId() == 2) {
//            redirectPulse();// puls
//        } else if (v.getId() == 3) {
//            redirectBP(); // bp
//        } else if (v.getId() == 6) {
//            redirectTemp();// temp
//        } else if (v.getId() == 1) {
//            redirectToGlucose();
//        }
//
//        Log.i("home", "dynamic btn clicked  id. " + v.getId());

    }

    public void redirectToGlucose() {

        // String tmpMac = dbSensor.SelectGlucoName();
        String sensor_name = dbSensor.SelectGlucose_sensor_Name();

        if (sensor_name.contains("One Touch Ultra")) {

            Intent intent = new Intent(getApplicationContext(),
                    GlucoseReader.class);

            startActivity(intent);
            overridePendingTransition(0, 0);
            this.finish();

        } else if (sensor_name.contains("Bayer")) {

            Intent intent = new Intent(getApplicationContext(),
                    GlucoseReader.class);

            startActivity(intent);
            overridePendingTransition(0, 0);
            this.finish();

        } else if (sensor_name.contains("Accu-Chek")) {

            if (Build.VERSION.SDK_INT >= 18) {
                Intent intent = new Intent(getApplicationContext(),
                        GlucoseAccuChek.class);
                startActivity(intent);
                finish();
            } else {

                Toast.makeText(getBaseContext(), "not support ble",
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),
                        GlucoseEntry.class);
                startActivity(intent);
                finish();

            }

        } else if (sensor_name.contains("Taidoc")) {

            if (Build.VERSION.SDK_INT >= 18) {
                Intent intent = new Intent(getApplicationContext(),
                        GlucoseTaidoc.class);
                startActivity(intent);
                finish();
            } else {

                Toast.makeText(getBaseContext(), "not support ble",
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(),
                        GlucoseEntry.class);
                startActivity(intent);
                finish();

            }

        } else if (sensor_name.trim().length() > 0) {
            Intent intent = new Intent(getApplicationContext(),
                    GlucoseEntry.class);
            startActivity(intent);
            finish();
        }

        dbSensor.cursorsensor.close();
        dbSensor.close();
    }

    public void redirectToQuestion() {

        String question = dbSensor.SelectQuestion();
        if (question.trim().length() > 0) {
            Intent intent = new Intent(getApplicationContext(),
                    StartQuestion.class);
            Constants.setquestionPresent(0);
            startActivity(intent);
            finish();
        } else {
                /*
                 * Toast.makeText(Home.this, "No Question for this time",
				 * Toast.LENGTH_LONG).show();
				 */
        }
        // qs
    }

//    private void anim() {
//
//        WindowManager.LayoutParams layout = getWindow().getAttributes();
//        layout.screenBrightness = 1F;
//        getWindow().setAttributes(layout);
//
//        AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
//        AnimPlz.setDuration(11000);
//        AnimPlz.start();
//
//        AnimPlz.addListener(new AnimatorListenerAdapter() {
//            public void onAnimationEnd(Animator animation) {
//
//                WindowManager.LayoutParams layout = getWindow().getAttributes();
//                layout.screenBrightness = 0.05F;
//                getWindow().setAttributes(layout);
//
//            }
//        });
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked on HOME page:");
//            this.finishAffinity();
//        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void redirectPulse() {

        Log.i(TAG, "start to Datareader activity from home : ");

        Cursor cursorsensor = dbSensor.SelectMeasuredweight();
        String Sensor_name = "", Mac = "";
        Log.i("Droid", "First SensorName : " + Sensor_name);
        cursorsensor.moveToFirst();
        if (cursorsensor.getCount() > 0) {

            Sensor_name = cursorsensor.getString(3);
            Mac = getForaMAC(cursorsensor.getString(9));

        }

        Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
        if (Mac.trim().length() == 17) {

            if (Sensor_name.contains("3230")) {
                if (Build.VERSION.SDK_INT >= 18) {
                    Intent intent = new Intent(getApplicationContext(),
                            BLECheckActivity.class);
                    intent.putExtra("macaddress", Mac);
                    startActivity(intent);
                    finish();
                } else {

                    Toast.makeText(getBaseContext(), "not support ble",
                            Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }
            } else if (Sensor_name.contains("9560")) {
                Intent intent = new Intent(getApplicationContext(),
                        DataReaderActivity.class);
                intent.putExtra("macaddress", Mac);
                startActivity(intent);
                this.finish();
            }

            overridePendingTransition(0, 0);
        } else if (Sensor_name.contains("Manual")) {
            Intent intent = new Intent(getApplicationContext(),
                    PulseEntry.class);

            startActivity(intent);
            overridePendingTransition(0, 0);
            this.finish();

        } else {

			/*
             * Toast.makeText(this, "Please assign Blood Oxygen.",
			 * Toast.LENGTH_LONG).show();
			 */

        }

    }

    private void redirectTemp() {

        String tmpMac = dbSensor.SelectTempSensorName();
        Log.i(TAG, "start to tepm  activity from home : ");
        Log.i(TAG, "tmpMac" + tmpMac);

        if (tmpMac.trim().length() > 4) {
            Intent intentfr = new Intent(getApplicationContext(),
                    ForaMainActivity.class);
            intentfr.putExtra("deviceType", 2);
            // intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
            intentfr.putExtra("macaddress", getForaMAC(tmpMac));
            startActivity(intentfr);
            overridePendingTransition(0, 0);
            this.finish();

        } else if (tmpMac.equals("-1")) {
            Intent intentfr = new Intent(getApplicationContext(),
                    TemperatureEntry.class);
            startActivity(intentfr);
            overridePendingTransition(0, 0);
            this.finish();
        } else {

			/*
             * Toast.makeText(this, "Please assign Temperature .",
			 * Toast.LENGTH_LONG).show();
			 */

        }

    }

    private void redirectBP() {
        Log.i(TAG, "start to bp  activity from home : ");
        Cursor cursorsensor = dbSensor.SelectBPSensorName();
        String Sensor_name = "", Mac = "";
        Log.i("Droid", "First SensorName : " + Sensor_name);
        cursorsensor.moveToFirst();
        if (cursorsensor.getCount() > 0) {

            Sensor_name = cursorsensor.getString(3);
            Mac = getForaMAC(cursorsensor.getString(9));

        }
        Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);

        if (Sensor_name.contains("A and D Bluetooth smart")) {
            if (Build.VERSION.SDK_INT >= 18) {

                Intent intent = new Intent(getApplicationContext(),
                        AandDSmart.class);
                intent.putExtra("macaddress", Mac);
                startActivity(intent);
                overridePendingTransition(0, 0);
                this.finish();
            } else {

                Toast.makeText(getBaseContext(), "no ble found",
                        Toast.LENGTH_SHORT).show();
                finish();
                return;
            }

        } else if (Sensor_name.contains("Omron HEM 9200-T")) {
            if (Build.VERSION.SDK_INT >= 18) {
                Constants.setPIN(cursorsensor.getString(10));
                Intent intent = new Intent(getApplicationContext(),
                        OmronBlsActivity.class);
                intent.putExtra("macaddress", Mac);
                this.finish();
                startActivity(intent);
                overridePendingTransition(0, 0);
            } else {

                Toast.makeText(getBaseContext(), "no ble found",
                        Toast.LENGTH_SHORT).show();
                finish();
                return;
            }

        } else if (Sensor_name.contains("P724-BP")) {

            if (Build.VERSION.SDK_INT >= 18) {

                Intent intent = new Intent(getApplicationContext(),
                        P724BpSmart.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                this.finish();
            } else {
                Log.i(TAG,
                        "SensorName P724 : no ble fund redirecting to normal page ");
                Intent intent = new Intent(this, P724Bp.class);
                startActivity(intent);
                finish();
                return;
            }
        } else if (Sensor_name.contains("Wellex")) {

            if (Build.VERSION.SDK_INT >= 18) {

                Intent intent = new Intent(getApplicationContext(),
                        PressureWellex.class);
                intent.putExtra("macaddress", Mac);
                startActivity(intent);
                overridePendingTransition(0, 0);
                this.finish();
            } else {

                Toast.makeText(getBaseContext(), "no ble found",
                        Toast.LENGTH_SHORT).show();
                finish();
                return;
            }

        } else if (Sensor_name.contains("A and D")) {

            if (Sensor_name.contains("UA-767BT-Ci")) {
                Intent intentwt = new Intent(getApplicationContext(),
                        AandContinua.class);
                startActivity(intentwt);
                overridePendingTransition(0, 0);
                this.finish();
            } else {

                Intent intentwt = new Intent(getApplicationContext(),
                        AandDReader.class);
                intentwt.putExtra("deviceType", 1);
                startActivity(intentwt);
                overridePendingTransition(0, 0);
                this.finish();
            }

        } else if (Sensor_name.contains("FORA")) {
            if (Mac.trim().length() == 17) {
                Intent intentfr = new Intent(getApplicationContext(),
                        ForaMainActivity.class);
                intentfr.putExtra("deviceType", 1); // pressure
                intentfr.putExtra("macaddress", Mac);
                startActivity(intentfr);
                overridePendingTransition(0, 0);
                this.finish();
            } else if (Sensor_name.contains("Manual")) {
                Intent intentfr = new Intent(getApplicationContext(),
                        PressureEntry.class);

                startActivity(intentfr);
                overridePendingTransition(0, 0);
                this.finish();

            } else {
                /*
				 * Toast.makeText(this, "Please assign Blood Pressure.",
				 * Toast.LENGTH_LONG).show();
				 */
            }
        } else if (Sensor_name.contains("Manual")) {
            Intent intentfr = new Intent(getApplicationContext(),
                    PressureEntry.class);

            startActivity(intentfr);
            overridePendingTransition(0, 0);
            this.finish();

        } else {

			/*
			 * Toast.makeText(this, "Please assign Blood Pressure.",
			 * Toast.LENGTH_LONG).show();
			 */
        }

    }

    private void redirectWT() {

        Log.i(TAG, "start to wt  activity from home : ");
        Cursor cursorsensor = dbSensor.SelectWeightSensorName();
        String Sensor_name = "", Mac = "";
        Log.i("Droid", "First SensorName : " + Sensor_name);
        cursorsensor.moveToFirst();
        if (cursorsensor.getCount() > 0) {
            Sensor_name = cursorsensor.getString(3);
            Mac = getForaMAC(cursorsensor.getString(9));
        }
        Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
        if (Sensor_name.contains("A and D")) {
            Intent intentwt = new Intent(getApplicationContext(),
                    AandDReader.class);
            intentwt.putExtra("deviceType", 0);
            startActivity(intentwt);
            overridePendingTransition(0, 0);
            this.finish();
        } else if (Sensor_name.contains("FORA")) {
            Intent intentfr = new Intent(getApplicationContext(),
                    ForaMainActivity.class);
            intentfr.putExtra("deviceType", 0);
            intentfr.putExtra("macaddress", Mac);
            startActivity(intentfr);
            overridePendingTransition(0, 0);
            this.finish();
        } else if (Sensor_name.contains("P724")) {
            Intent intentfr = new Intent(getApplicationContext(),
                    P724Activity.class);
            intentfr.putExtra("deviceType", 0);
            intentfr.putExtra("macaddress", Mac);
            startActivity(intentfr);
            overridePendingTransition(0, 0);
            this.finish();
        } else if (Sensor_name.trim().length() > 0) {
            final Context context = this;
            Intent intent = new Intent(context, WeightEntry.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
            this.finish();
        } else {

			/*
			 * Toast.makeText(this, "Please assign Body Weight.",
			 * Toast.LENGTH_LONG).show();
			 */

        }

    }

    private String getForaMAC(String mac) {

        String macAddress = "";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length(); i = i + 2) {
            sb.append(mac.substring(i, i + 2));
            sb.append(":");
        }

        macAddress = sb.toString();
        macAddress = macAddress.substring(0, macAddress.length() - 1);

        Log.i(TAG, " : Connect to macAddress " + macAddress);
        // #
        if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
            return macAddress;
        } else {
            return "";
        }
    }

    @Override
    public void onStop() {

        Log.i(TAG, "-------------------onStop--------------  home  activity");
        // Commit the edits!
        super.onStop();
        //stopHandler();
        Util.Stopsoundplay();
//        Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

        Log.i(TAG, "onStop on home  activity");

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }


    public void downloadSensorLastReading() {
        DownloadLastReadingDetails taskUpdate = new DownloadLastReadingDetails();
        taskUpdate.execute(new String[]{serviceUrl
                + "/droid_website/mob_get_last_readings_dates.ashx"});
    }

    private class DownloadLastReadingDetails extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(Home.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String response = "";
            try {

                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");
                HttpResponse response1 = Util.connect(serviceUrl
                        + "/droid_website/mob_get_last_readings_dates.ashx", new String[]{
                        "authentication_token", "patient_id"}, new String[]{
                        token, patientId});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return ""; // process
                }
                String str = Util
                        .readStream(response1.getEntity().getContent());

                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                str.toString();
                // Log.i("response:", "handler_response:" + str);

                if (str.length() < 10) {
                    Log.i(TAG, "Advice message data null");
                    return "";
                }

                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList AF_Nodes = doc.getElementsByTagName("vital_data");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    response = Util.getTagValue(AF_Nodes, i, "mob_response");
                    if (response.equals("AuthenticationError")) {

                        return response;
                    }
                    String last_reading_glucose = Util.getTagValue(AF_Nodes, i, "last_reading_glucose");
                    String last_reading_oxygen = Util.getTagValue(AF_Nodes, i, "last_reading_oxygen");
                    String last_reading_pressure = Util.getTagValue(AF_Nodes, i, "last_reading_pressure");
                    String last_reading_temperature = Util.getTagValue(AF_Nodes, i, "last_reading_temperature");
                    String last_reading_weight = Util.getTagValue(AF_Nodes, i, "last_reading_weight");
                    String last_reading_questions = Util.getTagValue(AF_Nodes, i, "last_reading_questions");

                    SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
                    SharedPreferences.Editor lastStatusEditor = lastStatus.edit();
                    lastStatusEditor.putString("glucose_lastReading", last_reading_glucose);
                    lastStatusEditor.putString("oxygen_lastReading", last_reading_oxygen);
                    lastStatusEditor.putString("pressure_lastReading", last_reading_pressure);
                    lastStatusEditor.putString("temperature_lastReading", last_reading_temperature);
                    lastStatusEditor.putString("weight_lastReading", last_reading_weight);
                    lastStatusEditor.putString("question_lastReading", last_reading_questions);
                    lastStatusEditor.commit();
                }

            } catch (Exception e) {

            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            //loadApkDetails();
            progressDialog.dismiss();
            SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
            txt_weightLastReading.setText(" " + lastStatus.getString("weight_lastReading",""));
            txt_tempLastReading.setText(" " + lastStatus.getString("temperature_lastReading",""));
            txt_questionLastReading.setText(" " + lastStatus.getString("question_lastReading",""));
            txt_pressureLastReading.setText(" " + lastStatus.getString("pressure_lastReading",""));
            txt_oxygenLastReading.setText(" " + lastStatus.getString("oxygen_lastReading",""));
            txt_glucoseLastReading.setText(" " + lastStatus.getString("glucose_lastReading",""));
            txt_pressureLastReading1.setText(" " + lastStatus.getString("pressure_lastReading",""));
            txt_questionLastReading1.setText(" " + lastStatus.getString("question_lastReading",""));
            txt_tempLastReading1.setText(" " + lastStatus.getString("temperature_lastReading",""));
        }
    }
}
