package com.optum.telehealth;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.dal.AdviceMessage_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

public class PopActivity extends Titlewindow {

	private String serviceUrl = "";
	private String TAG = "PopActivity Sierra";
	private GlobalClass appClass;
	Cursor adviceMessageCursor;
	//String[] data_date = new String[] {};

	//int[] userid, patientid, status;
	AdviceMessage_db dbcreate = new AdviceMessage_db(this);
	TextView txtadvice;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_pop);
		appClass = (GlobalClass) getApplicationContext();

		txtadvice = (TextView) findViewById(R.id.advice);


		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		txtadvice.setTypeface(type, Typeface.NORMAL);

		adviceMessageCursor = dbcreate.SelectAdviceMessageForDisplay();
		Log.i("cursorCount:   ", ""+adviceMessageCursor.getCount());
		if (adviceMessageCursor.getCount() > 0) {
			adviceMessageCursor.moveToFirst();
			showAdviceMessage();
		}
		//txtadvice.setText(extras.getString("message"));
		// tts.speak(extras.getString("message"), TextToSpeech.QUEUE_FLUSH,
		// null);

		Log.i(TAG, "on create Advice Message shown Sierra ...");

		SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
		serviceUrl = settings.getString("Server_post_url", "-1");

		//UpdateAdviceMessage();

		Button exit = (Button) findViewById(R.id.exit);

		exit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				adviceMessageCursor.moveToNext();
				showAdviceMessage();

//				Intent intent = new Intent(PopActivity.this,
//						Welcome_Activity.class);
//				startActivity(intent);
//				finish();
//				overridePendingTransition(0, 0);
			}
		});

		Log.i(TAG, "-------------------oncreate--------------  popup  activity");
	}
	private void showAdviceMessage(){

		if(adviceMessageCursor.isAfterLast() == false){

			dbcreate.UpdateAdviceMessageByDate(Integer.parseInt(adviceMessageCursor.getString(8)));
			txtadvice.setText(adviceMessageCursor.getString(4));
			//appClass.tts.speak(adviceMessageCursor.getString(4), TextToSpeech.QUEUE_FLUSH, null);


		}else{
			UpdateAdviceMessage();
			adviceMessageCursor.close();
			Intent intent = new Intent(PopActivity.this,
					Welcome_Activity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(0, 0);
		}
	}
	@Override
	public void onStop() {
		super.onStop();
		//Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		//Log.e(TAG, "onRestart" + extras.getString("message"));
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//			Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//
//			Intent intent = new Intent(PopActivity.this,
//					Welcome_Activity.class);
//			startActivity(intent);
//			finish();
//			overridePendingTransition(0, 0);
//
//		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.e(TAG, "onPause");
	}

	/***************************************************************** update advicemessage to server start ****************************************************/
	public void UpdateAdviceMessage() {


			Update taskUpdate = new Update();
			taskUpdate.execute(new String[] { serviceUrl
					+ "/droid_website/mob_rm_feedback.ashx" });





	}

	private class Update extends AsyncTask<String, Void, String> {


		@Override
		protected void onPostExecute(String result) {
			//Log.i("onPostExecute", result);
			 if (result.equalsIgnoreCase("AuthenticationError")) {
				 SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
				 String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
				Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
				logoutClick();
			}


		}
		protected String doInBackground(String... urls) {
			String response = "";

			try {

				Cursor c = dbcreate.SelectAdviceMessageForupload();

				c.moveToFirst();
				if (c.getCount() > 0) {
					while (c.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading advice message to server started ...");
						SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
						String  token = tokenPreference.getString("Token_ID", "-1");
						String patientId = tokenPreference.getString("patient_id", "-1");
						HttpClient client = new DefaultHttpClient();
						Log.i(TAG, "UploadAdviceMessage url ..." + serviceUrl
								+ "/droid_website/mob_rm_feedback.ashx");
						HttpPost post = new HttpPost(serviceUrl
								+ "/droid_website/mob_rm_feedback.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();
						post.setHeader("authentication_token", token);
						post.setEntity(new UrlEncodedFormEntity(pairs));
						post.setHeader("mode", "1");
						post.setHeader("user_id", c.getString(5));
						// Log.i("Droid","Uploading user_id ..."+c.getString(2));
						post.setHeader("patient_id", patientId);
						post.setHeader("advice_id",	c.getString(8));
						// Log.i("Droid","Uploading create_date ..."+c.getString(1));
						HttpResponse response1 = client.execute(post);

						String str = Util.readStream(response1.getEntity()
								.getContent());

						str.toString();
						//Log.i("response:", "handler_response:" + str);
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc.getElementsByTagName("optum");
							for (int i = 0; i < nodes.getLength(); i++) {

								response = Util.getTagValue(nodes, i, "response");
							}
							if (response.equals("Success")) {
								// /// if success delete the value from the
								// tablet db
								Log.i(TAG,"Deleted adviceid ..." + c.getString(0));
								if (Integer.parseInt(c.getString(7)) == 1) {
									dbcreate.DeleteMessageByid(Integer
											.parseInt(c.getString(8)));
								} else if (Integer.parseInt(c.getString(7)) == 3) {
									dbcreate.Delete(Integer.parseInt(c.getString(8)));
								}
							}

						}
						c.moveToNext();

					}
					c.close();
					dbcreate.cursorAdviceMessage.close();

				}
			} catch (Exception e) {
				// e.printStackTrace();
				Log.i("Exception", e.getMessage());
			}
			// DownloadAdviceMessage();
			return response;
		}

	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public void logoutClick() {

//		SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
//		SharedPreferences.Editor editor = settings.edit();
//		editor.remove("patient_name");
//		editor.remove("patient_id");
//		editor.remove("contract_code_name");
//		editor.remove("language_id");
//		editor.remove("Token_ID");
//		editor.commit();
//		SharedPreferences clusterSsettings = getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
//		SharedPreferences.Editor clusterEditor = clusterSsettings.edit();
//		clusterEditor.remove("clustorNo");
//		clusterEditor.remove("time_zone_id");
//		clusterEditor.commit();

		Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
		startActivity(authenticationIntent);
		overridePendingTransition(0, 0);
	}

	@Override
	public void onBackPressed()
	{
		// code here to show dialog
		//super.onBackPressed();  // optional depending on your needs
	}

}
