package com.optum.telehealth;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.dal.Glucose_db;
import com.optum.telehealth.dal.MeasureType_db;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;

import java.util.Timer;
import java.util.TimerTask;

public class ShowGlucoseActivity extends Titlewindow {

	private Glucose_db dbglucose = new Glucose_db(this);
	private String TAG = "SetGlucoseActivity";
	private Button NextButton = null, retryButton = null;
	private GlobalClass appClass;
	MeasureType_db dbsensor = new MeasureType_db(this);
	private int redirectFlag = 0;
	private TextView txt_errorMsg;
	boolean resumeFlag = false;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_glucose);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		try {
			TextView msg = (TextView) findViewById(R.id.t);
			int language = Constants.getLanguage();
			if (language == 11) {

				msg.setText(Html.fromHtml("Su nivel de glucosa en la sangre es de <b>" + extras.getString("text") + "</b> miligramos por decilitro."));
			} else {

				msg.setText(Html.fromHtml("Your Blood glucose level are at <b>" + extras.getString("text") + "</b> milligrams per deciliter."));
			}

			addListenerOnButton();
			Log.i(TAG, "----------on create  show glucose----------- ");
		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception show show glucose  on create ");
		}
		redirector();
	}
	public void redirect() {
		if (redirectFlag == 0) {
			dbsensor.updateStatus(1);// update the vital taken

			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				appClass.setBundle(extras);
			} else {
				extras = appClass.getBundle();
			}
			dbglucose.UpdateglucoseData_as_valid(extras.getInt("glucoseid"));

			Log.i(TAG, "NextButton clicked  show glucose---- ");
			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.RETAKE_SP, 0);
			SharedPreferences.Editor editor_retake = settings1.edit();
			editor_retake.putInt("retake_status", 0);
			editor_retake.commit();
			//if (appClass.isConnectingToInternet(getApplicationContext())) {
				Intent intent = new Intent(getApplicationContext(),
						UploadMeasurement.class);
				intent.putExtra("reType", 3);
				startActivity(intent);
				finish();
				Log.e(TAG, "Redirect to upload glucose");
//			} else {
//
//				errorShow(this.getString(R.string.netConnectionErrorMsg));
//			}

		} else {
			Log.i(TAG, "Retake Pressed");
		}
	}

	@Override
	public void onStop() {
		redirectFlag = 1;
		Log.i(TAG, "------------------onStop------- show glucose----------");
		//Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		super.onStop();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//			Log.i(TAG, "USER KEYCODE_BACK clicked:");
//
//			Intent intent = new Intent(this, FinalMainActivity.class);
//			startActivity(intent);
//			finish();
//
//		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	private void addListenerOnButton() {

		NextButton = (Button) findViewById(R.id.btnacceptglu);

		NextButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				appClass.appTracking("Blood Glucose reading display page","Click on Accept button");
				redirect();
			}
		});

		retryButton = (Button) findViewById(R.id.btnwtretakeglu);
		SharedPreferences settingsNew = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		Log.i(TAG, "retryButton clicked  show glucose---- ");
		int retake = settingsNew.getInt("retake_status", 0);

		/*if (retake == 1) {
			retryButton.setVisibility(View.INVISIBLE);
		}*/

		retryButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i(TAG, "retry clicked");
				appClass.appTracking("Blood Glucose reading display page","Click on Retake button");
				delete_last_meassurement();
				// set retak sp
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.RETAKE_SP, 0);
				SharedPreferences.Editor editor_retake = settings.edit();
				editor_retake.putInt("retake_status", 1);
				editor_retake.commit();
				redirectFlag = 1;
				RedirectGlucose();
			}
		});
	}

	private void RedirectGlucose() {

		/*
		 * final Context context = this; Intent intent = new Intent(context,
		 * GlucoseEntry.class); this.finish(); startActivity(intent);
		 */

		Sensor_db dbSensor = new Sensor_db(this);
		String sensor_name = dbSensor.SelectGlucose_sensor_Name();

		if (sensor_name.contains("One Touch Ultra")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		} else if (sensor_name.contains("Bayer")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		} else if (sensor_name.contains("Accu-Chek")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseAccuChek.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				startActivity(intent);
				finish();

			}

		} else if (sensor_name.contains("Taidoc")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseTaidoc.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				startActivity(intent);
				finish();

			}

		}

		else if (sensor_name.trim().length() > 0) {
			Intent intent = new Intent(getApplicationContext(),
					GlucoseEntry.class);
			startActivity(intent);
			finish();
		}

		dbSensor.cursorsensor.close();
		dbSensor.close();

	}

	private void delete_last_meassurement() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbglucose.UpdateglucoseData(extras.getInt("glucoseid"));
		

	}

	public void errorShow(String msg) {
		txt_errorMsg.setVisibility(View.VISIBLE);
		txt_errorMsg.setText(msg);
		NextButton.setEnabled(false);
		final Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						txt_errorMsg.animate().alpha(0.0f);
						txt_errorMsg.setVisibility(View.GONE);
						NextButton.setEnabled(true);
						timer.cancel();
					}
				});

			}
		}, 3000, 3000);
	}
	private void redirector() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				ActivityManager am = (ActivityManager) getApplicationContext()
						.getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				if (cn.getClassName().equals(
						"com.optum.telehealth.ShowGlucoseActivity"))
					redirect();
			}
		}, 15000);
	}
	@Override
	protected void onPause() {
		Log.i(TAG, "onPause");
		resumeFlag = true;
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "onPause" + resumeFlag);
		if (resumeFlag) {
			redirectFlag = 0;
			redirector();
		}
		super.onResume();
	}

	@Override
	public void onBackPressed()
	{
		// code here to show dialog
		//super.onBackPressed();  // optional depending on your needs
	}
}
