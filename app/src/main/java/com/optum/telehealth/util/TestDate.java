package com.optum.telehealth.util;

import android.annotation.SuppressLint;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

@SuppressLint("SimpleDateFormat")
public class TestDate {
	public static long startTime = 0;
	public static Calendar cal = null;
	
	public TestDate() {
		// start block
		cal = GregorianCalendar.getInstance();

		cal.set(2013, 04, 27, 9, 45, 50);

		Date sDate = cal.getTime();
		startTime = System.currentTimeMillis();
		System.out.println("Current Time 1 : " + sDate);
		System.out.print("SEt time:-" + cal.getTime());

	}

	public static void setCalender(int year, int month, int day, int hour,
			int min, int sec) {
		// cal = GregorianCalendar.getInstance();
		/*
		 * String PREFS_NAME = "DroidPrefSc"; SharedPreferences settings =
		 * getSharedPreferences("TimePrefSc", 0);
		 * 
		 * String ServerTime = settings.getString("timezone", "AA");
		 * 
		 * int year=Integer.parseInt(ServerTime.substring(6, 10)); int
		 * month=Integer.parseInt(ServerTime.substring(0, 2)); int
		 * day=Integer.parseInt(ServerTime.substring(3, 5)); int
		 * hour=Integer.parseInt(ServerTime.substring(11,13)); int
		 * minute=Integer.parseInt(ServerTime.substring(14, 16)); int
		 * seconds=Integer.parseInt(ServerTime.substring(17, 19));
		 */

		cal.set(year, month - 1, day, hour, min, sec);
	}

	@SuppressLint("SimpleDateFormat")
	public static String getCurrentTime() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.US);
		Date today = new Date();
		String IST = df.format(today);
		return (((IST.replace("p.m.", "PM")).replace("a.m.", "AM")).replace("pm", "PM")).replace("am", "AM");
	}

//	@SuppressWarnings("static-access")
//	public static void main(String[] args) {
//		try {
//			Thread.currentThread().sleep(60000);
//		} catch (Exception e) {
//		}
//	}
}