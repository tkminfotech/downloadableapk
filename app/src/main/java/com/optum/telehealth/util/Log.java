package com.optum.telehealth.util;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;

import com.optum.telehealth.GlobalClass;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by muhammad.mohsen on 4/18/2017.
 * overrides android.util.Log
 */
public class Log {

	private Log() {}

	// enables/disables logcat logging
	private static final boolean LOGCAT_ACTIVE = true;

	// enables/disables file logging
	// requires storage permission for android version >= 6 - permission has to be granted manually from app settings
	public static final boolean FILE_LOG_ACTIVE = true;

	public static void v(String tag, String entry) {
		if (LOGCAT_ACTIVE)
			android.util.Log.v(tag, entry);

		if (FILE_LOG_ACTIVE)
			LogFile.write("v", tag, entry);

	}
	public static void i(String tag, String entry) {
		if (LOGCAT_ACTIVE)
			android.util.Log.i(tag, entry);

		if (FILE_LOG_ACTIVE)
			LogFile.write("i", tag, entry);
	}
	public static void d(String tag, String entry) {
		if (LOGCAT_ACTIVE)
			android.util.Log.d(tag, entry);

		if (FILE_LOG_ACTIVE)
			LogFile.write("d", tag, entry);
	}
	public static void e(String tag, String entry) {
		if (LOGCAT_ACTIVE)
			android.util.Log.e(tag, entry);
		if (FILE_LOG_ACTIVE)
			LogFile.write("e", tag, entry);
	}
	public static void w(String tag, String entry) {
		if (LOGCAT_ACTIVE)
			android.util.Log.w(tag, entry);
		if (FILE_LOG_ACTIVE)
			LogFile.write("w", tag, entry);
	}
	public static void e(String tag, String msg, Throwable tr){
		if (LOGCAT_ACTIVE)
			android.util.Log.e(tag, msg, tr);
		if (FILE_LOG_ACTIVE)
			LogFile.write("e", tag, msg + '\n' +  android.util.Log.getStackTraceString(tr));
	}

	private static class LogFile {
		private static final String LOG_FILE_NAME = "";
		private static final String LOG_FILE_DIR = "";


		static File logFile;
		static Handler writingHandler;

		private static File getLogFile() {
			try {
				if (logFile == null) {
					File appDirectory = new File(Environment.getExternalStorageDirectory() + "/OptumFolder");
					File logDirectory = new File(appDirectory + "/log");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.getDefault());

					logFile = new File(logDirectory, "log" + sdf.format(System.currentTimeMillis()) + ".txt");

					// create app folder
					if (!appDirectory.exists()) {
						appDirectory.mkdir();
					}

					// create log folder
					if (!logDirectory.exists()) {
						logDirectory.mkdir();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return logFile;
		}

		static void write(final String id, final String tag, final String entry) {
			final String dateTime = DateFormat.getDateTimeInstance().format(new Date());
			LogFile.write(id, dateTime, tag, entry);
		}

		static void write(final String id, final String dateTime, final String tag, final String entry) {
			try {
				if (writingHandler == null)
					writingHandler = new Handler();

				writingHandler.post(new Runnable() {
					@Override
					public void run() {
						LogFile._write(id, dateTime, tag, entry);
					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		static PrintWriter pw;
		static synchronized void _write(String id, String dateTime, String tag, String entry) {
			File logFile = getLogFile();

			try {
				if(pw == null) {
					FileOutputStream f = new FileOutputStream(logFile);
					pw = new PrintWriter(f);
				}
				pw.println(dateTime + " " + id + "/ " + tag + ": " + entry);
				pw.flush();

			} catch (IOException e) {
				e.printStackTrace();
				try {
					pw.close();

				} catch (Exception e1){
					e.printStackTrace();
				}

				pw = null;
				LogFile.write(id, dateTime, tag, entry);
			}
		}
	}

}