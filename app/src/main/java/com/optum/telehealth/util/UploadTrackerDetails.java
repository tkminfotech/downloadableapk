package com.optum.telehealth.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;

import com.optum.telehealth.AuthenticationActivity;
import com.optum.telehealth.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.optum.telehealth.dal.MyDataTracker_db;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by tkmif-ws011 on 11/7/2016.
 */
public class UploadTrackerDetails extends AsyncTask<String, Void, String> {
    private MyDataTracker_db myDataTracker_db;
    //ProgressDialog progressDialog;
    Context context;
    private String postUrl;

    public UploadTrackerDetails(Context context, String serviceUrl) {

        this.context = context;
        postUrl = serviceUrl;
        myDataTracker_db = new MyDataTracker_db(context);
    }

    @Override
    protected void onPostExecute(String result) {
        //progressDialog.dismiss();
        if (result.equalsIgnoreCase("AuthenticationError")) {
            SharedPreferences ERROR_MSG = context.getSharedPreferences("ERROR_MSG", 0);
            String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
            Toast.makeText(context.getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
            logoutClick();
        }else if (result.equalsIgnoreCase("success")) {

        }
    }

    @Override
    protected void onPreExecute() {
        try {
//            progressDialog = new ProgressDialog(context);
//            progressDialog.setMessage("Loading MyData...");
//            progressDialog.show();
//            progressDialog.setCancelable(false);
//            progressDialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
            Log.i("Exception", "Exception in ProgressDialog.show");
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";
        String request = "";
        SharedPreferences tokenPreference = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
        String  token = tokenPreference.getString("Token_ID", "-1");
        String patientId = tokenPreference.getString("patient_id", "-1");
        try {
            Cursor tracker = myDataTracker_db.SelectAllMyDataTracker();

            if (tracker.getCount() > 0) {
                request = writeUsingXMLSerializer(tracker);
                Log.i("TRACKER_REQUEST", request);
                StringEntity se = new StringEntity(request, HTTP.UTF_8);
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(postUrl
                        + "/droid_website/mob_add_usage_analytics.ashx");
                se.setContentType("text/xml");
                post.setHeader("Content-Type", "application/soap+xml;charset=UTF-8");
                post.setHeader("authentication_token", token);
                post.setHeader("login_patient_id", patientId);
                post.setEntity(se);

                HttpResponse response1 = client.execute(post);
                int a = response1.getStatusLine().getStatusCode();
                InputStream in = response1.getEntity().getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));
                StringBuilder str = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    str.append(line + "\n");
                }
                in.close();
                str.toString();
                //Log.i("response:", "bodyWeight_response:" + str);
                if (str.length() > 0) {
                    DocumentBuilder db = DocumentBuilderFactory
                            .newInstance().newDocumentBuilder();
                    InputSource is1 = new InputSource();
                    is1.setCharacterStream(new StringReader(str
                            .toString()));
                    Document doc = db.parse(is1);
                    NodeList nodes = doc.getElementsByTagName("optum");
                    for (int i = 0; i < nodes.getLength(); i++) {

                        response = Util.getTagValue(nodes, i, "response");
                    }
                    if (response.equalsIgnoreCase("Success")) {
                        // /// if success delete the value from the
                        // tablet db

                        myDataTracker_db.delete();
                    }

                }

            }
            tracker.close();
            myDataTracker_db.cursorsensor.close();


        } catch (Exception e) {
            // e.printStackTrace();
        }
        return response;
    }


    private String writeUsingXMLSerializer(Cursor tracker) {

        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            xmlSerializer.setOutput(writer);
            // start DOCUMENT
            xmlSerializer.startDocument("UTF-8", false);
            // open tag: <trackerList>
            xmlSerializer.startTag("", "track_details");
            tracker.moveToFirst();
            while (tracker.isAfterLast() == false) {
                // open tag: <tracker>
                xmlSerializer.startTag("", "tracker");
                xmlSerializer.startTag("", "patient_id");
                xmlSerializer.text(""+tracker.getInt(1));
                xmlSerializer.endTag("", "patient_id");
                xmlSerializer.startTag("", "tracked_date_time");
                xmlSerializer.text(tracker.getString(2));
                xmlSerializer.endTag("", "tracked_date_time");
                xmlSerializer.startTag("", "device");
                xmlSerializer.text(tracker.getString(3));
                xmlSerializer.endTag("", "device");
                xmlSerializer.startTag("", "os");
                xmlSerializer.text(tracker.getString(4));
                xmlSerializer.endTag("", "os");
                xmlSerializer.startTag("", "version");
                xmlSerializer.text(tracker.getString(5));
                xmlSerializer.endTag("", "version");
                xmlSerializer.startTag("", "page");
                xmlSerializer.text(tracker.getString(6));
                xmlSerializer.endTag("", "page");
                xmlSerializer.startTag("", "action");
                xmlSerializer.text(tracker.getString(7));
                xmlSerializer.endTag("", "action");
                xmlSerializer.endTag("", "tracker");
                tracker.moveToNext();
            }
            xmlSerializer.endTag("", "track_details");
            // end DOCUMENT
            xmlSerializer.endDocument();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        String ss = "<?xml version='1.0' encoding='UTF-8'?>" +
//                "<track_details>" +
//                "	<tracker>" +
//                "		<patient_id></patient_id>" +
//                "		<date_time></date_time>" +
//                "		<device></device>" +
//                "		<os></os>" +
//                "		<version></version>" +
//                "		<vital></vital>" +
//                "		<mode></mode>" +
//                "		<event></event>" +
//                "		<value></value>" +
//                "	</tracker>" +
//                "</track_details>";
        return writer.toString();
    }

    public void logoutClick() {

        Intent authenticationIntent = new Intent(context.getApplicationContext(), AuthenticationActivity.class);
        authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(authenticationIntent);

    }
}
