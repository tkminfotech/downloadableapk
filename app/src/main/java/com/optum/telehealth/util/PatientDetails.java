package com.optum.telehealth.util;

/**
 * Created by tkmif-ws011 on 4/21/2016.
 */
public class PatientDetails {

    private String patient_id;
    private String full_name;
    private String nick_name;
    private String vsb_password;
    private String dob;
    private String sex;
    private String temperature_unit_id;
    private String weight_unit_id;
    private String interview_mode;
    private String server_time;
    private String ampm;
    private String contract_code;
    private String contract_code_name;
    private String log_status;
    private String language_id;
    private String cluster_id;
    private String token_uuid;
    private String time_zone_id;

    public String getTime_zone_id() {
        return time_zone_id;
    }

    public void setTime_zone_id(String time_zone_id) {
        this.time_zone_id = time_zone_id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getVsb_password() {
        return vsb_password;
    }

    public void setVsb_password(String vsb_password) {
        this.vsb_password = vsb_password;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTemperature_unit_id() {
        return temperature_unit_id;
    }

    public void setTemperature_unit_id(String temperature_unit_id) {
        this.temperature_unit_id = temperature_unit_id;
    }

    public String getWeight_unit_id() {
        return weight_unit_id;
    }

    public void setWeight_unit_id(String weight_unit_id) {
        this.weight_unit_id = weight_unit_id;
    }

    public String getInterview_mode() {
        return interview_mode;
    }

    public void setInterview_mode(String interview_mode) {
        this.interview_mode = interview_mode;
    }

    public String getServer_time() {
        return server_time;
    }

    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public String getAmpm() {
        return ampm;
    }

    public void setAmpm(String ampm) {
        this.ampm = ampm;
    }

    public String getContract_code() {
        return contract_code;
    }

    public void setContract_code(String contract_code) {
        this.contract_code = contract_code;
    }

    public String getContract_code_name() {
        return contract_code_name;
    }

    public void setContract_code_name(String contract_code_name) {
        this.contract_code_name = contract_code_name;
    }

    public String getLog_status() {
        return log_status;
    }

    public void setLog_status(String log_status) {
        this.log_status = log_status;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(String language_id) {
        this.language_id = language_id;
    }

    public String getCluster_id() {
        return cluster_id;
    }

    public void setCluster_id(String cluster_id) {
        this.cluster_id = cluster_id;
    }

    public String getToken_uuid() {
        return token_uuid;
    }

    public void setToken_uuid(String token_uuid) {
        this.token_uuid = token_uuid;
    }
}
