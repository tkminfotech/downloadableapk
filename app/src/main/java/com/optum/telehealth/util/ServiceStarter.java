package com.optum.telehealth.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.service.AlarmService;

public class ServiceStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            SharedPreferences settings = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
            String patient_id = settings.getString("patient_id", "-1");
            String Token_ID = settings.getString("Token_ID", "-1");
            Log.i("ServiceStarter", "patient_id=>" + patient_id + "  Token_ID=>" + Token_ID);
            if (patient_id.equals("-1") && Token_ID.equals("-1")) {
                Log.i("ServiceStarter", "Wait for First Login");
            } else {
                Intent reminder = new Intent();
                reminder.setClass(context, AlarmService.class);
                context.startService(reminder);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}