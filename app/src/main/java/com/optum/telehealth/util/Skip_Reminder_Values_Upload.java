package com.optum.telehealth.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.optum.telehealth.AuthenticationActivity;
import com.optum.telehealth.R;
import com.optum.telehealth.dal.Skip_Reminder_Value_db;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import com.optum.telehealth.util.Log;
import android.widget.Toast;

public class Skip_Reminder_Values_Upload extends
		AsyncTask<String, Void, String> {

	private String TAG = "Skip_Reminder_Values_Upload";
	private String ServerURL;
	public Skip_Reminder_Value_db reminder_skip_db;
	String[] temp1, temp2, temp3;
	int[] temp4;
	private Context context;
	private String serviceUrl;
	public Skip_Reminder_Values_Upload(Context context,	String ServerURL) {
		Log.i("Skip_Reminder", "IN");
		this.ServerURL = ServerURL;
		this.context = context;
		reminder_skip_db = new Skip_Reminder_Value_db(context);
		SharedPreferences settings = context.getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
		serviceUrl = settings.getString("Server_post_url", "-1");
	}

	@Override
	protected void onPostExecute(String result) {
		//Log.i("onPostExecute", result);
		if (result.equalsIgnoreCase("AuthenticationError")) {
			SharedPreferences ERROR_MSG = context.getSharedPreferences("ERROR_MSG", 0);
			String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
			Toast.makeText(context, msg_authentication_error, Toast.LENGTH_LONG).show();
			logoutClick();
		}


	}

	protected String doInBackground(String... urls) {

		String response = "";
		//String date = TestDate.getCurrentTime();
		reminder_skip_db.open();
		Cursor cursor1 = reminder_skip_db.selectAll();
		temp1 = new String[cursor1.getCount()];
		temp2 = new String[cursor1.getCount()];
		temp3 = new String[cursor1.getCount()];
		temp4 = new int[cursor1.getCount()];
		int j = 0;
	
		while (cursor1.moveToNext()) {
			temp1[j] = cursor1.getString(cursor1.getColumnIndex("patient_id"));
			temp2[j] = cursor1.getString(cursor1.getColumnIndex("time"));
			temp3[j] = cursor1.getString(cursor1.getColumnIndex("status"));
			temp4[j] = Integer.parseInt(cursor1.getString(cursor1.getColumnIndex("id")));
			
			System.out.println("patient_id==="+temp1[j]+"\ntime==="+temp2[j]+"\nstatus"+temp3[j]);
			
			try {
				StringBuilder str = new StringBuilder();
				//Log.i(TAG, "Uploading skip data to server started ...");
				SharedPreferences tokenPreference = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
				String  token = tokenPreference.getString("Token_ID", "-1");
				String patientId = tokenPreference.getString("patient_id", "-1");
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(serviceUrl
						+ "/droid_website/mob_reminder_vitals.ashx");

				List<NameValuePair> pairs = new ArrayList<NameValuePair>();

				post.setEntity(new UrlEncodedFormEntity(pairs));

				//Log.i(TAG, "authentication_token=>"+token);
				//Log.i(TAG, "patient_id=>"+patientId);
				//Log.i(TAG, "transmit_date=>"+Util.get_patient_time_zone_time(context));
				//Log.i(TAG, "section_date=>"+temp2[j]);

				post.setHeader("authentication_token", token);
				post.setHeader("login_patient_id", patientId);
				post.setHeader("patient_id", temp1[j]);
				post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
				post.setHeader("section_date", temp2[j]);

				//Log.i(TAG,"patient_id"+ temp1[j]);
				//Log.i(TAG,"section_date"+temp2[j]);
				
				
				HttpResponse response1 = client.execute(post);

				InputStream in = response1.getEntity().getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));
				String line = null;
				while ((line = reader.readLine()) != null) {
					str.append(line + "\n");
				}
				in.close();
				str.toString();
				//Log.i("response:", "reminder_vitals_response:" + str);
				if (str.length() > 0) {
					DocumentBuilder db = DocumentBuilderFactory
							.newInstance().newDocumentBuilder();
					InputSource is1 = new InputSource();
					is1.setCharacterStream(new StringReader(str.toString()));
					Document doc = db.parse(is1);
					NodeList nodes = doc.getElementsByTagName("optum");
					for (int i = 0; i < nodes.getLength(); i++) {

						response = Util.getTagValue(nodes, i, "response");
					}
					if (response.equals("Success")) {
						reminder_skip_db.update_db(temp4[j]);
					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			j += 1;
		}
		
		reminder_skip_db.close();
		
		return response;
	}

	public void logoutClick() {

//		SharedPreferences settings = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
//		SharedPreferences.Editor editor = settings.edit();
//		editor.remove("patient_name");
//		editor.remove("patient_id");
//		editor.remove("contract_code_name");
//		editor.remove("language_id");
//		editor.remove("Token_ID");
//		editor.commit();
//		SharedPreferences clusterSsettings = context.getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
//		SharedPreferences.Editor clusterEditor = clusterSsettings.edit();
//		clusterEditor.remove("clustorNo");
//		clusterEditor.remove("time_zone_id");
//		clusterEditor.commit();

		Intent authenticationIntent = new Intent(context.getApplicationContext(), AuthenticationActivity.class);
		authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(authenticationIntent);

	}

}