package com.optum.telehealth.util;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.optum.telehealth.AandDSmart;
import com.optum.telehealth.util.Log;

import java.lang.ref.WeakReference;

/**
 * Used to auto-pair AnD sensor
 * IMPORTANT: At least on Samsung S7 Android 7, the broadcast receiver is not used to pair the sensor! the sensor is paired automatically
 */
public class PairAandDSmart  extends BroadcastReceiver {

	public static final String EXTRA_DEVICE = "android.bluetooth.device.extra.DEVICE";
	private String TAG="PINReceiver";

	WeakReference<AandDSmart> mAnD; // to protect against circular references

	public PairAandDSmart(AandDSmart andObject) {
		mAnD = new WeakReference<> (andObject);
	}

	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		Log.d(TAG, "onReceive: PairAandDSmart, Action: " + action);

		// intent.getAction() may return null
		if (action == null) return;

		if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
			BluetoothDevice device = intent.getParcelableExtra(EXTRA_DEVICE);
			setBluetoothPairingPin(device);

		} else if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {

			if (mAnD != null && mAnD.get() != null) {
				final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
				BluetoothDevice currentDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				if (state == BluetoothDevice.BOND_BONDED) {
					Log.d(TAG, "onReceive: BOND_BONDED");
					mAnD.get().connect(currentDevice);
				}
			}
		}
	}

	public void setBluetoothPairingPin(BluetoothDevice device)
	{ 
		try {
			device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
			Log.d(TAG, "Success to setPairingConfirmation for a and d smart.");

		} catch (Exception e) {
			Log.e(TAG, e.toString()); // e.toString() causes no crashes. If e.getMessage() returns null Log.e throws an exception!!
			e.printStackTrace();
		}
	}
}