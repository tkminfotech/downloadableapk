package com.optum.telehealth.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import com.optum.telehealth.util.Log;
import android.widget.Toast;

import com.optum.telehealth.AuthenticationActivity;
import com.optum.telehealth.R;
import com.optum.telehealth.dal.Glucose_db;
import com.optum.telehealth.dal.Pressure_db;
import com.optum.telehealth.dal.Pulse_db;
import com.optum.telehealth.dal.Question_skip_db;
import com.optum.telehealth.dal.Questions_db;
import com.optum.telehealth.dal.Temperature_db;
import com.optum.telehealth.dal.Weight_db;

public class UploadVitalReadingsTask extends AsyncTask<String, Void, String> {

    private String TAG = "UploadVitalReadingsTask";
    private String serviceUrl;

    int[] blood_oxygen, pulse, Measurement_Id;
    private Context context;
    private Weight_db dbweight;
    private Pulse_db dbpulse_ox;
    private Pressure_db dbpressure;
    private Glucose_db dbglucose;
    private Temperature_db dbtemparature;
    private Question_skip_db dbquestion;
    private Questions_db dbcreate1;
    private String token;
    private String patientId;
    String login_patient_id;
    String[] measure_date = new String[]{};
    //String patient_id, item;
    int type = 0;

	/*
     * 1.glucose 2.Oxygen 3.pressure 4.pt/Inr 5.-- 6.Temperature 7.Weight
	 * 8.fluid level 11.Pain Level 12. Peak flow
	 */

    public UploadVitalReadingsTask(Context ctx, String serverpath) {

        SharedPreferences settings = ctx.getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = settings.getString("Server_post_url", "-1");
        SharedPreferences tokenPreference = ctx.getSharedPreferences(CommonUtilities.USER_SP, 0);
        token = tokenPreference.getString("Token_ID", "-1");
        patientId = tokenPreference.getString("patient_id", "-1");
        this.dbweight = new Weight_db(ctx);
        this.dbpulse_ox = new Pulse_db(ctx);
        this.dbpressure = new Pressure_db(ctx);
        this.dbglucose = new Glucose_db(ctx);
        this.dbtemparature = new Temperature_db(ctx);
        this.dbquestion = new Question_skip_db(ctx);
        this.dbcreate1 = new Questions_db(ctx);
        this.context = ctx;
    }

    @Override
    protected void onPostExecute(String result) {

        // send broad cast to all listening activities
        // Intent i = new Intent();
        // i.setAction(Util.UPLOAD_VITALS);
        Log.i(TAG, "onPostExecute vitals has been updated..");if (result.equalsIgnoreCase("AuthenticationError")) {
            if (result.equalsIgnoreCase("AF")) {
                SharedPreferences ERROR_MSG = context.getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(context.getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            }
        }

        // context.sendBroadcast(i);

		/*
		 * if(result.length()>0) // If vitals uploaded succesfully {
		 * 
		 * }
		 */

    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();
    }

    protected String doInBackground(String... urls) {

        return upload_vitals();
    }

    private String upload_vitals() {
        // DownloadAdviceMessage();
        String response = "";
        Log.i(TAG, "Uploading vitals server started ...");
        SharedPreferences USER_SP = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
        login_patient_id = USER_SP.getString("patient_id", "-1");
        if(upload_weight().equals("AuthenticationError")){

            return "AF";
        }else  if(upload_glucose().equals("AuthenticationError")){

            return "AF";
        } else  if(upload_pulse_ox().equals("AuthenticationError")){

            return "AF";
        }else  if(upload_pressure().equals("AuthenticationError")){

            return "AF";
        }else  if(upload_temperature().equals("AuthenticationError")){

            return "AF";
        }else  if(upload_question().equals("AuthenticationError")){

            return "AF";
        }else  if(upload_previous_question().equals("AuthenticationError")){

            return "AF";
        }else{
            return response;
        }

    }

    /*********************************
     * weight
     ****************************/
    private String upload_weight() {
        String response = "";
        try {
            dbweight.delete_weight_101();
            Cursor cweight = dbweight.SelectMeasuredweight();
            cweight.moveToFirst();
            if (cweight.getCount() > 0) {

                while (cweight.isAfterLast() == false) {
                    String dbPatId = ""+cweight.getInt(2);
                    Log.i(TAG, "Uploading weight data to server started ...");

                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(serviceUrl
                            + "/droid_website/mob_add_tpm_body_weight.ashx");
                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();

                    post.setEntity(new UrlEncodedFormEntity(pairs));
                    post.setHeader("authentication_token", token);
                    post.setHeader("login_patient_id", login_patient_id);
                    post.setHeader("patient_id", cweight.getString(2));
                    post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
                    post.setHeader("measure_date", cweight.getString(1)
                            .replace("\n", ""));
                    post.setHeader("kilogram",
                            cweight.getString(3).replace("\r", ""));
                    post.setHeader("kind", "1");
                    post.setHeader("fat_percent", "0.0");
                    post.setHeader("input_mode", cweight.getString(5));
                    post.setHeader("section_date", cweight.getString(7));
                    post.setHeader("is_reminder", cweight.getString(8));
                    HttpResponse response1 = client.execute(post);
                    InputStream in = response1.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        str.append(line + "\n");
                    }
                    in.close();
                    str.toString();
                    //Log.i("response:", "bodyWeight_response:" + str);
                    if (str.length() > 0) {
                        DocumentBuilder db = DocumentBuilderFactory
                                .newInstance().newDocumentBuilder();
                        InputSource is1 = new InputSource();
                        is1.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is1);
                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                        }
                        if (response.equals("Success")) {
                            // /// if success delete the value from the
                            // tablet db
                            //Log.i(TAG, "Update weight data with id ..."
                             //       + cweight.getString(0));
                            dbweight.UpdateMeasureData(Integer.parseInt(cweight
                                    .getString(0)));

                            dbweight.delete_weight_101();
							/*
							 * dbweight.delete_weight_data(Integer.parseInt(cweight
							 * .getString(0)));
							 */

                        }

                    }
                    cweight.moveToNext();
                }
            }
            cweight.close();
            dbweight.cursorWeight.close();
            dbweight.closeOpenWTdbConnection();

        } catch (Exception e) {
            System.out.println("Weight Not Posted aswellas not Entered in DB");
        }
        return response;
    }

    /*********************************
     * Oxygen
     ****************************/
    private String upload_pulse_ox() {
        String response = "";
        Log.i(TAG, "uploading started Oxgen from skip.");
        try {
            /***************** Oxgen ********************/

            Cursor c = dbpulse_ox.SelectMeasuredData();
			/*
			 * measure_date = new String[c.getCount()]; blood_oxygen = new
			 * int[c.getCount()]; pulse = new int[c.getCount()]; Measurement_Id
			 * = new int[c.getCount()];
			 */

            c.moveToFirst();
            if (c.getCount() > 0) {
                while (c.isAfterLast() == false) {
                    Log.i(TAG,
                            "Uploading oxygen measure data to server started ...");
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(serviceUrl
                            + "/droid_website/mob_add_tpm_blood_oxygen.ashx");
                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();

                    post.setEntity(new UrlEncodedFormEntity(pairs));

                    post.setHeader("authentication_token", token);
                    post.setHeader("login_patient_id", login_patient_id);
                    post.setHeader("patient_id", c.getString(5));
                    post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
                    post.setHeader("measure_date", c.getString(3));
                    post.setHeader("blood_oxygen", c.getString(1));
                    post.setHeader("pulse", c.getString(2));
                    post.setHeader("input_mode", c.getString(6));
                    post.setHeader("section_date", c.getString(7));
                    post.setHeader("is_reminder", c.getString(9));
                    HttpResponse response1 = client.execute(post);
                    InputStream in = response1.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        str.append(line + "\n");
                    }
                    in.close();
                    str.toString();
                    //Log.i("response:", "bloodOxygen_response:" + str);
                    if (str.length() > 0) {
                        DocumentBuilder db = DocumentBuilderFactory
                                .newInstance().newDocumentBuilder();
                        InputSource is1 = new InputSource();
                        is1.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is1);
                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                        }
                        if (response.equals("Success")) {
                            // /// if success delete the value from the
                            // tablet db
                            Log.i(TAG,
                                    "Update Measure data with id ..."
                                            + c.getString(0));
                            dbpulse_ox.UpdateMeasureData(Integer.parseInt(c
                                    .getString(0)));
                        }

                    }
                    c.moveToNext();

                }
            }
            c.close();
            dbpulse_ox.cursorMeasurement.close();
            dbpulse_ox.close();

        } catch (Exception e) {
            // e.printStackTrace();
        }
        return response;
    }

    /*********************************
     * Pressure
     **************************/
    private String upload_pressure() {
        String response = "";
        Log.i(TAG, "uploading started.");

        try {

            Cursor cpressure = dbpressure.SelectMeasuredpressure();
            cpressure.moveToFirst();
            if (cpressure.getCount() > 0) {

                while (cpressure.isAfterLast() == false) {
                    Log.i(TAG, "Uploading pressure  data to server started ...");

                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(serviceUrl
                            + "/droid_website/mob_add_tpm_blood_pressure.ashx");
                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();

                    post.setEntity(new UrlEncodedFormEntity(pairs));

                    post.setHeader("authentication_token", token);
                    post.setHeader("login_patient_id", login_patient_id);
                    post.setHeader("patient_id", cpressure.getString(1));
                    post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
                    post.setHeader("measure_date", cpressure.getString(5));
                    post.setHeader("diastolic", cpressure.getString(3));
                    post.setHeader("systolic", cpressure.getString(2));
                    post.setHeader("pulse", cpressure.getString(4));
                    post.setHeader("input_mode", cpressure.getString(7));
                    post.setHeader("section_date", cpressure.getString(8));

                    post.setHeader("prompt_flag", cpressure.getString(10));
                    post.setHeader("body_movement", cpressure.getString(11));
                    post.setHeader("irregular_pulse", cpressure.getString(12));
                    post.setHeader("battery_level", cpressure.getString(13));
                    post.setHeader("measurement_position_flag", cpressure.getString(14));
                    post.setHeader("flag_cuff_fit_detection_flag", cpressure.getString(15));
                    post.setHeader("is_reminder", cpressure.getString(16));
                    HttpResponse response1 = client.execute(post);
                    InputStream in = response1.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        str.append(line + "\n");
                    }
                    in.close();
                    str.toString();
                   // Log.i(TAG, "bloodPressure_response:" + str);
                    if (str.length() > 0) {
                        DocumentBuilder db = DocumentBuilderFactory
                                .newInstance().newDocumentBuilder();
                        InputSource is1 = new InputSource();
                        is1.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is1);
                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                        }
                        if (response.equals("Success")) {
                            // /// if success delete the value from the
                            // tablet db
                            Log.i(TAG, "Update Pressure data with id ..."
                                    + cpressure.getString(0));
                            dbpressure.UpdatepressureData(Integer
                                    .parseInt(cpressure.getString(0)));
                        }

                    }
                    cpressure.moveToNext();
                }
            }
            cpressure.close();
            dbpressure.cursorPreesure.close();
            dbpressure.close();

        } catch (Exception e) {
            // e.printStackTrace();
        }
        return response;
    }

    /*********************************
     * Glucose
     ***************************/
    private String upload_glucose() {
        String response = "";
        try {
            Cursor cglucose = dbglucose.SelectMeasuredglucose();
            cglucose.moveToFirst();
            if (cglucose.getCount() > 0) {

                while (cglucose.isAfterLast() == false) {
                    Log.i(TAG, "Uploading Glucose data to server started ...");

                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(serviceUrl
                            + "/droid_website/mob_add_tpm_blood_glucose.ashx");
                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    post.setEntity(new UrlEncodedFormEntity(pairs));
                    post.setHeader("authentication_token", token);
                    post.setHeader("login_patient_id", login_patient_id);
                    post.setHeader("patient_id", cglucose.getString(4));
                    post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
                    post.setHeader("measure_date", cglucose.getString(2));
                    post.setHeader("milligram", cglucose.getString(1));
                    post.setHeader("input_mode", cglucose.getString(5));
                    post.setHeader("section_date", cglucose.getString(6));

                    post.setHeader("prompt_flag", cglucose.getString(8));
                    post.setHeader("is_reminder", cglucose.getString(9));
                    HttpResponse response1 = client.execute(post);
                    InputStream in = response1.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        str.append(line + "\n");
                    }
                   // Log.i(TAG,
                   //         " glucose data with id ..." + cglucose.getString(0));
                    in.close();
                    str.toString();
                   // Log.i("response:", "bloodGlucose_response:" + str);
                    if (str.length() > 0) {
                        DocumentBuilder db = DocumentBuilderFactory
                                .newInstance().newDocumentBuilder();
                        InputSource is1 = new InputSource();
                        is1.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is1);
                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                        }
                        if (response.equals("Success")) {
                            // /// if success delete the value from the
                            // tablet db
                            Log.i(TAG, "Update glucose data with id ..."
                                    + cglucose.getString(0));
                            dbglucose.UpdateglucoseData(Integer
                                    .parseInt(cglucose.getString(0)));
                        }
                    }
                    cglucose.moveToNext();
                }
            }
            dbglucose.cursorMeasurement.close();
            dbglucose.close();

        } catch (Exception e) {
            // e.printStackTrace();
        }
        return response;
    }

    /********************************
     * Temperature
     ************************/
    private String upload_temperature() {
        String response = "";
        try {

            Cursor ctemperature = dbtemparature.SelectMeasuredTemperature();
            ctemperature.moveToFirst();
            if (ctemperature.getCount() > 0) {

                while (ctemperature.isAfterLast() == false) {
                    Log.i(TAG,
                            "Uploading temparature data to server started ...");

                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(serviceUrl
                            + "/droid_website/mob_add_tpm_body_temperature.ashx");
                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    post.setEntity(new UrlEncodedFormEntity(pairs));
                    post.setHeader("authentication_token", token);
                    post.setHeader("login_patient_id", login_patient_id);
                    post.setHeader("patient_id", ctemperature.getString(4));
                    post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
                    post.setHeader("measure_date", ctemperature.getString(2));
                    post.setHeader("fahrenheit", ctemperature.getString(1));
                    post.setHeader("input_mode", ctemperature.getString(5));
                    post.setHeader("section_date", ctemperature.getString(6));
                    post.setHeader("is_reminder", ctemperature.getString(8));
                    HttpResponse response1 = client.execute(post);
                    InputStream in = response1.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        str.append(line + "\n");
                    }

                    in.close();
                    str.toString();
                    //Log.i("response:", "bodyTemp_response:" + str);
                    if (str.length() > 0) {
                        DocumentBuilder db = DocumentBuilderFactory
                                .newInstance().newDocumentBuilder();
                        InputSource is1 = new InputSource();
                        is1.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is1);
                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                        }
                        if (response.equals("Success")) {
                            // /// if success delete the value from the
                            // tablet db
                            Log.i(TAG, "Update temperature data with id ..."
                                    + ctemperature.getString(0));
                            dbtemparature.UpdatetemparatureData(Integer
                                    .parseInt(ctemperature.getString(0)));
                        }
                    }
                    ctemperature.moveToNext();
                }
            }
            dbtemparature.cursorTemperature.close();
            dbtemparature.close();

        } catch (Exception e) {
            // e.printStackTrace();
        }
        return response;
    }

    /*********************************************
     * question
     ********************************************/
    private String upload_question() {
        String response = "";
        try {
            Cursor cr = dbquestion.Select_question();
            if (cr.getCount() <= 0) {

            }
            cr.moveToFirst();

            String finalxml = "";
            int skip_id = 0;
            String PatientId = "";
            String MeasureDate = "";
            String sectionDate = "";
            String ir_is_reminder ="";
            while (cr.isAfterLast() == false) {
                PatientId = cr.getString(1);
                MeasureDate = cr.getString(3);
                sectionDate = cr.getString(2);
                skip_id = Integer.parseInt(cr.getString(0));
                ir_is_reminder=cr.getString(4);
                HttpClient client = new DefaultHttpClient();
                Log.i(TAG,
                        "upload_question:"
                                + serviceUrl
                                + "/droid_website/mob_add_tpm_interview_result_branchable.ashx");
                HttpPost post = new HttpPost(
                        serviceUrl
                                + "/droid_website/mob_add_tpm_interview_result_branchable.ashx");
                StringEntity se = new StringEntity(finalxml, HTTP.UTF_8);
                post.setHeader("authentication_token", token);
                post.setHeader("login_patient_id", login_patient_id);
                post.setHeader("patient_id", PatientId);
                post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
                post.setHeader("measure_date", MeasureDate);
                post.setHeader("tree_number", "-101");
                post.setHeader("section_date", sectionDate);
                post.setHeader("ir_is_reminder", ir_is_reminder);
                //Log.i(TAG, "finalxml ...HEADERS====>patient_id:"+PatientId+" transmit_date:"+Util.get_patient_time_zone_time(context)+" tree_number:-101"
                //        +" section_date"+sectionDate+" measure_date:"+MeasureDate+" ir_is_reminder:"+ir_is_reminder);
                post.setEntity(se);
                if (sectionDate.length() > 5) {
                    HttpResponse response1 = client.execute(post);
                    InputStream in = response1.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    StringBuilder str = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        str.append(line + "\n");
                    }
                    in.close();
                    str.toString();
                    //Log.e(TAG,"upload_question ...RESPONSE: "+str.toString());
                    if (str.length() > 0) {
                        DocumentBuilder db = DocumentBuilderFactory
                                .newInstance().newDocumentBuilder();
                        InputSource is1 = new InputSource();
                        is1.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is1);
                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                        }
                        if (response.equals("Success")) {

                            dbquestion.delete_skip_question_data(skip_id);
                            Log.i(TAG, "question skip_id ..." + skip_id);
                        }
                    }
                }
                cr.moveToNext();

            }

            cr.close();
            dbquestion.curso_qusetion.close();
            dbquestion.close();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return response;
    }

    private String upload_previous_question() {
        String response = "";
        Log.i(TAG, "Upload upload_previous_question to server start ");
        // String query =
        // "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber from Droid_DMP_User_Response a where a.Status=0";
        String query = "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber,a.Item_Content,a.sectionDate,a.ir_is_reminder,a.Node_Type from Droid_DMP_User_Response a where a.Status=0";

        Cursor cr = dbcreate1.SelectDBValues(query);
        if (cr.getCount() <= 0) {
            Log.i(TAG, "Upload question to server Cursor is null ");
            return "";
        }
        int j = 0;
        Log.i(TAG, "Upload question to server --------------------->>>>>> ");
        String[] childxml = new String[cr.getCount()];
        String[] treeNumber = new String[cr.getCount()];
        String[] finalxml = new String[cr.getCount()];
        String[] PatientId = new String[cr.getCount()];
        String[] MeasureDate = new String[cr.getCount()];
        String[] ItemContent = new String[cr.getCount()];
        String[] sectionDate = new String[cr.getCount()];
        String[] ir_is_reminder = new String[cr.getCount()];
        while (cr.moveToNext()) {
            String xml1, xml2, xml3, xml4, xml5 = "";
            xml1 = "<response>";
            xml2 = "<sequencenumber>" + cr.getString(1)
                    + "</sequencenumber>";
            // xml3 = "<itemnumber>" + response.getString(0) +
            // "</itemnumber>";
            if (cr.getString(8).equals("5")) {
                if (cr.getInt(0) == 0) {
                    xml3 = "<itemnumber>-1</itemnumber>";
                } else {
                    xml3 = "<itemnumber>-2</itemnumber>";

                }

            } else {
                ItemContent[j] = String
                        .valueOf(dbcreate1.SelectItemNumber(cr.getString(5), cr.getString(1)));
                xml3 = "<itemnumber>" + ItemContent[j] + "</itemnumber>";
            }

            xml4 = "<measure_date>" + cr.getString(2) + "</measure_date>";
            xml5 = "</response>";
            childxml[j] = xml1 + xml2 + xml3 + xml4 + xml5;
            if (childxml[j] != null) {
                finalxml[j] = "<responsetree>" + childxml[j]
                        + "</responsetree>";
            }
            PatientId[j] = cr.getString(3);
            MeasureDate[j] = cr.getString(2);
            treeNumber[j] = cr.getString(4);
            sectionDate[j] = cr.getString(6);
            ir_is_reminder[j] = cr.getString(7);
            Log.i(TAG, "Upload question to server patient_id "+cr.getString(3));
            try {
                InputStream content = null;
                HttpClient client = new DefaultHttpClient();
                Log.i(TAG,
                        "Updating serverurl"
                                + serviceUrl
                                + "/droid_website/mob_add_tpm_interview_result_branchable.ashx");
                HttpPost post = new HttpPost(
                        serviceUrl
                                + "/droid_website/mob_add_tpm_interview_result_branchable.ashx");
                Log.i(TAG, "finalxml ..." + finalxml[j].toString());
                StringEntity se = new StringEntity(finalxml[j], HTTP.UTF_8);
                List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                post.setHeader("authentication_token", token);
                post.setHeader("login_patient_id", login_patient_id);
                post.setHeader("patient_id", PatientId[j]);
                post.setHeader("transmit_date", Util.get_patient_time_zone_time(context));
                post.setHeader("measure_date", MeasureDate[j]);
                post.setHeader("tree_number", treeNumber[j]);
                post.setHeader("section_date", sectionDate[j]);
                post.setHeader("ir_is_reminder", ir_is_reminder[j]);
                Log.i(TAG, "finalxml ...HEADERS====>patient_id:"+PatientId[j]+" transmit_date:"+Util.get_patient_time_zone_time(context)+" tree_number"
                +treeNumber[j]+" section_date"+sectionDate[j]+" measure_date:"+MeasureDate[j]+" ir_is_reminder:"+ir_is_reminder[j]);
                post.setEntity(se);
                HttpResponse response1 = client.execute(post);
                int a = response1.getStatusLine().getStatusCode();
                InputStream in = response1.getEntity().getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));
                StringBuilder str = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    str.append(line + "\n");
                }
                in.close();
                str.toString();
                Log.e(TAG,"mob_add_tpm_interview_result_branchable ...RESPONSE: "+str.toString());
                if (str.length() > 0) {
                    DocumentBuilder db = DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder();
                    InputSource is1 = new InputSource();
                    is1.setCharacterStream(new StringReader(str.toString()));
                    Document doc = db.parse(is1);
                    NodeList nodes = doc.getElementsByTagName("optum");
                    for (int i = 0; i < nodes.getLength(); i++) {

                        response = Util.getTagValue(nodes, i, "response");
                    }
                    if (response.equalsIgnoreCase("Success")) {

                        dbcreate1.updateToQuestionToServer();
                    }

                }

            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }

            j += 1;
        }
        cr.close();
        dbcreate1.cursorQuestions.close();
        dbcreate1.closeopendbConnection();
        return response;
    }

    public void logoutClick() {

//        SharedPreferences settings = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.remove("patient_name");
//        editor.remove("patient_id");
//        editor.remove("contract_code_name");
//        editor.remove("language_id");
//        editor.remove("Token_ID");
//        editor.commit();
//        SharedPreferences clusterSsettings = context.getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
//        SharedPreferences.Editor clusterEditor = clusterSsettings.edit();
//        clusterEditor.remove("clustorNo");
//        clusterEditor.remove("time_zone_id");
//        clusterEditor.commit();

        Intent authenticationIntent = new Intent(context.getApplicationContext(), AuthenticationActivity.class);

        context.startActivity(authenticationIntent);

    }

}