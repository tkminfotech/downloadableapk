package com.optum.telehealth.bean;

/**
 * Created by tkmif-ws011 on 5/11/2016.
 */
public class CrashLog {
    private String patient_id;
    private String device_type;
    private String android_version;
    private String application_version;
    private String crash_details;
    private String exception_time;

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getAndroid_version() {
        return android_version;
    }

    public void setAndroid_version(String android_version) {
        this.android_version = android_version;
    }

    public String getApplication_version() {
        return application_version;
    }

    public void setApplication_version(String application_version) {
        this.application_version = application_version;
    }

    public String getCrash_details() {
        return crash_details;
    }

    public void setCrash_details(String crash_details) {
        this.crash_details = crash_details;
    }

    public String getException_time() {
        return exception_time;
    }

    public void setException_time(String exception_time) {
        this.exception_time = exception_time;
    }
}
