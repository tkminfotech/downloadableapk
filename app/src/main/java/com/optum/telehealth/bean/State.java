package com.optum.telehealth.bean;

/**
 * Created by tkmif-ws011 on 2/21/2017.
 */

public class State {
    private int stateId;
    private String stateName;

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
