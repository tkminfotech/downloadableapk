package com.optum.telehealth.bean;


import java.sql.Date;

public class ClassSensor {
	
	private int sensorId, measureTypeId, weightUnitId,Status;
	private String pin,patientId;
	private Date downloadDate;
	String weightUnitName, macId, measureTypeName, sensorName;
	public String getMacId() {
		return macId;
	}
	public void setMacId(String macId) {
		this.macId = macId;
	}
	public int getSensorId() {
		return sensorId;
	}
	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}
	public int getMeasureTypeId() {
		return measureTypeId;
	}
	public void setMeasureTypeId(int measureTypeId) {
		this.measureTypeId = measureTypeId;
	}
	public int getWeightUnitId() {
		return weightUnitId;
	}
	public void setWeightUnitId(int weightUnitId) {
		this.weightUnitId = weightUnitId;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public int getStatus() {
		return Status;
	}
	public void setStatus(int status) {
		Status = status;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public Date getDownloadDate() {
		return downloadDate;
	}
	public void setDownloadDate(Date downloadDate) {
		this.downloadDate = downloadDate;
	}
	public String getWeightUnitName() {
		return weightUnitName;
	}
	public void setWeightUnitName(String weightUnitName) {
		this.weightUnitName = weightUnitName;
	}
	public String getMeasureTypeName() {
		return measureTypeName;
	}
	public void setMeasureTypeName(String measureTypeName) {
		this.measureTypeName = measureTypeName;
	}
	public String getSensorName() {
		return sensorName;
	}
	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}
	
}
