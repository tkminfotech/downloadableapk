package com.optum.telehealth.bean;

public class ClassMeasureType {

	int measureTypeId;
	int status;
	String date,patientId;

	public void setMeasuretypeId(int measureTypeId1) {

		this.measureTypeId = measureTypeId1;

	}

	public int getMeasuretypeId() {

		return measureTypeId;
	}

	public void setPatientId(String patientId1) {

		this.patientId = patientId1;

	}

	public String getPatientId() {

		return patientId;
	}

	public void setStatus(int i) {

		this.status = i;
	}

	public int getStatus() {

		return status;
	}

	public void setDate(String date1) {

		this.date = date1;

	}

	public String getDate() {

		return date;
	}

}
