package com.optum.telehealth;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import com.optum.telehealth.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class PinValidationActivity extends Activity {

    private Button btn_submit, btn_resend;
    private EditText edt_pin;
    private String TAG = "PinValidationActivity", serviceUrl;
    private String temp_pin, temp_pinid, imeilNo = "", message, class_status;
    private TextView txt_errorMessage;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 3452;
    private GlobalClass appClass;
    public String authentication_Token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appClass = (GlobalClass) getApplicationContext();
        appClass.changeLanguage(this);
        setContentView(R.layout.activity_pin_validation);

        txt_errorMessage = (TextView) findViewById(R.id.txt_errorMessage);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_resend = (Button) findViewById(R.id.btn_resend);
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                temp_pinid = bundle.getString("pin_id");//this is for String pin_id
                message = bundle.getString("message");//this is for String message
                class_status = bundle.getString("class_status");//this is for String class_status
                //Log.i(TAG, "temp_pinid = " + temp_pinid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (message.equals("-1")) {
            //NO MESSAGE
        } else {
            setNotificationMessage(message);
        }
        readPhoneState();

        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appClass.isConnectingToInternet(getBaseContext())) {
                    new mob_resend_pin().execute();
                } else {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                    setNotificationMessage(NoConnectionError);
                }
            }
        });

        edt_pin = (EditText) findViewById(R.id.edt_pin);
        edt_pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edt_pin.getText().length() == 6) {
                    btn_submit.setEnabled(true);
                    if (getResources().getBoolean(R.bool.isTab)) {
                        btn_submit.setBackground(getResources().getDrawable(R.drawable.orange));
                    } else {
                        btn_submit.setBackground(getResources().getDrawable(R.drawable.button_orange));
                    }
                } else {
                    btn_submit.setEnabled(false);
                    if (getResources().getBoolean(R.bool.isTab)) {
                        btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable_tab));
                    } else {
                        btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable));
                    }
                }
            }
        });

        SharedPreferences urlSettings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = urlSettings.getString("Server_post_url", "-1");

        btn_submit.setText("     " + getResources().getString(R.string.submit) + "     ");
        btn_submit.setEnabled(false);
        if (getResources().getBoolean(R.bool.isTab)) {
            btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable_tab));
        } else {
            btn_submit.setBackground(getResources().getDrawable(R.drawable.send_disable));
        }
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                temp_pin = edt_pin.getText().toString().trim();
                //Log.i(TAG, "temp_pin= " + temp_pin);
                if (appClass.isConnectingToInternet(getBaseContext())) {
                    new mob_pin_validation().execute();
                } else {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                    setNotificationMessage(NoConnectionError);
                }
            }
        });
    }

    public void readPhoneState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getApplicationContext().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_COARSE_LOCATION);
            } else {
                readerImei();
            }
        } else {
            readerImei();
        }
    }

    public void readerImei() {
        try {
            String android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
//            TelephonyManager tManager = (TelephonyManager) this
//                    .getSystemService(Context.TELEPHONY_SERVICE);
            imeilNo = android_id;
            Log.i(TAG, "imeilNo= " + imeilNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    private class mob_pin_validation extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        String res_message, res_credential_status, res_invalid_entry, res_patient_id, res_message_description;

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_request_pin");
            try {
                progressDialog = new ProgressDialog(PinValidationActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_pin_validation #doInBackground");
            String response = "";
            try {
                Log.i(TAG, "imeilNo On Request= " + imeilNo);
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_validate_pin.ashx", new String[]{
                                "pin_id", "pin", "imei_no"},
                        new String[]{temp_pinid, temp_pin, imeilNo});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i(TAG, "mob_pin_validation: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("validate_pin");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("Success")) {
                        response = "success";
                        res_patient_id = Util.getTagValue(nodes, i, "patient_id");
                        res_credential_status = Util.getTagValue(nodes, i, "credential_status");
                        authentication_Token = Util.getTagValue(nodes, i, "token_uuid");
                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("Token_ID", authentication_Token);
                        editor.commit();
                        appClass.token = authentication_Token;
                        //Log.i(TAG, "==========SUCCESS==========" + res_patient_id + "<=>" + res_credential_status);
                        return response;
                    } else {
                        response = "failed";
                        res_message = Util.getTagValue(nodes, i, "message");
                        res_invalid_entry = Util.getTagValue(nodes, i, "invalid_entry");
                        res_message_description = Util.getTagValue(nodes, i, "message_description");
                        //Log.i(TAG, "==========FAILED==========" + res_message + "<=>" + res_invalid_entry);
                        return response;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
               // Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("success")) {
                    if (res_credential_status.equals("1")) {
                        if (class_status.equals("true")) {
                            Intent intent = new Intent(getApplicationContext(),
                                    FinalMainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                            finish();
                        } else {
                            Intent i = new Intent(PinValidationActivity.this, AuthenticationActivity.class);
                            startActivity(i);
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    } else {
                        Intent i = new Intent(PinValidationActivity.this, RegisterCredentialActivity.class);
                        i.putExtra("patient_id", res_patient_id);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
//                        Intent i = new Intent(PinValidationActivity.this, Terms_ConditionsActivity.class);
//                        i.putExtra("patient_id", res_patient_id);
//                        i.putExtra("flow_from", "registration");
//                        startActivity(i);
//                        overridePendingTransition(0, 0);
//                        finish();
                    }
                } else if (s.equals("failed")) {
                    if (res_invalid_entry.equals("3")) {
                        Intent i = new Intent(PinValidationActivity.this, ConfigErrorActivity.class);
                        i.putExtra("message", res_message);
                        i.putExtra("message_desc", res_message_description);
                        i.putExtra("class_name", "com.optum.telehealth.PinValidationActivity");
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    } else {
                        edt_pin.setText("");
                        setNotificationMessage(res_message);
                    }
                } else {
                    edt_pin.setText("");
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String connection_error  = ERROR_MSG.getString("connection_error", "-1");
                    setNotificationMessage(connection_error);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }

    public void setNotificationMessage(String msg) {
        txt_errorMessage.setVisibility(View.VISIBLE);
        txt_errorMessage.setText(msg);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMessage.animate().alpha(0.0f);
                        txt_errorMessage.setVisibility(View.GONE);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PERMISSION_REQUEST_COARSE_LOCATION) {
//            if (resultCode == RESULT_CANCELED) {
//                readPhoneState();
//            } else {
//                readerImei();
//            }
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    readerImei();
                } else {
                    readPhoneState();
                }
                return;
            }
        }
    }

    private class mob_resend_pin extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        String res_message, resend_count;

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "#onPreExecute #mob_resend_pin");
            try {
                progressDialog = new ProgressDialog(PinValidationActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.pleasewait));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "#mob_resend_pin #doInBackground");
            String response = "";
            try {
                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_resend_pin.ashx", new String[]{
                                "pin_id"},
                        new String[]{temp_pinid});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    response = "error";
                    return response;
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i(TAG,
                            "No details for the current serial number from server");
                    response = "error";
                    return response;
                }
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                //Log.i(TAG, "mob_resend_pin: " + str);
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("resend_pin");
                for (int i = 0; i < nodes.getLength(); i++) {
                    String mob_response = Util.getTagValue(nodes, i, "mob_response");
                    if (mob_response.equals("Success")) {
                        response = "success";
                        res_message = Util.getTagValue(nodes, i, "message");
                        resend_count = Util.getTagValue(nodes, i, "resend_count");
                        return response;
                    } else {
                        response = "failed";
                        res_message = Util.getTagValue(nodes, i, "message");
                        return response;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressDialog.dismiss();
                //Log.i(TAG, "#onPostExecute Response: " + s);
                if (s.equals("error")) {
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String NoConnectionError  = ERROR_MSG.getString("NoConnectionError", "-1");
                    setNotificationMessage(NoConnectionError);
                } else if (s.equals("failed")){
                    setNotificationMessage(res_message);
                } else if (s.equals("success")){

                    if (resend_count.equals("3")) {
                        Intent i = new Intent(PinValidationActivity.this, ConfigErrorActivity.class);
                        i.putExtra("message", res_message);
                        i.putExtra("message_desc", res_message);
                        i.putExtra("class_name", "reSendPin");
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    }else{
                        setNotificationMessage(res_message);
                    }
//                    if(resend_count.equals("3")){
//                        btn_resend.setEnabled(false);
//                        btn_resend.setBackground(getResources().getDrawable(R.drawable.btn_resend_disable_background));
//                        btn_resend.setTextColor(getResources().getColor(R.color.Mob_Gray3));
//                        btn_resend.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_right_disable, 0);
//                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
}