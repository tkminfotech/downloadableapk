package com.optum.telehealth;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class ConfigErrorActivity extends Activity {
    private Button btn_back;
    private int redirector;
    private TextView txt_config_error1, txt_config_error2, txt_config_error3, txt_errorMessage;
    private String message_desc = "", message = "", class_name = "", TAG = "ConfigErrorActivity";
private GlobalClass appClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_error);
        appClass = (GlobalClass)getApplicationContext();
        appClass.changeLanguage(this);
        btn_back = (Button) findViewById(R.id.btn_back);
        txt_config_error1 = (TextView) findViewById(R.id.txt_config_error1);
        txt_config_error2 = (TextView) findViewById(R.id.txt_config_error2);
        txt_config_error3 = (TextView) findViewById(R.id.txt_config_error3);

        txt_errorMessage = (TextView) findViewById(R.id.txt_errorMessage);

        btn_back.setText(R.string.back);

        SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);

        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                message_desc = bundle.getString("message_desc");//this is for String
                message = bundle.getString("message");//this is for String
                class_name = bundle.getString("class_name");//this is for String

                //Log.i(TAG, "message = " + message_desc);
                String message_txt1 = settings.getString(message_desc, "");
                if (class_name.equals("reSendPin")) {

                    txt_config_error1.setText(message);
                    txt_config_error2.setText(Html.fromHtml(getResources().getString(R.string.forTechHelp)));
                    txt_config_error3.setText(settings.getString("reg_error_message4", ""));
                } else {

                    txt_config_error1.setText(message_txt1);
                    txt_config_error2.setText(Html.fromHtml(settings.getString("reg_error_message2", "") + "<b> " + settings.getString("reg_error_message3", "") + "</b>"));
                    txt_config_error3.setText(settings.getString("reg_error_message4", ""));
                }

                if (message.equals("-1")) {
                    //NO MESSAGE
                } else {
                    setNotificationMessage(message);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Class<?> c = null;
                if(class_name != null) {
                    try {
                        c = Class.forName(class_name);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }*/
                Intent i = new Intent(ConfigErrorActivity.this, RegistrationActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    public void setNotificationMessage(String msg) {
        txt_errorMessage.setVisibility(View.VISIBLE);
        txt_errorMessage.setText(msg);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMessage.animate().alpha(0.0f);
                        txt_errorMessage.setVisibility(View.GONE);
                        timer.cancel();
                    }
                });
            }
        }, 3000, 3000);
    }
}
