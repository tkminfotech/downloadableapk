package com.optum.telehealth;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.optum.telehealth.util.Log;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassMeasureType;
import com.optum.telehealth.bean.ClassSchedule;
import com.optum.telehealth.dal.MeasureType_db;
import com.optum.telehealth.dal.Schedule_db;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.service.CheckService;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.Util;

@SuppressLint("HandlerLeak")
public class Welcome_Activity extends Titlewindow {
    private Sensor_db sensor_db = new Sensor_db(this);
    private Schedule_db dbSchedule = new Schedule_db(this);
    // Sensor_db dbSensor = new Sensor_db(this);
    public static int PatientIdDroid;
    public static String serialNo = "";
    private String TAG = "LoginActivity";
    //private TextToSpeech tts;
    TextView username, message;
    // ProgressDialog pd = null;
    public int flag_login = 0;
    BackgroundThread backgroundThread;
    public int startingType = 0;
    MeasureType_db db_measureType = new MeasureType_db(this);
    private String Check;
    private boolean isBackPressed = false;
//    private ProgressDialog progressDialog;
//private RelativeLayout layout_welcome;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        isBackPressed = false;
        appClass.isEllipsisEnable = false;
        appClass.isSupportEnable = false;
       // layout_welcome = (RelativeLayout)findViewById(R.id.login_mid);
        SharedPreferences reminderPreferences = getSharedPreferences("REMINDER_STATUS", Context.MODE_PRIVATE);
        SharedPreferences.Editor reminderEditor = reminderPreferences.edit();
        reminderEditor.putBoolean("REMINDER_STATUS", false);
        reminderEditor.commit();
        setWelcome(0);
        check_schedule_for_skip_vitals();

        backgroundThread = new BackgroundThread();
        backgroundThread.setRunning(true);
        backgroundThread.start();
        clickedhome = 0;

        // clear retak sp
        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.RETAKE_SP, 0);
        SharedPreferences.Editor editor_retake = settings.edit();
        editor_retake.putInt("retake_status", 0);
        editor_retake.commit();
        SharedPreferences flow = getSharedPreferences(
                CommonUtilities.USER_FLOW_SP, 0);

        SharedPreferences.Editor editor = flow.edit();
        editor.putInt("flow", 0);
        editor.putInt("reminder_path", 0);
        editor.commit();
        start_check_service();// check the status of reminder check service
        start_feedback();// start feedback servic
        Log.i(TAG, "-------------------oncreate--------------  login  activity");

    }

    private void playwelcome() {
        try {
            Thread.sleep(800);
            Log.i(TAG, "sleep 300");
        } catch (InterruptedException e) {

        }
    }

    private void setWelcome(int i) {
        try {

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.USER_SP, 0);

            username = (TextView) findViewById(R.id.txtusername);
            message = (TextView) findViewById(R.id.takereading);

            SharedPreferences settings1 = getSharedPreferences(
                    CommonUtilities.CLUSTER_SP, 0);

            String patient_id = settings.getString("patient_id",
                    "-1");
            ApkType = settings1.getString("clustorNo", "-1"); // #1
            String msgdf = this.getString(R.string.welcome_vitals);

            Constants.setquestionPresent(0);
            ClassSchedule classSchedule = new ClassSchedule();
            classSchedule = dbSchedule.selectSchedule("", patient_id);
            int measureTypeId = classSchedule.getMeasureTypeId();

            int a = 0;

            if (measureTypeId != 0) {

                a = measureTypeId;

            } else {
                a = sensor_db.Selectsensororder();
            }

            String val = Util.getMeasurement(a);

            if (a == 101 || a == 4) {
                Constants.setquestionPresent(1);
                if (i == 0)
                    message.setText(this.getString(R.string.welcome_question));

            } else if (a == 0) {
                if (i == 0)
                    message.setText(this.getString(R.string.nothingscheduled));

            } else {

                if (val.contains("weight")) {
                    if (i == 0)
                        message.setText(msgdf + " "
                                + this.getString(R.string.str_weight) + ".");
                } else if (val.contains("Pulse")) {
                    if (i == 0)
                        message.setText(msgdf + " "
                                + this.getString(R.string.bloodoxygen) + ".");
                } else if (val.contains("Temperature")) {
                    if (i == 0)
                        message.setText(msgdf + " "
                                + this.getString(R.string.str_temperature) + ".");
                } else {

                    if (val.contains("Glucose")) {

                        if (i == 0)
                            message.setText(msgdf + " "
                                    + this.getString(R.string.str_glucose) + ".");
                    } else {
                        if (i == 0)
                            message.setText(msgdf + " "
                                    + this.getString(R.string.blood) + " "
                                    + this.getString(R.string.str_pressure) + ".");
                    }
                }
            }

            String patientname = settings.getString("patient_name", "-1"); // #1

            if (!patientname.equals("-1")) {
                if (i == 0)
                    username.setText(this.getString(R.string.hello)
                            + " " + patientname + ".");
            }

            if (i == 0) {
                Typeface type = Typeface.createFromAsset(getAssets(),
                        "fonts/FrutigerLTStd-Roman.otf");
                username.setTypeface(type, Typeface.BOLD);
                message.setTypeface(type, Typeface.NORMAL);
            }
        } catch (Exception e) {
            // Log.e("Droid Error",e.getMessage());
            Log.i(TAG, " Exception show login activity setWelcome() ");
        }

    }

    @Override
    public void onStop() {

        Log.i(TAG, "-------------------onStop--------------  login  activity");

        // Commit the edits!
        super.onStop();
        flag_login = 1;
        // db_measureType.updateStatusforAll(1);
        //Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public class BackgroundThread extends Thread {
        volatile boolean running = false;
        int cnt;

        void setRunning(boolean b) {
            running = b;
            cnt = 9;
        }

        @Override
        public void run() {
            while (running) {
                try {

                    sleep(1000);
                    if (cnt == 8) {
                        playwelcome();
                    } else if (cnt == 5) {
                        setWelcome(1);
                    }
                    if (cnt-- == 0) {

                        running = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            handler.sendMessage(handler.obtainMessage());
        }
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            setProgressBarIndeterminateVisibility(false);
            // progressDialog.dismiss();

            boolean retry = true;
            while (retry) {
                try {
                    backgroundThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // call_alams();

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.CLUSTER_SP, 0);
            ApkType = settings.getString("clustorNo", "-1"); // #1

            if (clickedhome == 0) {

                    Intent intent = new Intent(getApplicationContext(),
                            ScheduleRedirect.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0, 0);

            }

        }

    };

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//
//            Log.i(TAG, "DROID KEYCODE_BACK clicked: NO ACTION");
//
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    private void check_schedule_for_skip_vitals() {

        String time = TestDate.getCurrentTime();

        Cursor cs_schedule = dbSchedule.selectSchedule_skip_status("",
                Constants.getdroidPatientid());
        cs_schedule.moveToFirst();
        if (cs_schedule.getCount() > 0) {
            db_measureType.delete();
            while (cs_schedule.isAfterLast() == false) {

                ClassMeasureType sensor1 = new ClassMeasureType();
                sensor1.setMeasuretypeId(Integer.parseInt(cs_schedule
                        .getString(4)));
                sensor1.setPatientId(Constants.getdroidPatientid());
                sensor1.setStatus(1);
                sensor1.setDate(time);
                db_measureType.insert(sensor1);
                cs_schedule.moveToNext();
            }
        }
        cs_schedule.close();
        dbSchedule.cursorSchedule.close();
        dbSchedule.close();

    }

    private void start_check_service() {
        try {
            if (!is_cCheckService_Running()) {
                Log.e(TAG, " starting  CheckService ");

                Intent reminder = new Intent();
                reminder.setClass(this, CheckService.class);
                startService(reminder);

            } else {

                Log.e(TAG, " Already   CheckService started ");
            }

        } catch (Exception e) {
            Log.e(TAG, "exception while starting or closing CheckService... ");
        }
    }

    private void start_feedback() {
        /*if (!isMyAdviceServiceRunning()) {

			Intent schedule = new Intent();
			schedule.setClass(Welcome_Activity.this, AdviceService.class);
			startService(schedule);

		} else {
			Log.i("Optum_Sierra", "advice Serice is already running");
		}*/
    }

    private boolean isMyAdviceServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.AdviceService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean is_cCheckService_Running() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.CheckService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

	@Override
	public void onBackPressed() {

	}
	/*
	 * private void call_alams1() { SharedPreferences flowsp =
	 * getSharedPreferences( CommonUtilities.USER_FLOW_SP,
	 * Context.MODE_PRIVATE); int val = flowsp.getInt("start_flow", 5); // #1
	 * 
	 * if (val == 1) { Log.i(TAG, "starting Regular alam---");
	 * 
	 * Regular_Alam_util.cancelAlarm(getApplicationContext());
	 * appClass.setRegular_Alarm_Triggered(false);
	 * appClass.setStart_Alarm_Count(0);
	 * Regular_Alam_util.startAlarm(getApplicationContext());// start // Regular
	 * // alarm // to // set // the // five // minutes // time // out
	 * appClass.setRegular_Alarm_Triggered(true); SharedPreferences.Editor
	 * editor = flowsp.edit(); editor.putInt("start_flow", 0);// for reloading 5
	 * minutes editor.commit();
	 * 
	 * SharedPreferences settings1 = getSharedPreferences(
	 * CommonUtilities.PREFS_NAME_date, 0); SharedPreferences.Editor
	 * editor_retake = settings1.edit(); editor_retake
	 * .putString("measure_time_skip", Util.get_server_time());
	 * editor_retake.putString("sectiondate",
	 * Util.get_patient_time_zone_time(Welcome_Activity.this));
	 * 
	 * editor_retake.commit();
	 * 
	 * } else { Log.i(TAG, "Cancel Regular alam---");
	 * 
	 * Regular_Alam_util.cancelAlarm(getApplicationContext());
	 * 
	 * } Log.i(TAG, "Util.get_server_time()---------------" +
	 * Util.get_server_time()); db_measureType.updateStatusforAll(1); }
	 */
}
