package com.optum.telehealth;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;

import com.optum.telehealth.dal.Flow_Status_db;
import com.optum.telehealth.service.CheckService;
import com.optum.telehealth.util.Alam_util;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Regular_Alam_util;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

@SuppressLint({"HandlerLeak", "InflateParams"})
public class FinalMainActivity extends Titlewindow {

    public static String PostUrl = "";
    public static int PatientIdDroid;
    public static String serialNo = "";
    public static String imeilNo = "";
    private String ApkType = "";
    private String TAG = "StartingPage";
    public int a = 0;
    public int flag_login = 0;
    BackgroundThread backgroundThread;
    GlobalClass appstate;
    Flow_Status_db Flow_db;
    public boolean reminder_flow = false;
    String posturl, token, patient_Id;
    private ProgressDialog progressDialog;

    @SuppressLint({"NewApi", "InflateParams"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_main);
        Log.e("FinalMainActivity", "ONCREATE");
        appstate = (GlobalClass) getApplicationContext();
        Log.e("TitleWindow", "IN #onCreate");
        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        appstate.serviceUrl = settings.getString("Server_post_url", "-1");
        appstate.countDownTimer.cancel();
        appstate.countDownTimer.start();
        appstate.isAlarmUtilStart = false;

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("FinalMainActivity", "onResume");
        String Flow;
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Flow = bundle.getString("Flow");//this is for Starting Reminder flow
                // Log.i(TAG, "Flow value = " + Flow);
                if (Flow.equals("true")) {
                    SharedPreferences sharedpreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("R_STATUS", 0);
                    editor.commit();

                    SharedPreferences reminderPreferences = getSharedPreferences("REMINDER_STATUS", Context.MODE_PRIVATE);
                    SharedPreferences.Editor reminderEditor = reminderPreferences.edit();
                    reminderEditor.putBoolean("REMINDER_STATUS", true);
                    reminderEditor.commit();
                    reminder_flow = true;
                    Log.i(TAG, "Reminder Flow");
                }
            }
        } catch (Exception e) {
            Log.i("AuthenticationActivity", "R_ID Exception:" + e);
        }

        SharedPreferences skipPreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
        int R_LOGOUT = skipPreferences.getInt("R_LOGOUT", -1);
        if (R_LOGOUT == 0) {
            String R_ID = Integer.toString(skipPreferences.getInt("R_ID", 0));
            Intent resultIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
            resultIntent.putExtra("R_ID", R_ID);
            startActivity(resultIntent);
        } else {

            Log.i("Optum_Sierra", "reminder Serice starting");
            Flow_db = new Flow_Status_db(FinalMainActivity.this);
            Flow_db.open();
            Flow_db.insert("1");

            Cursor cursor1 = Flow_db.selectAll();
            Status = new String[cursor1.getCount()];
            int j = 0;

            while (cursor1.moveToNext()) {
                Status[j] = cursor1.getString(cursor1.getColumnIndex("Status"));
                Status_Val = Status[0];
                j += 1;
            }

            Flow_db.close();

            SharedPreferences flow = getSharedPreferences(
                    CommonUtilities.USER_FLOW_SP, 0);
            SharedPreferences.Editor editor = flow.edit();
            editor.putInt("flow", 0);
            editor.putInt("reminder_flow", 0);
            editor.putInt("start_flow", 0); // for normal reloading
            editor.commit();

            start_check_service();
            try {
                Alam_util.cancelAlarm(FinalMainActivity.this);
                Regular_Alam_util.cancelAlarm(FinalMainActivity.this);
            } catch (Exception e) {
                Log.e(TAG, "exception while closing alam in final main act... ");
            }
            checkWiFi();
        }
    }

    private void checkWiFi() {
        try {
            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.WiFi_SP, 0);
            String android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
//            TelephonyManager tManager = (TelephonyManager) this
//                    .getSystemService(Context.TELEPHONY_SERVICE);
            imeilNo = android_id;
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("imei_no", imeilNo);

            serialNo = "0000000000000";
            editor.putString("wifi", serialNo);
            editor.commit();

            String macAddress = serialNo;
            if (macAddress.trim().length() > 4) {
                serialNo = macAddress.replace(":", "");
                editor.putString("wifi", serialNo);
                editor.commit();
            }
        } catch (Exception e) {
            Log.e(TAG, "checkWiFi wifi/imei number issue");
        }
        checkConfig();
    }

    public static void CancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx
                .getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Log.i(TAG, "DROID KEYCODE_BACK clicked on final main page:");

            /*//Intent intent = new Intent(this, FinalMainActivity.class);
            this.finish();
            //startActivity(intent);*/

        }
        return super.onKeyDown(keyCode, event);
    }

    private void checkConfig() {
        PostUrl = CommonUtilities.SERVER_URL;
        {
            SharedPreferences settingseifi = getSharedPreferences(
                    CommonUtilities.WiFi_SP, 0);
            serialNo = settingseifi.getString("wifi", "-1");
            imeilNo = settingseifi.getString("imei_no", "");

            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.SERVER_URL_SP, 0);
            String server1 = settings.getString("Server_url", "-1");
            String server2 = settings.getString("Server_port", "-1");
            posturl = settings.getString("Server_post_url", "-1");

            Constants.setIMEI_No(imeilNo);
            Constants.setParams(server1, server2, posturl, serialNo);
            PostUrl = posturl;

            Log.i(TAG, "Loading apk details...");
            SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
            token = tokenPreference.getString("Token_ID", "-1");
            patient_Id = tokenPreference.getString("patient_id", "-1");

            loadApkDetails();
        }
    }

    public void loadApkDetails() {
        SharedPreferences settings2 = getSharedPreferences(
                CommonUtilities.CLUSTER_SP, 0);
        ApkType = settings2.getString("clustorNo", "-1"); // #1

//        if (ApkType.equals("1000000")) // Sierra//
//        {
//            if (appstatus == 1 || !(title_apk.equals(ApkType))) {
//                Intent intent = new Intent(getApplicationContext(),
//                        FinalMainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(intent);
//                overridePendingTransition(0, 0);
//            }
//
//			/*Intent intent = new Intent(getApplicationContext(),
//                    MainActivity.class);
//			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//			startActivity(intent);
//			overridePendingTransition(0, 0);*/
//            if (reminder_flow) {
//                new AuthCheck().execute();
//            } else {
//                reminder_flow = false;
//                homeClick();
//            }
//        } else if (ApkType.equals("1000001")) // HFP//
//        {

            if (appstatus == 1 || !(title_apk.equals(ApkType))) {
                Intent intent = new Intent(getApplicationContext(),
                        FinalMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                overridePendingTransition(0, 0);

            }
            if (reminder_flow) {
                new AuthCheck().execute();
            } else {
                reminder_flow = false;
                homeClick();
            }
            /*Intent intent = new Intent(getApplicationContext(),
					HFPMainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			// finish();
			startActivity(intent);
			overridePendingTransition(0, 0);*/
//        } else {
//            if (!PostUrl.equals("-1"))
//                Log.i(TAG,
//                        "NOT ASSIGNED TO A PATIENT");
//        }
    }

    public void startReminder() {
        Log.e(TAG, "#In startReminder");
        SharedPreferences sharedpreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
        String measure_time_skip = sharedpreferences.getString("measure_time_skip", "-1");
        String sectiondate = sharedpreferences.getString("sectiondate", "-1");

        SharedPreferences sharedpref = getSharedPreferences(
                CommonUtilities.PREFS_NAME_date, 0);
        SharedPreferences.Editor editor_retake = sharedpref
                .edit();
        editor_retake.putString("measure_time_skip", measure_time_skip);
        editor_retake
                .putString("sectiondate", sectiondate);

        editor_retake.commit();

        Log.e(TAG, "1000001 reminder_flow   AlarmService start");
        appstate.setRegular_alarm_count(0);
        Log.e("startAlarm", "appstate->>" + appstate.getRegular_alarm_count());
        appstate.isAlarmUtilStart = true;
        Alam_util.startAlarm(FinalMainActivity.this);
        Flow_db.open();
        Flow_db.deleteAll();
        Flow_db.insert("0");
        Flow_db.close();
    }

    @Override
    public void onStop() {
        // Commit the edits!
        flag_login = 1;
        Log.i(TAG, "onStop on finalmain activity");
        super.onStop();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public class BackgroundThread extends Thread {
        volatile boolean running = false;
        int cnt;

        void setRunning(boolean b) {
            running = b;
            cnt = 5;
        }

        @Override
        public void run() {
            while (running) {
                try {
                    sleep(400);
                    if (cnt-- == 0) {
                        running = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            handler.sendMessage(handler.obtainMessage());
        }
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            boolean retry = true;
            while (retry) {
                try {
                    backgroundThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            checkWiFi();
        }

    };

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart starting page");

        Intent intent = new Intent(getApplicationContext(),
                FinalMainActivity.class);

        startActivity(intent);
        this.finish();

    }

    /*private boolean isMyReminderServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.AlarmService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }*/

    private boolean is_cCheckService_Running() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.CheckService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void start_check_service() {
        try {
            if (!is_cCheckService_Running()) {
                Log.e(TAG, " starting  CheckService ");

                Intent reminder = new Intent();
                reminder.setClass(this, CheckService.class);
                startService(reminder);

            } else {

                Log.e(TAG, " Already   CheckService started ");
            }

        } catch (Exception e) {
            Log.e(TAG, "exception while starting or closing CheckService... ");
        }
    }

    private class AuthCheck extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Log.i("onPreExecute", "onPreExecute MainActivity");
            try {
                progressDialog = new ProgressDialog(FinalMainActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progressDialog.dismiss();
                Log.e(TAG, "onPostExecute=>" + result);
                if (result.equals("Invalid AuthenticationID")) {
                    Intent intent = new Intent(getApplicationContext(),
                            AuthenticationActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    startReminder();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        protected String doInBackground(String... urls) {

            String response = "";

            try {
                Log.i(TAG, "Checking device registration MAC-" + serialNo
                        + " IMEI-" + imeilNo);
                HttpResponse response1 = Util.connect(posturl
                                + "/droid_website/mob_authentication_checking.ashx",
                        new String[]{"authentication_token", "patient_id"}, new String[]{
                                token, patient_Id});

                if (response1 == null) {
                    Log.e(TAG, "Connection Failed!");
                    return "";
                }
                String str = Util
                        .readStream(response1.getEntity().getContent());
                str = str.replaceAll("&", "and");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                str.toString();
                //Log.i("response:", "Authentication Response :" + str);
                if (str.length() < 10) {
                    Log.i(TAG, "Authentication Response null");
                    return "";
                }
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);
                NodeList nodes = doc.getElementsByTagName("optum");
                for (int i = 0; i < nodes.getLength(); i++) {
                    response = Util.getTagValue(nodes, i, "response");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Error Reminder downloading and saving  " + e);

            }
            return response;
        }
    }

    @Override
    public void onBackPressed() {
    }
}