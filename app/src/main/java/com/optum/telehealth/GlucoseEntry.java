package com.optum.telehealth;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassMeasurement;
import com.optum.telehealth.dal.Glucose_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class GlucoseEntry extends Titlewindow {

    int glucoseId;
    Dialog dialog;
    private static final String TAG = "Glucose Manual Entry Sierra";
    RadioButton Pre_Meal, Post_Meal;
    int Prompt_Flag = 3;
    private Button Glucose_Next;
    TextView glucoseMessage, glucoseText, glucose;
    private TextView txt_errorMsg;
    RelativeLayout glucoseParent;
    RadioGroup radioGroup1;
    EditText edtglucose;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glucose_entry);
        appClass.isSupportEnable = true;
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);

        try {
            Glucose_Next = (Button) findViewById(R.id.btn_bloodGlucoseNext);
            Glucose_Next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appClass.appTracking("Blood Glucose manual entry page","Click on Next button");

                    nextClick();
                }
            });

            glucoseMessage = (TextView) findViewById(R.id.glucose);
            glucose = (TextView) findViewById(R.id.glucosemessage);
            glucoseText = (TextView) findViewById(R.id.glucose);

            Pre_Meal = (RadioButton) findViewById(R.id.Pre_Meal);
            Post_Meal = (RadioButton) findViewById(R.id.Post_Meal);
            radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
            radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                    View radioButton = radioGroup1.findViewById(checkedId);
                    int index = radioGroup1.indexOfChild(radioButton);
                    switch (index) {
                        case 0: // first button
                            Pre_Meal.setTextColor(getResources().getColor(R.color.Mob_Orange_Dark));
                            Post_Meal.setTextColor(getResources().getColor(R.color.Mob_Gray6));
                            break;
                        case 1: // secondbutton
                            Post_Meal.setTextColor(getResources().getColor(R.color.Mob_Orange_Dark));
                            Pre_Meal.setTextColor(getResources().getColor(R.color.Mob_Gray6));
                            break;
                    }
                }
            });
            Typeface type1 = Typeface.createFromAsset(getAssets(),
                    "fonts/FrutigerLTStd-Roman.otf");
            glucoseMessage.setTypeface(type1, Typeface.NORMAL);
            glucoseText.setTypeface(type1, Typeface.NORMAL);
            glucose.setTypeface(type1, Typeface.NORMAL);
            Pre_Meal.setTypeface(type1, Typeface.NORMAL);
            Post_Meal.setTypeface(type1, Typeface.NORMAL);
            glucoseParent = (RelativeLayout) findViewById(R.id.glucoseParent);
            edtglucose = (EditText) findViewById(R.id.glucosevalue);
            edtglucose.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    GradientDrawable border = new GradientDrawable();
                    border.setColor(0xFFFFFFFF); //white background
                    int dp = 2;
                    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
                    if (hasFocus) {
                        border.setStroke(px, getResources().getColor(R.color.Mob_Orange_Dark));
                    } else {
                        border.setStroke(px, getResources().getColor(R.color.Mob_EditText_Border));
                    }
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        glucoseParent.setBackgroundDrawable(border);
                    } else {
                        glucoseParent.setBackground(border);
                    }
                }
            });
        } catch (Exception e) {
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            nextClick();
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            this.finish();
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStop() {
        //Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
        super.onStop();
    }

    private void nextClick() {
        // switch (msg.what) {
        try {
            String s_val = edtglucose.getText().toString();
            double d_val  = 0;
            if(!s_val.equals("") && !s_val.equals(".")) {
                d_val = Double.parseDouble(s_val); // Make use of autoboxing.  It's also easier to read.
            }
            Glucose_db dbcreateglucose = new Glucose_db(GlucoseEntry.this);
            if ((edtglucose.getText().toString()).length() >= 1
                    && !(edtglucose.getText().toString()).contains(".")
                    && d_val != 0) {

                String Patientid = Constants.getdroidPatientid();
                ClassMeasurement glucose = new ClassMeasurement();

                SharedPreferences flow = getSharedPreferences(
                        CommonUtilities.USER_FLOW_SP, 0);
                int val = flow.getInt("flow", 0); // #1
                if (val == 1) {

                    SharedPreferences section = getSharedPreferences(
                            CommonUtilities.PREFS_NAME_date, 0);
                    SharedPreferences.Editor editor_retake = section.edit();
                    editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(GlucoseEntry.this));
                    editor_retake.commit();
                }


                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "MM/dd/yyyy hh:mm:ss aa", Locale.US);
                String currentDateandTime = dateFormat.format(new Date());

                if (Pre_Meal.isChecked()) {
                    Prompt_Flag = 1;
                }
                if (Post_Meal.isChecked()) {
                    Prompt_Flag = 2;
                }

                glucose.setLast_Update_Date(currentDateandTime);
                glucose.setStatus(0);
                glucose.setGlucose_Value(Integer.parseInt(edtglucose.getText()
                        .toString()));
                glucose.setInputmode(1);
                SharedPreferences settings1 = getSharedPreferences(
                        CommonUtilities.PREFS_NAME_date, 0);
                String sectiondate = settings1.getString("sectiondate", "0");

                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.USER_TIMESLOT_SP, 0);
                String slot = settings.getString("timeslot", "AM");

                glucose.setSectionDate(sectiondate);
                glucose.setTimeslot(slot);
                glucose.setPrompt_flag(Prompt_Flag + "");
                glucoseId = dbcreateglucose.InsertGlucoseMeasurement(glucose);

                Intent intent = new Intent(getApplicationContext(),
                        ShowGlucoseActivity.class);
                intent.putExtra("text", edtglucose.getText().toString());
                intent.putExtra("glucoseid", glucoseId);
                edtglucose.setText("");
                startActivity(intent);
                finish();
                Util.stopPlaySingle();
                overridePendingTransition(0, 0);
            } else {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String GlucoseValidationErrorMessage  = ERROR_MSG.getString("GlucoseValidationErrorMessage", "-1");
                errorShow(GlucoseValidationErrorMessage);
                return;
            }
        } catch (Exception e) {
            // p did not contain a valid double
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        Glucose_Next.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        Glucose_Next.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
