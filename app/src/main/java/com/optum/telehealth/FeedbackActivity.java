package com.optum.telehealth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class FeedbackActivity extends Titlewindow implements View.OnClickListener {

    private GlobalClass globalClass;
    private TextView txt_feedbackLength;
    private EditText edt_feedback;
    private Button btn_sendFeedback;
    private Button btn_backFeedback;
    private String feedback;
    private String serviceUrl;
    private TextView txt_errorMsg;
    private String TAG = "FeedbackActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = settings.getString("Server_post_url", "-1");
        globalClass = (GlobalClass) getApplicationContext();
        globalClass.isEllipsisEnable = true;
        globalClass.isSupportEnable = true;
        txt_feedbackLength = (TextView) findViewById(R.id.txt_feedbackLength);
        edt_feedback = (EditText) findViewById(R.id.edt_feedback);
//        String s = null;
//        String p = Integer.toString(Integer.parseInt(s));
        btn_backFeedback = (Button) findViewById(R.id.btn_backFeedback);
        btn_sendFeedback = (Button) findViewById(R.id.btn_sendFeedback);
        btn_sendFeedback.setOnClickListener(this);
        btn_backFeedback.setOnClickListener(this);
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        txt_feedbackLength.setText(Html.fromHtml(getString(R.string.feedbackCharLength)));
        edt_feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("feedback", s.toString());
                if (s.toString().equals("")) {
                    btn_sendFeedback.setEnabled(false);
                    if (getResources().getBoolean(R.bool.isTab)) {
                        btn_sendFeedback.setBackground(getResources().getDrawable(R.drawable.send_disable_tab));
                    } else {

                        btn_sendFeedback.setBackground(getResources().getDrawable(R.drawable.send_disable));
                    }

                } else if (s.toString().equals(" ")) {
                    btn_sendFeedback.setEnabled(false);
                    edt_feedback.setText("");
                    if (getResources().getBoolean(R.bool.isTab)) {
                        btn_sendFeedback.setBackground(getResources().getDrawable(R.drawable.send_disable_tab));
                    } else {

                        btn_sendFeedback.setBackground(getResources().getDrawable(R.drawable.send_disable));
                    }

                } else {
                    btn_sendFeedback.setEnabled(true);
                    if (getResources().getBoolean(R.bool.isTab)) {
                        btn_sendFeedback.setBackground(getResources().getDrawable(R.drawable.orange));
                    } else {
                        btn_sendFeedback.setBackground(getResources().getDrawable(R.drawable.button_orange));
                    }
                }
                String feedbackLength = Integer.toString(1000 - edt_feedback.getText().length());
                txt_feedbackLength.setText(Html.fromHtml("<b>" + feedbackLength + "</b> " + getResources().getString(R.string.feedbackCharLengthIn)));
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_sendFeedback:
                globalClass.appTracking("Feedback Page","Click on Send button");
                sendFeedback();
                break;
            case R.id.btn_backFeedback:
                globalClass.appTracking("Feedback Page","Click on Back button");
                Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }

    private void sendFeedback() {
        feedback = edt_feedback.getText().toString().trim();
        if (globalClass.isConnectingToInternet(this)) {
            UploadFeedback uploadFeedback = new UploadFeedback();
            uploadFeedback.execute(new String[]{serviceUrl + "/droid_website/mob_add_feedback.ashx"});
        } else {
            SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
            String NoConnectionError = ERROR_MSG.getString("NoConnectionError", "-1");
            errorShow(NoConnectionError);
        }
    }

    private String getDeviceType() {

        return Build.MODEL;
    }

    private String getDeviceOSVersion() {

        return Build.VERSION.RELEASE;
    }


    private class UploadFeedback extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {

            Log.i("onPreExecute", "onPreExecute Authentication");
            try {
                progressDialog = new ProgressDialog(FeedbackActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            //Log.i("onPostExecute", result);
            progressDialog.dismiss();
            if (result.equalsIgnoreCase("success")) {
                errorShow(getResources().getString(R.string.feedbackSuccessfull));
                edt_feedback.setText("");
                btn_sendFeedback.setEnabled(false);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getBaseContext(), Home.class);
                        startActivity(intent);
                        finish();
                    }
                }, 3300);




            } else if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            } else {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_feedback_failed = ERROR_MSG.getString("msg_feedback_failed", "-1");
                errorShow(msg_feedback_failed);
            }


        }

        protected String doInBackground(String... urls) {

            Log.i("doInBackground", "doInBackground");
            String response = "";
            try {
                SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
                String token = tokenPreference.getString("Token_ID", "-1");
                String patientId = tokenPreference.getString("patient_id", "-1");

                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/mob_add_feedback.ashx", new String[]{
                                "authentication_token", "patient_id", "device_type", "android_version",
                                "app_version", "feedback_message", "submitted_date"},
                        new String[]{token, patientId, getDeviceType(), getDeviceOSVersion(),
                                getResources().getString(R.string.version), feedback.replace("\n", "~"), TestDate.getCurrentTime()});

                if (response1 == null) {
                    Log.e("Connection Failed", "Connection Failed!");
                    return "Connection Failed!"; // process
                }

                String str = Util.readStream(response1.getEntity().getContent());
                if (str.trim().length() == 0) {
                    Log.i("No aPK details",
                            "No aPK details for the current serial number from server");
                    return "Server Failed";
                }

                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");
                // Log.i("response:", "feedback_response:" + str.toString());
                DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);

                NodeList nodes = doc.getElementsByTagName("optum");
                for (int i = 0; i < nodes.getLength(); i++) {

                    response = Util.getTagValue(nodes, i, "response");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        //btn_sendFeedback.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        //btn_sendFeedback.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            finish();
//
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
