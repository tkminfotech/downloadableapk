package com.optum.telehealth;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RequestPasswordActivity extends Activity implements View.OnClickListener {
    private Button btn_back, btn_reset;
    private TextView lost_password_desc1, lost_password_desc2;
    String TAG = "RequestPasswordActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_password);

        lost_password_desc1 = (TextView) findViewById(R.id.lost_password_desc1);
        lost_password_desc2 = (TextView) findViewById(R.id.lost_password_desc2);
        lost_password_desc2.setOnClickListener(this);
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_reset = (Button) findViewById(R.id.btn_resetPassword);
        btn_reset.setOnClickListener(this);
        try {
            SharedPreferences settings = getSharedPreferences("ERROR_MSG", 0);

            String message_txt1 = settings.getString("forgot_password_msg1", "");
            String message_txt2 = settings.getString("forgot_password_msg2", "");
            String message_txt3 = settings.getString("forgot_password_msg3", "");
            String message_txt4 = getResources().getString(R.string.resetAt);
            //lost_password_desc1.setText(Html.fromHtml(message_txt1 + "<b> " + message_txt2 + "</b>" + " " + message_txt4 + ":"));
            lost_password_desc2.setText(Html.fromHtml("<u>" + message_txt3 + "</u>"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RequestPasswordActivity.this, AuthenticationActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_resetPassword:
                redirectToRestPassword();
                break;
            case R.id.lost_password_desc2:
                callSupportNumber();
                break;

        }
    }

    private void redirectToRestPassword() {

        Intent i = new Intent(RequestPasswordActivity.this, ResetPasswordActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        finish();
    }

    private void callSupportNumber() {

//        PackageManager pm = this.getPackageManager();
//        if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
//            System.out.println("horray");
//        } else {
//            System.out.println("nope");
//        }
        try {
            String phone_no = lost_password_desc2.getText().toString().trim().replaceAll("-", "");

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phone_no));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            Log.i("ActivityNotFoundExcept", e.getMessage());
        }
    }
    @Override
    public void onBackPressed() {
        //NO ACTION
    }
}
