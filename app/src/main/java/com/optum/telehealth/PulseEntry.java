package com.optum.telehealth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassMeasurement;
import com.optum.telehealth.dal.Pulse_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class PulseEntry extends Titlewindow{

    Pulse_db dbcreate1 = new Pulse_db(this);
    private int pulseId;
    private TextView oxygenText, percent, pulseMessage;
    private String TAG = "PulseEntry";
    private Button Pulse_Next;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            nextClick();
        }
    };
    private TextView txt_errorMsg;
    private EditText edtoxy;
    private RelativeLayout oxygenParent;
    private boolean isOxygenFocus = false;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulse_entry);
        appClass.isSupportEnable = true;
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        Pulse_Next = (Button) findViewById(R.id.btn_pulseNext);
        oxygenText = (TextView) findViewById(R.id.oxygentext);
        percent = (TextView) findViewById(R.id.percent);
        pulseMessage = (TextView) findViewById(R.id.pulsemessage);
        Typeface type1 = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        oxygenText.setTypeface(type1, Typeface.NORMAL);
        Typeface type2 = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Light.otf");
        percent.setTypeface(type2, Typeface.NORMAL);
        pulseMessage.setTypeface(type1, Typeface.NORMAL);
        oxygenParent = (RelativeLayout) findViewById(R.id.oxygenParent);
        edtoxy = (EditText) findViewById(R.id.oxygenvalue);
        edtoxy.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                GradientDrawable border = new GradientDrawable();
                border.setColor(0xFFFFFFFF); //white background
                int dp = 2;
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
                if (hasFocus) {
                    isOxygenFocus = true;
                    border.setStroke(px, getResources().getColor(R.color.Mob_Orange_Dark));
                } else {
                    border.setStroke(px, getResources().getColor(R.color.Mob_EditText_Border));
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    oxygenParent.setBackgroundDrawable(border);
                } else {
                    oxygenParent.setBackground(border);
                }
            }
        });

        Pulse_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appClass.appTracking("Blood Oxygen manual entry page","Click on Next button");

                if(isOxygenFocus){
                    nextClick();
                }else{
                    nextClick();
                }

            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            this.finish();
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStop() {
        //Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
        super.onStop();
    }

    private void nextClick() {
        try {
            String s_val = edtoxy.getText().toString();
            double d_val = 0;
            if (!s_val.equals("") && !s_val.equals(".")) {
                d_val = Double.parseDouble(s_val); // Make use of autoboxing.  It's also easier to read.
            }
            Log.i(TAG, "VALUE:" + d_val);

            if ((edtoxy.getText().toString()).length() >= 1
                    && !(edtoxy.getText().toString()).contains(".")
                    && !(edtoxy.getText().toString().length() > 3)
                    && d_val != 0 ) {
                SharedPreferences flow = getSharedPreferences(
                        CommonUtilities.USER_FLOW_SP, 0);
                int val = flow.getInt("flow", 0); // #1
                if (val == 1) {
                    SharedPreferences section = getSharedPreferences(
                            CommonUtilities.PREFS_NAME_date, 0);
                    SharedPreferences.Editor editor_retake = section.edit();
                    editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(PulseEntry.this));
                    editor_retake.commit();
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "MM/dd/yyyy hh:mm:ss aa", Locale.US);
                String currentDateandTime = dateFormat.format(new Date());
                ClassMeasurement measurement = new ClassMeasurement();

                measurement.setStatus(0);
                measurement.setOxygen_Value(Integer.parseInt(edtoxy.getText()
                        .toString()));
                measurement.setLast_Update_Date(currentDateandTime);
                measurement.setPressure_Value(Integer.parseInt("00"));
                measurement.setInputmode(1);
                SharedPreferences settings1 = getSharedPreferences(
                        CommonUtilities.PREFS_NAME_date, 0);
                String sectiondate = settings1.getString("sectiondate", "0");

                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.USER_TIMESLOT_SP, 0);
                String slot = settings.getString("timeslot", "AM");

                measurement.setSectionDate(sectiondate);
                measurement.setTimeslot(slot);

                pulseId = dbcreate1.InsertOxymeterMeasurement(measurement);
                Log.e(TAG, "Saving pulse value to db");
                // dbcreate1.InsertOxymeterMeasurement(100,classOxy.getOxygen(),
                // currentDateandTime, 0,classOxy.getOxyPulse());

                // PresureActivity();

				/*
                 * Toast.makeText(getApplicationContext(), "Saved Successfully",
				 * Toast.LENGTH_SHORT).show();
				 */

                Intent intent = new Intent(PulseEntry.this,
                        ShowPulseActivity.class);
                intent.putExtra("pulse", edtoxy.getText().toString());
                intent.putExtra("pulseid", pulseId);
                edtoxy.setText("");
                Log.e(TAG, "Redirecting to show pulse page");
                startActivity(intent);
                finish();
                Util.stopPlaySingle();
                overridePendingTransition(0, 0);
            } else {
                Log.e(TAG, "invalid pulse entry by user");
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String PulseValidationErrorMessage  = ERROR_MSG.getString("OxygenValidationErrorMessage", "-1");
                errorShow(PulseValidationErrorMessage);
                return;
            }
            // insert function
        } catch (Exception e) {
            // p did not contain a valid double
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        Pulse_Next.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        Pulse_Next.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
