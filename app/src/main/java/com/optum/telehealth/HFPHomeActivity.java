package com.optum.telehealth;

import android.os.Bundle;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

/**
 * Created on 4/26/2016.
 * JUST A VERSION INFO ACTIVITY FOR OTH APK
 */
public class HFPHomeActivity extends Titlewindow {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hfp_home);
        Button btn_hfp_session = (Button) findViewById(R.id.btn_hfp_session);
        btn_hfp_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSession();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i("HFPHomeActivity", "DROID KEYCODE_BACK clicked on HOME page:");
//            this.finishAffinity();
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
