package com.optum.telehealth;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ReminderCallActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_calling);
        Intent resultIntent = new Intent(getApplicationContext(),
                FinalMainActivity.class);
        //Toast.makeText(getApplicationContext(),"notification happend",Toast.LENGTH_SHORT).show();
        resultIntent.putExtra("Flow", "true");
        //resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(resultIntent);
        finish();
    }
}
