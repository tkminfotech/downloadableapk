package com.optum.telehealth;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.optum.telehealth.dal.Questions_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.ConnectionDetector;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class StartQuestion extends Titlewindow {

    private String serviceUrl = "";
    private String serialnumber = "";
    public static String imeilNo = "";
    Questions_db dbcreate1 = new Questions_db(this);
    private String TAG = "StartQuestion Sierra";
    TextView textview, username;
    String ApkType1;
    private int click = 0;
    // private boolean isBackClick = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "Start question page");
        setContentView(R.layout.activity_start_question);

    }

    @Override
    public void onResume() {
        super.onResume();
        click = 0;
        // isBackClick = false;
        try {
            Typeface type = Typeface.createFromAsset(getAssets(),
                    "fonts/FrutigerLTStd-Roman.otf");

            int a = Constants.getquestionPresent();

            username = (TextView) findViewById(R.id.txtusername1);
            textview = (TextView) findViewById(R.id.txtwelcomestart);
            textview.setTypeface(type, Typeface.NORMAL);

            if (a == 1) {
                textview.setText(this.getString(R.string.welcome_question));
                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.USER_SP, 0);
                String patientname = settings.getString("patient_name", "-1"); // #1

                if (patientname.trim().length() >= 2) {

                    username.setText(this.getString(R.string.hello)
                            + " " + patientname + ".");
                }
                Typeface type1 = Typeface.createFromAsset(getAssets(),
                        "fonts/FrutigerLTStd-Roman.otf");
                username.setTypeface(type1, Typeface.BOLD);
                textview.setTypeface(type1, Typeface.NORMAL);

            } else {

                Typeface type1 = Typeface.createFromAsset(getAssets(),
                        "fonts/FrutigerLTStd-Roman.otf");

                username.setVisibility(View.INVISIBLE);
                textview.setText(this.getString(R.string.askquestion));

                textview.setTextColor(getResources().getColor(
                        R.color.optumorange));
                textview.setTypeface(type1, Typeface.NORMAL);
            }
            SharedPreferences settings1 = getSharedPreferences(
                    CommonUtilities.CLUSTER_SP, 0);
            ApkType1 = settings1.getString("clustorNo", "-1"); // #1

            Boolean isInternetPresent = (new ConnectionDetector(
                    getApplicationContext())).isConnectingToInternet();
            /*if (isInternetPresent) {
                DownloadQuestionFromServer();
            } else {*/
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (click == 0) {
                        if (clickedhome == 0) {
                            Log.e(TAG, "NO INTERNET");
                            Intent intent = new Intent(StartQuestion.this,
                                    QuestionView.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(0, 0);
                        }
                    }
                }
            }, 2500);
            // }
            /*
             * if (!isMyAdviceServiceRunning()) { Intent schedule = new
			 * Intent(); schedule.setClass(getApplicationContext(),
			 * AdviceService.class); startService(schedule); // Starting Advice
			 * Service }
			 */
        } catch (Exception e) {
            // Log.e("Droid Error",e.getMessage());
            Log.i(TAG, " Exception start question  on create ");
        }
        Constants.set_question_id("0");
    }

    private boolean isMyAdviceServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.AdviceService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//            //isBackClick = true;
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            this.finish();
//            /*Intent intent = new Intent(context, StartQuestion.class);
//            startActivity(intent);*/
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    /***************************************
     * DownloadQuestionFromServer start
     ********************************************/

    public void DownloadQuestionFromServer() {

        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = settings.getString("Server_post_url", "-1");
        serialnumber = Constants.getSerialNo();// m
        imeilNo = Constants.getIMEI_No();
        Log.i(TAG, "DownloadQuestionFromServer started ");
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(new String[]{serviceUrl
                + "/droid_website/mob_patient_questions.ashx"});
    }

    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {

        protected String getTagValue(NodeList nodes, int i, String tag) {
            return getCharacterDataFromElement((org.w3c.dom.Element) (((org.w3c.dom.Element) nodes
                    .item(i)).getElementsByTagName(tag)).item(0));

        }

		/*
         * final ProgressDialog dialog = ProgressDialog.show(StartQuestion.this,
		 * "", "Loading Questions. Please wait...", true);
		 */

        @Override
        protected void onPreExecute() {

            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "onPostExecute");
            if (result.equalsIgnoreCase("AuthenticationError")) {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String msg_authentication_error  = ERROR_MSG.getString("msg_authentication_error_1", "-1");
                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
                logoutClick();
            }
            try {
                Thread.sleep(2600);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (click == 0) {
                if (result.trim().length() > 500) {

                    Intent intent = new Intent(StartQuestion.this,
                            QuestionView.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0, 0);


                } else {
                    Boolean isInternetPresent = (new ConnectionDetector(
                            getApplicationContext())).isConnectingToInternet();
                    if (isInternetPresent) {
                        Log.e(TAG, "INTERNET");

                        Toast.makeText(StartQuestion.this, R.string.noquestion,
                                Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(StartQuestion.this,
                                Questions.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);

                    } else {
                        Log.e(TAG, "NO INTERNET");

                        Intent intent = new Intent(StartQuestion.this,
                                QuestionView.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);

                    }
                }
            }
        }

        protected HttpResponse connect(String url, String[] header,
                                       String[] value) {
            try {
                InputStream content = null;
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                post.setEntity(new UrlEncodedFormEntity(pairs));
                for (int i = 0; i < header.length; i++) {
                    post.setHeader(header[i], value[i]);
                }
                return client.execute(post);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
            return null;
        }

        public Boolean preference() {
            Boolean flag = null;
            try {

                String PREFS_NAME = "DroidPrefSc";
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                int scheduletatus = settings.getInt("Scheduletatus", -1);
                Log.i("Droid", " SharedPreferences : "
                        + getApplicationContext() + " is : " + scheduletatus);
                if (scheduletatus == 1)
                    flag = true;
                else
                    flag = false;
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            return flag;
        }

        protected String readStream(InputStream in) {
            try {

                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));
                StringBuilder str = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    str.append(line + "\n");
                }

                in.close();

                return str.toString();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            return "";
        }

        protected String doInBackground(String... urls) {
            String response = "";
            SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
            String token = tokenPreference.getString("Token_ID", "-1");
            String patientId = tokenPreference.getString("patient_id", "-1");
            SharedPreferences settings = getSharedPreferences("DroidPrefSc", 0);
            String language;
            if (getResources().getBoolean(R.bool.isSpanish)) {
                language = "SPANISH";
            } else {
                language = "ENGLISH";
            }

            String scheduletatus = settings.getString("Scheduleid", "AA");
            //int patientId = Constants.getdroidPatientid();
            // Log.i(TAG, " Scheduleid"+scheduletatus+"");
            try {
                HttpResponse response1 = null;

                if (ApkType1.equals("1000001")) {
                    Log.i(TAG, "hfp question url serverurl" + serviceUrl
                            + "/droid_website/mob_patient_questions.ashx");
                    response1 = connect(serviceUrl
                                    + "/droid_website/mob_patient_questions.ashx",
                            new String[]{"authentication_token", "mode", "patient_id", "language"},
                            new String[]{token, "0", patientId, language});

                } else {
                    if (preference()) {

                        Log.i(TAG, "download question with Scheduleid"
                                + scheduletatus + "");

                        Log.i(TAG,
                                "Updating serverurl"
                                        + serviceUrl
                                        + "/droid_website/mob_patient_schedule_question.ashx");
                        response1 = connect(
                                serviceUrl
                                        + "/droid_website/mob_patient_schedule_question.ashx",
                                new String[]{"authentication_token", "schedule_id", "patient_id", "language"},
                                new String[]{token, scheduletatus, patientId, language});
                    } else {
                        Log.i(TAG, "Updating serverurl" + serviceUrl
                                + "/droid_website/mob_patient_questions.ashx");
                        response1 = connect(
                                serviceUrl
                                        + "/droid_website/mob_patient_questions.ashx",
                                new String[]{"authentication_token", "mode", "patient_id", "language"},
                                new String[]{token, "0", patientId, language});
                    }
                }
                int Statuscode = response1.getStatusLine().getStatusCode(); //
                // checking for 200 success code

                String str = readStream(response1.getEntity().getContent());
                System.out.println(str);
                response = str;
                if (Statuscode == 200) {
                    Log.i(TAG, "Question status 200 from the server");

                    dbcreate1.Cleartable();
                }

                if (str.trim().length() == 0) {
                    Log.i(TAG, "No Question from the server");
                    return "";
                }
                Log.i(TAG, "Question from the server downloaded");
                str = str.replaceAll("&", "and");
                // str.replaceAll(" & ", " and ");
                str = str.replaceAll("\r\n", "");
                str = str.replaceAll("\n", "");
                str = str.replaceAll("\r", "");

                // str=str.replaceAll("\\s","");
                Log.i("response:", "patientQuestion_response:" + str.toString());
                DocumentBuilder db = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(str.toString()));
                Document doc = db.parse(is);

                String patient_id = "";
                String question_id = "";
                String pattern_name = "";
                String top_node_number = "";
                String node_number = "";
                String sequence_number = "";
                String item_number = "";
                String child_node_number = "";
                String guidance = "";
                String sequence_number1 = "";
                String explanation = "";
                String kind = "";
                String sequence_number2 = "";
                String item_number1 = "";
                String item_content = "";


                NodeList AF_Nodes = doc.getElementsByTagName("whole_tree");
                for (int i = 0; i < AF_Nodes.getLength(); i++) {

                    if (Util.getTagValue(AF_Nodes, i, "mob_response").equalsIgnoreCase("AuthenticationError")) {

                        return "AuthenticationError";
                    }
                }

                NodeList nodes = doc.getElementsByTagName("tree");
                for (int i = 0; i < nodes.getLength(); i++) {
                    patient_id = getTagValue(nodes, i, "patient_id");
                    question_id = getTagValue(nodes, i, "question_id");
                    pattern_name = getTagValue(nodes, i, "pattern_name");
                    top_node_number = getTagValue(nodes, i, "top_node_number");
                    dbcreate1.InsertDmp(Integer.parseInt(patient_id),
                            Integer.parseInt(question_id), pattern_name,
                            Integer.parseInt(top_node_number));
                }
                NodeList branch_tree = doc.getElementsByTagName("branch_tree");

                for (int i = 0; i < branch_tree.getLength(); i++) {
                    // b = getTagValue(nodes, i, "question_id");
                    // Node item=branch_tree.item(i) ;
                    NodeList branch = doc.getElementsByTagName("branch");

                    for (int j = 0; j < branch.getLength(); j++) {

                        node_number = getTagValue(branch, j, "node_number");
                        sequence_number = getTagValue(branch, j,
                                "sequence_number");
                        item_number = getTagValue(branch, j, "item_number");
                        child_node_number = getTagValue(branch, j,
                                "child_node_number");

                        if (item_number.equals("")) {
                            item_number = "0";
                        }

                        dbcreate1.InsertDmpBranching(
                                Integer.parseInt(patient_id), node_number,
                                child_node_number,
                                Integer.parseInt(sequence_number),
                                Integer.parseInt(item_number), "",
                                Integer.parseInt(question_id));
                        // dbcreate1.InsertDmpBranching(Patient_Code,
                        // Node_Number, Child_Node_Number, Sequence_Number,
                        // Item_Number, Last_Update_Date, Question_Id)
                    }
                    Constants.setPatientid(patient_id);

                }

                NodeList interview = doc.getElementsByTagName("interview");
                for (int i = 0; i < interview.getLength(); i++) {
                    // b = getTagValue(nodes, i, "question_id");
                    // Node item=branch_tree.item(i) ;
                    NodeList interview_content = doc
                            .getElementsByTagName("interview_content");

                    for (int j = 0; j < interview_content.getLength(); j++) {

                        sequence_number1 = getTagValue(interview_content, j,
                                "sequence_number");
                        guidance = getTagValue(interview_content, j, "guidance");
                        explanation = getTagValue(interview_content, j,
                                "explanation");
                        kind = getTagValue(interview_content, j, "kind");

                        dbcreate1.InsertDmpQuestion(
                                Integer.parseInt(sequence_number1),
                                explanation, guidance, Integer.parseInt(kind),
                                "");

                    }

                }

                NodeList interview_response = doc
                        .getElementsByTagName("interview_response");
                for (int i = 0; i < interview_response.getLength(); i++) {
                    // b = getTagValue(nodes, i, "question_id");
                    // Node item=branch_tree.item(i) ;
                    NodeList answer = doc.getElementsByTagName("response");

                    for (int j = 0; j < answer.getLength(); j++) {

                        sequence_number2 = getTagValue(answer, j,
                                "sequence_number");
                        item_number1 = getTagValue(answer, j, "item_number");
                        item_content = getTagValue(answer, j, "item_content");
                        dbcreate1.InsertDmpAnswers(
                                Integer.parseInt(sequence_number2),
                                Integer.parseInt(item_number1), item_content);

                    }

                }

            } catch (Exception e) {
                // e.printStackTrace();
            }

            return response;
        }

    }

    /***************************************
     * DownloadQuestionFromServer end
     ********************************************/

    public String getCharacterDataFromElement(org.w3c.dom.Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    @Override
    public void onStop() {
        //Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
        click = 1;
        super.onStop();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {

    }
}
