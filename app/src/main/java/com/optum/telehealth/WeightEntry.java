package com.optum.telehealth;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassWeight;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.dal.Weight_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

import java.util.Timer;
import java.util.TimerTask;

public class WeightEntry extends Titlewindow {

    private static String TAG = "WeightEntry";
    int measureId;
    TextView weightText, weightUnit, weightMessage;
    Sensor_db dbSensor = new Sensor_db(this);
    Dialog dialog;
    int unitval = 0;
    private Button W_Next;
    //RelativeLayout error_notify;
    TextView error_display;
    EditText edtpulse;
    RelativeLayout weightParent;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_entry);
        error_display = (TextView) findViewById(R.id.error_display);
        edtpulse = (EditText) findViewById(R.id.weightvalue);
        weightParent = (RelativeLayout) findViewById(R.id.weightParent);
        appClass.isSupportEnable = true;
        W_Next = (Button) findViewById(R.id.btn_weightNext);
        W_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appClass.appTracking("Weight manual entry page","Click on Next button");
                nextClick();
            }
        });
        weightText = (TextView) findViewById(R.id.weight);
        weightMessage = (TextView) findViewById(R.id.weightmessage);
        weightUnit = (TextView) findViewById(R.id.weightunit);
        try {
            String unit = dbSensor.measureunitName();
            if (unit.contains("Kilogram")) {
                weightUnit.setText(" kg");
                unitval = 1;
            }

            Typeface type1 = Typeface.createFromAsset(getAssets(),
                    "fonts/FrutigerLTStd-Roman.otf");
            weightText.setTypeface(type1, Typeface.NORMAL);
            weightMessage.setTypeface(type1, Typeface.NORMAL);
            Typeface type2 = Typeface.createFromAsset(getAssets(),
                    "fonts/FrutigerLTStd-Light.otf");
            weightUnit.setTypeface(type2, Typeface.NORMAL);

            SharedPreferences settings1 = getSharedPreferences(
                    CommonUtilities.CLUSTER_SP, 0);
            String ApkType1 = settings1.getString("clustorNo", "-1"); // #1

			

				/*if (!isMyAdviceServiceRunning()) {
                    Intent schedule = new Intent();
					schedule.setClass(getApplicationContext(),
							AdviceService.class);
					startService(schedule); // Starting Advice Service
				}*/

            edtpulse.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    GradientDrawable border = new GradientDrawable();
                    border.setColor(0xFFFFFFFF); //white background
                    int dp =  2;
                    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
                    if(hasFocus){
                        border.setStroke(px, getResources().getColor(R.color.Mob_Orange_Dark));
                    }else{
                        border.setStroke(px, getResources().getColor(R.color.Mob_EditText_Border));
                    }
                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        weightParent.setBackgroundDrawable(border);
                    } else {
                        weightParent.setBackground(border);
                    }
                }
            });

            Log.i(TAG,
                    "-------------------oncreate--------------  weight entry  activity");
        } catch (Exception e) {
            // Log.e("Droid Error",e.getMessage());
            Log.i(TAG, " Exception  wt entry on create ");
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            finish();
//
//        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean isMyAdviceServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if ("com.optum.telehealth.service.AdviceService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onStop() {

        Log.i(TAG,
                "-------------------ONSTOP--------------  weight entry  activity");

        super.onStop();
    }

    public void nextClick() {
        try {
            String s_val = edtpulse.getText().toString();
            double d_val  = 0;
            if(!s_val.equals("") && !s_val.equals(".")) {
                d_val = Double.parseDouble(s_val); // Make use of autoboxing.  It's also easier to read.
            }
            Log.i(TAG, "VALUE:"+ d_val);
            // switch (msg.what) {
            Weight_db dbcreateweight = new Weight_db(WeightEntry.this);
            if (!(edtpulse.getText().toString()).equals(".")
                    && (edtpulse.getText().toString()).length() >= 1
                    && !(Util.round(Double.parseDouble(edtpulse.getText()
                    .toString())) > 999) && d_val != 0) {
                // tts.speak("your weight is "+edtpulse.getText().toString()
                // +"pounds", TextToSpeech.QUEUE_FLUSH, null);
                String Patientid = Constants.getdroidPatientid();
                ClassWeight weight = new ClassWeight();
                weight.setPatientID(Patientid);

                double weightKG = 0.0;

                if (unitval == 0) {

                    weightKG = Util.round(Double.parseDouble(edtpulse.getText()
                            .toString()));
                } else {
                    weightKG = Util.round(Double.parseDouble(edtpulse.getText()
                            .toString()) * 2.204);
                }
                SharedPreferences flow = getSharedPreferences(
                        CommonUtilities.USER_FLOW_SP, 0);
                int val = flow.getInt("flow", 0); // #1
                if (val == 1) {
                    SharedPreferences section = getSharedPreferences(
                            CommonUtilities.PREFS_NAME_date, 0);
                    SharedPreferences.Editor editor_retake = section.edit();
                    editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(WeightEntry.this));

                    editor_retake.commit();
                }
                weight.setWeightInKg(weightKG + "");
                weight.setInputmode(1);
                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.USER_TIMESLOT_SP, 0);
                String slot = settings.getString("timeslot", "AM");
                weight.setTimeslot(slot);

                SharedPreferences settings1 = getSharedPreferences(
                        CommonUtilities.PREFS_NAME_date, 0);
                String sectiondate = settings1.getString("sectiondate", "0");

                weight.setSectionDate(sectiondate);
                Log.e(TAG, "Weightmanual section date-" + sectiondate);

                measureId = dbcreateweight.InsertWeight(weight);
                Log.e(TAG, "Weight value saved");

                // edtpulse.setText("");

                try {
                    // Thread.sleep(7000);

                    Intent intent = new Intent(WeightEntry.this,
                            ShowWightActivity.class);
                    intent.putExtra(
                            "text",
                            ""
                                    + Util.round(Double.parseDouble(edtpulse
                                    .getText().toString())));
                    intent.putExtra("measureId", measureId);
                    intent.putExtra("type", 1);

                    edtpulse.setText("");
                    Log.e(TAG, "Redirecting top show weight page");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0, 0);
                } catch (Exception e) {
                    Log.e(TAG, "Exception in delay!" + e.getMessage());
                }

            } else {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String WeightValidationErrorMessage  = ERROR_MSG.getString("WeightValidationErrorMessage", "-1");
                errorShow(WeightValidationErrorMessage);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            nextClick();
        }
    };

    public void errorShow(String msg) {
        W_Next.setEnabled(false);
        error_display.setVisibility(View.VISIBLE);
        error_display.setText(msg);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        error_display.animate().alpha(0.0f);
                        error_display.setVisibility(View.GONE);
                        W_Next.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
