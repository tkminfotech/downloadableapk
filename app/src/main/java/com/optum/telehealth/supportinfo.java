package com.optum.telehealth;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import com.optum.telehealth.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;

import java.io.File;

/**
 * Created on 4/25/2016.
 * JUST A SUPPORT INFO ACTIVITY FOR OTH HFP PATIENT
 */
public class supportinfo extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.supportinfo);
//        TextView txt_techSupportNumber = (TextView)findViewById(R.id.txt_supportTecHelpNumber);
//        TextView txt_supportTecHelp = (TextView)findViewById(R.id.txt_supportTecHelp);

        ImageButton versionclose = (ImageButton) findViewById(R.id.supportclose);
        ImageView imageView = (ImageView) findViewById(R.id.supportImage);
        versionclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.USER_SP, 0);
        Typeface type1 = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        //txt_supportTecHelp.setTypeface(type1, Typeface.NORMAL);
        Constants.setPatientid(settings.getString(
                "patient_id", "0"));

        String contractcode = settings.getString("contract_code_name",
                "-1");
        SharedPreferences settingsError = getSharedPreferences("ERROR_MSG", 0);
        //txt_techSupportNumber.setText(settingsError.getString("forgot_password_msg3", ""));
        if (!contractcode.equals("-1")) {

            String mCurrentimage_path = Environment
                    .getExternalStorageDirectory().toString()
                    + "/versions/HFP/Images/";

            File directoryFile = new File(mCurrentimage_path);

            File myFile = new File(directoryFile.getAbsolutePath()
                    + "/" + contractcode + ".png");
                    /*
					 * new DownloadContractCode(getApplicationContext(),
					 * Constants.getPostUrl()).execute();
					 */

            if (myFile.exists()) {

                Log.i("titl window", "File already exists");
                String mCurrentPhotoPath = Environment
                        .getExternalStorageDirectory().toString()
                        + "/versions/HFP/Images/"
                        + contractcode
                        + ".png";

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

                bmOptions.inJustDecodeBounds = false;
                bmOptions.inPurgeable = true;
                Bitmap bitmap = BitmapFactory.decodeFile(
                        mCurrentPhotoPath, bmOptions);
                imageView.setImageBitmap(bitmap);
            }
        }
    }
}