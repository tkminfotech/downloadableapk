package com.optum.telehealth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.optum.telehealth.bean.ClassMeasureType;
import com.optum.telehealth.dal.MeasureType_db;
import com.optum.telehealth.dal.Schedule_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.TestDate;
import com.optum.telehealth.util.Util;

public class ScheduleThanksActivity extends Titlewindow {

    Schedule_db dbSchedule = new Schedule_db(this);
    private Button SaveButton;
    final Context context = this;
    private String TAG = "ScheduleThanksActivity";
    MeasureType_db db_measureType = new MeasureType_db(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_thanks);
        SaveButton = (Button) findViewById(R.id.btn_nextSchedule);
        SaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                check_schedule_for_skip_vitals();
                Intent intentwt = new Intent(getApplicationContext(), ScheduleRedirect.class);
                startActivity(intentwt);
                finish();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//            Intent intent = new Intent(context, FinalMainActivity.class);
//            startActivity(intent);
//            finish();
//        }
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Log.i(TAG, "DROID home  clicked:");
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "schedule thanks on Stop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void check_schedule_for_skip_vitals() {

        String s_time = TestDate.getCurrentTime();
        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.PREFS_NAME_date, 0);
        SharedPreferences.Editor editor_retake = settings1.edit();
        editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
        editor_retake.commit();

        Cursor cs_schedule = dbSchedule.selectSchedule_skip_status("",
                Constants.getdroidPatientid());
        cs_schedule.moveToFirst();
        if (cs_schedule.getCount() > 0) {
            db_measureType.delete();
            while (cs_schedule.isAfterLast() == false) {

                ClassMeasureType sensor1 = new ClassMeasureType();
                sensor1.setMeasuretypeId(Integer.parseInt(cs_schedule
                        .getString(4)));
                sensor1.setPatientId(Constants.getdroidPatientid());
                sensor1.setStatus(1);
                sensor1.setDate(s_time);
                db_measureType.insert(sensor1);
                cs_schedule.moveToNext();
            }
        }
        cs_schedule.close();
        dbSchedule.cursorSchedule.close();
        dbSchedule.close();
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}
