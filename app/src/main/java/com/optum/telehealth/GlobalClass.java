package com.optum.telehealth;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import com.optum.telehealth.util.Log;

import com.optum.telehealth.bean.CrashLog;
import com.optum.telehealth.dal.CrashLog_db;
import com.optum.telehealth.dal.MyDataTracker_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.FontsOverride;
import com.optum.telehealth.util.LogcatCapture;
import com.optum.telehealth.util.SkipVitals;
import com.optum.telehealth.util.Util;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Global Application class-- Used to store temp data Data is maintained when
 * ever the app is available in background or foreground thread.
 */
public class GlobalClass extends Application {

    //private int alarm_count;
    private PowerManager.WakeLock wakelock;
    private Bundle bundle;
    public int regular_alarm_count = 0;
    public int Start_Alarm_Count = 0;
    public boolean deviceType = true;
    private boolean Regular_Alarm_Triggered = false;
    public String postUrl = "https://othdevold.portal724.us:8725";
    public boolean isEllipsisEnable = true;
    public void setDeviceType(boolean deviceType) {
        DeviceType = deviceType;
    }

    public boolean DeviceType;
    public long startTime = 15 * 60 * 1000; // 15 MINS IDLE TIME
    public final long interval = 1 * 1000;
    public MyCountDownTimer countDownTimer = new MyCountDownTimer(startTime, interval);
    public boolean paused = false, redirect = false;
    public MyDataTracker_db myDataTracker_db = null;
    private Thread.UncaughtExceptionHandler defaultUEH;
    public static String token;
    public static String patientId;
    public String serviceUrl;
    public static String excMsg = "";
    public static String version;
    public static String error_time = "";
    public static CrashLog crashLog;
    public String crashLog_Id;
    public static CrashLog_db crashLog_db;
    public static int active = 0;
public boolean isAuthenticationLanguage_english = false;
    public boolean hfpClicked;
public boolean isSupportEnable = true;
    public boolean isAlarmUtilStart = false;
    public GlobalClass() {
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        // setup handler for uncaught exception
        Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/FrutigerLTStd-Roman.otf");
        //TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/FrutigerLTStd-Roman.otf");

        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/FrutigerLTStd-Roman.otf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/FrutigerLTStd-Roman.otf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/FrutigerLTStd-Roman.otf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/FrutigerLTStd-Roman.otf");

        crashLog_db = new CrashLog_db(getApplicationContext());
        version = getResources().getString(R.string.version);
        //serviceUrl = getResources().getString(R.string.serverUrl);
        // register to be informed of activities starting up
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity,
                                          Bundle savedInstanceState) {
                active++;
                // new activity created; force its orientation to portrait
                if (getResources().getBoolean(R.bool.isTab)) {
                    activity.setRequestedOrientation(
                            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    activity.setRequestedOrientation(
                            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                Log.e("OTH MOBILE DOWNLOAD", "onActivityCreated" + activity.getLocalClassName());
            }

            @Override
            public void onActivityStarted(Activity activity) {
                Log.e("OTH MOBILE DOWNLOAD", "onActivityStarted" + activity.getLocalClassName());
                paused = false;
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Log.e("OTH MOBILE DOWNLOAD", "onActivityResumed" + activity.getLocalClassName());
                paused = false;
                if (redirect) {
                    redirect = false;
                    ActivityManager am = (ActivityManager) getApplicationContext()
                            .getSystemService(Context.ACTIVITY_SERVICE);
                    ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

                    String mClassName = cn.getClassName();
                    if (!mClassName.contains("AuthenticationActivity")) {
                        Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
                        authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(authenticationIntent);
                    }
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Log.e("OTH MOBILE DOWNLOAD", "onActivityPaused" + activity.getLocalClassName());
                paused = true;
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Log.e("OTH MOBILE DOWNLOAD", "onActivityStopped" + activity.getLocalClassName());
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                Log.e("OTH MOBILE DOWNLOAD", "onActivityCreated" + activity.getLocalClassName());
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                active--;
                if(active ==0){

                    ActivityManager am = (ActivityManager) getApplicationContext()
                            .getSystemService(Context.ACTIVITY_SERVICE);
                    ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                    String mClassName = cn.getClassName();
                    Log.e("OTH MOBILE DOWNLOAD", "APP CLOSED_onActivityDestroyed: "+ mClassName);
                    if(!mClassName.contains("com.optum.telehealth")){
                        Log.e("OTH MOBILE DOWNLOAD", "APP CLOSED");
                        SharedPreferences skipPreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
                        SharedPreferences.Editor S_editor = skipPreferences.edit();
                        S_editor.putInt("R_LOGOUT", 0);
                        S_editor.commit();
                    }else{
                        Log.e("OTH MOBILE DOWNLOAD", "NOTIFICATION CLICKED");
                    }
                }
                Log.e("OTH MOBILE DOWNLOAD", "onActivityDestroyed" + activity.getLocalClassName());
            }

        });

        // capture logcat logs in a file if debug mode is active
        if (Log.FILE_LOG_ACTIVE)
            LogcatCapture.startCapturing();
    }

    public boolean isRegular_Alarm_Triggered() {
        return Regular_Alarm_Triggered;
    }

    public boolean isDeviceType() {
        return deviceType;
    }

    public int getStart_Alarm_Count() {
        return Start_Alarm_Count;
    }

    public void setStart_Alarm_Count(int start_Alarm_Count) {
        Start_Alarm_Count = start_Alarm_Count;
    }

    public int getRegular_alarm_count() {
        return regular_alarm_count;
    }

    public void setRegular_alarm_count(int regular_alarm_count) {
        this.regular_alarm_count = regular_alarm_count;
    }

    public void setRegular_Alarm_Triggered(boolean regular_Alarm_Triggered) {
        Regular_Alarm_Triggered = regular_Alarm_Triggered;
    }

    /**
     * Used to manage wake lock (allows process to run during sleep) This should
     * only be retained while required and released when all tasks are done.
     */
    public void acquireWakelock() {
        if(wakelock == null){
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakelock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "Optum_Wake_Lock");
            wakelock.setReferenceCounted(false);

//        wakelock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
//        wakelock.acquire(10000);
//        PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
//        wl_cpu.acquire(10000);
         }
         wakelock.acquire();
    }

    /**
     * Release the wake lock
     */
    public void releaseWakelock() {
        if (wakelock != null) {
            wakelock.release();
        }
    }

    public Bundle getBundle() {
        return bundle;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }


    public boolean isConnectingToInternet(Context context) {
        boolean isConnected = false;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        isConnected = true;
                        break;
                    }
            }
        }
        return isConnected;
    }

    // Session Timeout
    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            Log.e("OTH MOBILE DOWNLOAD", "SESSION TIMEOUT");
            SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
            int flow_type = flowsp.getInt("is_reminder_flow", 0);

            if (flow_type == 1)// skip for reminder
            {

                Log.e("TitleWindow", "During reminder flow user click on START/HOM- Inserting skip data");
                SkipVitals.skip_patient_vitalse(getApplicationContext());
                SharedPreferences.Editor seditor = flowsp.edit();
                seditor.putInt("is_reminder_flow", 0);
                seditor.commit();
            }


            SharedPreferences skipPreferences = getSharedPreferences("REMINDER_PREF", Context.MODE_PRIVATE);
            SharedPreferences.Editor S_editor = skipPreferences.edit();
            S_editor.putInt("R_LOGOUT", 0);
            S_editor.commit();

            if (paused) {
                redirect = true;
            } else {
                ActivityManager am = (ActivityManager) getApplicationContext()
                        .getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

                String mClassName = cn.getClassName();
                if (!mClassName.contains("AuthenticationActivity")) {
                    Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
                    authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(authenticationIntent);
                }
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }

    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
//                    String stacktrace = this.getClass().getName() + " : "
//                            + ex.getMessage();
                    String timestamp = "" + System.currentTimeMillis();
                    final Writer result = new StringWriter();
                    final PrintWriter printWriter = new PrintWriter(result);
                    ex.printStackTrace(printWriter);
                    String stacktrace = result.toString();
                    printWriter.close();
                    String filename = timestamp + ".stacktrace";
                    if (!excMsg.equals(stacktrace)) {
                        error_time = get_patient_time_zone_time();
                        writeLogToTextFile(stacktrace, filename);// here I do logging of exception to sdCard
                        Intent authenticationIntent = new Intent(getApplicationContext(), AuthenticationActivity.class);
                        authenticationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(authenticationIntent);
                    }
                    defaultUEH.uncaughtException(thread, ex);
                }
            };

    public void writeLogToTextFile(String stacktrace, String filename) {
        excMsg = stacktrace;
        crashLog = new CrashLog();
        crashLog.setPatient_id(patientId);
        crashLog.setDevice_type(getDeviceType());
        crashLog.setAndroid_version(getDeviceOSVersion());
        crashLog.setApplication_version(version);
        crashLog.setCrash_details(excMsg.replace("\n", "~"));
        crashLog.setException_time(error_time);
        crashLog_Id = crashLog_db.InsertCrashLog(crashLog);

        if (isConnectingToInternet(this)) {
            UploadCrashLog uploadCrashLog = new UploadCrashLog();
            uploadCrashLog.execute(new String[]{serviceUrl
                    + "/droid_website/mob_add_crashlog.ashx"});
        }
    }

    private class UploadCrashLog extends AsyncTask<String, Void, String> {
        // ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(String result) {
           // Log.i("onPostExecute", result);

        }

        protected String doInBackground(String... urls) {
            Log.i("doInBackground", "doInBackground");
            String response = "";
            try {

                Cursor crCrashLog = crashLog_db.SelectCrashLog(patientId);
                crCrashLog.moveToFirst();
                if (crCrashLog.getCount() > 0) {

                    while (crCrashLog.isAfterLast() == false) {
                        String errorDetails = crCrashLog.getString(5);
                        String finalErrorDetails = errorDetails.substring(0, crCrashLog.getString(5).length() - 9).replace("\n", "~");
                        HttpResponse response1 = Util.connect(serviceUrl
                                        + "/droid_website/mob_add_crashlog.ashx", new String[]{
                                        "authentication_token", "patient_id", "device_type", "android_version",
                                        "application_version", "crash_details", "exception_time", "transmitted_time"},
                                new String[]{token, patientId, crCrashLog.getString(2), crCrashLog.getString(3),
                                        crCrashLog.getString(4), finalErrorDetails,
                                        crCrashLog.getString(6), get_patient_time_zone_time()});

                        if (response1 == null) {
                            Log.e("Connection Failed", "Connection Failed!");
                            return "Connection Failed!"; // process
                        }

                        String str = Util.readStream(response1.getEntity().getContent());
                        if (str.trim().length() == 0) {
                            Log.i("No aPK details",
                                    "No aPK details for the current serial number from server");
                            return "Server Failed";
                        }

                        str = str.replaceAll("\n", "");
                        str = str.replaceAll("\r", "");
                        //Log.i("response:", "crashLog_response:" + str.toString());
                        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        InputSource is = new InputSource();
                        is.setCharacterStream(new StringReader(str.toString()));
                        Document doc = db.parse(is);

                        NodeList nodes = doc.getElementsByTagName("optum");
                        for (int i = 0; i < nodes.getLength(); i++) {

                            response = Util.getTagValue(nodes, i, "response");
                            if (response.equalsIgnoreCase("Success")) {

                                crashLog_db.delete_CrashLog_ById(crCrashLog.getString(0));
                                Log.i("crashLog", "crashLog_deleted from db:" + crashLog_Id);
                            }
                        }
                        crCrashLog.moveToNext();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    public String getDeviceType() {
        return Build.MODEL;
    }

    public String getDeviceOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String get_patient_time_zone_time() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.US);
        Date today = new Date();
        String IST = df.format(today);
        return (IST.replace("p.m.", "PM")).replace("a.m.", "AM");
    }

    public static String get_myData_date() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        Date today = new Date();
        String dt = df.format(today);
        return dt;
    }


    public boolean isHfpClicked() {

        return hfpClicked;
    }

    public void setHfpClicked(boolean hfpClicked) {

        this.hfpClicked = hfpClicked;
    }

    public void appTracking(String page, String action) {
        if(myDataTracker_db == null){

            myDataTracker_db = new MyDataTracker_db(getApplicationContext());
        }
        ContentValues values = new ContentValues();
        values.put("Patient_Id", patientId);
        values.put("Date_Time", get_patient_time_zone_time());
        values.put("Device", getDeviceType());
        values.put("OS", getDeviceOSVersion());
        values.put("Version", getResources().getString(R.string.version));
        values.put("Page", page);
        values.put("Action", action);
        values.put("Status", 0);
        myDataTracker_db.insertMyDataTracker(values);
    }

    public void changeLanguage(Context context){

        int ln;
        SharedPreferences settings_n = getSharedPreferences(
                CommonUtilities.USER_SP, 0);
        ln = Integer.parseInt(settings_n.getString("language_id", "0"));

        Constants.setLanguage(ln);
        if (ln == 11) {
            Locale locale = new Locale("es");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            context.getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

        } else {
            Locale.setDefault(Locale.US);
            Configuration config = new Configuration();
            config.locale = Locale.US;
            context.getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
    }

    public void setEnglishLanguage(Context context){

            Locale.setDefault(Locale.US);
            Configuration config = new Configuration();
            config.locale = Locale.US;
            context.getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

    }
}