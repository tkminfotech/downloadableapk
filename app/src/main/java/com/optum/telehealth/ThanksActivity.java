package com.optum.telehealth;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassReminder;
import com.optum.telehealth.dal.Flow_Status_db;
import com.optum.telehealth.dal.Reminder_db;
import com.optum.telehealth.dal.Sensor_db;
import com.optum.telehealth.util.Alam_util;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Regular_Alam_util;
import com.optum.telehealth.util.SkipVitals;
import com.optum.telehealth.util.Skip_Reminder_Values_Upload;

public class ThanksActivity extends Titlewindow {

    BackgroundThread backgroundThread;
    TextView username, message;
    ObjectAnimator AnimPlz, ObjectAnimator;
    public int flag = 0;
    public boolean closeflag = false;
    private String TAG = "ThanksActivity ";
    private String serverurl = "";
    private GlobalClass appState;
    private Reminder_db reminder_db;
    Flow_Status_db Flow_db;
    String[] Status;
    String Status_Val;
    private Sensor_db sensor_db = new Sensor_db(this);
    boolean resumeFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);
        appState = (GlobalClass) getApplicationContext();
        reminder_db = new Reminder_db(this);
        Flow_db = new Flow_Status_db(ThanksActivity.this);
        appClass.isEllipsisEnable = false;
        appClass.isSupportEnable = false;
        setConfig();

        username = (TextView) findViewById(R.id.txtusername_thanks);
        message = (TextView) findViewById(R.id.takereading);

        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.USER_SP, 0);
        String patientname = settings.getString("patient_name", "-1"); // #1
        Typeface type = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        username.setTypeface(type, Typeface.BOLD);
        message.setTypeface(type, Typeface.NORMAL);

        if (!patientname.equals("-1")) {

            int a = sensor_db.Selectsensororder();
            if (a != 0) {
                username.setText(this.getString(R.string.thanks) + " "+patientname
                        + ".");
            } else {
                Typeface type_in = Typeface.createFromAsset(getAssets(),
                        "fonts/FrutigerLTStd-Roman.otf");
                username.setVisibility(View.INVISIBLE);
                message.setText(R.string.nothingscheduled);
                message.setTextColor(getResources()
                        .getColor(R.color.black));
                message.setTypeface(type_in, Typeface.NORMAL);
            }
        }

        // uploadSensordetails();

        backgroundThread = new BackgroundThread();
        backgroundThread.setRunning(true);
        backgroundThread.start();

        SkipVitals.skip_patient_vitalse(getApplicationContext());
        Regular_Alam_util.cancelAlarm(ThanksActivity.this);
        Alam_util.cancelAlarm(ThanksActivity.this);

        Flow_db.open();
        Cursor cursor1 = Flow_db.selectAll();
        Status = new String[cursor1.getCount()];
        int j = 0;

        while (cursor1.moveToNext()) {
            Status[j] = cursor1.getString(cursor1.getColumnIndex("Status"));

            Status_Val = Status[0];
            j += 1;
        }
        Flow_db.close();

        if (Status_Val.equals("0")) {
            Flow_db.open();
            Flow_db.deleteAll();
            Flow_db.insert("1");
            Flow_db.close();
            ClassReminder reminder = new ClassReminder();
            if (reminder_db.GetStartedAlarm() != null) {
                reminder = reminder_db.GetStartedAlarm();
                reminder.setStatus(2); // Set status to SUCCESS
                reminder_db.UpdateReminderStatus(reminder);
            }

        } else {
            Log.i("TitleWindow", "appState.isAlarmTriggered()--> " + Status_Val);
        }
        if (appState.isRegular_Alarm_Triggered() == true) {
            Regular_Alam_util.cancelAlarm(ThanksActivity.this);
            appState.setRegular_Alarm_Triggered(false);
            appState.setStart_Alarm_Count(0);
        }

        // clear retak sp
        SharedPreferences settings1 = getSharedPreferences(
                CommonUtilities.RETAKE_SP, 0);
        SharedPreferences.Editor editor_retake = settings1.edit();
        editor_retake.putInt("retake_status", 0);
        editor_retake.commit();
        Log.i(TAG, "--oncreate completed thanks actt---- ");

        new Skip_Reminder_Values_Upload(this, serverurl).execute();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (flag == 1) {
            Intent intent = new Intent(ThanksActivity.this, HFPHomeActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
            flag = 0;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            closeflag = true;
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            overridePendingTransition(0, 0);
//            this.finish();
//        }
        return super.onKeyDown(keyCode, event);
    }

    public class BackgroundThread extends Thread {
        volatile boolean running = false;
        int cnt;

        void setRunning(boolean b) {
            running = b;
            cnt = 11;
        }

        @Override
        public void run() {
            while (running) {
                try {
                    sleep(1000);
                    if (cnt == 10) {

                        int a = sensor_db.Selectsensororder();
                        if (a != 0) {

                        } else {
                            cnt = 1;
                        }
                    }
                    if (cnt-- == 0) {

                        running = false;
                    }
                } catch (InterruptedException e) {
                    // e.printStackTrace();
                }
            }
            handler.sendMessage(handler.obtainMessage());
        }
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            setProgressBarIndeterminateVisibility(false);
            // progressDialog.dismiss();

            boolean retry = true;
            while (retry) {
                try {
                    backgroundThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                    // e.printStackTrace();
                }
            }



            SharedPreferences settings = getSharedPreferences(
                    CommonUtilities.CLUSTER_SP, 0);
            String Type = settings.getString("clustorNo", "-1"); // #1

            // Sierra
//            if (isClickFlag.equals("home")) {
                if (!closeflag) {
                    SharedPreferences settings2 = getSharedPreferences(
                            CommonUtilities.CLUSTER_SP, 0);
                    int isHomeEnable =  settings2.getInt("homeButton", 0);
                    if(isHomeEnable == 1){

                        Intent i = new Intent(ThanksActivity.this,
                                Home.class);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    }else{

                        Intent i = new Intent(ThanksActivity.this,
                                HFPHomeActivity.class);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                        finish();
                    }
//                    Intent intent = new Intent(ThanksActivity.this, Home.class);
//                    startActivity(intent);
//                    overridePendingTransition(0, 0);
                } else {
                    Log.i(TAG,
                            "user clicked title window, re starting application");
                }

//            } else if (Type.equals("start")) // HFP
//            {
//                if (!closeflag) {
//                    flag = 1;
////                    Typeface type_in = Typeface.createFromAsset(getAssets(),
////                            "fonts/FrutigerLTStd-Roman.otf");
////                    username.setVisibility(View.INVISIBLE);
////                    message.setText(R.string.nothingscheduled);
////                    message.setTextColor(getResources()
////                            .getColor(R.color.black));
////                    message.setTypeface(type_in, Typeface.NORMAL);
//                    Intent intent = new Intent(ThanksActivity.this, HFPHomeActivity.class);
//                    startActivity(intent);
//                    overridePendingTransition(0, 0);
//                } else {
//                    Log.i(TAG,
//                            "user clicked title window, re starting application");
//                }
//            }

        }

    };

    @Override
    public void onStop() {
        closeflag = true;
        Log.i(TAG, "--on stop thanks act---- ");
        //Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
        super.onStop();
    }

    private void setConfig() {
        SharedPreferences settings = getSharedPreferences(
                CommonUtilities.SERVER_URL_SP, 0);

        serverurl = settings.getString("Server_post_url", "-1");
        CommonUtilities.SERVER_URL = serverurl;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        resumeFlag = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onPause" + resumeFlag);
        if (resumeFlag) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    closeflag = false;
                    handler.sendMessage(handler.obtainMessage());
                }
            }, 2000);
        }
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}