package com.optum.telehealth;

import java.util.ArrayList;
import java.util.List;

import com.optum.telehealth.bean.State;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SpinnerAdapter extends ArrayAdapter<State>{

	private Activity activity;
	private List<State> stateList;

	LayoutInflater inflater;


	public SpinnerAdapter(Activity context, int textViewResourceId, List<State> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub

		activity = context;
		stateList = objects;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getDropDownView(int position, View convertView,ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	// This funtion called for each row ( Called data.size() times )
	public View getCustomView(int position, View convertView, ViewGroup parent) {

		/********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/

		View row = inflater.inflate(R.layout.spinner_row, parent, false);
		State state = stateList.get(position);
		TextView name = (TextView)row.findViewById(R.id.txt_stateName);
		if(position ==0){

			name.setTextColor(activity.getResources().getColor(R.color.Mob_Gray3));
		}else{

			name.setTextColor(Color.BLACK);
		}
		name.setText(state.getStateName());
		return row;
	}
}