package com.optum.telehealth;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import com.optum.telehealth.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.optum.telehealth.bean.ClassTemperature;
import com.optum.telehealth.dal.Temperature_db;
import com.optum.telehealth.util.CommonUtilities;
import com.optum.telehealth.util.Constants;
import com.optum.telehealth.util.Util;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class TemperatureEntry extends Titlewindow {

    int tempId;
    private String TAG = "TemperatureEntry Sierra";
    TextView tempMessage, temptext, tempUnit;
    Dialog dialog;
    private Button temperature_Next;
    private TextView txt_errorMsg;
    RelativeLayout tempParent;
    EditText edttemp;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            nextClick();
        }
    };
    boolean C = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        setContentView(R.layout.activity_temperature_entry);
        txt_errorMsg = (TextView) findViewById(R.id.txt_errorMsg);
        appClass.isSupportEnable = true;
        temperature_Next = (Button) findViewById(R.id.btn_temperatureNext);
        temperature_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appClass.appTracking("Temperature manual entry page","Click on Next button");
                nextClick();
            }
        });

        tempMessage = (TextView) findViewById(R.id.temperaturemessage);
        temptext = (TextView) findViewById(R.id.temptext);

        tempUnit = (TextView) findViewById(R.id.tempunit);
        Typeface type1 = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        tempMessage.setTypeface(type1, Typeface.NORMAL);
        temptext.setTypeface(type1, Typeface.NORMAL);
        Typeface type2 = Typeface.createFromAsset(getAssets(),
                "fonts/FrutigerLTStd-Light.otf");
        tempUnit.setTypeface(type2, Typeface.NORMAL);

        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
        int temperature_unit_id = settings.getInt("temperature_unit_id", -1);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("manualEntry", 1);
        editor.commit();

        if(temperature_unit_id == 1){
            tempUnit.setText(getResources().getText(R.string.tempunit_C));
            C = true;
        } else {
            tempUnit.setText(getResources().getText(R.string.tempunit));
        }

        tempParent = (RelativeLayout) findViewById(R.id.tempParent);
        edttemp = (EditText) findViewById(R.id.tempvalue);
        edttemp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                GradientDrawable border = new GradientDrawable();
                border.setColor(0xFFFFFFFF); //white background
                int dp = 2;
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
                if (hasFocus) {
                    border.setStroke(px, getResources().getColor(R.color.Mob_Orange_Dark));
                } else {
                    border.setStroke(px, getResources().getColor(R.color.Mob_EditText_Border));
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    tempParent.setBackgroundDrawable(border);
                } else {
                    tempParent.setBackground(border);
                }
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            Log.i(TAG, "DROID KEYCODE_BACK clicked:");
//
//            Intent intent = new Intent(this, FinalMainActivity.class);
//            startActivity(intent);
//            finish();
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStop() {
       // Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
        Log.i(TAG, "--on stop temp entry---- ");
        super.onStop();
    }

    private void nextClick() {
        try {
            String s_val = edttemp.getText().toString();
            double d_val  = 0;
            if(!s_val.equals("") && !s_val.equals(".")) {
                d_val = Double.parseDouble(s_val); // Make use of autoboxing.  It's also easier to read.
            }
            Log.i(TAG, "VALUE:" + d_val);
            Temperature_db dbcreatetemperature = new Temperature_db(
                    TemperatureEntry.this);

            String text = edttemp.getText().toString(); // temp value String


            if (text.length() >= 1
                    && !text.equals(".") && d_val != 0) {
                double value = Double.parseDouble(text);
                if (text.length() == 5) {
                    Character point = text.charAt(3);
                    if (point != '.') {
                        SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                        String TemperatureValidationErrorMessage  = ERROR_MSG.getString("TemperatureValidationErrorMessage", "-1");
                        errorShow(TemperatureValidationErrorMessage);
                        return;
                    }
                }

                if (value <= 999.9) {
                    //Valid Value
                } else{
                    //Invalid Value
                    SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                    String TemperatureValidationErrorMessage  = ERROR_MSG.getString("TemperatureValidationErrorMessage", "-1");
                    errorShow(TemperatureValidationErrorMessage);
                    return;
                }

                SharedPreferences flow = getSharedPreferences(
                        CommonUtilities.USER_FLOW_SP, 0);
                int val = flow.getInt("flow", 0); // #1
                if (val == 1) {

                    SharedPreferences section = getSharedPreferences(
                            CommonUtilities.PREFS_NAME_date, 0);
                    SharedPreferences.Editor editor_retake = section.edit();
                    editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(TemperatureEntry.this));
                    editor_retake.commit();
                }
                // tts.speak("your temperature is "+edttemp.getText().toString()
                // +"Fahrenheit", TextToSpeech.QUEUE_FLUSH, null);
                String Patientid = Constants.getdroidPatientid();
                ClassTemperature temp = new ClassTemperature();
                double tempValue = 0.0;
                double showValue = Util.round(Double.parseDouble(edttemp.getText()
                        .toString()));
                if(C){
                    tempValue = Util.round((Double.parseDouble(edttemp.getText()
                            .toString()) * 1.8) + 32);
                }else {
                    tempValue = Util.round(Double.parseDouble(edttemp.getText()
                            .toString()));
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "MM/dd/yyyy hh:mm:ss aa", Locale.US);

                String currentDateandTime = dateFormat.format(new Date());
                temp.setLastUpdateDate(currentDateandTime);
                temp.setStatus(0);
                temp.setTemperatureValue(tempValue + "");

                String today;
                String tomorrow;
                Date date;
                Format formatter;
                Calendar calendar = Calendar.getInstance();

                date = calendar.getTime();
                formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.US);
                today = formatter.format(date);

                calendar.add(Calendar.DATE, 1);
                date = calendar.getTime();
                formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.US);
                tomorrow = formatter.format(date);
                temp.setInputmode(1);
                SharedPreferences settings1 = getSharedPreferences(
                        CommonUtilities.PREFS_NAME_date, 0);
                String sectiondate = settings1.getString("sectiondate", "0");

                SharedPreferences settings = getSharedPreferences(
                        CommonUtilities.USER_TIMESLOT_SP, 0);
                String slot = settings.getString("timeslot", "AM");

                temp.setSectionDate(sectiondate);
                temp.setTimeslot(slot);
                tempId = dbcreatetemperature.InsertTemperatureMeasurement(temp);

                Log.i(TAG, "User Entered tepm- " + tempValue);

                Intent intent = new Intent(TemperatureEntry.this,
                        ShowTemperatureActivity.class);
                intent.putExtra("temp", "" + showValue);
                intent.putExtra("tempid", tempId);

                edttemp.setText("");

                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
                Log.e(TAG, "Redirecting to show temp page");

				/*
                 * Intent intent = new
				 * Intent(getApplicationContext(),GlucoseEntry.class); finish();
				 * startActivity(intent);
				 */
            } else {
                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
                String TemperatureValidationErrorMessage  = ERROR_MSG.getString("TemperatureValidationErrorMessage", "-1");
                errorShow(TemperatureValidationErrorMessage);
                return;
            }

        } catch (Exception e) {
            // p did not contain a valid double
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void errorShow(String msg) {
        txt_errorMsg.setVisibility(View.VISIBLE);
        txt_errorMsg.setText(msg);
        temperature_Next.setEnabled(false);
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        txt_errorMsg.animate().alpha(0.0f);
                        txt_errorMsg.setVisibility(View.GONE);
                        temperature_Next.setEnabled(true);
                        timer.cancel();
                    }
                });

            }
        }, 3000, 3000);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
}